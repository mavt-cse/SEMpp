/*
 * Based on cppunit-template.
 * Important note: 
 * - remove GCC_PREPROCESSOR_DEFINITIONS = _GLIBCXX_DEBUG=1 _GLIBCXX_DEBUG_PEDANTIC=1
 *   (in build settings for target)
 * Most important doc: http://cppunit.sourceforge.net/doc/lastest/group___assertions.html
 */

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
// stuff to test
#include "Iterators.h"
// system includes
#include <iostream>
#include <vector>

/**
 * Test class checking iterators from Iterators.h.
 */
class TestIterators : public CppUnit::TestCase {
    
    CPPUNIT_TEST_SUITE( TestIterators );
    
    CPPUNIT_TEST( testIterators );
    
	CPPUNIT_TEST_SUITE_END();
    
public:
    /** Create data structures here. */
    void setUp() { }
    
    /** Release data structures here. */
    void tearDown() { }
    
    /** Test for iterators. */
    void testIterators() {
        // test
        const int N = 10;
        typedef std::vector<int> V;
        typedef std::vector<int*> V2;
        
        // fill
        V v;
        V2 v2;
        int a[N];
        int* ap[N];
        for (int i = 0; i < N; ++i) {
            v.push_back(i);
            v2.push_back(new int(i));
            a[i] = i;
            ap[i] = new int(i);
            // I am aware of the memory leak...
        }
        
        // test
        _testIterators(ContainerIterator<V>(v), N);
        _testIterators(ContainerIteratorP<V2, int>(v2), N);
        _testIterators(ArrayIterator<int>(a, N), N);
        _testIterators(ArrayIteratorP<int>(ap, N), N);
    }
    
private:
    
    /** Helper for testIterators(). Assumes iter filled with values from 0 to N-1. */
    template <typename Iterator>
    void _testIterators(Iterator iter, const int N) {
        
        // check concept
        CONCEPT_ASSERT(CIterator<Iterator>);
        
        // increase all values by 1
        for (iter.first(); !iter.isDone(); iter.next()) {
            typename Iterator::ValueType* item = iter.currentItem();
            ++(*item);
        }
        // check new values
        int n = 0;
        for (iter.first(); !iter.isDone(); iter.next()) {
            int i = *(iter.currentItem());
            ++n;
            CPPUNIT_ASSERT_EQUAL(n, i);
        }
        CPPUNIT_ASSERT_EQUAL(n, N);
        
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION( TestIterators );
