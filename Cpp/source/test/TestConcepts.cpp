/*
 * Based on cppunit-template.
 * Important note: 
 * - remove GCC_PREPROCESSOR_DEFINITIONS = _GLIBCXX_DEBUG=1 _GLIBCXX_DEBUG_PEDANTIC=1
 *   (in build settings for target)
 * Most important doc: http://cppunit.sourceforge.net/doc/lastest/group___assertions.html
 */

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <iostream>
#include <algorithm>
#include <vector>
// stuff to test
#include "Environment.h"
#include "CParticles.h"
#include "Iterators.h"
#include "NeighborhoodMaps.h"
#include "Operators.h"
#include "Surfaces.h"
#include "TimeIntegrators.h"
#include "WorkDistributors.h"
#include "Matrix.h"
#include "Kernels.h"
#include "Interpolators.h"
#include "Grids.h"

/**
 * Test class "performing" concept assertions.
 * Note that none of it will be executed. This is a pure compile-time test.
 */
class TestConcepts : public CppUnit::TestCase {
    
	CPPUNIT_TEST_SUITE( TestConcepts );
    
	CPPUNIT_TEST_SUITE_END();
    
public:
    /** Create data structures here. */
    void setUp() {
    }
    
    /** Release data structures here. */
    void tearDown() {
    }
    
    /** Dummy model of #CBasicParticle. */
    class DummyBasicParticle {
    public:
        // implementation of CBasicParticle
        VecType getPos() const { return mPos; }
        VecType getVel() const { return mVel; }
    protected:
        VecType mPos, mVel;
    };
    
    /** Dummy model of #CBasicParticle with extra stuff for CellList. */
    class DummyCLParticle: public DummyBasicParticle {
    public:
        void setPos(VecType pos) { mPos = pos; }
    };
    
    /** Dummy model of #CParticle. */
    class DummyParticle: public DummyBasicParticle {
    public:
        // implementation of CBasicParticle
        typedef VecType StateType;
        void resetRHS() { mVel = 0; }
        void setState(StateType state) { mPos = state; }
        StateType getState() const { return mPos; }
        StateType getRHS() const { return mVel; }
        void setStateT1(StateType state) { mTmp = state; }
        StateType getStateT1() const { return mTmp; }
        
    private:
        StateType mTmp;
    };
    
    /** Dummy model of #CParticle with extra stuff for some time integrators. */
    class DummyExtendedParticle: public DummyParticle {
    public:
        void setPos(VecType pos) { mPos = pos; }
        VecType getAcc() const { return mAcc; }
        VecType getVelTmp() const { return mVel; }
        void setStateT2(StateType state) { mTmp2 = state; }
        StateType getStateT2() const { return mTmp2; }
    private:
        VecType mAcc;
        StateType mTmp2;
    };
    
    /** Dummy model of #CIterable. */
    class DummyIterable {
    public:
        // implementation of CIterable
        typedef ArrayIterator<DummyParticle> IteratorType;
        IteratorType getIterator() { return IteratorType(v,N); }
        // TODO: ???
        typedef IteratorType::ValueType IterableType;
    private:
        static const int N = 10;
        DummyParticle v[N];
    };
    
    /** Dummy model of #CInterpolator. */
    class DummyInterpolator {
    public:
        static const size_t sGhostStart = 3;
        static const size_t sGhostEnd = 3;
        template <typename Matrix>
        static typename Matrix::ElementType scalar(const Matrix& m, const VecType pos) {
            typedef typename Matrix::ElementType ET;
            ET result;
            return result;
        }
        template <typename Matrix>
        static VectorElement<typename Matrix::ElementType, DIM> gradient(const Matrix& m, const VecType pos, const VecType invh) {
            typedef typename Matrix::ElementType ET;
            typedef VectorElement<ET, DIM> GT;
            GT result;
            return result;
        }
    };
    
    /** Dummy model of #CKernel. */
    class DummyKernel {
    public:
        static const int sSupport = 1;
        static const int sStencilStart = 0;
        static const int sStencilEnd = 0;
        static void weights(Real* w, const Real t) { w[0] = 1; }
        static void weightsGrad(Real* dw, Real* w, const Real t) { dw[0] = 0; w[0] = 1; }
    };
    
    /** Iterator that is "only" model of #CIterator. */
    typedef CLLocalNH< DummyBasicParticle, false >::IteratorTypeVar DummyIterator;
    /** Iterator that is model of #CIterator with CParticle types. */
    typedef CLLocalNH< DummyParticle, false >::IteratorTypeVar DummyIteratorP;
    /** Iterator that is model of #CIterator with extended particle types. */
    typedef CLLocalNH< DummyExtendedParticle, false >::IteratorTypeVar DummyIteratorEx;
    
    /** Model of #COperator. */
    typedef GenericOperator< DummyBasicParticle > DummyOperator;
    
    /** Model of #CNeighborhoodMap. */
    typedef CellList< DummyCLParticle > DummyNH;
    
    /** Model of #CWorkDistributor. */
    typedef DummyDistributor< DummyIterator > DummyWD;
    /** Iterator that is model of #CIterator with CParticle types. */
    typedef DummyDistributor< DummyIteratorP > DummyWDP;
    /** Iterator that is model of #CIterator with extended particle types. */
    typedef DummyDistributor< DummyIteratorEx > DummyWDEx;
    
    /** Model of #CMatrix2D. */
    typedef Matrix::D2D<Real> DummyMatrix2D;
    /** Model of #CMatrix3D. */
    typedef Matrix::D3D<Real> DummyMatrix3D;
    /** Choose Matrix for given DIM. */
#if DIM == 2
    typedef DummyMatrix2D DummyMatrix;
#elif DIM == 3
    typedef DummyMatrix3D DummyMatrix;
#endif
    /** Model of #CKernelDirichlet. */
    typedef KernelBilinear DummyKernelDirichlet;
    /** Model of #CGrid. */
    typedef GridGeneric<DummyMatrix, DummyInterpolator > DummyGrid;
    
    // Check dummies
    CONCEPT_ASSERT(CBasicParticle< DummyBasicParticle >);
    CONCEPT_ASSERT(CParticle< DummyParticle >);
    CONCEPT_ASSERT(CIterable< DummyIterable >);
    CONCEPT_ASSERT(CInterpolator< DummyInterpolator, DummyMatrix >);
    CONCEPT_ASSERT(CIterator< DummyIterator >);
    CONCEPT_ASSERT(CIterator< DummyIteratorP >);
    CONCEPT_ASSERT(CIterator< DummyIteratorEx >);
    CONCEPT_ASSERT(COperator< DummyOperator, DummyIterator >);
    CONCEPT_ASSERT(CNeighborhoodMap< DummyNH >);
    CONCEPT_ASSERT(CMatrix2D< DummyMatrix2D >);
    CONCEPT_ASSERT(CMatrix3D< DummyMatrix3D >);
    CONCEPT_ASSERT(CKernel< DummyKernel >);
    CONCEPT_ASSERT(CKernelDirichlet< DummyKernelDirichlet >);
    CONCEPT_ASSERT(CGrid< DummyGrid >);
    
    // Iterators
    CONCEPT_ASSERT(CSubdivisibleIterator< ArrayIterator< DummyBasicParticle > >);
    CONCEPT_ASSERT(CSubdivisibleIterator< ArrayIteratorP< DummyBasicParticle > >);
    CONCEPT_ASSERT(CSubdivisibleIterator< ContainerIterator< std::vector< DummyBasicParticle > > >);
    CONCEPT_ASSERT(CSubdivisibleIterator< ContainerIteratorP< std::vector< DummyBasicParticle* >, DummyBasicParticle > >);
    // fancy ones
    CONCEPT_ASSERT(CSubdivisibleIterator< ArrayIteratorIterable< DummyIterable > >);
    // ones in neighborhood maps checked with CNeighborhoodMap
    
    // Neighborhood maps
    CONCEPT_ASSERT(CNeighborhoodMap< CellList < DummyCLParticle > >);
    CONCEPT_ASSERT(CNeighborhoodMap< CLLocalNH < DummyBasicParticle, true > >);
    CONCEPT_ASSERT(CNeighborhoodMap< CLLocalNH < DummyBasicParticle, false > >);
    CONCEPT_ASSERT(CNeighborhoodMap< DummyNeighborhoodMap< DummyBasicParticle, DummyIterator > >);
    
    // Operators
    CONCEPT_ASSERT(COperator< GenericOperator< DummyBasicParticle >, DummyIterator >);
    CONCEPT_ASSERT(COperator< GenericOperatorNH< DummyNH, DummyCLParticle, DummyNH::IteratorType >, DummyNH::IteratorType >);
    
    // Work distributors
    CONCEPT_ASSERT(CWorkDistributor< DummyDistributor< DummyIterator >, DummyOperator >);
    CONCEPT_ASSERT(CWorkDistributor< TbbDistributor< ArrayIterator< DummyBasicParticle > >, DummyOperator >);
    
    // Time integrators
    CONCEPT_ASSERT(CTimeIntegrator<LeapfrogV1bIntegrator, DummyWDEx >);
    CONCEPT_ASSERT(CTimeIntegrator<LeapfrogV1Integrator, DummyWDEx >);
    CONCEPT_ASSERT(CTimeIntegrator<LeapfrogV2bIntegrator, DummyWDEx >);
    CONCEPT_ASSERT(CTimeIntegrator<LeapfrogV2Integrator, DummyWDEx >);
    CONCEPT_ASSERT(CTimeIntegrator<MidpointIntegrator, DummyWDP >);
    CONCEPT_ASSERT(CTimeIntegrator<RK1Integrator, DummyWDP >);
    CONCEPT_ASSERT(CTimeIntegrator<RK2Integrator, DummyWDP >);
    CONCEPT_ASSERT(CTimeIntegrator<RK3Integrator, DummyWDP >);
    CONCEPT_ASSERT(CTimeIntegrator<RK4Integrator, DummyWDEx >);
    
    // Surfaces
    CONCEPT_ASSERT(CSurface< ChannelSurface >);
    CONCEPT_ASSERT(CSurface< CubeSurface >);
    CONCEPT_ASSERT(CSurface< CylinderSurface >);
    CONCEPT_ASSERT(CSurface< RestrictSurface< ChannelSurface > >);
    CONCEPT_ASSERT(CSurface< SphereSurface >);
    CONCEPT_ASSERT(CSurface< GridSurface<DummyGrid, false> >);
    CONCEPT_ASSERT(CSurface< GridSurface<DummyGrid, true> >);
    
    // Grids
    CONCEPT_ASSERT(CGrid< GridGeneric<DummyMatrix, DummyInterpolator > >);
    CONCEPT_ASSERT(CGrid< GridGhostedD<DummyMatrix, DummyInterpolator > >);
    
    // Interpolators
    CONCEPT_ASSERT(CInterpolator<InterpolatorNoGhosts<DummyKernelDirichlet>, DummyMatrix>);
    CONCEPT_ASSERT(CInterpolator<InterpolatorGhosts<DummyKernel>, DummyMatrix>);
    CONCEPT_ASSERT(CInterpolator<InterpolatorPeriodic<DummyKernel>, DummyMatrix>);
    
    // Matrices
    CONCEPT_ASSERT(CMatrix2D< Matrix::D2D<int> >);
    CONCEPT_ASSERT(CMatrix3D< Matrix::D3D<int> >);
    
    // Kernels
    CONCEPT_ASSERT(CKernelDirichlet< KernelBilinear >);
    CONCEPT_ASSERT(CKernelDirichlet< KernelHermite >);
    CONCEPT_ASSERT(CKernelDirichlet< KernelMPMSpline2 >);
    CONCEPT_ASSERT(CKernelDirichlet< KernelMPMSpline3 >);
};

CPPUNIT_TEST_SUITE_REGISTRATION( TestConcepts );
