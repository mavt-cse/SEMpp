/*
 * Based on cppunit-template.
 * Important note: 
 * - remove GCC_PREPROCESSOR_DEFINITIONS = _GLIBCXX_DEBUG=1 _GLIBCXX_DEBUG_PEDANTIC=1
 *   (in build settings for target)
 * Most important doc: http://cppunit.sourceforge.net/doc/lastest/group___assertions.html
 */

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <iostream>
#include <algorithm>
#include <vector>
// stuff to test
#include "Environment.h"
#include "CParticles.h"
#include "Iterators.h"
#include "NeighborhoodMaps.h"

/**
 * Support for C++-streams.
 * Can use \code std::cout << vector; \endcode
 */
template<typename T>
std::ostream& operator << (std::ostream& out, const std::vector<T>& v) {
    // max element to put on single line
    const int max_N = 6;
    // get num_elem
    int num_elem = v.size();
    // start
    if (num_elem < max_N) {
        out << "(";
    } else {
        out << std::endl;
    }
    // go through elements (besides last one)
    for (int i = 0; i < num_elem-1; ++i) {
        if (num_elem < max_N) {
            out << v[i] << ", ";
        } else {
            out << i << ": " << v[i] << std::endl;
        }
    }
    // end
    if (num_elem < max_N) {
        out << v[num_elem-1] << ")";
    } else {
        out << num_elem-1 << ": " << v[num_elem-1] << std::endl;
    }
	return out;
}

/** Helper class -> dummy particles. */
class DummyParticle {
    VecType mPos;
    
public:
    // implementation of CBasicParticle
    DummyParticle(const VecType& pos): mPos(pos) {}
    VecType getPos() const { return mPos; }
    VecType getVel() const { return VecType(); }
    // more stuff for CellList (periodic)
    void setPos(VecType pos) { mPos = pos; }
};

/**
 * Test class checking stuff in CellList.h.
 */
class TestCellList : public CppUnit::TestCase {
    
	CPPUNIT_TEST_SUITE( TestCellList );
    
	CPPUNIT_TEST( testCellList );
    CPPUNIT_TEST( testCellListFancy );
    
	CPPUNIT_TEST_SUITE_END();
    
public:
    /** Create data structures here. */
    void setUp() {
    }
    
    /** Release data structures here. */
    void tearDown() {
    }
    
    /** Test constructor. */
    void testCellList() {
        // typedefs
        typedef std::vector<DummyParticle> Collection;
        
        // define some dummy particles
        Collection collection;
        initParticles(collection);
        
        // define iterator for it
        ContainerIterator<Collection> collectionIterator(collection);
        // define a pseudo domain
        VecType domain_offset(0);
        VecType domain_size(N);
        // init cell list
        Real cell_size = 1;
        CLBoundaryCondition domain_bc = CL_PERIODIC;
        CellList<DummyParticle> cell_list;
        cell_list.setup(domain_offset, domain_size, cell_size, domain_bc);
        
        // put particles in cell list
        cell_list.reset(collectionIterator);
        // check neighbors
        for (collectionIterator.first(); !collectionIterator.isDone(); collectionIterator.next()) {
            const DummyParticle& item = *(collectionIterator.currentItem());
            checkHood(cell_list, item, domain_bc);
        }
        
        // repeat with different bc
        domain_bc = CL_NONE;
        cell_list.setup(domain_offset, domain_size, cell_size, domain_bc);
        
        // put particles in cell list
        cell_list.reset(collectionIterator);
        // check neighbors
        for (collectionIterator.first(); !collectionIterator.isDone(); collectionIterator.next()) {
            const DummyParticle& item = *(collectionIterator.currentItem());
            checkHood(cell_list, item, domain_bc);
        }
        
    }
    
    /** Test fancy optimized access on cell lists. */
    void testCellListFancy() {
        // typedefs
        typedef std::vector<DummyParticle> Collection;
        typedef CellList<DummyParticle> CL;
        
        // define some dummy particles
        Collection collection;
        initParticles(collection);
        
        // define iterator for it
        ContainerIterator<Collection> collectionIterator(collection);
        // define a pseudo domain
        VecType domain_offset(0);
        VecType domain_size(N);
        // init cell list
        Real cell_size = 1;
        CLBoundaryCondition domain_bc = CL_PERIODIC;
        CL cell_list;
        cell_list.setup(domain_offset, domain_size, cell_size, domain_bc);
        
        // put particles in cell list
        cell_list.reset(collectionIterator);
        // check neighbors
        checkFancyHoods(cell_list, domain_bc);
        
        // repeat with different bc
        domain_bc = CL_NONE;
        cell_list.setup(domain_offset, domain_size, cell_size, domain_bc);
        
        // put particles in cell list
        cell_list.reset(collectionIterator);
        // check neighbors
        checkFancyHoods(cell_list, domain_bc);
    }
    
private:
    
    /** Test on domain from 0 to N with N particles in each dimension. */
    static const int N = 4;
    
    /** Initialize particles. */
    template <typename Collection>
    void initParticles(Collection& collection) {
#if DIM == 1
        for (int i = 0; i < N; ++i) {
            collection.push_back(DummyParticle(VecType(i + 0.5)));
        }
#elif DIM == 2
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                collection.push_back(DummyParticle(VecType(i,j) + 0.5));
            }
        }
#else
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                for (int k = 0; k < N; ++k) {
                    collection.push_back(DummyParticle(VecType(i,j,k) + 0.5));
                }
            }
        }
#endif
    }
    
    /** Storage for list of expected neighbors. */
    std::vector<VecType> expectedNeighbors;
    
    /**
     * Add list of expected neighbors (1D) to expectedNeighbors.
     * If i and posT are given the result will contain elements copied from posT with
     * only posT[i] changed.
     */
    void addNeighbors(VecType pos, CLBoundaryCondition bc, int i = 0) {
        VecType tmp = pos;
        if (pos[i] == 0.5) {
            if (bc == CL_PERIODIC) {
                tmp[i] = pos[i] - 1;
                expectedNeighbors.push_back(tmp);
            }
            tmp[i] = pos[i] + 1;
            expectedNeighbors.push_back(tmp);
        } else if (pos[i] == N-0.5) {
            tmp[i] = pos[i] - 1;
            expectedNeighbors.push_back(tmp);
            if (bc == CL_PERIODIC) {
                tmp[i] = pos[i] + 1;
                expectedNeighbors.push_back(tmp);
            }
        } else {
            tmp[i] = pos[i] - 1;
            expectedNeighbors.push_back(tmp);
            tmp[i] = pos[i] + 1;
            expectedNeighbors.push_back(tmp);
        }
    }
    
    /**
     * Set expectedNeighbors to list of expected neighbors.
     */
    void getNeighbors(VecType pos, CLBoundaryCondition bc) {
        // init
        expectedNeighbors.clear();
        // go through dimensions
        for (int d = 0; d < DIM; ++d) {
            // determine CURRENT size of expectedNeighbors
            size_t eNsize = expectedNeighbors.size();
            for (size_t i = 0; i < eNsize; ++i) {
                VecType posT = expectedNeighbors[i];
                addNeighbors(posT, bc, d);
            }
            // self
            addNeighbors(pos, bc, d);
        }
    }
    
    /** Functor to check if two positions match. */
    struct CheckPos {
        VecType mPos;
        CheckPos(VecType pos): mPos(pos) { }
        bool operator()(VecType otherPos) {
            return (mPos - otherPos).abs() < 0.0001;
        }
    };
    
    /** Check neighborhoods for fancy style. */
    template <typename CL>
    void checkFancyHoods(CL& cell_list, CLBoundaryCondition domain_bc) {
        typename CL::CellIterator cell_iter = cell_list.getCellIterator();
        for (cell_iter.first(); !cell_iter.isDone(); cell_iter.next()) {
            typename CL::CellIterator::ValueType& cur_cell = *(cell_iter.currentItem());
            
            // guaranteed to be non-empty
            CPPUNIT_ASSERT(!cur_cell.empty());
            
            // first without copy
            typedef CLLocalNH<DummyParticle, false> NHM;
            NHM nhMap = cell_list.getLocalHood(cur_cell);
            // loop over particles
            typename CL::ParticleInCellIterator p_iter = cell_list.getParticleInCellIterator(cur_cell);
            for (p_iter.first(); !p_iter.isDone(); p_iter.next()) {
                const DummyParticle& item = *(p_iter.currentItem());
                checkHood(nhMap, item, domain_bc);
            }
            // don't forget to clean up
            nhMap.cleanUp();
            
            // now with copy
            typedef CLLocalNH<DummyParticle, true> NHMC;
            NHMC nhMapC = cell_list.getLocalHoodCopy(cur_cell);
            // loop over particles
            for (p_iter.first(); !p_iter.isDone(); p_iter.next()) {
                const DummyParticle& item = *(p_iter.currentItem());
                checkHood(nhMapC, item, domain_bc);
            }
            // don't forget to clean up
            nhMapC.cleanUp();
        }
    }
    
    /** Check neighborhood for a given particle. */
    template <typename NeighborhoodMap>
    void checkHood(NeighborhoodMap& nhMap, const DummyParticle& part, CLBoundaryCondition bc) {
        // assert it is a nhMap
        CONCEPT_ASSERT(CNeighborhoodMap<NeighborhoodMap>);
        // get iterator
        typename NeighborhoodMap::IteratorType iter = nhMap.getHood(part);
        // check
        VecType pos = part.getPos();
        getNeighbors(pos, bc);
        int nNeighbors = 0;
        for (iter.first(); !iter.isDone(); iter.next()) {
            const DummyParticle& item = *(iter.currentItem());
            bool found = (find_if(expectedNeighbors.begin(), 
                                  expectedNeighbors.end(), 
                                  CheckPos(item.getPos()))
                          != expectedNeighbors.end());
            ++nNeighbors;
            // check -> do we match?
            // TEST -> this and debugDump calls
            if (!found) std::cout << pos << " has neighbor " << item.getPos() << std::endl;
            CPPUNIT_ASSERT(found);
        }
        // right number of neighbors?
        CPPUNIT_ASSERT_EQUAL((int)expectedNeighbors.size(), nNeighbors);
    }
    
};

CPPUNIT_TEST_SUITE_REGISTRATION( TestCellList );
