/**\file TestVirtualCall.cpp
 * Assert that we can do virtual calls:
 * - on reference types
 * - via member-function-pointers
 * - on virtual operator()
 */

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <iostream>

// Tests
#include <stdio.h>
#include <iostream>
#include <functional>   // for mem_fun

/** Sample base class with virtual methods. */
struct Base {
    virtual int operator()() const = 0;
    
//    template <typename X>
//    virtual int operator()(X x) const = 0;
    
    int func1() {
        return 11;
    }
    
    virtual int func2() {
        return 12;
    }
};

/** Sample derived class with virtual methods. */
struct Derived: public Base {
    virtual int operator()() const {
        return 20;
    }
    
//    template <typename X>
//    int operator()(X x) const {
//        return 20 + x;
//    }
    
    virtual int func2() {
        return 22;
    }
};

/**
 * Test class checking virtual calls.
 */
class TestVirtualCall : public CppUnit::TestCase {
    
	CPPUNIT_TEST_SUITE( TestVirtualCall );
    
	CPPUNIT_TEST( calls );
    
	CPPUNIT_TEST_SUITE_END();
    
public:
    /** Create data structures here. */
    void setUp() {
    }
    
    /** Release data structures here. */
    void tearDown() {
    }
    
    /** Test constructor. */
    void calls() {
        // init
        Derived muh;
        Base& muh1 = muh;
        // warm up
        CPPUNIT_ASSERT_EQUAL(20, muh1());
        //CPPUNIT_ASSERT_EQUAL(20+10, muh1(10)); -> FAIL
        CPPUNIT_ASSERT_EQUAL(11, muh1.func1());
        CPPUNIT_ASSERT_EQUAL(22, muh1.func2());
        // serious shit
        CPPUNIT_ASSERT_EQUAL(20, doIt(muh1));
        //CPPUNIT_ASSERT_EQUAL(20+10, doIt(muh1, 10)); -> FAIL
        CPPUNIT_ASSERT_EQUAL(11, doIt(muh1, &Base::func1));
        CPPUNIT_ASSERT_EQUAL(22, doIt(muh1, &Base::func2));
    }
    
private:
    
    /** Use operator(). */
    template <class M>
    inline int doIt(const M& muh) {
        return muh();
    }
    
    /** Use operator(X). */
//    template <class M, class X>
//    inline int doIt(const M& muh, X x) {
//        return muh(x);
//    }
    
    /** Use function pointer. */
    template <class M>
    inline int doIt(M& muh, int (M::* func)()) {
        return (muh.*func)();
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION( TestVirtualCall );
