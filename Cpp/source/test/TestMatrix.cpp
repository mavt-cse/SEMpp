/*
 * Based on cppunit-template.
 * Important note: 
 * - remove GCC_PREPROCESSOR_DEFINITIONS = _GLIBCXX_DEBUG=1 _GLIBCXX_DEBUG_PEDANTIC=1
 *   (in build settings for target)
 * Most important doc: http://cppunit.sourceforge.net/doc/lastest/group___assertions.html
 */

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
// System
#include <stdio.h>						// Standard I/O
#include <stdlib.h>						// Standard helper
#include <iostream>                     // Standard I/O
#include <vector>
#include <fstream>
#include <sstream>
// lib
#include "Environment.h"                // Particle lib environment
// stuff to test
#include "Matrix.h"
#include "Grids.h"
#include "Interpolators.h"
#include "Kernels.h"

// CONSTANTS
#define FLT_EPS 1e-6

/** Test stuff on Matrices, Grids, Interpolators and Kernels. */
class TestMatrix : public CppUnit::TestCase {
    
	CPPUNIT_TEST_SUITE( TestMatrix );
    
	CPPUNIT_TEST( basics );
	CPPUNIT_TEST( dumpAndRead );
	CPPUNIT_TEST( dumpAndReadGrid );
    CPPUNIT_TEST( testKernels );
	
	CPPUNIT_TEST_SUITE_END();
    
public:
    /** Create data structures here. */
    void setUp() {
    }
    
    /** Release data structures here. */
    void tearDown() {
    }
    
    /** Test basic stuff. */
    void basics() {
		basicsD2D< Matrix::D2D<int> >();
		basicsD2D< Matrix::D2D<float> >();
		basicsD2D< Matrix::D2D<double> >();
    }
	
	/** Dump and read matrix. */
	void dumpAndRead() {
		typedef Matrix::D2D<double> MatrixD2D;
		
		// generate it
		MatrixD2D m(5,5);
		fillMatrix(m);
		
		// dump it to file & read
		m.dump("matrix.dat");
		MatrixD2D m2("matrix.dat");
		checkMatrix(m2);
		m.load("matrix.dat");
		checkMatrix(m);
		
		// dump it to stream & read
		std::ostringstream oss(std::ios::binary);
		m.dump(oss);
		CPPUNIT_ASSERT_EQUAL_MESSAGE("dump to stream", true, (bool)oss);
		std::istringstream iss(std::ios::binary);
		iss.str(oss.str());
		MatrixD2D m3(iss);
		checkMatrix(m3);
		iss.str(oss.str());
		m.load(iss);
		CPPUNIT_ASSERT_EQUAL_MESSAGE("load from stream", true, (bool)iss);
		checkMatrix(m);
		
		// fancy dump
		m.dump("matrixF.dat", Matrix::TID_FLOAT);
		m2 = MatrixD2D("matrixF.dat");
		checkMatrix(m2);
		m.dump("matrixI.dat", Matrix::TID_INT);
		m2 = MatrixD2D("matrixI.dat");
		checkMatrix(m2);
	}
	
	/** Dump and read grid. */
	void dumpAndReadGrid() {
		typedef InterpolatorGhosts<KernelHermite> I;
#if DIM == 2
		typedef Matrix::D2D<double> M;
		typedef GridGhostedD<M,I> G;
		
		// generate it
		M m(5,5);
		fillMatrix(m);
		G g(m, VecType(0), VecType(4));
		
		// dump it to stream & read
		std::ostringstream oss(std::ios::binary);
		g.getM().dump(oss);
		CPPUNIT_ASSERT_EQUAL_MESSAGE("dump to stream", true, (bool)oss);
		std::istringstream iss(std::ios::binary);
		iss.str(oss.str());
		g.setM(M(iss));
		CPPUNIT_ASSERT_EQUAL_MESSAGE("load from stream", true, (bool)iss);
		checkMatrix(g.getM());
		
#elif DIM == 3
		typedef Matrix::D3D<double> M;
		typedef GridGhostedD<M,I> G;
		
		// generate it
		M m(5,5,5);
		m(1,1,1) = m(3,3,3) = m(4,4,4) = 3;	m(0,0,0) = m(2,2,2) = 1;
		G g(m, VecType(0), VecType(4));
		
		// dump it to stream & read
		std::ostringstream oss(std::ios::binary);
		g.getM().dump(oss);
		CPPUNIT_ASSERT_EQUAL_MESSAGE("dump to stream", true, (bool)oss);
		std::istringstream iss(std::ios::binary);
		iss.str(oss.str());
		g.setM(M(iss));
		CPPUNIT_ASSERT_EQUAL_MESSAGE("load from stream", true, (bool)iss);
#endif
		
		// check diagonals
		CPPUNIT_ASSERT_EQUAL((G::ElementType)1, g(VecTypeI(0)));
		CPPUNIT_ASSERT_EQUAL((G::ElementType)3, g(VecTypeI(1)));
		CPPUNIT_ASSERT_EQUAL((G::ElementType)1, g(VecTypeI(2)));
		CPPUNIT_ASSERT_EQUAL((G::ElementType)3, g(VecTypeI(3)));
		CPPUNIT_ASSERT_EQUAL((G::ElementType)3, g(VecTypeI(4)));
	}
	
	/** Test grid. */
	void testKernels() {
		testKernel<KernelBilinear>("matrixBilinear");
		testKernel<KernelHermite>("matrixHermite");
		testKernel<KernelMPMSpline2>("matrixMPM2");
		testKernel<KernelMPMSpline3>("matrixMPM3");
		
		// Note:
		// - hand checked in Matlab -> testMatrix.m
	}
	
private:
	/** Templatized basic test for dynamic 2D matrix. */
	template <typename MatrixD2D>
	void basicsD2D() {
		CONCEPT_ASSERT(CMatrix2D< MatrixD2D >);
		
		// generate it
		MatrixD2D m(5,5);
		fillMatrix(m);
		
		// copy
		MatrixD2D m2(m);
		
		// check
		checkMatrix(m2);
	}
	
	/** Test a kernel. */
	template <typename Kernel>
	void testKernel(const std::string& filebase) {
		interpolate<InterpolatorNoGhosts<Kernel>, GridGeneric>(filebase);
		interpolate<InterpolatorPeriodic<Kernel>, GridGeneric>(filebase + "P");
		interpolateG<InterpolatorGhosts<Kernel> >(filebase + "D2", 0);
		interpolateG<InterpolatorGhosts<Kernel> >(filebase + "D3", 1);
		interpolateG<InterpolatorGhosts<Kernel> >(filebase + "N2", 2);	// with bc = 1
		interpolateG<InterpolatorGhosts<Kernel> >(filebase + "N3", 3);	// with bc = 1
		interpolateG<InterpolatorGhosts<Kernel> >(filebase + "P2", 4);
	}
	
	/** Templatized interpolation test. */
	template <typename Interpolator, template <typename M, typename I> class Grid>
	void interpolate(const std::string& filebase) {
#if DIM == 2
		// typedefs
		typedef Matrix::D2D<double> M;
		typedef Interpolator I;
		typedef Grid<M,I> G;
		
		// get test matrix
		M m(5,5);
		fillMatrix(m);
		G g(m, VecType(0), VecType(4));
		dumpGrid(filebase, g, 257);
#else
		// just concepts
		// typedefs
		typedef Matrix::D3D<double> M;
		typedef Interpolator I;
		typedef Grid<M,I> G;
		
		// get test matrix
		M m(5,5,5);
		G g(m, VecType(0), VecType(4));
#endif
		// check types
		CONCEPT_ASSERT(CInterpolator<I, M>);
		CONCEPT_ASSERT(CGrid<G>);
        
        // check grid with constant value
        fillGrid(g);
#if DIM == 2
        checkGrid(g, 129);
#else
        checkGrid(g, 17);
#endif
	}
	
	/** Templatized interpolation test with ghosts. */
	template <typename Interpolator>
	void interpolateG(const std::string& filebase, const int bc_type) {
#if DIM == 2
		// typedefs
		typedef Matrix::D2D<double> M;
		typedef Interpolator I;
		typedef GridGhostedD<M,I> G;
		
		// get test matrix
		M m(5,5);
		fillMatrix(m);
		G g(m, VecType(0), VecType(4));
		switch (bc_type) {
			case 0:
				g.ghostsExtrapolate2();
				break;
			case 1:
				g.ghostsExtrapolate3();
				break;
			case 2:
				g.ghostsNeumann2(1);
				break;
			case 3:
				g.ghostsNeumann3(1);
				break;
			case 4:
				g.ghostsPeriodic();
				break;
			default:
				break;
		}
		dumpGrid(filebase, g, 129);
#else
		// just concepts
		// typedefs
		typedef Matrix::D3D<double> M;
		typedef Interpolator I;
		typedef GridGhostedD<M,I> G;
		
		// get test matrix
		M m(5,5,5);
		G g(m, VecType(0), VecType(4));
#endif
		// check types
		CONCEPT_ASSERT(CInterpolator<I, M>);
		CONCEPT_ASSERT(CGrid<G>);
        
        // check grid with constant value
        fillGrid(g);
		switch (bc_type) {
			case 0:
				g.ghostsExtrapolate2();
				break;
			case 1:
				g.ghostsExtrapolate3();
				break;
			case 2:
				g.ghostsNeumann2(); // otherwise 1 not enforced
				break;
			case 3:
				g.ghostsNeumann3(); // otherwise 1 not enforced
				break;
			case 4:
				g.ghostsPeriodic();
				break;
			default:
				break;
		}
        // check
#if DIM == 2
        checkGrid(g, 129);
#else
        checkGrid(g, 17);
#endif
	}
	
	/** Dump enlarged grid (2D only). */
	template <typename G>
	void dumpGrid(const std::string& filebase, const G& g, const int N = 1025) {
		// extract stuff from g
		const Real dom_size = g.getDomainSize()[0];
		typedef typename G::MatrixType M;
		// enlarge
		M mBIG(N,N);
		const Real h = Real(dom_size)/Real(N-1);
		for (int j = 0; j < N; ++j) {
			for (int i = 0; i < N; ++i) {
				VecType pos(i*h, j*h);
				mBIG(i, j) = g(pos);
			}
		}
		// get some normals too (x-component stored)
		const int Nn = 17;
		M mBIGn(Nn,Nn);
		const Real hn = Real(dom_size)/Real(Nn-1);
		for (int j = 0; j < Nn; ++j) {
			for (int i = 0; i < Nn; ++i) {
				VecType pos(i*hn, j*hn);
				typename G::GradientType n = g.gradient(pos);
				mBIGn(i, j) = n[0];
			}
		}
		
		// dump
		mBIG.dump((filebase+".dat").c_str(), Matrix::TID_FLOAT);
		mBIGn.dump((filebase+"Grad.dat").c_str(), Matrix::TID_FLOAT);
	}
    
	/** Check constant value 1 on enlarged grid. */
	template <typename G>
	void checkGrid(const G& g, const int N = 1025) {
		// extract stuff from g
		const Real dom_size = g.getDomainSize()[0];
		const Real h = Real(dom_size)/Real(N-1);
        typedef typename G::ElementType ET;
#if DIM == 2
		for (int j = 0; j < N; ++j) {
			for (int i = 0; i < N; ++i) {
				VecType pos(i*h, j*h);
				ET val = g(pos);
				typename G::GradientType n = g.gradient(pos);
                CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, val, FLT_EPS);
                CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, n[0], FLT_EPS);
                CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, n[1], FLT_EPS);
			}
		}
#elif DIM == 3
		for (int k = 0; k < N; ++k) {
            for (int j = 0; j < N; ++j) {
                for (int i = 0; i < N; ++i) {
                    VecType pos(i*h, j*h, k*h);
                    ET val = g(pos);
                    typename G::GradientType n = g.gradient(pos);
                    CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, val, FLT_EPS);
                    CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, n[0], FLT_EPS);
                    CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, n[1], FLT_EPS);
                    CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, n[2], FLT_EPS);
                }
            }
        }
#endif
	}
    
	/** Get test matrix for interpolations. */
	template <typename Matrix>
	void fillMatrix(Matrix& m) {
		// fill it (source wiki bicubic interp.)
		m(0,0) = 1; m(1,0) = 2; m(2,0) = 4; m(3,0) = 1; m(4,0) = 1;
		m(0,1) = 6; m(1,1) = 3; m(2,1) = 5; m(3,1) = 2; m(4,1) = 4;
		m(0,2) = 4; m(1,2) = 2; m(2,2) = 1; m(3,2) = 5; m(4,2) = 4;
		m(0,3) = 5; m(1,3) = 4; m(2,3) = 2; m(3,3) = 3; m(4,3) = 4;
		m(0,4) = 3; m(1,4) = 3; m(2,4) = 3; m(3,4) = 2; m(4,4) = 3;
	}
    
    /** Fill grid with constant values 1. */
	template <typename Grid>
    void fillGrid(Grid& g) {
        VecTypeI posI, nx = g.getGridSize();
#if DIM == 2
        for (posI[1] = 0; posI[1] < nx[1]; ++posI[1]) {
            for (posI[0] = 0; posI[0] < nx[0]; ++posI[0]) {
                g(posI) = 1;
            }
        }
#elif DIM == 3
        for (posI[2] = 0; posI[2] < nx[2]; ++posI[2]) {
            for (posI[1] = 0; posI[1] < nx[1]; ++posI[1]) {
                for (posI[0] = 0; posI[0] < nx[0]; ++posI[0]) {
                    g(posI) = 1;
                }
            }
        }
#endif
    }
	
    /** Check values of test matrix. */
	template <typename Matrix>
	void checkMatrix(const Matrix& m) {
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)1, m(0,0));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)2, m(1,0));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)4, m(2,0));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)1, m(3,0));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)1, m(4,0));
		
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)6, m(0,1));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)3, m(1,1));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)5, m(2,1));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)2, m(3,1));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)4, m(4,1));
		
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)4, m(0,2));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)2, m(1,2));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)1, m(2,2));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)5, m(3,2));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)4, m(4,2));
		
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)5, m(0,3));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)4, m(1,3));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)2, m(2,3));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)3, m(3,3));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)4, m(4,3));
		
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)3, m(0,4));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)3, m(1,4));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)3, m(2,4));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)2, m(3,4));
		CPPUNIT_ASSERT_EQUAL((typename Matrix::ElementType)3, m(4,4));
	}
};

CPPUNIT_TEST_SUITE_REGISTRATION( TestMatrix );
