/**\file ConceptCheck.h
 * Includes everything we need to check concepts.
 * 
 * Please consider using #CONCEPT_ASSERT defined in ConceptAssert.h since the
 * main restriction here is that we cannot check types which do not have a 
 * default constructor.
 *
 * Otherwise see #CONCEPT_CHECK macro for usage.
 *
 * Notes on runtime cost:
 * - no code is executed for the tests!
 * - not a single byte is added to the executable (or assembler file)
 * - YES code is checked even though no assembly is produced for it...
 *   (also with -O2 or so set) (tested it with gcc 4.0 and icc 11.0)
 *
 */

#pragma once

/**
 * Concept checker.
 * Template parameter is function pointer to static function containing the
 * concept checks. When this is used in a typedef the compiler will check the
 * function and issue error messages if something wrong there. This does not
 * lead to any runtime overhead!
 * Usage either by typedef or with #CONCEPT_CHECK macro:
 * \code typedef ConceptCheck< Concepts::Fooism<Foo> > UniqueName; \endcode
 * or simpler:
 * \code CONCEPT_CHECK(Concepts::Fooism<Foo>) \endcode
 */
template <void(*)()> struct ConceptCheck {};

/**
 * Concatenate a and b (both may be macros to be expanded).
 * This makes sure we can concatenate 2 names and expand them (e.g. b=__LINE__).
 * Source: boost-library (preprocessor/cat.hpp)
 */
#define NAME_CAT(a, b) NAME_CAT_I(a, b)
// Helpers for NAME_CAT to work
#define NAME_CAT_I(a, b) NAME_CAT_II(a ## b)
#define NAME_CAT_II(res) res

/** Translate define argument into string (for debugging). */
#define TOSTRING(x) STRINGIFY(x)
#define STRINGIFY(x) #x

/**
 * Macro to check concept conformance.
 * Usage:
 * Say you have a templated class Bar whose template type Foo needs to conform
 * to the concept "Fooism" which is defined in a class Concepts:
\code 
class Concepts {
public:
    template< typename Foo >
    static void Fooism() {
        // write here whatever you need from Foo
    }
    // ...
}
\endcode
 * Now in the class Bar we write the following to check concept conformance:
\code
template<typename Foo>
class Bar {
    CONCEPT_CHECK(Concepts::Fooism<Foo>);
    // ...
};
\endcode
 * 
 * Remarks:
 * - We can also use the macro within a (templated) method to check stuff there.
 * - We can also use the macro within the concept check method itself.
 * - We need to write all checks in a method. This will almost always require that
 *   we can construct variables of type of interest. We will therefore always
 *   require that the checked types have default constructors.
 *
 */
#define CONCEPT_CHECK(Function ...) typedef ConceptCheck< Function > NAME_CAT(conceptCheck,__LINE__)
