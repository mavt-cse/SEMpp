/*
 *  Concepts.h
 *
 *  Created by Gerardo Tauriello on 6/4/09.
 *  Copyright 2009 ETH Zürich. All rights reserved.
 *
 */

#pragma once

#include "ConceptCheck.h"

/**
 * Class containing templated public static void methods used to check
 * conformance of types to certain concepts.
 * See #CONCEPT_CHECK for Usage.
 */
class Concepts {
public:
    /**
     * Concept for type that allows to do all kinds of math.
     * Requirements:
     * - T must have a default constructor, copy constructor and assignment
     *   operator (taking T as rhs) (compiler-generated ones may suffice)
     * - We can do math with variables of type T:
     *   - Binary +, -, /, *, +=, -=, /=, *=
     *   - Unary -
     * - We can do math with int, double and float:
     *   - Binary +, -, /, * with T on both sides (e.g. "(int)i + (T)t" and
     *     "(T)t + (int)i")
     *   - Binary +=, -=, /=, *= with T on left hand side.
     * - For all math operations we must be able to write back result into a
     *   variable of type T.
     */
    template< typename T >
    static void MathOperators() {
        // Check constructors (def., copy, assign)
        T t1;
        T t2(t1);
        t2 = t1;
        // Check math between T
        t2 = t1 + t1;
        t2 = t1 - t1;
        t2 = -t1;
        t2 = t1 / t1;
        t2 = t1 * t1;
        t2 += t1;
        t2 -= t1;
        t2 /= t1;
        t2 *= t1;
        // Check math between double and T
        t2 = 1.0 + t1;
        t2 = 1.0 - t1;
        t2 = 1.0 / t1;
        t2 = 1.0 * t1;
        // Check math between T and double
        t2 = t1 + 1.0;
        t2 = t1 - 1.0;
        t2 = t1 / 1.0;
        t2 = t1 * 1.0;
        t2 += 1.0;
        t2 -= 1.0;
        t2 /= 1.0;
        t2 *= 1.0;
        // Check math between float and T
        t2 = 1.0f + t1;
        t2 = 1.0f - t1;
        t2 = 1.0f / t1;
        t2 = 1.0f * t1;
        // Check math between T and float
        t2 = t1 + 1.0f;
        t2 = t1 - 1.0f;
        t2 = t1 / 1.0f;
        t2 = t1 * 1.0f;
        t2 += 1.0f;
        t2 -= 1.0f;
        t2 /= 1.0f;
        t2 *= 1.0f;
        // Check math between int and T
        t2 = 1 + t1;
        t2 = 1 - t1;
        t2 = 1 / t1;
        t2 = 1 * t1;
        // Check math between T and int
        t2 = t1 + 1;
        t2 = t1 - 1;
        t2 = t1 / 1;
        t2 = t1 * 1;
        t2 += 1;
        t2 -= 1;
        t2 /= 1;
        t2 *= 1;
    }
    
    /**
     * Concept for casts from Source to Target.
     * Requirements:
     * - Source has a default constructor
     * - One can explicitly cast from Source to Target (i.e. (Target)source)
     */
    template< typename Source, typename Target >
    static void Castable() {
        // Source must be default constructable
        Source s;
        Target t;
        // We must be able to cast from Source to Target
        (Target)s;
    }
    
    /**
     * Concept for assignments from Source to Target (without cast).
     * Requirements:
     * - Source has a default constructor
     * - Target can be constructed using a Source (i.e. Target target = source).
     * - One can assign Source to Target (i.e. target = source)
     */
    template< typename Source, typename Target >
    static void Assignable() {
        // Source must be default constructable
        Source s;
        // We can create a Target out of a Source
        Target t = s;
        // We can assign a Source to a Target
        t = s;
    }
    
    /**
     * Concept for 2 types which need to match.
     * Requirements:
     * - Both types have a default constructor
     * - Both types are identical
     */
    template< typename T1, typename T2 >
    static void SameType() {
        // Both types must be default constructable
        T1 t1;
        T2 t2;
        // T1 and T2 are exactly same type
        same_type(t1, t2);
    }

private:
    // put some helpers here
    
    /** Use this to check if 2 types are equal. */
    template <typename T>
    static void same_type(T const&, T const&);
};