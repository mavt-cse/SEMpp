/**\file ConceptAssertBoost.h
 * Replacement for ConceptAssert.h (see descriptions there) using boost.
 * This requires proper setting of -I flag to include boost headers.
 * Does not support putting CONCEPT_ASSERT in concept class unless it is within
 * the body of the CONCEPT_USAGE function.
 *
 * Source: http://www.boost.org/doc/libs/1_41_0/libs/concept_check/implementation.htm
 */

#pragma once

#include "ConceptCheck.h"
#include "boost/concept/assert.hpp"
#include "boost/concept_check.hpp"

#define CONCEPT_USAGE(Concept) BOOST_CONCEPT_USAGE(Concept)

#define CONCEPT_ASSERT(Concept ...) BOOST_CONCEPT_ASSERT((Concept))

#define ignore_unused_variable_warning boost::ignore_unused_variable_warning
