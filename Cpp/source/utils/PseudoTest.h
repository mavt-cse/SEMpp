/** \file PseudoTest.h
 * Some defines that allow CppUnit tests to be run without CppUnit.
 * Translates asserts into verbose output.
 *
 * Usage:
 * - Copy/paste class from CppUnit test case (only class and test-related includes)
 * - Remove inheritance (public CppUnit::TestCase)
 * - Call tests using TestClassName::RunTests();
 *   (replace TestClassName with actual name of test case class)
 *
 * #include "PseudoTest.h"
 *
 * @author Gerardo Tauriello
 */

#pragma once

#include <iostream>
#include <string>
#include <cmath>

/** Replacement for helper macro in CppUnit. */
#define CPPUNIT_TEST_SUITE(C) \
public: \
    typedef C __SelfType; \
    static void RunTests() { \
        std::cout << "================================================\n"; \
        std::cout << "Testing class: " << #C << std::endl;

/** Replacement for helper macro in CppUnit. */
#define CPPUNIT_TEST(m) \
        { \
            __SelfType test; \
            std::cout << "--------------------------------------------\n"; \
            std::cout << "Test case: " << #m << std::endl; \
            std::cout << "--------------------------------------------\n"; \
            test.setUp(); \
            test.m(); \
            test.tearDown(); \
        }

/** Replacement for helper macro in CppUnit. */
#define CPPUNIT_TEST_SUITE_END() \
        std::cout << "================================================\n"; \
    }

/** Replacement for helper macro in CppUnit. */
template <typename T>
inline void CPPUNIT_ASSERT_EQUAL(T expected, T actual, const std::string& msg = "") {
    if (expected == actual) {
        std::cout << "Got: " << actual << " (as expected) " << msg << " \n";
    } else {
        std::cout << "!!! Got: " << actual << " (instead of: " << expected << ") " << msg << " !!!\n";
    }
}

/** Replacement for helper macro in CppUnit. */
template <typename T>
inline void CPPUNIT_ASSERT_DOUBLES_EQUAL(T expected, T actual, T delta, const std::string& msg = "") {
    T actual_delta = fabs(expected-actual);
    if (actual_delta <= delta) {
        std::cout << "Got: " << actual << " which is off by " << actual_delta;
        std::cout << " (good enough) " << msg << " \n";
    } else {
        std::cout << "!!! Got: " << actual << " which is off by " << actual_delta;
        std::cout << " (which sucks) !!! " << msg << " \n";
    }
}

/** Replacement for helper macro in CppUnit. Note: CppUnit-version not working!! */
template <typename T>
inline void CPPUNIT_ASSERT_EQUAL_MESSAGE(const std::string& msg, T expected, T actual) {
    CPPUNIT_ASSERT_EQUAL(expected, actual, msg);
}
