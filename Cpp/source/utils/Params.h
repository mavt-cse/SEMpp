/**
 * Class containing all parameters for the application.
 *
 * #include "Params.h"
 *
 * There's no use to create instances of this class, since all features are 
 * static.
 *
 * @author Gerardo Tauriello
 */

#ifndef Params_h
#define Params_h

// SYSTEM INCLUDES
//

// PROJECT INCLUDES
//

// LOCAL INCLUDES
//
#include "VectorElement/VectorElement.h"
#include <vector>

// FORWARD REFERENCES
//


class Params {
public:
// OPERATIONS

	/**
	 * Load parameters from a given file.
	 *
	 * File-Syntax:
	 * Each line of the file may either contain:
	 * - a comment (starting with "#") or
	 * - a parameter-value given as "<parameter> = <value[s]>".
     *   - The \<parameter\>-string must match the parameter-variable-name (see
     *     list of Model Parameters below). White spaces and case are ignored.
     *   - Type of value[s] must be assignable to type of variable. For vector
     *     values the vector is filled with as many values as given (rest is 
     *     not set). No errors occur if vector is smaller/bigger than number
     *     of given values. 
	 *   - Existing parameters will be overwritten.
     * - Trailing comments (e.g. "nx = 10   # bla") work for all types besides
     *   strings
	 *
	 * @param filename File from which to load parameters
	 * @return 0 if everything is OK, 1 otherwise
	 */
	static int Load(const char* filename);
    
    /**
     * Generate an adapted Control-File from a given one.
     * This is meant to be copied and adapted for your purposes.
     * Note that the following is hard-coded:
     * - lines to be adapted
     * Put this in a small application, hard-code input and output filename and
     * change the lines to be adapted. Your main would then just call this
     * method with the required set of parameters.
     *
	 * @param filenameIn    File from which to load reference Ctrl file
	 * @param filenameOut   Filename of adapted Ctrl file
	 * @param params        Parameters to set
	 * @return 0 if everything is OK, 1 otherwise
     */
    static int CreateCtrl(const char* filenameIn, const char* filenameOut, const std::vector<double>& params);

// ACCESS

	/**
	 * \f$n_x\f$ Number of Grid-Points in x-direction.
	 * 
	 * Including the edges, which have a fixed glucose-level of 0 and may not
	 * contain cells.
	 */
	static int nx;
	/**
	 * \f$n_y\f$ Number of Grid-Points in y-direction.
	 * 
	 * Including the edges, which have a fixed glucose-level of 0 and may not
	 * contain cells.
	 */
	static int ny;
	/**
	 * \f$n_z\f$ Number of Grid-Points in z-direction.
	 * 
	 * Including the edges, which have a fixed glucose-level of 0 and may not
	 * contain cells.
	 */
	static int nz;

	/** \f$\Delta x\f$ Grid-Point-Distance in x-direction in \f$\mu m\f$. */
	static float dx;
	/** \f$\Delta y\f$ Grid-Point-Distance in y-direction in \f$\mu m\f$. */
	static float dy;
	/** \f$\Delta z\f$ Grid-Point-Distance in z-direction in \f$\mu m\f$. */
	static float dz;
    
    /** Up to 10 initial parameters. */
    static VectorElement<double, 10> ip;
    /** More parameters. */
    static VectorElement<int, 4> ip2;
    /** More parameters. */
    static std::vector<double> ip3;
    
    /** Kind of model. */
    enum ModelType {
        MT_0,
        MT_1,
        MT_LAST
    };
    
    /** Type of model to use. */
    static ModelType modelType;
    
    /** Name of run. */
    static std::string name;
    
    /** Decsription of run. */
    static std::string desc;

private:

	/**
	 * Prevent use of constructor.
	 */
	Params();

};

// INLINE METHODS
//

// EXTERNAL REFERENCES
//

#endif	// _Params_h_
