/**\file SSEVecs.h
 * Extended SSE-vectors (fvec.h, dvec.h) to be able to do more operations.
 * See:
 * - file:///opt/intel/Compiler/11.0/064/Documentation/compiler_c/com.intel.compilers.icc.docset/Contents/Resources/Documents/icc/doc_files/index.htm
 * - file:///opt/intel/Compiler/11.0/064/Documentation/compiler_c/com.intel.compilers.icc.docset/Contents/Resources/Documents/icc/doc_files/index.htm?page=source%2Fcref_cls%2Fcommon%2Fcppref_class_fp_ovrvw.htm 
 */

#pragma once

#include "fvec.h"
#include "dvec.h"

#pragma mark F64vec2
inline F64vec2& operator+=(F64vec2& lhs, const float rhs){
	lhs += F64vec2(rhs);
	return lhs;
}

inline F64vec2& operator-=(F64vec2& lhs, const float rhs){
	lhs -= F64vec2(rhs);
	return lhs;
}

inline F64vec2& operator*=(F64vec2& lhs, const float rhs){
	lhs *= F64vec2(rhs);
	return lhs;
}

inline F64vec2& operator/=(F64vec2& lhs, const float rhs){
	lhs /= F64vec2(rhs);
	return lhs;
}

inline const F64vec2 operator-(const F64vec2 rhs) {
    return F64vec2(0.0f)-rhs;
}

inline const F64vec2 operator+(const F64vec2 lhs, const float rhs) {
    return lhs+F64vec2(rhs);
}

inline const F64vec2 operator-(const F64vec2 lhs, const float rhs) {
    return lhs-F64vec2(rhs);
}

inline const F64vec2 operator*(const F64vec2 lhs, const float rhs) {
    return lhs*F64vec2(rhs);
}

inline const F64vec2 operator/(const F64vec2 lhs, const float rhs) {
    return lhs/F64vec2(rhs);
}

inline const F64vec2 operator+(const float left, const F64vec2 right) {
    return F64vec2(left) + right;
}

inline const F64vec2 operator-(const float left, const F64vec2 right) {
    return F64vec2(left) - right;
}

inline const F64vec2 operator*(const float left, const F64vec2 right) {
    return F64vec2(left) * right;
}

inline const F64vec2 operator/(const float left, const F64vec2 right) {
    return F64vec2(left) / right;
}

#pragma mark F32vec1
inline F32vec1& operator+=(F32vec1& lhs, const float rhs){
	lhs += F32vec1(rhs);
	return lhs;
}

inline F32vec1& operator-=(F32vec1& lhs, const float rhs){
	lhs -= F32vec1(rhs);
	return lhs;
}

inline F32vec1& operator*=(F32vec1& lhs, const float rhs){
	lhs *= F32vec1(rhs);
	return lhs;
}

inline F32vec1& operator/=(F32vec1& lhs, const float rhs){
	lhs /= F32vec1(rhs);
	return lhs;
}

inline const F32vec1 operator-(const F32vec1 rhs) {
    return F32vec1(0.0f)-rhs;
}

inline const F32vec1 operator+(const F32vec1 lhs, const float rhs) {
    return lhs+F32vec1(rhs);
}

inline const F32vec1 operator-(const F32vec1 lhs, const float rhs) {
    return lhs-F32vec1(rhs);
}

inline const F32vec1 operator*(const F32vec1 lhs, const float rhs) {
    return lhs*F32vec1(rhs);
}

inline const F32vec1 operator/(const F32vec1 lhs, const float rhs) {
    return lhs/F32vec1(rhs);
}

inline const F32vec1 operator+(const float left, const F32vec1 right) {
    return F32vec1(left) + right;
}

inline const F32vec1 operator-(const float left, const F32vec1 right) {
    return F32vec1(left) - right;
}

inline const F32vec1 operator*(const float left, const F32vec1 right) {
    return F32vec1(left) * right;
}

inline const F32vec1 operator/(const float left, const F32vec1 right) {
    return F32vec1(left) / right;
}

#pragma mark F32vec4
inline F32vec4& operator+=(F32vec4& lhs, const float rhs){
	lhs += F32vec4(rhs);
	return lhs;
}

inline F32vec4& operator-=(F32vec4& lhs, const float rhs){
	lhs -= F32vec4(rhs);
	return lhs;
}

inline F32vec4& operator*=(F32vec4& lhs, const float rhs){
	lhs *= F32vec4(rhs);
	return lhs;
}

inline F32vec4& operator/=(F32vec4& lhs, const float rhs){
	lhs /= F32vec4(rhs);
	return lhs;
}

inline const F32vec4 operator-(const F32vec4 rhs) {
    return F32vec4(0.0f)-rhs;
}

inline const F32vec4 operator+(const F32vec4 lhs, const float rhs) {
    return lhs+F32vec4(rhs);
}

inline const F32vec4 operator-(const F32vec4 lhs, const float rhs) {
    return lhs-F32vec4(rhs);
}

inline const F32vec4 operator*(const F32vec4 lhs, const float rhs) {
    return lhs*F32vec4(rhs);
}

inline const F32vec4 operator/(const F32vec4 lhs, const float rhs) {
    return lhs/F32vec4(rhs);
}

inline const F32vec4 operator+(const float left, const F32vec4 right) {
    return F32vec4(left) + right;
}

inline const F32vec4 operator-(const float left, const F32vec4 right) {
    return F32vec4(left) - right;
}

inline const F32vec4 operator*(const float left, const F32vec4 right) {
    return F32vec4(left) * right;
}

inline const F32vec4 operator/(const float left, const F32vec4 right) {
    return F32vec4(left) / right;
}