/**
 * Unrolled vector of 4 elements of type T.
 *
 * #include "VectorElementUnroll4.h"
 * 
 * Additional features:
 * - Constructor to fill 4 elements of vector
 * 
 * @see #VectorElement
 *
 * @author Gerardo Tauriello
 */

#pragma once

#include "VectorElementGeneric.h"

template<typename T>
class VectorElement<T,4> {
	T data[4];
	typedef VectorElement<T,4> SelfType;
	
public:
	inline T& operator[](int i) { return data[i]; }
	inline T operator[](int i) const { return data[i]; }
	
	inline VectorElement(const T scalar = T(0)) {
		data[0] = scalar;
		data[1] = scalar;
		data[2] = scalar;
		data[3] = scalar;
	}
	
	inline VectorElement(const SelfType &rhs) {
		data[0] = rhs.data[0];
		data[1] = rhs.data[1];
		data[2] = rhs.data[2];
		data[3] = rhs.data[3];
	}
    
    /** Constructor setting content to f0,f1,f2,f3. */
    inline VectorElement(const T f0, const T f1, const T f2, const T f3) { 
		data[0] = f0;
		data[1] = f1;
		data[2] = f2;
		data[3] = f3;
    }
	
	inline T max() const {
		return std::max( std::max(data[0],data[1]), std::max(data[2],data[3]) );
	}

	inline T min() const {
		return std::min( std::min(data[0],data[1]), std::min(data[2],data[3]) );
	}
	
	inline T absSq() const {
		return data[0]*data[0] + data[1]*data[1] + 
               data[2]*data[2] + data[3]*data[3];
	}
	
	inline T abs() const {
		return std::sqrt(data[0]*data[0] + data[1]*data[1] + 
                    data[2]*data[2] + data[3]*data[3]);
	}
    
	inline T fac() const {
		return data[0]*data[1]*data[2]*data[3];
	}
    
	inline T sum() const {
		return data[0]+data[1]+data[2]+data[3];
	}
    
	inline VectorElement<int,4> ceil() const {
		return VectorElement<int,4>(std::ceil(data[0]),std::ceil(data[1]),
                                    std::ceil(data[2]),std::ceil(data[3]));
	}
	
    inline T dot(const SelfType &other) const {
        return data[0]*other[0] + data[1]*other[1] + 
            data[2]*other[2] + data[3]*other[3];
	}
	
	inline SelfType& operator=(const SelfType &rhs)
	{
		data[0] = rhs.data[0];
		data[1] = rhs.data[1];
		data[2] = rhs.data[2];
		data[3] = rhs.data[3];
		return *this;
	}
	
	inline SelfType& operator+=(const SelfType &rhs)
	{
		data[0] += rhs.data[0];
		data[1] += rhs.data[1];
		data[2] += rhs.data[2];
		data[3] += rhs.data[3];
		return *this;
	}
	
	inline SelfType& operator+=(const T rhs)
	{
		data[0] += rhs;
		data[1] += rhs;
		data[2] += rhs;
		data[3] += rhs;
		return *this;
	}
	
	inline SelfType& operator-=(const SelfType &rhs)
	{
		data[0] -= rhs.data[0];
		data[1] -= rhs.data[1];
		data[2] -= rhs.data[2];
		data[3] -= rhs.data[3];
		return *this;
	}
	
	inline SelfType& operator-=(const T rhs)
	{
		data[0] -= rhs;
		data[1] -= rhs;
		data[2] -= rhs;
		data[3] -= rhs;
		return *this;
	}
	
	inline SelfType& operator*=(const SelfType &rhs)
	{
		data[0] *= rhs.data[0];
		data[1] *= rhs.data[1];
		data[2] *= rhs.data[2];
		data[3] *= rhs.data[3];
		return *this;
	}
	
	inline SelfType& operator*=(const T rhs)
	{
		data[0] *= rhs;
		data[1] *= rhs;
		data[2] *= rhs;
		data[3] *= rhs;
		return *this;
	}
    
	inline SelfType& operator/=(const SelfType &rhs)
	{
		data[0] /= rhs.data[0];
		data[1] /= rhs.data[1];
		data[2] /= rhs.data[2];
		data[3] /= rhs.data[3];
		return *this;
	}
	
	inline SelfType& operator/=(const T rhs)
	{
		data[0] /= rhs;
		data[1] /= rhs;
		data[2] /= rhs;
		data[3] /= rhs;
		return *this;
	}
	
	inline const SelfType operator+(const T rhs) const {
		SelfType result = *this;
		result += rhs;
		return result;
	}
	
	inline const SelfType operator-() const {
		SelfType result = SelfType(0.0);
		result -= *this;
		return result;
	}
	
	inline const SelfType operator-(const T rhs) const {
		SelfType result = *this;
		result -= rhs;
		return result;
	}
	
	inline const SelfType operator*(const T rhs) const {
		SelfType result = *this;
		result *= rhs;
		return result;
	}
	
	inline const SelfType operator/(const T rhs) const {
		SelfType result = *this;
		result /= rhs;
		return result;
	}
};
