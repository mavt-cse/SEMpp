/**
 * Unrolled vector of 3 elements of type T.
 *
 * #include "VectorElementUnroll3.h"
 * 
 * Additional features:
 * - Constructor to fill 3 elements of vector
 * - Function VectorElement<T,3>::cross to compute cross product.
 * 
 * @see #VectorElement
 *
 * @author Florian Milde
 */

#pragma once

#include "VectorElementGeneric.h"

template<typename T>
class VectorElement<T,3> {
	T data[3];
	typedef VectorElement<T,3> SelfType;
	
public:
	inline T& operator[](int i) { return data[i]; }
	inline T operator[](int i) const { return data[i]; }
	
	inline VectorElement(const T scalar = T(0))
	{
		data[0] = scalar;
		data[1] = scalar;
		data[2] = scalar;
	}
	
	inline VectorElement(const SelfType &rhs)
	{
		data[0] = rhs.data[0];
		data[1] = rhs.data[1];
		data[2] = rhs.data[2];
	}
	
    /** Constructor setting content to d0,d1,d2. */
	inline VectorElement(const T d0, const T d1, const T d2)
	{
		data[0] = d0;
		data[1] = d1;
		data[2] = d2;
	}
	
	inline T max() const
	{
		return std::max( std::max(data[0], data[1]), data[2] );
    }
	
	inline T min() const
	{
		return std::min( std::min(data[0], data[1]), data[2] );
    }
	
	inline T absSq() const
	{
		return data[0]*data[0]+data[1]*data[1]+data[2]*data[2];
	}
	
	inline T abs() const
	{
		return std::sqrt(data[0]*data[0]+data[1]*data[1]+data[2]*data[2]);
	}
	
	inline T fac() const
	{
		return data[0]*data[1]*data[2];
	}
    
	inline T sum() const
	{
		return data[0]+data[1]+data[2];
	}
	
	inline VectorElement<int,3> ceil() const
	{
		return VectorElement<int,3>(std::ceil(data[0]),std::ceil(data[1]),std::ceil(data[2]));
	}
	
    inline T dot(const SelfType &other) const {
        return data[0]*other[0] + data[1]*other[1] + 
            data[2]*other[2];
	}
	
    /** Cross product of this x other. */
    inline SelfType cross(const SelfType &other) const {
        return SelfType(data[1]*other[2] - data[2]*other[1],
                        data[2]*other[0] - data[0]*other[2],
                        data[0]*other[1] - data[1]*other[0]);
	}
	
	inline SelfType& operator=(const SelfType &rhs)
	{
		data[0] = rhs.data[0];
		data[1] = rhs.data[1];
		data[2] = rhs.data[2];
		return *this;
	}
	
	inline SelfType& operator+=(const SelfType &rhs)
	{
		data[0] += rhs.data[0];
		data[1] += rhs.data[1];
		data[2] += rhs.data[2];
		return *this;
	}
	
	inline SelfType& operator+=(const T rhs)
	{
		data[0] += rhs;
		data[1] += rhs;
		data[2] += rhs;
		return *this;
	}
	
	inline SelfType& operator-=(const SelfType &rhs)
	{
		data[0] -= rhs.data[0];
		data[1] -= rhs.data[1];
		data[2] -= rhs.data[2];
		return *this;
	}
	
	inline SelfType& operator-=(const T rhs)
	{
		data[0] -= rhs;
		data[1] -= rhs;
		data[2] -= rhs;
		return *this;
	}
	
	inline SelfType& operator*=(const SelfType &rhs)
	{
		data[0] *= rhs.data[0];
		data[1] *= rhs.data[1];
		data[2] *= rhs.data[2];
		return *this;
	}
	
	inline SelfType& operator*=(const T rhs)
	{
		data[0] *= rhs;
		data[1] *= rhs;
		data[2] *= rhs;
		return *this;
	}
	
	inline SelfType& operator/=(const SelfType &rhs)
	{
		data[0] /= rhs.data[0];
		data[1] /= rhs.data[1];
		data[2] /= rhs.data[2];
		return *this;
	}
	
	inline SelfType& operator/=(const T rhs)
	{
		data[0] /= rhs;
		data[1] /= rhs;
		data[2] /= rhs;
		return *this;
	}
	
	inline const SelfType operator+(const T rhs) const {
		SelfType result = *this;
		result += rhs;
		return result;
	}
	
	inline const SelfType operator-() const {
		SelfType result = SelfType(0.0);
		result -= *this;
		return result;
	}
	
	inline const SelfType operator-(const T rhs) const {
		SelfType result = *this;
		result -= rhs;
		return result;
	}
	
	inline const SelfType operator*(const T rhs) const {
		SelfType result = *this;
		result *= rhs;
		return result;
	}
	
	inline const SelfType operator/(const T rhs) const {
		SelfType result = *this;
		result /= rhs;
		return result;
	}
};