/**
 * Specialized SSE version for 1 element vector of float.
 *
 * #include "VectorElementSSE1f.h"
 *
 * Uses SSE F32vec1. NOT RECOMMENDED.
 * Additional features:
 * - Constructor to copy from F32vec1.
 * 
 * @see #VectorElement
 *
 * @author Gerardo Tauriello
 */

#pragma once

#include "SSEVecs.h"
#include "VectorElementGeneric.h"

template<>
class VectorElement<float,1>: public F32vec1 {
	typedef VectorElement<float,1> SelfType;
	
public:
	inline VectorElement<float,1>(const float scalar = 0.0f): F32vec1(scalar) { }
	
	inline VectorElement<float,1>(const SelfType& rhs): F32vec1(rhs) { }
	
    /** Copy constructor from F32vec1. */
	inline VectorElement<float,1>(const F32vec1& rhs): F32vec1(rhs) { }
    
    inline float& operator[](int i) {
        // ugly hack based on F32vec4 implementation...
        float * const fp = (float*)&vec;
        return *fp;
    }
    
	inline float operator[](int i) const {
        // ugly hack based on F32vec4 implementation...
        float * const fp = (float*)&vec;
        return *fp;
    }
    
	inline float max() const {
		return (*this)[0];
	}

	inline float min() const {
		return (*this)[0];
	}
	
	inline float absSq() const {
		return (*this)[0] * (*this)[0];
	}
    
	inline float abs() const {
		return abs((*this)[0]);
	}
	
	inline float fac() const {
		return (*this)[0];
	}
    
	inline float sum() const {
		return (*this)[0];
	}
	
	inline VectorElement<int,1> ceil() const {
		return VectorElement<int,1>(std::ceil((*this)[0]));
	}
	
    inline float dot(const SelfType &other) const {
        return (*this)[0] * other[0];
	}
	
	inline const SelfType operator+(const float rhs) const {
		return (*this)+F32vec1(rhs);
	}
	
    inline const SelfType operator-() const {
		return F32vec1(0.0f) - (*this);
	}
    
	inline const SelfType operator-(const float rhs) const {
		return (*this)-F32vec1(rhs);
	}
	
	inline const SelfType operator*(const float rhs) const {
		return (*this)*F32vec1(rhs);
	}
	
	inline const SelfType operator/(const float rhs) const {
		return (*this)/F32vec1(rhs);
	}
};
