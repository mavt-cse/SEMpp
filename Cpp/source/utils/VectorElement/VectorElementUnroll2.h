/**
 * Unrolled vector of 2 elements of type T.
 *
 * #include "VectorElementUnroll2.h"
 * 
 * Additional features:
 * - Constructor to fill 2 elements of vector
 * 
 * @see #VectorElement
 *
 * @author Florian Milde
 */

#pragma once

#include "VectorElementGeneric.h"

template<typename T>
class VectorElement<T,2> {
	T data[2];
	typedef VectorElement<T,2> SelfType;
	
public:
	inline T& operator[](int i) { return data[i]; }
	inline T operator[](int i) const { return data[i]; }
	
	inline VectorElement(const T scalar = T(0))
	{
		data[0] = scalar;
		data[1] = scalar;
	}
	
	inline VectorElement(const SelfType &rhs)
	{
		data[0] = rhs.data[0];
		data[1] = rhs.data[1];
	}
    
    /** Constructor setting content to d0,d1. */
	inline VectorElement(const T d0, const T d1) {
		data[0] = d0;
		data[1] = d1;
	}
	
	inline T max() const {
		return std::max(data[0],data[1]);
	}

	inline T min() const {
		return std::min(data[0],data[1]);
	}
	
	inline T absSq() const {
		return data[0]*data[0]+data[1]*data[1];
	}
	
	inline T abs() const {
		return std::sqrt(data[0]*data[0]+data[1]*data[1]);
	}
    
	inline T fac() const {
		return data[0]*data[1];
	}
    
	inline T sum() const {
		return data[0]+data[1];
	}
    
	inline VectorElement<int,2> ceil() const {
		return VectorElement<int,2>(std::ceil(data[0]),std::ceil(data[1]));
	}
	
    inline T dot(const SelfType &other) const {
        return data[0]*other[0] + data[1]*other[1];
	}
	
	inline SelfType& operator=(const SelfType &rhs)
	{
		data[0] = rhs.data[0];
		data[1] = rhs.data[1];
		return *this;
	}
	
	inline SelfType& operator+=(const SelfType &rhs)
	{
		data[0] += rhs.data[0];
		data[1] += rhs.data[1];
		return *this;
	}
	
	inline SelfType& operator+=(const T rhs)
	{
		data[0] += rhs;
		data[1] += rhs;
		return *this;
	}
	
	inline SelfType& operator-=(const SelfType &rhs)
	{
		data[0] -= rhs.data[0];
		data[1] -= rhs.data[1];
		return *this;
	}
	
	inline SelfType& operator-=(const T rhs)
	{
		data[0] -= rhs;
		data[1] -= rhs;
		return *this;
	}
	
	inline SelfType& operator*=(const SelfType &rhs)
	{
		data[0] *= rhs.data[0];
		data[1] *= rhs.data[1];
		return *this;
	}
	
	inline SelfType& operator*=(const T rhs)
	{
		data[0] *= rhs;
		data[1] *= rhs;
		return *this;
	}
    
	inline SelfType& operator/=(const SelfType &rhs)
	{
		data[0] /= rhs.data[0];
		data[1] /= rhs.data[1];
		return *this;
	}
	
	inline SelfType& operator/=(const T rhs)
	{
		data[0] /= rhs;
		data[1] /= rhs;
		return *this;
	}
	
	inline const SelfType operator+(const T rhs) const {
		SelfType result = *this;
		result += rhs;
		return result;
	}
	
	inline const SelfType operator-() const {
		SelfType result = SelfType(0.0);
		result -= *this;
		return result;
	}
	
	inline const SelfType operator-(const T rhs) const {
		SelfType result = *this;
		result -= rhs;
		return result;
	}
	
	inline const SelfType operator*(const T rhs) const {
		SelfType result = *this;
		result *= rhs;
		return result;
	}
	
	inline const SelfType operator/(const T rhs) const {
		SelfType result = *this;
		result /= rhs;
		return result;
	}
};