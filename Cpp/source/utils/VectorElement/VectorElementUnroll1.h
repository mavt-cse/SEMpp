/**
 * Unrolled vector of 1 element of type T.
 *
 * #include "VectorElementUnroll1.h"
 * 
 * @see #VectorElement
 *
 * @author Gerardo Tauriello
 */

#pragma once

#include "VectorElementGeneric.h"

template<typename T>
class VectorElement<T,1> {
	T data;
	typedef VectorElement<T,1> SelfType;
	
public:
	inline T& operator[](int i) { return data; }
	inline T operator[](int i) const { return data; }
	
	inline VectorElement<T,1>(const T scalar = T(0)): data(scalar) { }
	
	inline VectorElement<T,1>(const SelfType& rhs): data(rhs.data) { }
    
	inline T max() const {
		return data;
	}
	
	inline T min() const {
		return data;
	}
	
	inline T absSq() const {
		return data*data;
	}
	
	inline T abs() const {
		return std::abs(data);
	}
    
	inline T fac() const {
		return data;
	}
    
	inline T sum() const {
		return data;
	}
    
	inline VectorElement<int,1> ceil() const {
		return VectorElement<int,1>(std::ceil(data));
	}
	
    inline T dot(const SelfType &other) const {
        return data*other[0];
	}
	
	inline SelfType& operator=(const SelfType rhs) {
		data = rhs.data;
		return *this;
	}
	
	inline SelfType& operator+=(const SelfType rhs) {
		data += rhs.data;
		return *this;
	}
	
	inline SelfType& operator+=(const T rhs) {
		data += rhs;
		return *this;
	}
	
	inline SelfType& operator-=(const SelfType rhs) {
		data -= rhs.data;
		return *this;
	}
	
	inline SelfType& operator-=(const T rhs) {
		data -= rhs;
		return *this;
	}
	
	inline SelfType& operator*=(const SelfType rhs) {
		data *= rhs.data;
		return *this;
	}
	
	inline SelfType& operator*=(const T rhs) {
		data *= rhs;
		return *this;
	}
	
	inline SelfType& operator/=(const SelfType rhs) {
		data /= rhs.data;
		return *this;
	}
	
	inline SelfType& operator/=(const T rhs) {
		data /= rhs;
		return *this;
	}
	
	inline const SelfType operator+(const T rhs) const {
		return SelfType(data + rhs);
	}
	
	inline const SelfType operator-() const {
		return SelfType(-data);
	}
	
	inline const SelfType operator-(const T rhs) const {
		return SelfType(data - rhs);
	}
	
	inline const SelfType operator*(const T rhs) const {
		return SelfType(data * rhs);
	}
	
	inline const SelfType operator/(const T rhs) const {
		return SelfType(data / rhs);
	}
};
