/**
 * Specialized SSE version for 2 element vector of double.
 *
 * #include "VectorElementSSE2d.h"
 *
 * Uses SSE F64vec2.
 * Additional features:
 * - Constructor to copy from F64vec2.
 * - Constructor to fill 2 elements of vector
 * 
 * @see #VectorElement
 *
 * @author Gerardo Tauriello
 */

#pragma once

#include "SSEVecs.h"
#include "VectorElementGeneric.h"

template<>
class VectorElement<double,2>: public F64vec2 {
	typedef VectorElement<double,2> SelfType;
	
public:
	inline VectorElement<double,2>(const double scalar = 0.0): F64vec2(scalar) { }
	
	inline VectorElement<double,2>(const SelfType& rhs): F64vec2(rhs) { }
	
    /** Copy constructor from F64vec2. */
	inline VectorElement<double,2>(const F64vec2& rhs): F64vec2(rhs) { }
    
    /** Constructor setting content to d0,d1. */
    inline VectorElement(const double d0, const double d1): F64vec2(d1, d0) { }
	
	inline double max() const {
		return std::max((*this)[0],(*this)[1]);
	}

	inline double min() const {
		return std::min((*this)[0],(*this)[1]);
	}	
	
	inline double absSq() const {
		return (*this)[0]*(*this)[0]+(*this)[1]*(*this)[1];
	}
    
	inline double abs() const {
		return std::sqrt((*this)[0]*(*this)[0]+(*this)[1]*(*this)[1]);
	}
    
	inline double fac() const {
		return (*this)[0]*(*this)[1];
	}
    
	inline double sum() const {
        return add_horizontal(*this);
	}
    
	inline VectorElement<int,2> ceil() const {
		VectorElement<int,2> result = VectorElement<int,2>(std::ceil((*this)[0]),std::ceil((*this)[1]));
		return result;
	}
	
    inline double dot(const SelfType &other) const {
		return (*this)[0]*other[0] + (*this)[1]*other[1];
	}
	
	inline void orientRand()
	{
		float alpha = M_PI*2.0*(double)rand()/(double)RAND_MAX;
		(*this)[0]=cos(alpha);
		(*this)[1]=sin(alpha);
	}
	
	inline const SelfType operator+(const float rhs) const {
		return (*this)+F64vec2(rhs);
	}
	
    inline const SelfType operator-() const {
		return F64vec2(0.0) - (*this);
	}
    
	inline const SelfType operator-(const float rhs) const {
		return (*this)-F64vec2(rhs);
	}
	
	inline const SelfType operator*(const float rhs) const {
		return (*this)*F64vec2(rhs);
	}
	
	inline const SelfType operator/(const float rhs) const {
		return (*this)/F64vec2(rhs);
	}
};
