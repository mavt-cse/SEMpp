/**
 * Specialized SSE version for 4 element vector of float.
 *
 * #include "VectorElementSSE4f.h"
 *
 * Uses SSE F32vec4.
 * Additional features:
 * - Constructor to copy from F32vec4.
 * - Constructor to fill 4 elements of vector
 * 
 * @see #VectorElement
 *
 * @author Gerardo Tauriello
 */

#pragma once

#include "SSEVecs.h"
#include "VectorElementGeneric.h"

template<>
class VectorElement<float,4>: public F32vec4{
	typedef VectorElement<float,4> SelfType;
	
public:
	inline VectorElement<float,4>(const float scalar = 0.0f): F32vec4(scalar) { }
	
	inline VectorElement<float,4>(const SelfType& rhs): F32vec4(rhs) { }
	
	inline VectorElement<float,4>(const F32vec4& rhs): F32vec4(rhs) { }
	
    /** Constructor setting content to f0,f1,f2,f3. */
    inline VectorElement(const float f0, const float f1,
                         const float f2, const float f3): F32vec4(f3,f2,f1,f0) { }
    
	inline float max() const {
		return std::max( std::max( (*this)[0], (*this)[1] ), std::max( (*this)[2], (*this)[3] ) );
	}

	inline float min() const {
		return std::min( std::min( (*this)[0], (*this)[1] ), std::min( (*this)[2], (*this)[3] ) );
	}	
	
	inline float absSq() const {
		return (*this)[0]*(*this)[0] + (*this)[1]*(*this)[1] + 
               (*this)[2]*(*this)[2] + (*this)[3]*(*this)[3];
	}	
	
	inline float abs() const {
		return std::sqrt((*this)[0]*(*this)[0] + (*this)[1]*(*this)[1] + 
                         (*this)[2]*(*this)[2] + (*this)[3]*(*this)[3]);
	}
    
	inline float fac() const {
		return (*this)[0]*(*this)[1]*(*this)[2]*(*this)[3];
	}
    
	inline float sum() const {
        return add_horizontal(*this);
	}
    
	inline VectorElement<int,4> ceil() const {
		return VectorElement<int,4>(std::ceil((*this)[0]),std::ceil((*this)[1]),
                                    std::ceil((*this)[2]),std::ceil((*this)[3]));
	}
	
    inline float dot(const SelfType &other) const {
        return (*this)[0]*other[0] + (*this)[1]*other[1] + 
            (*this)[2]*other[2] + (*this)[3]*other[3];
	}
    
	inline const SelfType operator+(const float rhs) const {
		return (*this)+F32vec4(rhs);
	}
	
    inline const SelfType operator-() const {
		return F32vec4(0.0f) - (*this);
	}
    
	inline const SelfType operator-(const float rhs) const {
		return (*this)-F32vec4(rhs);
	}
	
	inline const SelfType operator*(const float rhs) const {
		return (*this)*F32vec4(rhs);
	}
	
	inline const SelfType operator/(const float rhs) const {
		return (*this)/F32vec4(rhs);
	}
};
