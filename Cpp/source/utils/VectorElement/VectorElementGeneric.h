/**\file VectorElementGeneric.h
 * Collection of generic classes and functions for vectors that can do math.
 * Includes:
 * - generic #VectorElement class including most operations.
 * - generic operators for anything +,-,*,/ any vector (no need to specialize
 *   those!!). This includes vector OP vector.
 * - generic Utilities for vectors.
 * - Support for C++ stream output of #VectorElement.
 */

#pragma once

#include <cmath>
#include <stdio.h>						// Standard I/O
#include <stdlib.h>						// Standard helper
#include <iostream>                     // Standard C++ I/O
#include <cassert>

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif

/**
 * General vector of N elements of type T.
 * Enable the use of standard mathematical functions on vector. The operations
 * are performed element-by-element.
 * 
 * Additional reqs besides what is implemented in generic version:
 * - Vectors up to 4 elements require constructor to set all elements
 * - Vectors with 3 elements require cross product function
 *   \code inline SelfType cross(const SelfType &other) const \endcode
 *
 * #include "VectorElementGeneric.h"
 *
 * @author Gerardo Tauriello
 */
template<typename T, int num_elem>
class VectorElement {
    /** Internal representation of data. */
	T data[num_elem];
    /** Shorthand for own type. */
	typedef VectorElement<T,num_elem> SelfType;
	
public:
    /** Access i-th element in [0, num_eleme-1]. */
	inline T& operator[](int i) { assert(i<num_elem); return data[i]; }
    /** Access i-th element in [0, num_eleme-1]. */
	inline T operator[](int i) const { assert(i<num_elem); return data[i]; }
	
    /** Default constructor setting all elements to scalar. */
	inline VectorElement(const T scalar = T(0)) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] = scalar;
		}
	}
    
    /** Copy constructor. */
	inline VectorElement(const SelfType &rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] = rhs.data[i];
		}
	}
    
    /** Get value of maximal element in vector. */
	inline T max() const {
		T result = data[0];
		for (int i = 1; i < num_elem; ++i) {
			result = std::max(data[i], result);
		}
		return result;
	}
	
    /** Get value of minimal element in vector. */
	inline T min() const {
		T result = data[0];
		for (int i = 1; i < num_elem; ++i) {
			result = std::min(data[i], result);
		}
		return result;
	}
    
    /**
     * Get squared length of vector.
     * \f$ \sum_i{a_i^2} \f$
     */
	inline T absSq() const {
		T result = T(0);
		for (int i = 0; i < num_elem; ++i) {
			result += data[i] * data[i];
		}
		return result;
	}
    
    /**
     * Get length of vector.
     * \f$ \sqrt{\sum_i{a_i^2}} \f$
     */
	inline T abs() const {
        return std::sqrt(absSq());
	}
	
    /**
     * Get product of all elements in vector.
     * \f$ \prod_i{a_i} \f$
     */
	inline T fac() const {
		T result = T(1);
		for (int i = 0; i < num_elem; ++i) {
			result *= data[i];
		}
		return result;
	}
    
    /** Get sum of all elements in vector. */
	inline T sum() const {
		T result = T(0);
		for (int i = 0; i < num_elem; ++i) {
			result += data[i];
		}
		return result;
	}
	
    /** Get integer vector form this one by rounding-up (ceiling) all elements. */
	inline VectorElement<int,num_elem> ceil() const {
		VectorElement<int,num_elem> result;
		for (int i = 0; i < num_elem; ++i) {
			result[i] = std::ceil(data[i]);
		}
		return result;
	}
	
    /**
     * Get dot product between this vector and other.
     * \f$ \sum_i{a_i b_i} \f$
     */
    inline T dot(const SelfType &other) const {
		T result = T(0);
		for (int i = 0; i < num_elem; ++i) {
			result += data[i] * other[i];
		}
		return result;
	}
    
    /** Assignment operator (overwrites this vector). */
	inline SelfType& operator=(const SelfType &rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] = rhs.data[i];
		}
		return *this;
	}
	
    /** Add rhs to this one (element-by-element). */
	inline SelfType& operator+=(const SelfType &rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] += rhs.data[i];
		}
		return *this;
	}
	
    /** Add scalar to each element of this vector. */
	inline SelfType& operator+=(const T rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] += rhs;
		}
		return *this;
	}
	
    /** Subtract rhs to this one (element-by-element). */
	inline SelfType& operator-=(const SelfType &rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] -= rhs.data[i];
		}
		return *this;
	}
	
    /** Subtract scalar to each element of this vector. */
	inline SelfType& operator-=(const T rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] -= rhs;
		}
		return *this;
	}
	
    /** Multiply this vector by rhs (element-by-element). */
	inline SelfType& operator*=(const SelfType &rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] *= rhs.data[i];
		}
		return *this;
	}
	
    /** Multipliy each element of this vector by scalar. */
	inline SelfType& operator*=(const T rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] *= rhs;
		}
		return *this;
	}
	
    /** Divide this vector by rhs (element-by-element). */
	inline SelfType& operator/=(const SelfType &rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] /= rhs.data[i];
		}
		return *this;
	}
	
    /** Divide each element of this vector by scalar. */
	inline SelfType& operator/=(const T rhs) {
		for (int i = 0; i < num_elem; ++i) {
			data[i] /= rhs;
		}
		return *this;
	}
	
    /** Return this + rhs. @see VectorElement::operator+=. */
	inline const SelfType operator+(const T rhs) const {
		SelfType result = *this;
		result += rhs;
		return result;
	}
	
    /** Unary minus (negates all elements). */
	inline const SelfType operator-() const {
		SelfType result = SelfType(0.0);
		result -= *this;
		return result;
	}
	
    /** Return this - rhs. @see VectorElement::operator-=. */
	inline const SelfType operator-(const T rhs) const {
		SelfType result = *this;
		result -= rhs;
		return result;
	}
    
    /** Return this * rhs. @see VectorElement::operator*=. */
	inline const SelfType operator*(const T rhs) const {
		SelfType result = *this;
		result *= rhs;
		return result;
	}
	
    /** Return this / rhs. @see VectorElement::operator/=. */
	inline const SelfType operator/(const T rhs) const {
		SelfType result = *this;
		result /= rhs;
		return result;
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Generic operators
////////////////////////////////////////////////////////////////////////////////////////////////////

/** Return left + right (left may be #VectorElement too). @see VectorElement::operator+=. */
template<typename TLeft, typename T, int num_elem>
inline const VectorElement<T,num_elem> operator+(const TLeft left, const VectorElement<T,num_elem>& right) {
	VectorElement<T,num_elem> result = VectorElement<T,num_elem>(left);
	result += right;
	return result;
}

/** Return left - right (left may be #VectorElement too). @see VectorElement::operator-=. */
template<typename TLeft, typename T, int num_elem>
inline const VectorElement<T,num_elem> operator-(const TLeft left, const VectorElement<T,num_elem>& right) {
	VectorElement<T,num_elem> result = VectorElement<T,num_elem>(left);
	result -= right;
	return result;
}

/** Return left * right (left may be #VectorElement too). @see VectorElement::operator*=. */
template<typename TLeft, typename T, int num_elem>
inline const VectorElement<T,num_elem> operator*(const TLeft left, const VectorElement<T,num_elem>& right) {
	VectorElement<T,num_elem> result = VectorElement<T,num_elem>(left);
	result *= right;
	return result;
}

/** Return left / right (left may be #VectorElement too). @see VectorElement::operator/=. */
template<typename TLeft, typename T, int num_elem>
inline const VectorElement<T,num_elem> operator/(const TLeft left, const VectorElement<T,num_elem>& right) {
	VectorElement<T,num_elem> result = VectorElement<T,num_elem>(left);
	result /= right;
	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Generic Utilities
////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Collection of utilities on VectorElements.
 * These methods only use public methods on VectorElement and can be specialized
 * independently of the full class specialization of VectorElement.
 */
namespace VE_Utilities {
    /**
     * Extract subset given by array of indices into data.
     * Example:
\code
// Given: VectorElement<T, N1> v1, N2 indices i1...iN2 (all < N1)
int a[N2] = {i1, i2, ..., iN2}
VectorElement<T, N2> v2 = VE_Utilities::SubSet<N2>(v1, a);
\endcode
     * -> v2[i] = v1[a[i]]
     */
    template <int N, typename T, int num_elem>
    inline VectorElement<T, N> SubSet(const VectorElement<T, num_elem>& v1, 
                                      const int a[N]) {
        VectorElement<T, N> result;
        for (int i = 0; i < N; ++i) {
            result[i] = v1[a[i]];
        }
        return result;
    }
    
    /**
     * Extract subset given by size and start-index.
     * Following is like setting a[N] = i..i+N-1 and using v2 = SubSet(v1, a):
\code
VectorElement<T, N2> v2 = VE_Utilities::SubSet<N2>(v1, i);
\endcode
     */
    template <int N, typename T, int num_elem>
    inline VectorElement<T, N> SubSet(const VectorElement<T, num_elem>& v1, 
                                      const int i) {
        VectorElement<T, N> result;
        for (int j = 0; j < N; ++j) {
            result[j] = v1[j+i];
        }
        return result;
    }
    
    /**
     * Write given data into subset given by array of indices.
     * Example:
\code
// Given:
// - VectorElement<T, N1> v1
// - N2 indices i1...iN2 (all < N1)
// - VectorElement<T, N2> v2
int a[N2] = {i1, i2, ..., iN2}
VE_Utilities::WriteSubSet(v1, a, v2);
\endcode
     * -> v1[a[i]] = v2[i] (rest of v1 unchanged)
     */
    template <int N, typename T, int num_elem>
    inline void WriteSubSet(VectorElement<T, num_elem>& v1, 
                            const int a[N],
                            const VectorElement<T, N>& v2) {
        for (int i = 0; i < N; ++i) {
            v1[a[i]] = v2[i];
        }
    }
    
    /**
     * Extract subset given by size and start-index.
     * Following is like setting a[N] = i..i+N-1 and using v2 = SubSet(v1, a):
\code
VectorElement<T, N2> v2 = VE_Utilities::SubSet<N2>(v1, i);
\endcode
     */
    template <int N, typename T, int num_elem>
    inline void WriteSubSet(VectorElement<T, num_elem>& v1, 
                            const int i,
                            const VectorElement<T, N>& v2) {
        for (int j = 0; j < N; ++j) {
            v1[j+i] = v2[j];
        }
    }
	
	/**
	 * Power of the vector elements
     * @param v     Vector with elements of type T
     * @param power Exponent of type T2 (either int or same as T)
     * @return Vector with i-th element being std::pow(v[i], power)
	 */
	template<typename T, int num_elem, typename T2>
	VectorElement<T,num_elem> Pow(const VectorElement<T,num_elem>& v, T2 power){
		VectorElement<T,num_elem> result;
		for(int i=0;i<num_elem;i++){
			result[i] = std::pow(v[i],power);
		}
		return result;
	}
	
	/**
	 * Square root of the vector elements
	 * usage: VE_Utilities::Sqrt(V)
	 */
	template<typename T,int num_elem>
	VectorElement<T,num_elem> Sqrt(const VectorElement<T,num_elem>& v){
		VectorElement<T,num_elem> result;
		for(int i=0;i<num_elem;i++){
			result[i] = std::sqrt(v[i]);
		}
		return result;
	}
	
	/**
	 * Logarithm of the vector elements
	 * usage: VE_Utilities::Log(V)
	 */
	template<typename T,int num_elem>
	VectorElement<T,num_elem> Log(const VectorElement<T,num_elem>& v){
		VectorElement<T,num_elem> result;
		for(int i=0;i<num_elem;i++){
			result[i] = std::log(v[i]);
		}
		return result;
	}

	/**
	 * Exp of the vector elements
	 * usage: VE_Utilities::Exp(V)
	 */
	template<typename T,int num_elem>
	VectorElement<T,num_elem> Exp(const VectorElement<T,num_elem>& v){
		VectorElement<T,num_elem> result;
		for(int i=0;i<num_elem;i++){
			result[i] = std::exp(v[i]);
		}
		return result;
	}	
    
    /**
     * Return elementwise minimum of two vectors.
     * Type of elements must be comparable.
     * Usage: <code>VE_Utilities::min(v1, v2)</code>
	 */
	template<typename T,int num_elem>
	VectorElement<T,num_elem> min(const VectorElement<T,num_elem>& v1, const VectorElement<T,num_elem>& v2) {
		VectorElement<T,num_elem> result;
		for(int i=0;i<num_elem;i++){
            result[i] = std::min(v1[i], v2[i]);
		}
        return result;
	}
    
    /**
     * Return elementwise maximum of two vectors.
     * Type of elements must be comparable.
     * Usage: <code>VE_Utilities::max(v1, v2)</code>
	 */
	template<typename T,int num_elem>
	VectorElement<T,num_elem> max(const VectorElement<T,num_elem>& v1, const VectorElement<T,num_elem>& v2) {
		VectorElement<T,num_elem> result;
		for(int i=0;i<num_elem;i++){
            result[i] = std::max(v1[i], v2[i]);
		}
        return result;
	}
    
    /**
     * Return elementwise modulo of two vectors.
     * Type of elements must be comparable.
     * Usage: <code>VE_Utilities::mod(v1, v2)</code>
     */
    template<typename T,int num_elem>
    VectorElement<T,num_elem> mod(const VectorElement<T,num_elem>& v1, const VectorElement<T,num_elem>& v2) {
	VectorElement<T,num_elem> result;
	for(int i=0;i<num_elem;i++){
            result[i] = fmod(v1[i],v2[i]);
	}
        return result;
    }
    
    /**
	 * Generate uniform random numbers in [0,1] (T must be floating point type).
     * Usage: <code>VE_Utilities::FillRandom(v)</code>
	 */
	template<typename T,int num_elem>
	void FillRandom(VectorElement<T,num_elem>& v) {
		for(int i=0;i<num_elem;i++){
			v[i] = (T)rand()/(T)RAND_MAX;
		}
	}
    
    /**
	 * Generate uniform random numbers in [0,1] (T must be floating point type).
     * Usage: <code>VE_Utilities::Random<double,10>()</code>
	 */
	template<typename T,int num_elem>
	VectorElement<T,num_elem> Random() {
		VectorElement<T,num_elem> result;
        VE_Utilities::FillRandom(result);
		return result;
	}
    
    /**
	 * Generate uniform random numbers in [0,1].
     * Useful when something like <code>typedef VectorElement<double,10> VectorType</code>.
     * Usage: <code>VE_Utilities::Random<VectorType>()</code>
	 */
	template<typename VectorType>
	VectorType Random() {
		VectorType result;
        VE_Utilities::FillRandom(result);
		return result;
	}
    
    
    /**
	 * Generate randomly oriented vector of length 1 (T must be floating point type).
     * Usage: <code>VE_Utilities::OrientRand2D<double>()</code>
	 */
	template<typename T>
	VectorElement<T,2> OrientRand2D() {
		const T alpha = T(M_PI)*2*(T)rand()/(T)RAND_MAX;
        return VectorElement<T,2>(cos(alpha), sin(alpha));
    }
    
    /**
	 * Generate randomly oriented vector of length 1 (T must be floating point type).
     * Usage: <code>VE_Utilities::OrientRand3D<double>()</code>
	 */
	template<typename T>
	VectorElement<T,3> OrientRand3D() {
#if 0
		const T beta = T(M_PI)*(T)rand()/(T)RAND_MAX;
		const T nr = sin(beta);
		const T alpha = T(M_PI)*2*(T)rand()/(T)RAND_MAX;
        
		return VectorElement<T,3>(cos(alpha)*nr,
                                  sin(alpha)*nr,
                                  cos(beta)        );
#else
        // source: Florian's fluctuation force for SCE
        // original comment: "dont rmember from where.."
		const T alpha = T(M_PI)*2*(T)rand()/(T)RAND_MAX;
		const T z = T(2)*(T)rand()/(T)RAND_MAX - T(1);
		const T sz = std::sqrt(T(1) - z*z);
		return VectorElement<T,3>(cos(alpha)*sz, sin(alpha)*sz, z);
#endif
    }
}

/**
 * Support for C++-streams.
 * Can use \code std::cout << vectorElement; \endcode
 */
template<typename T, int num_elem>
std::ostream& operator << (std::ostream& out, const VectorElement<T,num_elem>& v) {
    // max element to put on single line
    const int max_N = 6;
    // start
    if (num_elem < max_N) {
        out << "(";
    } else {
        out << std::endl;
    }
    // go through elements (besides last one)
    for (int i = 0; i < num_elem-1; ++i) {
        if (num_elem < max_N) {
            out << v[i] << ", ";
        } else {
            out << i << ": " << v[i] << std::endl;
        }
    }
    // end
    if (num_elem < max_N) {
        out << v[num_elem-1] << ")";
    } else {
        out << num_elem-1 << ": " << v[num_elem-1] << std::endl;
    }
	return out;
}
