/**\file VectorElement.h
 *
 * General and specialized vectors of N elements of type T.
 * 
 * #include "VectorElement/VectorElement.h"
 * 
 * @see #VectorElement, VectorElementGeneric.h
 *
 */

#pragma once

// Generic version
#include "VectorElementGeneric.h"

// Unrolled versions
#include "VectorElementUnroll1.h"
#include "VectorElementUnroll2.h"
#include "VectorElementUnroll3.h"
#include "VectorElementUnroll4.h"

#if __INTEL_COMPILER >= 1100 && __INTEL_COMPILER < 1200
// 1 element vector only for float (not recommended)
//#include "VectorElementSSE1f.h"
// 2 element vector only for double
#include "VectorElementSSE2d.h"
// 4 element vector only for float
#include "VectorElementSSE4f.h"
#endif
