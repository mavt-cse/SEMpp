/**\file ConceptAssert.h
 * Includes everything we need to assert concepts.
 * 
 * User will probably just use #CONCEPT_ASSERT macro.
 * This is rather closely based on boost concepts:
 * http://www.boost.org/doc/libs/1_41_0/libs/concept_check/implementation.htm
 * 
 * Lots of it is probably compiler dependent (tested with gcc 4.0,4.2 & icc 11.0)
 *
 * Notes on runtime cost:
 * - no code is executed for the tests!
 * - not a single byte is added to the executable (or assembler file)
 * - YES code is checked even though no assembly is produced for it...
 *   (also with -O2 or so set) (tested it with gcc 4.0 and icc 11.0)
 * 
 */

#pragma once

#include "ConceptCheck.h"

/**
 * Helper construct needed to force compilation without real call.
 * @see CONCEPT_ASSERT
 */
template <class Concept>
class ConceptAssert {
public:
    static void failed() { ((Concept*)0)->~Concept(); }
};

/**
 * Macro to define function that checks concept.
 * @see CONCEPT_ASSERT
 */
#define CONCEPT_USAGE(Concept) public: ~Concept()

/**
 * Macro to assert concept conformance.
 * Usage:
 * Say you have a templated class Bar whose template type Foo needs to conform
 * to the concept "Fooism" which is defined in a class Fooism:
\code
template< class Foo >
class Fooism {
    CONCEPT_USAGE(Fooism) {
        // write here whatever you need from Foo (using foo if instance needed)
    }
    // ...
private:
    // note that default constructor is not needed for this (see boost for details)
    Foo foo;
}
\endcode
 * Now in the class Bar we write the following to check concept conformance:
\code
template<typename Foo>
class Bar {
    CONCEPT_ASSERT(Fooism<Foo>);
    // ...
};
\endcode
 * 
 * Remarks:
 * - No runtime overhead is produced as none of the code is ever called.
 * - You can use the macro within a (templated) method to check stuff there
 * - You can use the macro within the concept class to check other concepts
 * - You can use (multiple) inheritance to couple concepts. For instance if
 *   the Fooism concept requires Foo to also be a model of Concept1 and
 *   Concept2 and Foo::value must be a model of Concept3 we write:
\code
template< class Foo >
class Fooism: Concept1<Foo>, Concept2<Foo> {
    CONCEPT_ASSERT(Concept3<typename Foo::value>);
    ...
}
\endcode
 *
 */
#define CONCEPT_ASSERT(Concept ...) CONCEPT_CHECK(ConceptAssert< Concept >::failed)

/** Use this to get rid of unused variable warnings in concept usage. */
template <class T> inline void ignore_unused_variable_warning(const T&) {}

/**
 * Helper construct needed for #CONCEPT_ASSERT_POINTER.
 */
template < template <class X2> class Concept, typename X >
class ConceptAssertPointer {
    
    CONCEPT_USAGE(ConceptAssertPointer) {
        // check actual value
        is_pointer(x);
    }
    
private:
    // dummy data (NEVER created)
    X x;
    // do actual test
    template <typename Xval>
    void is_pointer(Xval* val) {
        CONCEPT_ASSERT(Concept<Xval>);
    }
};

/**
 * Workaround to check whether a type X points to a model of a Concept.
 * So if we have a type FooP which is supposed to be of type Foo* with Foo being
 * a model of the concept Fooism, we can write:
\code
CONCEPT_ASSERT_POINTER(Fooism, FooP);
\endcode
 * Note that this is equivalent to
\code
CONCEPT_ASSERT(Fooism<Foo>);
\endcode
 * but it is useful when the type Foo is not known.
 *
 */
#define CONCEPT_ASSERT_POINTER(Concept, X ...) CONCEPT_ASSERT(ConceptAssertPointer < Concept, X >)
