// Implementation of Params

#include "Params.h"         // class implemented
#include <string.h>         // String handling
#include "Utilities.h"      // helper functions
#include <fstream>          // File streams

// CONSTANTS
//

/////////////////////////////// PUBLIC ///////////////////////////////////////
//============================= OPERATIONS ===================================

int Params::Load(const char* filename) {
    std::string line;
    std::ifstream file;
	int line_nr;

	// open file
	file.open(filename);
	if (!file.is_open()) {
        std::cout << "Failed to open " << filename << "!\n";
		return 1;
	}
    
    // go through file line by line
    line_nr = 0;
    // line read in while-condition and false returned at end of file
    // (or other failure...may want to check that in some cases)
    while (getline(file, line)) {
        // advance line counter
        line_nr++;
        
        // parse line otherwise
        std::istringstream iss(line);
        std::string var;
        // return value is false for empty lines
        getline(iss, var, '=');
        var = Utilities::StringToUpper(var);
        // check for empty line or comment
        if (var.empty() || var[0] == '#') continue;
        
        // var is upper case variable name with no white spaces and iss is set
        // such that it can be used with Utilities::NumFromStream
        
        // Note: Input is not checked for validity!!
        if (var == "NX") {
            Utilities::NumFromStream(nx, iss);
        } else if (var == "NY") {
            Utilities::NumFromStream(ny, iss);
        } else if (var == "NZ") {
            Utilities::NumFromStream(nz, iss);
        } else if (var == "DX") {
            Utilities::NumFromStream(dx, iss);
        } else if (var == "DY") {
            Utilities::NumFromStream(dy, iss);
        } else if (var == "DZ") {
            Utilities::NumFromStream(dz, iss);
        } else if (var == "IP") {
            Utilities::NumFromStream(ip, iss, ',');
        } else if (var == "IP2") {
            Utilities::NumFromStream(ip2, iss);
        } else if (var == "IP3") {
            Utilities::NumFromStream(ip3, iss, ',');
        } else if (var == "MODEL") {
            Utilities::EnumFromStream(modelType, iss);
        } else if (var == "NAME") {
            Utilities::StringFromStream(name, iss);
        } else if (var == "DESC") {
            Utilities::StringFromStream(desc, iss);
        } else {	// unknown
            std::cout << "Unknown variable " << var << " on line " << line_nr << " (ignored)!\n";
        }
    }

	// close file
	file.close();
    
	return 0;
}   // Load

int Params::CreateCtrl(const char* filenameIn, const char* filenameOut, const std::vector<double>& params) {
    
    // params = NX, NY, NZ
    assert(params.size() == 3);
    
    std::string line;
    std::ifstream file;
	int line_nr;
    
	// open file for input
	file.open(filenameIn);
	if (!file.is_open()) {
        std::cout << "Failed to open " << filenameIn << "!\n";
		return 1;
	}
    
    // open file for output
    std::ofstream fileOut(filenameOut);
	if (!fileOut.is_open()) {
        std::cout << "Failed to create " << filenameOut << "!\n";
		return 1;
	}
    
    // go through file line by line
    line_nr = 0;
    // line read in while-condition and false returned at end of file
    // (or other failure...may want to check that in some cases)
    while (getline(file, line)) {
        // advance line counter
        line_nr++;
        
        // parse line otherwise
        std::istringstream iss(line);
        std::string var;
        // return value is false for empty lines
        getline(iss, var, '=');
        var = Utilities::StringToUpper(var);
        
        // check for empty line or comment
        if (var.empty() || var[0] == '#') {
            // hard-coded: do "continue" to skip comments for resulting Ctrl file
            //continue;
        } else {
            // var is upper case variable name with no white spaces
            std::ostringstream oss;
            if (var == "NX") {
                oss << "NX = " << params[0];
                line = oss.str();
            } else if (var == "NY") {
                oss << "NY = " << params[1];
                line = oss.str();
            } else if (var == "NZ") {
                oss << "NZ = " << params[2];
                line = oss.str();
            };
        }
        
        // dump line
        fileOut << line << std::endl;
    }
    
	// close files
	file.close();
    fileOut.close();
    
	return 0;
}   // CreateCtrl

//============================= ACCESS     ===================================

int Params::nx;
int Params::ny;
int Params::nz;
float Params::dx;
float Params::dy;
float Params::dz;
VectorElement<double, 10> Params::ip;
VectorElement<int, 4> Params::ip2;
std::vector<double> Params::ip3;
Params::ModelType Params::modelType;
std::string Params::name;
std::string Params::desc;

/////////////////////////////// PROTECTED  ///////////////////////////////////

/////////////////////////////// PRIVATE    ///////////////////////////////////
