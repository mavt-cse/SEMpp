/**
 * Class to help you profile your code.
 *
 * Different parts of your code are identified by a user-specified string-ID.
 * For each string-ID we get a ProfileAgent, where we can store timings.
 * Profiler::printSummary() will then print a nice summary of the results.
 * You can start/stop timers in two ways:
 * -# Use profiler.push_start(string-ID) and Profiler::pop_stop(). This will
 *    start profilers hierarchically s.t. nested parts are not included in the
 *    upper part which is profiled.
 * -# Use profiler.getAgent(string-ID).start() and stop() if you want to include 
 *    the nested parts which are profiled.
 * 
 * Make sure to define USE_TBB before (!) including this if you want to use it
 * in a shared memory environment.
 * 
 * #include "Profiler.h"
 * 
 * \par Example usage:
\code
Profiler p;
 
// v1
p.push_start("Top");
// stuff included in Top
p.push_start("Nested");
// stuff included in Nested but not (!) in Top
p.pop_stop();
// stuff included in Top
p.pop_stop();
 
// v2
p.getAgent("Top").start();
// stuff included in Top
p.getAgent("Nested").start();
// stuff included in Nested and (!) in Top
p.getAgent("Nested").stop();
// stuff included in Top
p.getAgent("Top").stop();
 
// print stats
p.printSummary();
\endcode
 *
 * @see Profiler::push_start(), Profiler::pop_stop(), Profiler::getAgent(), Profiler::printSummary()
 * 
 * @author Diego Rossinelli, slight changes by Gerardo Tauriello
 */

// src: MRAGProfiler.h / cpp
// main changes: remove dependencies to MRAG, put all code in Header

#pragma once

#include <assert.h>
#undef min
#undef max
#include <vector>
#undef min
#undef max
#include <map>
#include <string>
#include <stack>

#ifdef USE_TBB
#include "tbb/tick_count.h"
#else
#include <time.h>
#endif

class Profiler
{
#pragma mark HELPERS
    /** Just for debugging (enables some printouts). */
	static const bool bVerboseProfiling = false;
	
	/**
	 * Profiles one part of the code (parts collected in MRAG::Profiler).
	 * The user should use ProfileAgent through MRAG::Profiler::getAgent().
	 * Stores (only accessible through MRAG::Profiler):
	 * - Accumulated timings whenever profiler is started/stopped (in s)
	 * - Number of measurements (i.e. once per start/stop)
	 * - optional: "earned Money" for all the stuff done (added at each stop)
	 * @see ProfileAgent::start(), ProfileAgent::stop()
	 */
	class ProfileAgent
	{
#ifdef USE_TBB
		typedef tbb::tick_count ClockTime;
#else
		typedef clock_t ClockTime;
#endif
		
		enum ProfileAgentState{ ProfileAgentState_Created, ProfileAgentState_Started, ProfileAgentState_Stopped};
		
		ClockTime m_tStart, m_tEnd;
		ProfileAgentState m_state;
		double m_dAccumulatedTime;
		int m_nMeasurements;
		int m_nMoney;
		
		static void _getTime(ClockTime& time) {
#ifdef USE_TBB
            time = tbb::tick_count::now();
#else
            time = clock();
#endif
        }
		static float _getElapsedTime(const ClockTime& tS, const ClockTime& tE) {
#ifdef USE_TBB
            return (tE - tS).seconds();
#else
            return (tE - tS)/(double)CLOCKS_PER_SEC;
#endif
        }
		
		void _reset()
		{
			m_tStart = ClockTime();
			m_tEnd = ClockTime();
			m_dAccumulatedTime = 0;
			m_nMeasurements = 0;
			m_nMoney = 0;
			m_state = ProfileAgentState_Created;
		}
		
	public:
		
		ProfileAgent():m_tStart(), m_tEnd(), m_state(ProfileAgentState_Created),
		m_dAccumulatedTime(0), m_nMeasurements(0), m_nMoney(0) {}
		
		/** Get timing (in seconds) of this profiler. */
		double getAccumulatedTime() const
		{
			return m_dAccumulatedTime;
		}
		
		/**
		 * Start a time measurement.
		 */
		void start()
		{
			assert(m_state == ProfileAgentState_Created || m_state == ProfileAgentState_Stopped);
			
			if (bVerboseProfiling) {printf("start\n");}
			
			_getTime(m_tStart);
			
			m_state = ProfileAgentState_Started;
		}
		
		/**
		 * Stop a time measurement.
		 * Time elapsed since ProfileAgent::start() was called is added to the timings.
		 * @param nMoney    Defines how much "money" we earned for whatever we did since calling ProfileAgent::start().
		 */
		void stop(int nMoney=0)
		{
			assert(m_state == ProfileAgentState_Started);
			
			if (bVerboseProfiling) {printf("stop\n");}
			
			_getTime(m_tEnd);
			m_dAccumulatedTime += _getElapsedTime(m_tStart, m_tEnd);
			m_nMeasurements++;
			m_nMoney += nMoney;
			m_state = ProfileAgentState_Stopped;
		}
		
		friend class Profiler;
	};
	
	/**
	 * Helper to collect statistics on profiled stuff.
	 */
	struct ProfileSummaryItem
	{
        std::string sName;
		double dTime;
		double dAverageTime;
		int nMoney;
		int nSamples;
		
		ProfileSummaryItem(std::string sName_, double dTime_, int nMoney_, int nSamples_):
		sName(sName_), dTime(dTime_), nMoney(nMoney_),nSamples(nSamples_), dAverageTime(dTime_/nSamples_){}
	};
    
#pragma mark LIFECYCLE
public:
    Profiler(): m_mapAgents(){}
    
    ~Profiler()
    {
        clear();
    }
    
#pragma mark OPERATIONS
public:
    /** Start profiler and stop all running ones. */
    void push_start(std::string sAgentName)
    {
        if (m_mapStoppedAgents.size() > 0)
            getAgent(m_mapStoppedAgents.top()).stop();
        
        m_mapStoppedAgents.push(sAgentName);
        getAgent(sAgentName).start();
    }
    
    /** Stop current profiler and restart last one that was running. */
    void pop_stop()
    {
        std::string sCurrentAgentName = m_mapStoppedAgents.top();
        getAgent(sCurrentAgentName).stop();
        m_mapStoppedAgents.pop();
        
        if (m_mapStoppedAgents.size() == 0) return;
        
        getAgent(m_mapStoppedAgents.top()).start();
    }
    
    /** Kill all profilers. */
    void clear()
    {
        for(std::map<std::string, ProfileAgent*>::iterator it = m_mapAgents.begin(); it != m_mapAgents.end(); it++)
        {
            delete it->second;
            
            it->second = NULL;
        }
        
        m_mapAgents.clear();
    }
    
    /** Print summary of timings. Optional output in file. */
    double printSummary(FILE *outFile=NULL) const
    {
        std::vector<ProfileSummaryItem> v = createSummary();
        
        double dTotalTime = 0;
        double dTotalTime2 = 0;
        for(std::vector<ProfileSummaryItem>::const_iterator it = v.begin(); it!= v.end(); it++)
            dTotalTime += it->dTime;
        
        for(std::vector<ProfileSummaryItem>::const_iterator it = v.begin(); it!= v.end(); it++)
            dTotalTime2 += it->dTime - it->nSamples*1.30e-6;
        
        for(std::vector<ProfileSummaryItem>::const_iterator it = v.begin(); it!= v.end(); it++)
        {
            const ProfileSummaryItem& item = *it;
            const double avgTime = item.dAverageTime;
            
            printf("[%30s]: \t%02.0f-%02.0f%%\t%03.3e (%03.3e) s\t%03.3f (%03.3f) s\t(%d samples)\n",
                   item.sName.data(), 100*item.dTime/dTotalTime, 100*(item.dTime- item.nSamples*1.3e-6)/dTotalTime2, avgTime,avgTime-1.30e-6,  item.dTime, item.dTime- item.nSamples*1.30e-6, item.nSamples);
            if (outFile) fprintf(outFile,"[%30s]: \t%02.2f%%\t%03.3f s\t(%d samples)\n",
                                 
                                 item.sName.data(), 100*item.dTime/dTotalTime, avgTime, item.nSamples);
        }
        
        printf("[Total time]: \t%f\n", dTotalTime);
        if (outFile) fprintf(outFile,"[Total time]: \t%f\n", dTotalTime);
        if (outFile) fflush(outFile);
        if (outFile) fclose(outFile);
        
        return dTotalTime;
    }
    
    /** Helper for printSummary (might also be used to extract data and process it). */
    std::vector<ProfileSummaryItem> createSummary() const
    {
        std::vector<ProfileSummaryItem> result;
        result.reserve(m_mapAgents.size());
        
        for(std::map<std::string, ProfileAgent*>::const_iterator it = m_mapAgents.begin(); it != m_mapAgents.end(); it++)
        {
            const ProfileAgent& agent = *it->second;
            
            result.push_back(ProfileSummaryItem(it->first, agent.m_dAccumulatedTime, agent.m_nMoney, agent.m_nMeasurements));
        }
        
        return result;
    }
    
    /** Reset all profilers to initial state. */
    void reset()
    {
        //printf("reset\n");
        for(std::map<std::string, ProfileAgent*>::const_iterator it = m_mapAgents.begin(); it != m_mapAgents.end(); it++)
            it->second->_reset();
    }
    
    /** Get a specific profiler. Start/stop it with methods start() and stop(). */
    ProfileAgent& getAgent(std::string sName)
    {
        if (bVerboseProfiling) {printf("%s ", sName.data());}
        
        std::map<std::string, ProfileAgent*>::const_iterator it = m_mapAgents.find(sName);
        
        const bool bFound = it != m_mapAgents.end();
        
        if (bFound) return *it->second;
        
        ProfileAgent * agent = new ProfileAgent();
        
        m_mapAgents[sName] = agent;
        
        return *agent;
    }
    
    friend class ProfileAgent;
    
#pragma mark DATA
protected:
    std::map<std::string, ProfileAgent*> m_mapAgents;
    std::stack<std::string> m_mapStoppedAgents;
};
