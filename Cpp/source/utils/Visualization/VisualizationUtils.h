// Created by Gerardo Tauriello on 11/25/09.

/** \file VisualizationUtils.h
 * Collection of helpers for visualization of data.
 * #include "Visualization/VisualizationUtils.h"
 */

#pragma once

// openGL
#include <GLUT/glut.h>
// system
#include <cmath>
#include <map>
// lib
#include "../VectorElement/VectorElement.h"

/** Storage for RGBA color. */
struct RGBA
{
#pragma mark ACCESS
    float r,g,b,a;
 
#pragma mark LIFECYCLE
    RGBA(): r(0), g(0), b(0), a(0) {}
    RGBA(float x, float y, float z, float w = 1.0f): r(x), g(y), b(z), a(w) {}
    
    RGBA(const double other[4]): r(other[0]), g(other[1]), b(other[2]), a(other[3]) {}
    RGBA(const float other[4]): r(other[0]), g(other[1]), b(other[2]), a(other[3]) {}
    
    // default destructor, copy constructor and assignment operator used
    
#pragma mark OPERATIONS
    /**
     * Alpha blend given color c over this one.
     * "c over this" used to blend alpha channel.
     */
    inline RGBA& blend(const RGBA& c) {
        r = c.a * c.r + (1 - c.a) * r;
        g = c.a * c.g + (1 - c.a) * g;
        b = c.a * c.b + (1 - c.a) * b;
        a = c.a + (1 - c.a) * a;
        return *this;
    }
    
    /**
     * Perform a gamma correction (all brighter for gamma < 1, darker for > 1).
     */
    inline RGBA& gamma_correct(const float gamma) {
        r = pow(r, gamma);
        g = pow(g, gamma);
        b = pow(b, gamma);
        return *this;
    }
    
    /**
     * Do luminance modulation of this color.
     * This is like having a luminance texture and modulating it with a color.
     * @param lum   Luminance in [0,1]
     */
    inline RGBA luminance(const float lum) const {
        return RGBA(r*lum, g*lum, b*lum, a);
    }
    
#pragma mark CONSTANTS
    inline static RGBA red(const float a=1.0f) { return RGBA(1,0,0,a); }
    inline static RGBA green(const float a=1.0f) { return RGBA(0,1,0,a); }
    inline static RGBA blue(const float a=1.0f) { return RGBA(0,0,1,a); }
    inline static RGBA magenta(const float a=1.0f) { return RGBA(1,0,1,a); }
    inline static RGBA yellow(const float a=1.0f) { return RGBA(1,1,0,a); }
    inline static RGBA cyan(const float a=1.0f) { return RGBA(0,1,1,a); }
    inline static RGBA white(const float a=1.0f) { return RGBA(1,1,1,a); }
    inline static RGBA black(const float a=1.0f) { return RGBA(0,0,0,a); }
    
    /**
     * Get piecewise linear function with result between 0 (value = a) and 1 (value = b).
     * \f[
     pwl(value,a,b) = \begin{cases}
     0 \quad & value <= a, \\
     1 \quad & value > b, \\
     \frac{\left(value - a \right)}{b-a} \quad & \text{else}.
     \end{cases}
     * \f]
     * Note that for a == b, this should behave like a threshold.
     */
    inline static float pwl(const float value, const float a, const float b) {
        if      (value <= a) return 0.0;
        else if (value >  b) return 1.0;
        else                 return (value-a)/(b-a);
    }
    
    /**
     * Convert HSVA to RGBA (h between 0 and 360, s,v,a between 0 and 1).
     * (cudos to Wikipedia)
     */
    inline static RGBA convertFromHSVA(const float h, const float s, const float v, const float a) {
        int hi = int(h)/60;
        const float f = h/60.0 - hi;
        // just to be on the safe side
        hi = hi % 6;
        const float p = v*(1-s);
        const float q = v*(1-f*s);
        const float t = v*(1-(1-f)*s);
        switch (hi) {
            case 0:
                return RGBA(v,t,p,a);
            case 1:
                return RGBA(q,v,p,a);
            case 2:
                return RGBA(p,v,t,a);
            case 3:
                return RGBA(p,q,v,a);
            case 4:
                return RGBA(t,p,v,a);
            case 5:
                return RGBA(v,p,q,a);
            default:
                // not gonna happen
                return RGBA(-1,-1,-1,-1);
                break;
        }
    }
	/**
	 * some other color map.. not really anything..
	 */
	inline static RGBA colorMapRed(const float value, const float a=1.0f, const float s=1.0f, const float v=1.0f) {
		
		return RGBA(value,0,0,a);
	}

	inline static RGBA colorMapBlue(const float value, const float a=1.0f, const float s=1.0f, const float v=1.0f) {
		
		return RGBA(value,value,value,a);
	}

	inline static RGBA colorMapWhite(const float value, const float a=1.0f, const float s=1.0f, const float v=1.0f) {
		
		return RGBA(value,value,value,value);
	}
	
	/**
	 * some other color map.. not really anything..
	 */
	inline static RGBA colorMapSome(const float value, const float a=1.0f, const float s=1.0f, const float v=1.0f) {
		
		return RGBA(1-value,value,1-value,a);
	}
    
    /**
     * Colormap for indexed stuff (just repeats a distinguishable pattern).
     */
    inline static RGBA colorMapRepeat(unsigned int value, const float a = 1.0f) {
        // make close colors go far apart (0.3)
        float val = value*0.1f;
        return colorMapWrappedHSV(val-int(val));
    }
    
    /**
     * Colormap for indexed stuff (random colors).
     */
    inline static RGBA colorMapRandom(int value, const float a = 1.0f) {
        // static map making sure we use same colors
        static std::map<int,RGBA> color_map;
        // new one?
        RGBA color;
        if (color_map.count(value) == 0) {
            color.r = (double)rand()/(double)RAND_MAX;
            color.g = (double)rand()/(double)RAND_MAX;
            color.b = (double)rand()/(double)RAND_MAX;
            color.a = a;
            color_map[value] = color;
        } else {
            color = color_map[value];
            color.a = a;
        }
        return color;
    }
    
    /**
     * Convert value (between 0 and 1) to colormap based on wrapped HSV.
     * Colors will go from red to red again for s=v=1.
     * Saturation s and value v for HSV can be set (def=1).
     * Alpha channel is set according to passed parameter a (def=1).
     */
    inline static RGBA colorMapWrappedHSV(const float value, const float a = 1.0f, const float s = 1.0f, const float v = 1.0f) {
        // V and S fixed at 1
        return convertFromHSVA(value * 360, s, v, a);
    }
    
    /**
     * Convert value (between 0 and 1) to colormap based on HSV.
     * Colors will go from red (0) to blue (1) for s=v=1.
     * Saturation s and value v for HSV can be set (def=1).
     * Alpha channel is set according to passed parameter a (def=1).
     */
    inline static RGBA colorMapHSV(const float value, const float a = 1.0f, const float s = 1.0f, const float v = 1.0f) {
        // V and S fixed at 1
        return convertFromHSVA(value * 240, s, v, a);
    }
    
#pragma mark IO
    /**
     * Support for C++-streams.
     * Can use \code std::cout << color; \endcode
     */
    friend std::ostream& operator<< (std::ostream& out, const RGBA& color) {
        out << "(" << color.r << ", " << color.g << ", " << color.b << ", " << color.a << ")";
        return out;
    }
    
    /**
     * Support for C++-streams.
     * Can use \code std::cin >> color; \endcode
     * Assumes input is formatted just like the output done with <<.
     */
    friend std::istream& operator>> (std::istream& in, RGBA& color) {
        char c;
        // c used to skip ( , and )
        in >> c >> color.r;
        assert(c == '(');
        in >> c >> color.g;
        assert(c == ',');
        in >> c >> color.b;
        assert(c == ',');
        in >> c >> color.a;
        assert(c == ',');
        in >> c;
        assert(c == ')');
        return in;
    }
};

/**
 * Collection of useful methods for visualization.
 */
class VisualizationUtils {
public:
    /**
     * Draw a circle.
     * @param pos       2D position of center of circle.
     * @param radius    Radius of circle.
     * @param color     RGBA for circle color (def.: opaque white).
     * @param nPoints   Number of vertices to approximate circle (def.: 6).
     * @param fill      Fill circle with same color if set to true (def.: true).
     * @tparam TReal    Automatically determined type of pos-elements.
     */
    template <typename TReal>
    static void drawCircle(const VectorElement<TReal,2> pos, 
                           const float radius, 
                           const RGBA& color = RGBA(1,1,1,1),
                           const int nPoints = 6,
                           const bool fill = true) {
        GLenum mode;
        if (fill) {
            mode = GL_POLYGON;
        } else {
            mode = GL_LINE_LOOP;
        }
        glColor4f(color.r,color.g,color.b,color.a);
        glBegin(mode);
        {
            for( unsigned int i = 0; i < nPoints; i++ )
            {
                const float angle = (i*2.0*M_PI)/nPoints;
                const float xx = pos[0] + radius * cos( angle );
                const float yy = pos[1] + radius * sin( angle );
                glVertex2f( xx, yy );
            }
        }
        glEnd();
    }
    
    /**
     * Draw a circle as projection of a sphere.
     * @param pos       3D position of center of sphere.
     * @param radius    Radius of circle.
     * @param color     RGBA for circle color (def.: opaque white).
     * @param nPoints   Number of vertices to approximate circle (def.: 6).
     * @param fill      Fill circle with same color if set to true (def.: true).
	 * @param proj      Projection direction. That coordinate is neglected (def.: 2).
     * @tparam TReal    Automatically determined type of pos-elements.
     */
    template <typename TReal>
    static void drawCircle(const VectorElement<TReal,3> pos,
                           const float radius,
                           const RGBA& color = RGBA(1,1,1,1), 
                           const int nPoints = 6, 
                           const bool fill = true, 
                           const int proj = 2) {
		// projection
		const int coord[3][2]={{1,2},{0,2},{0,1}};
        const VectorElement<float,2> pos2D(pos[coord[proj][0]], pos[coord[proj][1]]);
        drawCircle(pos2D, radius, color, nPoints, fill);
    }
    
    /**
     * Draw a sphere (3D).
     * Based on Diego's Brazil Nut.
     * @param pos       Position of center of sphere.
     * @param radius    Radius of sphere.
     * @param color     RGBA for sphere color (def.: opaque white).
     * @param fill      Make it a solid sphere if set to true (def.: true).
     * @tparam TReal    Automatically determined type of pos-elements.
     */
    template <typename TReal>
    static void drawSphere(const VectorElement<TReal,3> pos,
                           const float radius,
                           const RGBA& color = RGBA(1,1,1,1)) {
        
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_LIGHTING);
		
		GLfloat lightColor[] = {color.r*1.2, color.g*1.2, color.b*1.2, 1};
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
		
        glColor4f(color.r, color.g, color.b, color.a);
		glPushMatrix();
		glTranslated(pos[0], pos[1], pos[2]);
        glutSolidSphere(radius, std::max(int(20*radius/0.02),6), std::max(int(20*radius/0.02),6));
		glPopMatrix();
		
		glPopAttrib();
    }
    
    /**
     * Draw a sphere (2D).
     * Based on Diego's Brazil Nut.
     * @param pos       Position of center of sphere.
     * @param radius    Radius of sphere.
     * @param color     RGBA for sphere color (def.: opaque white).
     * @param fill      Make it a solid sphere if set to true (def.: true).
     * @tparam TReal    Automatically determined type of pos-elements.
     */
    template <typename TReal>
    static void drawSphere(const VectorElement<TReal,2> pos,
                           const float radius,
                           const RGBA& color = RGBA(1,1,1,1)) {
        
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_LIGHTING);
		
		GLfloat lightColor[] = {color.r*1.2, color.g*1.2, color.b*1.2, 1};
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
		
        glColor4f(color.r, color.g, color.b, color.a);
		glPushMatrix();
		glTranslated(pos[0], pos[1], 0);
        glutSolidSphere(radius, std::max(int(20*radius/0.02),6), std::max(int(20*radius/0.02),6));
		glPopMatrix();
		
		glPopAttrib();
    }
    
    /**
     * Draw a point (2D).
     * @param pos       Position of center of particle.
     * @param radius    Radius of particle.
     * @param pixelSize Pixel scaling in x-direction. May be determined as 
     *                  wd/wp with wd = width of shown domain (glOrtho) and
     *                  wp = width in pixels of window (glutInitWindowSize).
     * @param color     RGBA for color (def.: opaque white).
     */
    template <typename TReal>
    static void drawPoint(const VectorElement<TReal,2> pos,  
                          const float radius,
                          const float pixelSize, 
                          const RGBA& color = RGBA(1,1,1,1)) {
        // backup point size
        float ps;
        glGetFloatv(GL_POINT_SIZE, &ps);
        // compute new ps
        float newPS = std::max(radius*2/pixelSize, 1.f);
        glPointSize(newPS);
        glColor4f(color.r, color.g, color.b, color.a);
        glBegin(GL_POINTS);
        glVertex2f(pos[0], pos[1]);
        glEnd();
        // reset
        glPointSize(ps);
        
        // TODO: not working properly in smooth mode???
        // -> sometimes points not drawn, text drawing fails...
        
    }
    
    /**
     * Draw a particle (2D).
     * Tries to choose whatever looks cool.
     * @param pos       Position of center of particle.
     * @param radius    Radius of particle.
     * @param pixelSize Pixel scaling in x-direction. May be determined as 
     *                  wd/wp with wd = width of shown domain (glOrtho) and
     *                  wp = width in pixels of window (glutInitWindowSize).
     * @param color     RGBA for color (def.: opaque white).
     */
    template <typename TReal>
    static void drawParticle(const VectorElement<TReal,2> pos,
                             const float radius,
                             const float pixelSize, 
                             const RGBA& color = RGBA(1,1,1,1)) {
        // LOTS OF HEURISTIC HERE!
        // compare radius to line width
        float lw;
        glGetFloatv(GL_LINE_WIDTH, &lw);
        const float magicNr = radius*2/pixelSize;
        // determine corners
        const int corners = (magicNr > 5) ? 15 : 6;
        // reset line width
        glLineWidth(std::max(magicNr/6, 0.1f));
        if (magicNr < lw*7) {
            // draw it as a point with cute black border
            drawCircle(pos, radius, color, corners, true);
            drawCircle(pos, radius, RGBA(0.2,0.2,0.2,1.0), corners, false);
            //drawPoint(pos, radius, pixelSize, RGBA(0.1,0.1,0.1,1.0));
            //drawPoint(pos, radius*0.8, pixelSize, color);
        } else {
            //drawPoint(pos, radius, pixelSize, RGBA(0.1,0.1,0.1,1.0));
            drawSphere(pos, radius, color);
            drawCircle(pos, radius, RGBA(0.2,0.2,0.2,1.0), corners, false);
        }
        // RESET
        glLineWidth(lw);
    }
    
    /**
     * Draw text.
     * See VisualizationUtils::renderBitmapStringM for description of y-align.
     *
     * @param pos       2D position (bottom-left) for string.
     * @param string    String to draw.
     * @param color     RGBA for text color (def.: opaque white).
     * @param font      Font to be used (def.: GLUT_BITMAP_HELVETICA_18)
     *                  See: http://www.opengl.org/documentation/specs/glut/spec3/node76.html#SECTION000111000000000000000
     * @tparam TReal    Automatically determined type of pos-elements.
     */
    template <typename TReal>
    static void renderBitmapString(const VectorElement<TReal,2> pos,
                                   const char * string, 
                                   const RGBA& color = RGBA(1,1,1,1), 
                                   void * font = GLUT_BITMAP_HELVETICA_18) {
        // set pos. and color
        glColor4f(color.r,color.g,color.b,color.a);
        glRasterPos2f(pos[0],pos[1]);
        // loop through chars
        for (const char *c = string; *c != '\0'; c++) {
            glutBitmapCharacter(font, *c);
        }
    }
    
    /**
     * Draw text with middle alignment.
     * Bottom is not (!) minimal y-position but baseline (i.e. 'y' goes further 
     * down). Max. height of font (in pixels) is given by X in GLUT_BITMAP_.._X.
     * Spacing two texts by that amount of pixels will not lead to any overlap
     * (they may touch though).
     * Example:
     * All letters in GLUT_BITMAP_HELVETICA_18 will span y-positions from
     * pos[0]-4*pixelSize to pos[0]+13*pixelSize (i.e. height of 13--4+1=18).
     *
     * @param pos       2D position (bottom-middle) for string.
     * @param string    String to draw.
     * @param pixelSize Pixel scaling in x-direction. May be determined as 
     *                  wd/wp with wd = width of shown domain (glOrtho) and
     *                  wp = width in pixels of window (glutInitWindowSize).
     * @param color     RGBA for text color (def.: opaque white).
     * @param font      Font to be used (def.: GLUT_BITMAP_HELVETICA_18)
     *                  See: http://www.opengl.org/documentation/specs/glut/spec3/node76.html#SECTION000111000000000000000
     * @tparam TReal    Automatically determined type of pos-elements.
     */
    template <typename TReal>
    static void renderBitmapStringM(VectorElement<TReal,2> pos,
                                    const char * string, 
                                    const float pixelSize, 
                                    const RGBA& color = RGBA(1,1,1,1), 
                                    void * font = GLUT_BITMAP_HELVETICA_18) {
        // adapt pos.
        int width = 0;
        // loop through chars
        for (const char *c = string; *c != '\0'; c++) {
            width += glutBitmapWidth(font, *c);
        }
        // offset to align
        pos[0] -= pixelSize * width/2;
        // render it
        renderBitmapString(pos, string, color, font);
    }
    
private:
    
	/**
	 * Prevent use of constructor.
	 */
	VisualizationUtils();
    
};