/**
 * Some Utilities like converters from string to numerical and memory cleaners.
 *
 * #include "Utilities.h"
 *
 * There's no use to create instances of this class, since all features are 
 * static.
 *
 * @author Gerardo Tauriello
 */

#ifndef Utilities_h
#define Utilities_h

// SYSTEM INCLUDES
//

// LOCAL INCLUDES
//
#include <string>
#include <sstream>
#include <cctype>
#include "VectorElement/VectorElement.h"
#include <vector>
#include <cstdarg>

// FORWARD REFERENCES
//

class Utilities {
public:
    
    /**
	 * Convert stream to string.
     * Reads stream to end of line (or stream) and removes leading and trailing
     * blanks.
	 *
	 * @param t     Target variable.
     * @param is    Source stream.
	 * @return 0 if everything is OK, 1 otherwise (also if end of stream is reached).
	 */
    static int StringFromStream(std::string& t, std::istream& is)
    {
        // note: use this neat code to advance stream to first non-blank
        // while (isspace(iss.peek())) iss.get();
        
        // here we just use Trim-method
        std::string line;
        if (getline(is, line)) {
            t = Trim(line);
            return 0;
        } else {
            return 1;
        }
    }
    
    /**
     * Convert stream to enum.
     * Stream is advanced and we return 1 if end of stream is reached.
     * Basically it is reading an integer and casting to an enum.
	 *
	 * @param t     Target variable.
     * @param is    Source stream.
     * @tparam T    Enum-Type of target variable t (not necessary to pass explicitly!).
	 * @return 0 if everything is OK, 1 otherwise (also if end of stream is reached).
	 */
    template <typename T>
    static int EnumFromStream(T& t, 
                             std::istream& is, 
                             std::ios_base& (*f)(std::ios_base&) = std::dec)
    {
        int i;
        if (NumFromStream(i, is) == 0) {
            t = (T)i;
            return 0;
        } else {
            return 1;
        }
    }
    
    /**
	 * Convert stream to numerical.
     * Stream is advanced and we return 1 if end of stream is reached.
	 *
	 * @param t     Target variable.
     * @param is    Source stream.
     * @param f     (optional) ios_base to convert from a non-decimal numerical system.
     * @tparam T    Type of target variable t (not necessary to pass explicitly!).
	 * @return 0 if everything is OK, 1 otherwise (also if end of stream is reached).
	 */
	template <typename T>
    static int NumFromStream(T& t, 
                             std::istream& is, 
                             std::ios_base& (*f)(std::ios_base&) = std::dec)
    {
        if (is >> f >> t)  return 0;
        else               return 1;
    }
    
    /**
     * Specialized version of Utilities::NumFromStream for T = VectorElement<T,num_elem>.
     * Assumes that data is delimited by a space.
     * Please note that 1 will be returned if stream does not contain
     * enough numbers to fill vector (i.e. end of stream reached first).
     */
    template <typename T, int num_elem>
    static int NumFromStream(VectorElement<T,num_elem>& t, 
                             std::istream& is, 
                             std::ios_base& (*f)(std::ios_base&) = std::dec)
    {
        int i = 0;
        T t1;
        while (i < num_elem && NumFromStream(t1, is, f) == 0) {
            t[i] = t1;
            i++;
        }
        if (i < num_elem) return 1;
        else              return 0;
    }
    
    /**
	 * Convert stream to vector (VectorElement) of numericals.
     * Works like Utilities::NumFromStream but here a special delimiter may be given.
     * Please note that 1 will be returned if stream does not contain
     * enough numbers to fill vector (i.e. end of stream reached first).
	 */
    template <typename T, int num_elem>
    static int NumFromStream(VectorElement<T,num_elem>& t, 
                             std::istream& is, 
                             char delim,
                             std::ios_base& (*f)(std::ios_base&) = std::dec)
    {
        int i = 0;
        std::string s;
        while (i < num_elem && getline(is, s, delim)) {
            NumFromString(t[i], s);
            i++;
        }
        if (i < num_elem) return 1;
        else              return 0;
    }
    
    /**
     * Specialized version of Utilities::NumFromStream for T = std::vector<T>.
     * Assumes that data is delimited by a space.
     * Method will never return 1 (vector resized to fit given data).
     */
    template <typename T>
    static int NumFromStream(std::vector<T>& t, 
                             std::istream& is, 
                             std::ios_base& (*f)(std::ios_base&) = std::dec)
    {
        T t1;
        // clear vector first
        t.clear();
        while (NumFromStream(t1, is, f) == 0) {
            t.push_back(t1);
        }
        return 0;
    }
    
    /**
	 * Convert stream to vector (std::vector) of numericals.
     * Works like Utilities::NumFromStream but here a special delimiter may be given.
     * Method will never return 1 (vector resized to fit given data).
	 */
    template <typename T>
    static int NumFromStream(std::vector<T>& t, 
                             std::istream& is, 
                             char delim,
                             std::ios_base& (*f)(std::ios_base&) = std::dec)
    {
        std::string s;
        T t1;
        // clear vector first
        t.clear();
        while (getline(is, s, delim)) {
            NumFromString(t1, s);
            t.push_back(t1);
        }
        return 0;
    }
    
	/**
	 * Convert string to numerical.
	 *
	 * @param t     Target variable.
     * @param s     Source string.
     * @param f     (optional)
     * @tparam T    Type of target variable t (not necessary to pass explicitly!).
	 * @return 0 if everything is OK, 1 otherwise
	 */
	template <typename T>
    static int NumFromString(T& t, 
                             const std::string& s, 
                             std::ios_base& (*f)(std::ios_base&) = std::dec)
    {
        std::istringstream iss(s);
        return NumFromStream(t, iss, f);
    }
    
    /**
     * Convert string to all uppercase and removes all (!) white spaces.
     * @param s     Input string to convert.
     * @return Copy of s with all uppercase letters.
     */
    static std::string StringToUpper(const std::string& s)
    {
        std::string result;
        const int length = s.length();
        for(unsigned int i = 0; i != length ; ++i)
        {
            if (isspace(s[i]) == 0) result += std::toupper(s[i]);
        }
        return result;
    }
    
    /**
     * Trim leading and trailing spaces from a string.
     * Source: http://codereflect.com/2007/01/31/how-to-trim-leading-or-trailing-spaces-of-string-in-c/
     */
    static std::string Trim(const std::string& str) {
        size_t start = str.find_first_not_of(" \t\n\r");
        if (start == std::string::npos) return "";
        return str.substr(start, str.find_last_not_of(" \t\n\r") - start + 1);
    }
    
    /**
     * Return comma separated list of variable number of arguments.
     * NOTE: Set T to double when passing list of floats.
     * Uses standard stream output for values. Values must all be of type T.
     * Examples:
\code
Utilities::GetCSV(4, 1.0, 1.1, 1.2, (double)100)
Utilities::GetCSV<double>(3, 1.0f, 1.1f, 1.2f)
\endcode
     * @param num   Number of arguments passed.
     * @return Comma separated list of passed values.
     */
    template <typename T>
    static std::string GetCSV(unsigned int num, T t1, ...) {
        std::ostringstream oss;
        // init vararg-list
        va_list argptr;
        va_start(argptr, t1);
        // go through all arguments
        T cur = t1;
        for( ; num > 1; num-- ) {
            oss << cur << ", ";
            cur = va_arg(argptr, T);
        }
        oss << cur;
        
        // clean up
        va_end(argptr);
        
        return oss.str();
    }
    
    /**
     * Deletes all pointers contained in passed container.
     * Make sure you actually own those instances before calling this!
     * Meant to be used in destructors were container itself is killed
     * afterwards. Elements are NOT nullified.
	 *
	 * @param v     Vector whose content are pointers and shall be deleted.
     * @tparam T    Type compliant of C++ Container-concept type and containing
     *              Pointer-type variables. E.g.: std::vector<ELEM*>
     */
    template <typename T>
    static void deleteVectorElements(const T& v) {
        for (typename T::const_iterator it = v.begin(); it != v.end(); ++it) {   
            delete (*it);
        }
    }
    
private:
    
	/**
	 * Prevent use of constructor.
	 */
	Utilities();
    
};

#endif	// _Utilities_h_
