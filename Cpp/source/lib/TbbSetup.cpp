////////////////////////////////////////////////////////////////
// Compile this one to make tbb work!
////////////////////////////////////////////////////////////////

#include "TbbSetup.h"

// whole file ignored when no tbb
#if USE_TBB == 1
// static data 
TbbSetup TbbSetup::sStartup;
int TbbSetup::sTicket;
int TbbSetup::sNThreads;
#endif  // USE_TBB
