/**\file Environment.h
 * Environment settings for library.
 *
 * #include "Environment.h" (used by all headers in library)
 *
 * @author Gerardo Tauriello
 */

#pragma once

#include <assert.h>
#include <algorithm>

// Required headers
#include "VectorElement/VectorElement.h"

/** Dimensions to be used for simulations (2 or 3). */
#ifndef DIM
#define DIM 2
#endif

/**
 * Should double be used as the default type for data (\ref setup_use_double "more")?
 */
#ifndef USE_DOUBLE
#define USE_DOUBLE 1
#endif

/**
 * Use tbb for for multi-threading (\ref setup_use_tbb "more")?
 */
#ifndef USE_TBB
#define USE_TBB 0
#endif

/** Default floating point type. Depends on #USE_DOUBLE. */
#if USE_DOUBLE == 0
typedef float Real;
#else	// USE_DOUBLE == 1
typedef double Real;
#endif

/** Type for spatial vectors. */
typedef VectorElement<Real,DIM> VecType;
typedef VectorElement<int, DIM> VecTypeI;

/** The number pi. */
#ifdef M_PI
#undef M_PI
#endif
const Real M_PI = 3.14159265358979323846264338327950288;

 /** Machine Precision */
#ifdef M_EPS
#undef M_EPS
#endif
const Real M_EPS = pow(2,-24);