/** 
 * Collection of pairwise potential functions.
 *
 * #include "Functions.h"
 *
 * There's no use to create instances of this class, since all features are 
 * static.
 *
 * SS08: Sebastian Sandersius and Timothy Newman, 2008, DOI: 10.1088/1478-3975/5/1/015002
 * 
 * @author Florian Milde and Gerardo Tauriello
 */

#pragma once
#include <cmath>
#include "Environment.h"

class PotentialFunctions {
	
public:

	typedef const Real* ParamTypeSS08; 	//rho, u0, 1/re^2, cutoff | re=equilibrium distance
	typedef const Real* ParamTypeSSPlus; 	//rho, u0, 1/re^2, cutoff, alpha, densRad, scalingsth
    
    /**
     * Returns grad(V) for SS08 potential.
     * \f[ \begin{aligned}
             V(r,re^2,\rho,u_0) &= u_0 \left( \mathrm{e}^{2 \rho \left( 1-\frac{r^2}{re^2} \right) } - 2 \mathrm{e}^{\rho \left( 1-\frac{r^2}{re^2} \right) } \right), \\
             \nabla_{own} V &= dV/dr \cdot \vec{dx}/r \\
                 &= \frac{4 \rho u_0}{re^2} \left( \mathrm{e}^{ 2 \rho \left( 1-\frac{r^2}{re^2} \right) } - \mathrm{e}^{ \rho \left( 1-\frac{r^2}{re^2} \right) } \right) \cdot \vec{dx}, \\
             r &= |\vec{dx}| = |\vec{x}_{own} - \vec{x}_{other}|, \\
             \vec{F} &= -\nabla_{own} V.
           \end{aligned} 
       \f]
     * See Mathematica-file SEM_Potentials (Gerardo).
     *
     * @param dx    Distance vector (x_own - x_other).
     * @param r     Precomputed length of dx.
     * @param p     Parameters for potential (rho, u0, 1/re^2, cutoff (=3*re))
     */
    static VecType gradPotentialSS08(const VecType dx, const Real r, ParamTypeSS08 p) {
		if (r < p[3]) {
			Real f_tmp_ = r*r;
			f_tmp_ = p[0]*(1-f_tmp_*p[2]);
            const Real mag = 4*p[0]*p[1]*p[2]*(std::exp(f_tmp_)-std::exp(2*f_tmp_));
			return mag * dx;
        } else {
            return VecType(0);
		}
    }
    
    /**
     * Wrapper for gradPotentialSS08(VecType, Real, ParamTypeSS08).
     */
    static VecType gradPotentialSS08(const VecType dx, ParamTypeSS08 p) {
        return gradPotentialSS08(dx, dx.abs(), p);
    }
    
    /**
     * Returns force for modified SS08 potential (added alpha).
     * 
     * \f[ \begin{aligned}
             V(r,rem^2,\rho,u_0,\alpha) &= u_0 \left( \mathrm{e}^{2 \rho \left( 1-\frac{r^2}{rem^2} \right) } - \alpha \mathrm{e}^{\rho \left( 1-\frac{r^2}{rem^2} \right) } \right), \\
             \nabla_{own} V &= dV/dr \cdot \vec{dx}/r \\
                 &= \frac{2 \rho u_0}{rem^2} \left( 2 \mathrm{e}^{ 2 \rho \left( 1-\frac{r^2}{rem^2} \right) } - \alpha \mathrm{e}^{ \rho \left( 1-\frac{r^2}{rem^2} \right) } \right) \cdot \vec{dx}, \\
             r &= |\vec{dx}| = |\vec{x}_{own} - \vec{x}_{other}|, \\
             rem^2 &= re^2 \cdot \frac{\rho}{\rho + \log(2/\alpha)}, \\
             \vec{F} &= -\nabla_{own} V.
		   \end{aligned}
       \f]
     * See Mathematica-file SEM_Potentials (Gerardo).
     * With alpha = 2 we get same force as with PotentialFunctions::gradPotentialSS08.
     * Higher alpha can increase the adhesive part of the potential.
     * 
     * Note that this is the force vector and not the energy-gradient. That is
     * it corresponds to -grad(V) where V is the potential.
     *
     * @param dx    Distance vector (x_own - x_other).
     * @param rSq   Precomputed |dx|^2.
     * @param p     Parameters for potential (rho, u0, 1/rem^2, cutoff, alpha).
     *				rem^2 = re^2*rho/(rho + log(2/alpha)).
     */
    static VecType modifiedSS08Force(const VecType dx, const Real rSq, ParamTypeSSPlus p) {
        // note: used exp(a) + exp(2*a) = exp(a) * (1 + exp(a)) to simplify...
		if (rSq < p[3]) {
            const Real tmp = std::exp(p[0]*(1-rSq*p[2]));
            const Real mag = 2*p[0]*p[1]*p[2]*tmp*(2*tmp - p[4]);
			return mag * dx;
        } else {
            return VecType(0);
		}
    }
    
    /**
     * Wrapper for modifiedSS08Force(VecType, Real, ParamTypeSSPlus).
     */
    static VecType modifiedSS08Force(const VecType dx, ParamTypeSSPlus p) {
        return modifiedSS08Force(dx, dx.absSq(), p);
    }

	// intercellular potential - SS:2008
	static void addPotentialSS08(VecType pos1, VecType pos2, VecType &rhs, ParamTypeSS08 p){
		VecType vt_tmp = pos1-pos2;
		Real abs=vt_tmp.abs();
		if(abs<p[3]){
			Real f_tmp_ = abs*abs;
			f_tmp_ = p[0]*(1.-f_tmp_*p[2]);
			rhs -= 4.*vt_tmp*p[0]*p[1]*p[2]*(exp(f_tmp_)-exp(2.*f_tmp_));
		}
        // Note: could be replaced by rhs -= gradPotentialSS08(pos1-pos2, p);
	};
	
	// intercellular potential - SS:2008
	// - cuts the potential in case it gets to big
	static void addPotentialSS08Cut(VecType pos1, VecType pos2, VecType &rhs, ParamTypeSS08 p){
		VecType vt_tmp = pos1-pos2;
		Real abs=vt_tmp.abs();
		if(abs<p[3]){
			Real f_tmp_ = vt_tmp.abs()*vt_tmp.abs();
			f_tmp_ = p[0]*(1.-f_tmp_*p[2]);
			Real pot = 4*p[0]*p[1]*p[2]*(exp(f_tmp_)-exp(2*f_tmp_));
            pot = std::max(pot, -500*p[1]);
			rhs -= vt_tmp * pot;
		}
	};
	
	// intercellular potential - SS:2008
	// - cuts the potential in case it gets to big
	// - also store absolute potential on Particle
	static void addPotentialSS08PotCut(VecType &_pos1, VecType _pos2, VecType &_rhs, Real&_pot,ParamTypeSS08 _p){
		VecType vt_tmp = _pos1-_pos2;
		Real abs=vt_tmp.abs();
		if(abs<_p[3]){
			Real tmp = vt_tmp.abs()*vt_tmp.abs();
			tmp = _p[0]*(1.-tmp*_p[2]);
			Real pot = 4*_p[0]*_p[1]*_p[2]*(exp(tmp)-exp(2*tmp));
            pot = std::max(pot, -500*_p[1]);
			_pot += fabs(pot*abs);
			_rhs -= vt_tmp*pot;
		}
	};

	// intercellular potential - SS:2008
	// - cuts the potential in case it gets to big
	// - also store density like parameter
	static void addPotentialSS08DensCut(VecType &_pos1, VecType _pos2, VecType &_rhs, Real &_dens,ParamTypeSS08 _p){
		VecType vt_tmp = _pos1-_pos2;
		Real abs=vt_tmp.abs();
		if(abs<_p[3]){
			Real tmp = vt_tmp.abs()*vt_tmp.abs();
			tmp = _p[0]*(1.-tmp*_p[2]);
			Real pot = 4*_p[0]*_p[1]*_p[2]*(exp(tmp)-exp(2*tmp));
            pot = std::max(pot, -500*_p[1]);
			_dens += 1/(abs*abs)*_p[2];
			_rhs -= vt_tmp*pot;
		}
	};
	
	// intercellular potential - FM:2010
	// - store density like parameter
	// - store absolute potential on particle
	static void addPotentialFM10DensPot(VecType &_pos1, VecType _pos2, VecType &_rhs, Real&_dens, Real &_pot,ParamTypeSSPlus _p){
		// remember to scale distance
		// to enforce equilibrium distance
		VecType vt_tmp = (_pos1-_pos2)*_p[6];
		Real abs=vt_tmp.abs();
		if(abs<_p[3]){
			Real tmp = vt_tmp.abs()*vt_tmp.abs();
			tmp = _p[0]*(1-tmp*_p[2]);
			Real pot = 2.*_p[0]*_p[1]*_p[2]*(_p[4]*exp(tmp)-2.*exp(2*tmp)); // with alpha parameter in _p[4]

			if(abs<=_p[5]){
				_dens += 1;
			}
			_pot += fabs(pot*abs);
			_rhs -= vt_tmp*pot;
		}
	};

	// intercellular potential - FM:2010
	// - store density like parameter
	// - store absolute potential on particle
	// - elements can be partly polarized
	static void addPotentialFM10DensPotPoly(VecType &_pos1, VecType _pos2, VecType &_rhs, Real&_dens, Real &_pot,ParamTypeSSPlus _p, Real _polyFrac){
		// remember to scale distance
		// to enforce equilibrium distance
		if(_polyFrac >0){
			VecType vt_tmp = (_pos1-_pos2)*_p[6];
			Real abs=vt_tmp.abs();
			if(abs<_p[3]){
				const Real rho=_p[0];//*_polyFrac;  // factor rho
				const Real ire2 = _p[2]/(_polyFrac*_polyFrac);
				const Real alpha = _p[4]+(1.-_polyFrac)*8;
				Real tmp = vt_tmp.abs()*vt_tmp.abs();
				tmp = rho*(1-tmp*ire2);
				Real pot = 2.*rho*_p[1]*_p[2]*(alpha*exp(tmp)-2.*exp(2*tmp)); // with alpha parameter in _p[4]
				
				if(abs<=_p[5]){
					_dens += 1;
				}
				_pot -= pot*abs;
				_rhs -= vt_tmp*pot;
			}
		}
	};
	
	// Wall Potential
	// Same as 	// intercellular potential - SS:2008
	// Wall has same potential as if there was a SCE at the location on the wall closest to the cell
	// also store density like parameter and absolute potential on particle
	static void addPotentialSS08DensWallPot(VecType & _dir, VecType& _dist,VecType &_rhs, Real&_dens, Real &_pot,ParamTypeSS08 _p,Real dist){
		if(_dist.min()<_p[3]){
			VecType tmp = _p[0]*3*(1-_p[2]*_dist*_dist);
			//	Real pot = 4.*_p[0]*_p[1]*_p[2]*(exp(tmp)-exp(2*tmp));          // standard
			VecType pot = 2.*_p[0]*3*_p[1]*_p[2]*(_p[4]*VE_Utilities::Exp(tmp));//-2.*VE_Utilities::Exp(2*tmp)); // with alpha parameter in _p[4]
			// pot = std::max(pot, -500*_p[1]);
			if(_dist.min()*sqrt(_p[2])<=dist){
				_dens += 1;
			}
			_pot += (pot*_dist).sum();
			_rhs -= _dir*_pot;
		}
	};
    
    
    /**
     * Returns force for repulsive potential.
     * Basically a non-normalized M4 cubic spline (see Functions::gradM4spline).
     * u0 used to scale force (can put any normaization in there).
     * Result is a*dx with a < 0 for d/h < 2 and a = 0 otherwise.
     * Note that this is the force vector and not the energy-gradient. That is
     * it corresponds to -grad(U) where U is the repulsive potential.
     *
     * @param dx    Distance vector (x_own - x_other).
     * @param h     Smoothing length
     * @param u0	Scaling factor for force
     * @param r     Precomputed length of dx
     */
    static VecType repulsiveForce(const VecType dx, const Real h, const Real u0, const Real r) {
        const Real R = r / h;
        // compute it
        if (R < 1) {
            return (u0 * (12 - 9*R) / (h*h)) * dx;
        } else if (R < 2) {
            const Real tmp = 2-R;
            return (u0 * 3 * tmp*tmp / (h*r)) * dx;
        } else {
            return VecType(0);
        }
    }
    
    /**
     * Wrapper for gradPotentialSS08(VecType, Real, ParamTypeSS08).
     */
    static VecType repulsiveForce(const VecType dx, const Real h, const Real u0) {
        return repulsiveForce(dx, h, u0, dx.abs());
    }
    
};