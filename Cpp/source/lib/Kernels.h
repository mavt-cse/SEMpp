/**\file Kernels.h
 * Collection of kernels (models of #CKernel and #CKernelDirichlet).
 * 
 * #include "Kernels.h"
 *
 * @author Gerardo Tauriello
 * @date 7/20/10
 * 
 * @see CKernel, CKernelDirichlet, CInterpolators
 */

#pragma once

// project includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"
#include "Matrix.h"

// system includes

// forward references

#pragma mark CONCEPTS
/**\addtogroup concepts
 * @{
 */
/**
 * Concept for a kernel.
 *
 * This purely static class computes weights W(x) and W'(x) for a given t and
 * are meant as helpers for a model of #CInterpolator.
 * 
 * The kernels are tensorial (W(x,y) = W(x)*W(y)) and given in index space.
 * Interpolation then boils down to computing u(x) = sum(W(x - i) * ui).
 * 
 * With a finite interpolation stencil we can rewrite this as:
 * \f[ \sum_{i=\lfloor x \rfloor + KSS}^{\lfloor x \rfloor + KSE}{W(x-i) u_i} \f]
 * or more conveniently as:
 * \f[ \sum_{k=KSS}^{KSE}{W(t-k) u_{\lfloor x \rfloor + k}} \ \text{with } t = x - \lfloor x \rfloor \f]
 * For a symmetric stencil of width KS we have KSS = -KS/2+1 and KSE = KS/2.
 * 
 * Derivatives are taken on the interpolation kernels. For a tensorial kernel
 * we get grad(W(x,y)) = 1/h * (W'(x)*W(y), W'(y)*W(x)).
 *
 * More details are in Maple-file B-Splines.mw
 *
 * Requirements:
 * - Static constants:
 *   - <code>static const int sSupport</code> - Value of KS = KSE-KSS+1
 *   - <code>static const int sStencilStart</code> - Value of KSS
 *   - <code>static const int sStencilEnd</code> - Value of KSE
 * - Static methods:
 *   - <code>static void weights(Real* w, Real t)</code>\n
 *     Computes w[i] = W(t-k) = W(t-i-KSS) for i = 0..KS-1.
 *   - <code>static void weightsGrad(Real* dw, Real* w, Real t)</code>\n
 *     Computes w[i] as in weights(..) and dw[i] = W'(t-i-KSS).
 * .
 * Example (2D) using type X given position x and matrix m of type M:
\code
const int KSS = Kernel::sStencilStart;
const int KS = Kernel::sSupport;
// x
const size_t i = x[0];
Real wx[KS], dwx[KS];
X::weightsGrad(dwx, wx, x[0] - i);
// y
const size_t j = x[1];
Real wy[KS], dwy[KS];
X::weightsGrad(dwy, wy, x[1] - j);
// loop over grid points
M::ElementType result = 0;
VectorElement<M::ElementType, 2> grad = 0;
for (int jj = 0; jj < KS; ++jj) {
    for (int ii = 0; ii < KS; ++ii) {
        result += wx[ii]*wy[jj]*m(i+KSS+ii, j+KSS+jj);
		grad[0] += dwx[ii]*wy[jj]*m(i+KSS+ii, j+KSS+jj);
        grad[1] += dwy[jj]*wx[ii]*m(i+KSS+ii, j+KSS+jj);
    }
}
// note: we need to do grad /= h too...
\endcode
 *
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CKernel<X>) \endcode
 * 
 * @see CKernelDirichlet, CInterpolator
 */
template <class X>
class CKernel {
    
    CONCEPT_USAGE(CKernel) {
		// get kernel size (need KS points from i+KSS to i+KSE)
		const int KS = X::sSupport;
		const int KSS = X::sStencilStart;
		const int KSE = X::sStencilEnd;
		
		// get weights
		Real w[KS], dw[KS];
		X::weights(w, 0.2);
		X::weightsGrad(dw, w, 0.2);
		
		ignore_unused_variable_warning(KSS);
		ignore_unused_variable_warning(KSE);
    }
	
};

/**
 * Concept for a kernel which supports boundary conditions without ghosts.
 *
 * Here we get special weights for boundary points and move stencil such that we
 * do not use points outside of the grid. An interpolator can also use these
 * Kernels with ghost values and use the default weights.
 *
 * Requirements:
 * - Must be a model of #CKernel
 * - Static methods:
 *   - <code>static void weightsDirichlet(Real* w, const Real t, int& fx, const size_t N)</code>\n
 *     Computes w[i] as in CKernel::weights(w,t) but handles BC and will shift
 *     fx = floor(x) such that stencil (fx+KSS..fx+KSE) remains in [0,N-1].
 *   - <code>static void weightsDirichletGrad(Real* dw, Real* w, const Real t, int& fx, const size_t N)</code>\n
 *     Same as weightsDirichlet but also filling dw.
 * .
 * Example (2D) using type X given position x and matrix m of type M:
\code
const int KSS = Kernel::sStencilStart;
const int KS = Kernel::sSupport;
// x
int i = x[0];
Real wx[KS], dwx[KS];
X::weightsDirichletGrad(dwx, wx, x[0] - i, i, m.getSizeX());
// y
int j = x[1];
Real wy[KS], dwy[KS];
X::weightsDirichletGrad(dwy, wy, x[1] - j, j, m.getSizeY());
// loop over grid points
M::ElementType result = 0;
VectorElement<M::ElementType, 2> grad = 0;
for (int jj = 0; jj < KS; ++jj) {
    for (int ii = 0; ii < KS; ++ii) {
        result += wx[ii]*wy[jj]*m(i+KSS+ii, j+KSS+jj);
		grad[0] += dwx[ii]*wy[jj]*m(i+KSS+ii, j+KSS+jj);
        grad[1] += dwy[jj]*wx[ii]*m(i+KSS+ii, j+KSS+jj);
    }
}
// note: we need to do grad /= h too...
\endcode
 *
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CKernel<X>) \endcode
 * 
 * @see KernelBilinear, CInterpolator
 */
template <class X>
class CKernelDirichlet: CKernel<X> {
    
    CONCEPT_USAGE(CKernelDirichlet) {
		// get weights
		Real w[X::sSupport];
		Real dw[X::sSupport];
		int i = 10;
		X::weightsDirichlet(w, 0.2, i, 100);
		X::weightsDirichletGrad(dw, w, 0.2, i, 100);
    }
	
};
/** @} */ // end of group concepts

#pragma mark KERNELS
/** 
 * Bilinear interpolation (model of #CKernelDirichlet).
 * \f[ W(x) = \begin{cases}
 				1 - |x|, &|x| \le 1\\
 				0, &\text{else}
      \end{cases} \f]
 * Weights for u_0, u_1:
 * w = [1-t, t]
 * 
 * Gradient is discontinuous and W'(x) = -W'(x):
 * \f[ W'(x) = \begin{cases}
 				-1, &-1 < x < 0\\
 				 1, & 0 < x < 1\\
 				 0, &\text{else}
      \end{cases} \f]
 * Weights for u_0, u_1:
 * dw = [-1, 1]
 */
class KernelBilinear {
public:
	/** \name Concept-Implementation.
     * Implementation of #CKernel.
     */
    //@{
	static const int sSupport = 2;
	static const int sStencilStart = 0;
	static const int sStencilEnd = 1;
	static void weights(Real* w, const Real t) {
		w[0] = 1-t;
		w[1] = t;
	}
	static void weightsGrad(Real* dw, Real* w, const Real t) {
		dw[0] = -1;
		dw[1] = 1;
		weights(w, t);
	}
	//@}
	
	/** \name Concept-Implementation.
     * Implementation of #CKernelDirichlet.
     */
    //@{
	static void weightsDirichlet(Real* w, const Real t, int& i, const size_t N) {
		// only special case is i == N-1 and t == 0
		if (i == N-1) {
			i = N-2;
			w[0] = 0;
			w[1] = 1;
		} else {
			weights(w, t);
		}
	}
	static void weightsDirichletGrad(Real* dw, Real* w, const Real t, int& i, const size_t N) {
		// BC handled there
		weightsDirichlet(w, t, i, N);
		// weights for derivatives always the same here...
		dw[0] = -1; dw[1] = 1;
	}
	//@}
	
private:
	/** Purely static class, constructor hidden. */
	KernelBilinear() { }
};

/**
 * Cubic hermite spline interpolation (model of #CKernelDirichlet).
 * Note that this is exactly the same as M'4 used in PPM etc.
 * \f[ W(x) = \frac{1}{2} \cdot
      \begin{cases}
       2 + 3|x|^3 - 5|x|^2 & |x| \leq 1 \\ 
       4 - |x|^3 + 5|x|^2 - 8|x| & |x| \leq 2 \\
       0 & 2<|x|
      \end{cases} \f]
 * Weights for u_-1, u_0, u_1, u_2:
 * w = [-(1/2)*t^3+t^2-(1/2)*t, 1+(3/2)*t^3-(5/2)*t^2, -(3/2)*t^3+2*t^2+(1/2)*t, (1/2)*t^3-(1/2)*t^2]
 *
 * Gradient is smooth and W'(x) = -W'(x):
 * \f[ W'(x) = \frac{1}{2} \cdot
		\begin{cases}
		 0 & x \leq -2 \\
		 3x^2 + 10x + 8 & x \leq -1 \\
		 -9x^2 - 10x & x \leq 0 \\
		 9x^2 - 10x & x \leq 1 \\
		 -3x^2 + 10x - 8 & x \leq 2 \\
		 0 & 2 < x \\
		\end{cases} \f]
 * Weights for u_-1, u_0, u_1, u_2:
 * dw = [-(3/2)*t^2+2*t-1/2, (9/2)*t^2-5*t, -(9/2)*t^2+4*t+1/2, -t+(3/2)*t^2]
 *
 * Dirichlet Boundary Condition: 3rd order extrapolation from Keys-paper
 * - u_-1 = u_2 - 3*u_1 + 3*u_0
 * - u_N = 3*u_N-1 - 3*u_N-2 + u_N-3
 * 
 * Sources:
 * - Wiki on bicubic interpolation or cubic hermite spline (Catmull–Rom spline)
 * - R. Keys, (1981). "Cubic convolution interpolation for digital image processing". IEEE Transactions on Signal Processing, Acoustics, Speech, and Signal Processing 29: 1153.
 *   http://dx.doi.org/10.1109/TASSP.1981.1163711
 */
class KernelHermite {
public:
	/** \name Concept-Implementation.
     * Implementation of #CKernel.
     */
    //@{
	static const int sSupport = 4;
	static const int sStencilStart = -1;
	static const int sStencilEnd = 2;
	static void weights(Real* w, const Real t) {
		const Real t2 = t*t;
		_weights(w, t2*t, t2, t);
	}
	static void weightsGrad(Real* dw, Real* w, const Real t) {
		const Real t2 = t*t;
		_weights(w, t2*t, t2, t);
		_weightsGrad(dw, t2, t);
	}
	//@}
	
	/** \name Concept-Implementation.
     * Implementation of #CKernelDirichlet.
     */
    //@{
	static void weightsDirichlet(Real* w, const Real t, int& i, const size_t N) {
		if (i == N-1) {
			// i == N-1 and t == 0
			i = N-3;
			_weightsN1(w);
		} else if (i == N-2) {
			i = N-3;
			_weightsN2(w, t*t, t);
		} else if (i == 0) {
			i = 1;
			_weights0(w, t*t, t);
		} else {
			const Real t2 = t*t;
			_weights(w, t2*t, t2, t);
		}
	}
	static void weightsDirichletGrad(Real* dw, Real* w, const Real t, int& i, const size_t N) {
		if (i == N-1) {
			// i == N-1 and t == 0
			i = N-3;
			_weightsN1(w);
			_weightsGradN1(dw);
		} else if (i == N-2) {
			i = N-3;
			_weightsN2(w, t*t, t);
			_weightsGradN2(dw, t);
		} else if (i == 0) {
			i = 1;
			_weights0(w, t*t, t);
			_weightsGrad0(dw, t);
		} else {
			const Real t2 = t*t;
			_weights(w, t2*t, t2, t);
			_weightsGrad(dw, t2, t);
		}
	}
	//@}
	
private:
	/** Get weights with precomputed tN = t^N. */
	static void _weights(Real* w, const Real t3, const Real t2, const Real t) {
		w[0] = -Real(0.5)*t3 + t2 - Real(0.5)*t;
		w[1] = 1 + Real(1.5)*t3 - Real(2.5)*t2;
		w[2] = -Real(1.5)*t3 + 2*t2 + Real(0.5)*t;
		w[3] = Real(0.5)*t3 - Real(0.5)*t2;
	}
	/** Special weights at i = N-1, t = 0. */
	static void _weightsN1(Real* w) {
		w[0] = 0; w[1] = 0; w[2] = 0; w[3] = 1;
		// move i to N-3
	}
	/** Special weights at i = N-2. */
	static void _weightsN2(Real* w, const Real t2, const Real t) {
		w[0] = 0;
		w[1] = Real(0.5)*(t2 - t);
		w[2] = 1 - t2;
		w[3] = Real(0.5)*(t2 + t);
		// move i to N-3
	}
	/** Special weights at i = 0. */
	static void _weights0(Real* w, const Real t2, const Real t) {
		w[0] = 1 + Real(0.5)*t2 - Real(1.5)*t;
		w[1] = -t2 + 2*t;
		w[2] = Real(0.5)*(t2 - t);
		w[3] = 0;
		// move i to 1
	}
	
	/** Get weights for gradient with precomputed tN = t^N. */
	static void _weightsGrad(Real* w, const Real t2, const Real t) {
		w[0] = -Real(1.5)*t2 + 2*t - Real(0.5);
		w[1] = Real(4.5)*t2 - 5*t;
		w[2] = -Real(4.5)*t2 + 4*t + Real(0.5);
		w[3] = -t + Real(1.5)*t2;
	}
	/** Special weights at i = N-1, t = 0. */
	static void _weightsGradN1(Real* w) {
		w[0] = 0; w[1] = 0.5; w[2] = -2; w[3] = 1.5;
		// move i to N-3
	}
	/** Special weights at i = N-2. */
	static void _weightsGradN2(Real* w, const Real t) {
		w[0] = 0;
		w[1] = t - Real(0.5);
		w[2] = -2*t;
		w[3] = t + Real(0.5);
		// move i to N-3
	}
	/** Special weights at i = 0. */
	static void _weightsGrad0(Real* w, const Real t) {
		w[0] = t - Real(1.5);
		w[1] = -2*t + 2;
		w[2] = t - Real(0.5);
		w[3] = 0;
		// move i to 1
	}
	/** Purely static class, constructor hidden. */
	KernelHermite() {}
};

/**
 * Quadratic spline for MPM (model of #CKernelDirichlet).
 * Source: Steffen-2008
 * \f[ W(x) = \begin{cases}
		-|x|^2 + 3/4 & |x| < 1/2\\
		1/2 |x|^2 - 3/2 |x| + 9/8 & |x| < 3/2\\
		0 & 3/2 \leq |x| \\
	   \end{cases} \f]
 * Weights for u_-1, u_0, u_1, u_2: (2 versions depending on t!)
 * t < 0.5: w = [(1/2)*t^2-(1/2)*t+1/8, -t^2+3/4, (1/8)*(1+2*t)^2, 0]
 * t > 0.5: w = [0, (1/2)*t^2-(3/2)*t+9/8, -t^2+2*t-1/4, (1/8)*(-1+2*t)^2]
 *
 * Gradient is smooth and W'(-x) = -W'(x):
 * \f[ W'(x) = \begin{cases}
		0 & x \leq -3/2\\
		3/2 + x & x \leq -1/2\\
		-2x & x \leq 1/2\\
		x - 3/2 & x \leq 3/2\\
		0 & 3/2 < x
	   \end{cases} \f]
 * Weights for u_-1, u_0, u_1, u_2:
 * t < 0.5: dw = [t-1/2, -2*t, 1/2+t, 0]
 * t > 0.5: dw = [0, t-3/2, -2*t+2, t-1/2]
 * 
 * Dirichlet and 0-Neumann Boundary Condition with special kernels at
 * boundary points. In the end we need special weights at i = 0,1,N-2,N-1 for 
 * t < 0.5 and at i = 0,N-3,N-2 else (N-1 only happens for t=0).
 */
class KernelMPMSpline2 {
public:
	/** \name Concept-Implementation.
     * Implementation of #CKernel.
     */
    //@{
	// technically support is 3:
	// - for t < 0.5: u_-1, u_0, u_1
	// - for t >= 0.5: u_0, u_1, u_2
	// -> here we just set it to 4 and set one of the weights to 0
	static const int sSupport = 4;
	static const int sStencilStart = -1;
	static const int sStencilEnd = 2;
	static void weights(Real* w, const Real t) {
		if (t < 0.5)	_weights_0(w, t*t, t);
		else			_weights_1(w, t*t, t);
	}
	static void weightsGrad(Real* dw, Real* w, const Real t) {
		if (t < 0.5) {
			_weights_0(w, t*t, t);
			_weightsGrad_0(dw, t);
		} else {
			_weights_1(w, t*t, t);
			_weightsGrad_1(dw, t);
		}
	}
	//@}
	
	/** \name Concept-Implementation.
     * Implementation of #CKernelDirichlet.
     */
    //@{
	static void weightsDirichlet(Real* w, const Real t, int& i, const size_t N) {
		if (t < 0.5) {
			if (i == N-1) {
				// i == N-1 and t == 0
				i = N-3;
				_weightsN1_0(w);
			} else if (i == N-2) {
				i = N-3;
				_weightsN2_0(w, t*t, t);
			} else if (i == 1) {
				// keep i
				_weights1_0(w, t*t, t);
			} else if (i == 0) {
				i = 1;
				_weights0_0(w, t*t, t);
			} else {
				_weights_0(w, t*t, t);
			}
		} else {
			if (i == N-2) {
				i = N-3;
				_weightsN2_1(w, t*t, t);
			} else if (i == N-3) {
				// keep i
				_weightsN3_1(w, t*t, t);
			} else if (i == 0) {
				i = 1;
				_weights0_1(w, t*t, t);
			} else {
				_weights_1(w, t*t, t);
			}
		}
	}
	static void weightsDirichletGrad(Real* dw, Real* w, const Real t, int& i, const size_t N) {
		if (t < 0.5) {
			if (i == N-1) {
				// i == N-1 and t == 0
				i = N-3;
				_weightsN1_0(w);
				_weightsGradN1_0(dw);
			} else if (i == N-2) {
				i = N-3;
				_weightsN2_0(w, t*t, t);
				_weightsGradN2_0(dw, t);
			} else if (i == 1) {
				// keep i
				_weights1_0(w, t*t, t);
				_weightsGrad1_0(dw, t);
			} else if (i == 0) {
				i = 1;
				_weights0_0(w, t*t, t);
				_weightsGrad0_0(dw, t);
			} else {
				_weights_0(w, t*t, t);
				_weightsGrad_0(dw, t);
			}
		} else {
			if (i == N-2) {
				i = N-3;
				_weightsN2_1(w, t*t, t);
				_weightsGradN2_1(dw, t);
			} else if (i == N-3) {
				// keep i
				_weightsN3_1(w, t*t, t);
				_weightsGradN3_1(dw, t);
			} else if (i == 0) {
				i = 1;
				_weights0_1(w, t*t, t);
				_weightsGrad0_1(dw, t);
			} else {
				_weights_1(w, t*t, t);
				_weightsGrad_1(dw, t);
			}
		}
	}
	//@}
	
private:
	// Note: _weightsX_Y: Y=0: t < 0.5, Y=1: else, X (if there) = special weights at i = X (NI = N-I)
	// - Precomputed tN = t^N where needed
	
#pragma mark Weights
	
	/** Get weights for t < 0.5. */
	static void _weights_0(Real* w, const Real t2, const Real t) {
		w[0] = Real(0.5)*t2-Real(0.5)*t+Real(0.125);
		w[1] = -t2 + Real(0.75);
		w[2] = Real(0.125) + Real(0.5)*t + Real(0.5)*t2;
		w[3] = 0;
	}
	/** Get weights for t >= 0.5. */
	static void _weights_1(Real* w, const Real t2, const Real t) {
		w[0] = 0;
		w[1] = Real(0.5)*t2-Real(1.5)*t+Real(1.125);
		w[2] = -t2+2*t-Real(0.25);
		w[3] = Real(0.125) - Real(0.5)*t + Real(0.5)*t2;
	}
	
	/** Special weights at i = N-1, t = 0. */
	static void _weightsN1_0(Real* w) {
		w[0] = 0; w[1] = 0; w[2] = 0; w[3] = 1;
		// move i to N-3
	}
	
	/** Special weights at i = N-2, t < 0.5. */
	static void _weightsN2_0(Real* w, const Real t2, const Real t) {
		const Real r16 = Real(1)/Real(6);
		w[0] = 0;
		w[1] = Real(0.5)*t2-Real(0.5)*t+Real(0.125);
		w[2] = (-7*t2-t+Real(4.25))*r16;
		w[3] = (1+4*t+4*t2)*r16;
		// move i to N-3
	}
	/** Special weights at i = N-2, t >= 0.5. */
	static void _weightsN2_1(Real* w, const Real t2, const Real t) {
		const Real r43 = Real(4)/Real(3);
		w[0] = 0;
		w[1] = 0;
		w[2] = (t2-2*t+1)*r43;
		w[3] = (-t2+2*t-Real(0.25))*r43;
		// move i to N-3
	}
	
	/** Special weights at i = N-3, t >= 0.5. */
	static void _weightsN3_1(Real* w, const Real t2, const Real t) {
		const Real r16 = Real(1)/Real(6);
		w[0] = 0;
		w[1] = Real(0.5)*t2-Real(1.5)*t+Real(1.125);
		w[2] = (-7*t2+13*t-Real(1.75))*r16;
		w[3] = (4*t2-4*t+1)*r16;
		// keep i
	}
	/** Special weights at i = 1, t < 0.5. */
	static void _weights1_0(Real* w, const Real t2, const Real t) {
		const Real r16 = Real(1)/Real(6);
		w[0] = (4*t2 - 4*t + 1)*r16;
		w[1] = (-7*t2 + t + Real(4.25))*r16;
		w[2] = Real(0.125) + Real(0.5)*t + Real(0.5)*t2;
		w[3] = 0;
		// keep i
	}
	
	/** Special weights at i = 0, t < 0.5. */
	static void _weights0_0(Real* w, const Real t2, const Real t) {
		const Real r43 = Real(4)/Real(3);
		w[0] = -r43*t2+1;
		w[1] = r43*t2;
		w[2] = 0;
		w[3] = 0;
		// move i to 1
	}
	/** Special weights at i = 0, t >= 0.5. */
	static void _weights0_1(Real* w, const Real t2, const Real t) {
		const Real r23 = Real(2)/Real(3);
		const Real r76 = Real(1.75)*r23;
		w[0] = r23*t2-2*t+Real(1.5);
		w[1] = -r76*t2+Real(2.5)*t-Real(0.625);
		w[2] = Real(0.125) - Real(0.5)*t + Real(0.5)*t2;
		w[3] = 0;
		// move i to 1
	}
	
#pragma mark WeightsGrad
	
	/** Get weights for gradient for t < 0.5. */
	static void _weightsGrad_0(Real* w, const Real t) {
		w[0] = t-Real(0.5);
		w[1] = -2*t;
		w[2] = Real(0.5)+t;
		w[3] = 0;
	}
	/** Get weights for gradient for t >= 0.5. */
	static void _weightsGrad_1(Real* w, const Real t) {
		w[0] = 0;
		w[1] = t-Real(1.5);
		w[2] = -2*t+2;
		w[3] = t-Real(0.5);
	}
	
	/** Special weights at i = N-1, t = 0. */
	static void _weightsGradN1_0(Real* w) {
		w[0] = w[1] = w[2] = w[3] = 0;
		// move i to N-3
	}
	
	/** Special weights at i = N-2, t < 0.5. */
	static void _weightsGradN2_0(Real* w, const Real t) {
		const Real r43 = Real(4)/Real(3);
		w[0] = 0;
		w[1] = t-Real(0.5);
		w[2] = (-Real(1.75)*t - Real(0.125)) * r43;
		w[3] = (Real(0.5) + t) * r43;
		// move i to N-3
	}
	/** Special weights at i = N-2, t >= 0.5. */
	static void _weightsGradN2_1(Real* w, const Real t) {
		const Real r83 = Real(8)/Real(3);
		w[0] = 0;
		w[1] = 0;
		w[2] = (t-1)*r83;
		w[3] = (1-t)*r83;
		// move i to N-3
		// Note: t = 1 => w[i] = 0
	}
	
	/** Special weights at i = N-3, t >= 0.5. */
	static void _weightsGradN3_1(Real* w, const Real t) {
		const Real r43 = Real(4)/Real(3);
		w[0] = 0;
		w[1] = t-Real(1.5);
		w[2] = (-Real(1.75)*t + Real(1.625)) * r43;
		w[3] = (t - Real(0.5)) * r43;
		// keep i
	}
	/** Special weights at i = 1, t < 0.5. */
	static void _weightsGrad1_0(Real* w, const Real t) {
		const Real r43 = Real(4)/Real(3);
		w[0] = (t-Real(0.5))*r43;
		w[1] = (-Real(1.75)*t+Real(0.125))*r43;
		w[2] = Real(0.5)+t;
		w[3] = 0;
		// keep i
	}
	
	/** Special weights at i = 0, t < 0.5. */
	static void _weightsGrad0_0(Real* w, const Real t) {
		const Real r83 = Real(8)/Real(3);
		w[0] = -r83*t;
		w[1] = r83*t;
		w[2] = 0;
		w[3] = 0;
		// move i to 1
		// Note: t = 0 => w[i] = 0
	}
	/** Special weights at i = 0, t >= 0.5. */
	static void _weightsGrad0_1(Real* w, const Real t) {
		const Real r43 = Real(4)/Real(3);
		const Real r73 = Real(1.75)*r43;
		w[0] = r43*t-2;
		w[1] = -r73*t+Real(2.5);
		w[2] = t-Real(0.5);
		w[3] = 0;
		// move i to 1
	}
	
	/** Purely static class, constructor hidden. */
	KernelMPMSpline2() {}
};

/**
 * Cubic spline for MPM (model of #CKernelDirichlet).
 * Source: Steffen-2008
 * (note that this is a uniform cubic B-spline for interior points)
 * \f[ W(x) = \begin{cases}
		1/2 |x|^3 - |x|^2+2/3 & |x| < 1\\
		-1/6 |x|^3 + |x|^2 - 2 |x| + 4/3 & |x| <2\\
		0 & 2 \leq |x| \\
	   \end{cases} \f]
 * Weights for u_-1, u_0, u_1, u_2:
 * w = [-(1/6)*t^3+(1/2)*t^2-(1/2)*t+1/6, (1/2)*t^3-t^2+2/3, -(1/2)*t^3+(1/2)*t^2+(1/2)*t+1/6, (1/6)*t^3]
 *
 * Gradient is smooth and W'(-x) = -W'(x):
 * \f[ W'(x) = \begin{cases}
		0 & x \leq -2 \\
		1/2 (2+x)^2 & x \leq -1 \\
		-3/2 x^2 - 2 x & x \leq 0 \\
		3/2 x^2 - 2 x & x \leq 1 \\
		-1/2 x^2 + 2 x - 2 & x \leq 2 \\
		0 & 2 < x
	   \end{cases} \f]
 * Weights for u_-1, u_0, u_1, u_2:
 * dw = [-(1/2)*t^2+t-1/2, (3/2)*t^2-2*t, -(3/2)*t^2+t+1/2, (1/2)*t^2]
 * 
 * Dirichlet and 0-Neumann Boundary Condition with special kernels at
 * boundary points. In the end we need special weights at i = 0,1,N-3,N-2,N-1
 * (N-1 only happens for t=0).
 */
class KernelMPMSpline3 {
public:
	/** \name Concept-Implementation.
     * Implementation of #CKernel.
     */
    //@{
	// technically support is 3:
	// - for t < 0.5: u_-1, u_0, u_1
	// - for t >= 0.5: u_0, u_1, u_2
	// -> here we just set it to 4 and set one of the weights to 0
	static const int sSupport = 4;
	static const int sStencilStart = -1;
	static const int sStencilEnd = 2;
	static void weights(Real* w, const Real t) {
		const Real t2 = t*t;
		_weights(w, t2*t, t2, t);
	}
	static void weightsGrad(Real* dw, Real* w, const Real t) {
		const Real t2 = t*t;
		_weights(w, t2*t, t2, t);
		_weightsGrad(dw, t2, t);
	}
	//@}
	
	/** \name Concept-Implementation.
     * Implementation of #CKernelDirichlet.
     */
    //@{
	static void weightsDirichlet(Real* w, const Real t, int& i, const size_t N) {
		const Real t2 = t*t;
		if (i == N-1) {
			// i == N-1 and t == 0
			i = N-3;
			_weightsN1(w);
		} else if (i == N-2) {
			i = N-3;
			_weightsN2(w, t2*t, t2, t);
		} else if (i == N-3) {
			// keep i
			_weightsN3(w, t2*t, t2, t);
		} else if (i == 1) {
			// keep i
			_weights1(w, t2*t, t2, t);
		} else if (i == 0) {
			i = 1;
			_weights0(w, t2*t, t2, t);
		} else {
			_weights(w, t2*t, t2, t);
		}
	}
	static void weightsDirichletGrad(Real* dw, Real* w, const Real t, int& i, const size_t N) {
		const Real t2 = t*t;
		if (i == N-1) {
			// i == N-1 and t == 0
			i = N-3;
			_weightsN1(w);
			_weightsGradN1(dw);
		} else if (i == N-2) {
			i = N-3;
			_weightsN2(w, t2*t, t2, t);
			_weightsGradN2(dw, t2, t);
		} else if (i == N-3) {
			// keep i
			_weightsN3(w, t2*t, t2, t);
			_weightsGradN3(dw, t2, t);
		} else if (i == 1) {
			// keep i
			_weights1(w, t2*t, t2, t);
			_weightsGrad1(dw, t2, t);
		} else if (i == 0) {
			i = 1;
			_weights0(w, t2*t, t2, t);
			_weightsGrad0(dw, t2, t);
		} else {
			_weights(w, t2*t, t2, t);
			_weightsGrad(dw, t2, t);
		}
	}
	//@}
	
private:
	// Note: _weightsX: X (if there) = special weights at i = X (NI = N-I)
	// - Precomputed tN = t^N where needed
	
#pragma mark Weights
	
	/** Get weights. */
	static void _weights(Real* w, const Real t3, const Real t2, const Real t) {
		const Real r16 = Real(1)/Real(6);
		w[0] = -r16*t3+Real(0.5)*t2-Real(0.5)*t+r16;
		w[1] = Real(0.5)*t3-t2+4*r16;
		w[2] = -Real(0.5)*t3+Real(0.5)*t2+Real(0.5)*t+r16;
		w[3] = r16*t3;
	}
	
	/** Special weights at i = N-1, t = 0. */
	static void _weightsN1(Real* w) {
		w[0] = 0; w[1] = 0; w[2] = 0; w[3] = 1;
		// move i to N-3
	}
	
	/** Special weights at i = N-2. */
	static void _weightsN2(Real* w, const Real t3, const Real t2, const Real t) {
		const Real r16 = Real(1)/Real(6);
		w[0] = 0;
		w[1] = (-t3+3*t2-3*t+1) * r16;
		w[2] = (Real(5.5)*t3-Real(7.5)*t2-Real(1.5)*t+Real(3.5)) * r16;
		w[3] = -Real(0.75)*t3+Real(0.75)*t2+Real(0.75)*t+Real(0.25);
		// move i to N-3
	}
	
	/** Special weights at i = N-3. */
	static void _weightsN3(Real* w, const Real t3, const Real t2, const Real t) {
		const Real r16 = Real(1)/Real(6);
		w[0] = (-t3+3*t2-3*t+1) * r16;
		w[1] = Real(0.5)*t3-t2+4*r16;
		w[2] = (-Real(3.5)*t3 + 3*t2 + 3*t + 1) * r16;
		w[3] = Real(0.25)*t3;
		// keep i
	}
	
	/** Special weights at i = 1. */
	static void _weights1(Real* w, const Real t3, const Real t2, const Real t) {
		const Real r16 = Real(1)/Real(6);
		const Real r712 = Real(3.5)*r16;
		w[0] = -Real(0.25)*t3+Real(0.75)*t2-Real(0.75)*t+Real(0.25);
		w[1] = r712*t3-Real(1.25)*t2+Real(0.25)*t+r712;
		w[2] = -Real(0.5)*t3+Real(0.5)*t2+Real(0.5)*t+r16;
		w[3] = r16*t3;
		// keep i
	}
	
	/** Special weights at i = 0. */
	static void _weights0(Real* w, const Real t3, const Real t2, const Real t) {
		const Real r16 = Real(1)/Real(6);
		w[0] = -Real(1.5)*t2+Real(0.75)*t3+1;
		w[1] = -r16*t2*(Real(5.5)*t-9);
		w[2] = r16*t3;
		w[3] = 0;
		// move i to 1
	}
	
#pragma mark WeightsGrad
	
	/** Get weights for gradient. */
	static void _weightsGrad(Real* w, const Real t2, const Real t) {
		w[0] = -Real(0.5)*t2+t-Real(0.5);
		w[1] = Real(1.5)*t2-2*t;
		w[2] = -Real(1.5)*t2+t+Real(0.5);
		w[3] = Real(0.5)*t2;
	}
	
	/** Special weights at i = N-1, t = 0. */
	static void _weightsGradN1(Real* w) {
		w[0] = w[1] = w[2] = w[3] = 0;
		// move i to N-3
	}
	
	/** Special weights at i = N-2. */
	static void _weightsGradN2(Real* w, const Real t2, const Real t) {
		w[0] = 0;
		w[1] = -Real(0.5)*t2+t-Real(0.5);
		w[2] = Real(2.75)*t2-Real(2.5)*t-Real(0.25);
		w[3] = -Real(2.25)*t2+Real(1.5)*t+Real(0.75);
		// move i to N-3
	}
	
	/** Special weights at i = N-3. */
	static void _weightsGradN3(Real* w, const Real t2, const Real t) {
		w[0] = -Real(0.5)*t2+t-Real(0.5);
		w[1] = Real(1.5)*t2-2*t;
		w[2] = -Real(1.75)*t2+t+Real(0.5);
		w[3] = Real(0.75)*t2;
		// keep i
	}
	
	/** Special weights at i = 1. */
	static void _weightsGrad1(Real* w, const Real t2, const Real t) {
		w[0] = -Real(0.75)*t2+Real(1.5)*t-Real(0.75);
		w[1] = Real(1.75)*t2-Real(2.5)*t+Real(0.25);
		w[2] = -Real(1.5)*t2+t+Real(0.5);
		w[3] = Real(0.5)*t2;
		// keep i
	}
	
	/** Special weights at i = 0. */
	static void _weightsGrad0(Real* w, const Real t2, const Real t) {
		w[0] = -3*t+Real(2.25)*t2;
		w[1] = -Real(2.75)*t2+3*t;
		w[2] = Real(0.5)*t2;
		w[3] = 0;
		// move i to 1
	}
	
	/** Purely static class, constructor hidden. */
	KernelMPMSpline3() {}
};
