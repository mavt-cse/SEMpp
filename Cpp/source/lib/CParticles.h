/**\addtogroup concepts
 * @{
 */

/**\file CParticles.h
 * Include this file to get access to all concepts used in library.
 * This will also give access to the #CONCEPT_ASSERT macro to ensure a concept.
 * 
 * #include "CParticles.h"
 *
 * @author Florian Milde
 * @author Gerardo Tauriello
 *
 */

// Notes for implementers of models:
// - when a signature contains a type T it may always be replaced by const T&
//   to pass it by reference
// - same applies for return types IFF returned thingy is not local (BE CAREFUL)

#pragma once

// library includes
#include "Environment.h"
#include "BoundingBox.h"
// general concepts
#include "ConceptsGeneral.h"

/**
 * Concept for a basic Particle Type.
 *
 * Requirements:
 * - Must be a model of #CAssignable
 * - Methods:
 *   - <code> VecType getPos() const</code> - returns particle position
 *   - <code> VecType getVel() const</code> - returns particle velocity
 */
template <typename X>
class CBasicParticle: CAssignable<X> {
    
    CONCEPT_USAGE(CBasicParticle) {
        // Check copy constructor and assignment operator
        X p2(p1);
        // Check position and velocity access
		VecType vt = p1.getPos();
		vt = p1.getVel();
    }
    
private:
    // dummy data (NEVER created)
    X p1;
};

/**
 * Concept for a Particle Type with state.
 *
 * Requirements:
 * - Must be a model of #CBasicParticle
 * - Associated types:
 *   - <code>StateType</code> - type defining state and right hand site
 * - Methods:
 *   - <code> void resetRHS()</code> - resets the rhs state variable to zero
 *   - <code> void setState(StateType)</code> - set value of state variable
 *   - <code> StateType getState() const</code> - get state variable
 *   - <code> StateType getRHS() const</code> - get rhs (i.e. d/dt state)
 *   - <code> void setStateT1(StateType)</code> - sets temporal state variable
 *   - <code> StateType getStateT1() const</code> - gets value of temporal state variable
 */
template <typename X>
class CParticle: CBasicParticle<X> {
    
    CONCEPT_USAGE(CParticle) {
		// Check RHS modification methods
		p1.resetRHS();
		// Check access to State and RHS
        typename X::StateType st = p2.getState();
		p1.setState(st);
		st=p2.getRHS();
		// access to temporary state variable
		p1.setStateT1(st);
		st=p2.getStateT1();
    }
		
    // TODO: Describe how to compute RHS? Or link to example(s)?
    // -> gets iterator from nhMap -> method should concept-assert that Iterator::ValueType fits
    
private:
    // dummy data (NEVER created)
    X p1;
    const X p2;
};

/**
 * Concept for neighborhood map of a particle.
 * 
 * The idea is to be able to get neighborhood around a given position or for a
 * given particle as an iterator to loop through all neighboring particles.
 * 
 * iterate through that neighborhood. In all cases 
 * 
 * The actual implementation and use of such a map may vary a lot. The client is
 * responsible for properly initializing the map before it is used in library
 * functions.
 *
 * Requirements:
 * - Associated types:
 *   - <code>ParticleType</code> - Type of particle
 *   - <code>IteratorType</code>\n
 *     Model of #CIterator used to iterate through neighborhood of a particle.
 *     IteratorType::ValueType must match ParticleType and should be const (not enforced).
 *     One should not be able to change the neighborhood with this iterator.
 *   - <code>IteratorTypeVar</code>\n
 *     Model of #CIterator used to iterate through neighborhood of a particle.
 *     IteratorType::ValueType must match ParticleType. One must be able to
 *     change the neighbors with this iterator.
 * - Methods:
 *   - <code>IteratorType getHood(VecType) const</code>\n
 *     Get neighborhood around a given position.
 *   - <code>IteratorType getHood(ParticleType) const</code>\n
 *     Get neighborhood of a given particle. The particle itself must NOT be
 *     included in the neighborhood. Therefore this is never equivalent to
 *     <code>getHood(part.getPos())</code>.
 *     Access is meant to be read-only.
 *   - <code>IteratorType getHoodVar(...) const</code>\n
 *     Same but access might be read-write. Note that this does not work for 
 *     periodic BC on CellList.
 * .
 * Example using variable nhMap of type X with Particle part of type ParticleType:
\code
X::IteratorType iter = nhMap.getHood(part);
\endcode
 * 
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CNeighborhoodMap<X>) \endcode
 *
 * @see NeighborhoodMaps.h
 *
 */
template <class X>
class CNeighborhoodMap {
    
    CONCEPT_ASSERT(CIterator<typename X::IteratorType>);
    CONCEPT_ASSERT(CIterator<typename X::IteratorTypeVar>);
    CONCEPT_ASSERT(CSameType<typename X::ParticleType, typename X::IteratorType::ValueType>);
    // note: we cannot check for const-ness here... :(
    CONCEPT_ASSERT(CSameType<typename X::ParticleType, typename X::IteratorTypeVar::ValueType>);
    
    CONCEPT_USAGE(CNeighborhoodMap) {
        // make sure we can get neighborhood for a particle
        typename X::IteratorType iter = nhMap.getHood(VecType());
        typename X::IteratorType iter2 = nhMap.getHood(part);
        typename X::IteratorTypeVar iter3 = nhMap.getHoodVar(VecType());
        typename X::IteratorTypeVar iter4 = nhMap.getHoodVar(part);
        ignore_unused_variable_warning(iter);
        ignore_unused_variable_warning(iter2);
        ignore_unused_variable_warning(iter3);
        ignore_unused_variable_warning(iter4);
    }
    
private:
    // dummy data (NEVER created)
    const X nhMap;
    typename X::ParticleType part;
};

/**
 * Extension of #CIterator concept for iterators that can be split up into smaller ones.
 *
 * The idea is to use a model of #CIterator together with any work distribution
 * (e.g. multithreading with tbb).
 * 
 * Requirements:
 * - Must be a model of #CIterator
 * - Associated types:
 *   - <code>SubIteratorType</code>\n
 *     Model of #CIterator returned by getSubIterator()
 * - Methods:
 *   - <code>size_t size() const</code>\n
 *     Return number of elements of the iterator
 *   - <code>SubIteratorType getSubIterator(size_t begin, size_t N) const</code>\n
 *     Get iterator for elements begin to begin+N-1.
 *     Requires: <code>0 <= begin, begin+N-1 < size()</code>
 * .
 * Example using variable iter of type X:
\code
X::SubIteratorType sub_it = iter.getSubIterator(0, iter.size());
\endcode
 * 
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CSubdivisibleIterator<X>) \endcode
 *
 * @see ArrayIterator for an example of a model of it
 *
 */
template <class X>
class CSubdivisibleIterator: CIterator<X> {
    
    CONCEPT_ASSERT(CIterator<typename X::SubIteratorType>);
    
    CONCEPT_USAGE(CSubdivisibleIterator) {
        typename X::SubIteratorType sub_it = iter.getSubIterator(0, iter.size());
        ignore_unused_variable_warning(sub_it);
    }
    
private:
    // dummy data (NEVER created)
    const X iter;
};

/**
 * Concept for operator performing work on elements given by a #CIterator.
 *
 * The idea is to be able to perform work on a block of particles where the work
 * may be distributed by a model of #CWorkDistributor.
 *
 * Requirements:
 * - Copy constructor
 * - <code>void operator()(Iterator) const</code> where Iterator is a model of
 *   #CIterator
 * 
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(COperator<X, Iterator>) \endcode
 *
 * @see Operators.h
 * 
 */
template <class X, class Iterator>
class COperator {
    
    CONCEPT_USAGE(COperator) {
        X body2(body);
        body(iter);
    }
    
private:
    // dummy data (NEVER created)
    const X body;
    Iterator iter;
};

/**
 * Concept for class distributing work to be performed by a model of #COperator.
 *
 * The main use is for processing particles in parallel.
 *
 * Requirements:
 * - <code>void process(Operator)</code> where Operator is a model of #COperator
 * 
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CWorkDistributor<X, Operator>) \endcode
 *
 * @see WorkDistributors.h
 * 
 */
template <class X, class Operator>
class CWorkDistributor {
    
    CONCEPT_USAGE(CWorkDistributor) {
        distrubutor.process(body);
    }
    
private:
    // dummy data (NEVER created)
    X distrubutor;
    Operator body;
};

/**
 * Concept for time integrators.
 * 
 * The idea is to have a generic way to loop through the stages of a time
 * integration scheme without having to reimplement it for every Particle.
 * A large family of time integrators (including some for second order ODEs)
 * can be implemented following this concept.
 *
 * Requirements:
 * - <code>size_t getStages() const</code> which returns the number of stages
 *   for the time integration scheme
 * - <code>Real getTimeOffset(size_t stage)</code>, where stage < getStages()
 * - <code>void update(size_t stage, WorkDistributor&)</code>
 *   - stage < getStages() must hold and WorkDistributor is a model of #CWorkDistributor
 *   - performs the given stage of time integration assuming the rhs at time
 *     t + ti.getTimeOffset(stage) was computed on all particles
 * 
 * Notes:
 * - Constructor of X would usually take the time step as an input
 * - Each time integrator may have different requirements on the Particle type
 *   on which the WorkDistributor works. Those requirements are to be stated in
 *   the documentation of each integrator.
 * 
 * Example using variable ti of type X and a distributor of type WorkDistributor:
\code
// perform one time step from t to t + dt
for (int stage = 0; stage < ti.getStages(); ++stage) {
    // update RHS for all particles at time t + ti.getTimeOffset(stage)
    // ...
    // advance state (call once for each particle collection to be updated)
    ti.update(stage, distributor);
}
\endcode
 *
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CTimeIntegrator<X, WorkDistributor>) \endcode
 * 
 * @see TimeIntegrators.h, \ref timeintegration_page
 *
 */
template <class X, class WorkDistributor>
class CTimeIntegrator {
    
    CONCEPT_USAGE(CTimeIntegrator) {
        for (int stage = 0; stage < ti.getStages(); ++stage) {
            ti.update(stage, distributor);
        }
    }
    
private:
    // dummy data (NEVER created)
    X ti;
    WorkDistributor distributor;
};

/**
 * Concept for representations of closed surfaces.
 *
 * Uses:
 * - initialize elements inside of surface
 * - apply force to particles from that surface
 * - Box-surface enclosing whole domain may be used to have BC
 *
 * Requirements:
 * - <code>BoundingBox getBoundingBox()</code> - returns box enclosing surface
 * - <code>bool isInside(VecType pos)</code> - true if pos is inside the surface
 * - <code>Real signedDistance(VecType pos)</code> - distance to surface, neg.
 *                                                   if inside (levelset-style)
 * - <code>VecType normal(VecType pos)</code> - normal to surface
 *
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CSurface<X>) \endcode
 * 
 * @see Surfaces.h
 */
template <class X>
class CSurface {
    
    CONCEPT_USAGE(CSurface) {
        BoundingBox bbox = x.getBoundingBox();
        VecType pos;
        bool is_inside = x.isInside(pos);
        Real dist = x.signedDistance(pos);
		VecType normal = x.normal(pos);
        // note: this is not compiler-checked!
        if (is_inside) {
            assert(dist < 0);
        } else {
            assert(dist >= 0);
        }
        ignore_unused_variable_warning(dist);
    }
    
private:
    // dummy data (NEVER created)
    const X x;
};


/** @} */ // end of group concepts
