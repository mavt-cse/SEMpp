/**\file Iterators.h
 * Collection of models of #CIterator and #CSubdivisibleIterator.
 * Note that all the iterators' methods will be properly inlined (checked with
 * icc 11.0). Also if the contained elements (and all their ancestors) have no
 * virtual methods, all calls on the items returned by currentItem() can be
 * properly inlined.
 * 
 * Notes:
 * - Classes ending in P iterate over a collection containing pointers
 * - All classes return pointers to the data of interest
 * - #ContainerIterator and #ContainerIteratorP can work with dynamically
 *   resized vectors (as long as they don't change while looping through the
 *   iterator)
 * - #ArrayIterator and #ArrayIteratorP work on fixed size collections which
 *   are contiguous in memory. Note that this is also the case with std::vector
 *   as long as the size remains fixed during the iterator's lifetime. For
 *   instance the following is guaranteed (by C++ standard) to work:
\code
std::vector<Particle> v;
ArrayIterator iter(&v[0], v.size());
\endcode
 * 
 * Based on: http://www.lrde.epita.fr/dload/papers/coots01.html
 * 
 * @author Gerardo Tauriello
 * @see CIterator
 */

#pragma once

// library includes
#include "CParticles.h"

/**
 * Model of the #CSubdivisibleIterator concept for C arrays.
 * This will loop over the array's content and return a pointer to the
 * contained value.
 * 
 * Note that the array is not copied, so it's lifetime may not exceed the one
 * of the iterator.
 * 
 * @tparam T    Type of content of array (i.e. array has type T* or T t[N]).
 */
template< typename T >
class ArrayIterator {
protected:
    /** First element in array. */
//@@@    T* const mpFirst;
    T* mpFirst;
    /** Number of elements in array. */
//    const int mSize;
    int mSize;
    /** Current element in array. */
    int mCurrent;
public:
    /** Construct by linking it to a given array with size N. */
    ArrayIterator(T* const array, const int N):
        mpFirst(array), mSize(N), mCurrent(0) { }
    
    // default destructor and copy constructor used
    // assignment operator will fail due to read-only consts
    
    /** \name Concept-Implementation.
     * Implementation of #CIterator.
     */
    //@{
    typedef T ValueType;
    void first() { mCurrent = 0; }
    void next() { ++mCurrent; }
    bool isDone() const { return mCurrent >= mSize; }
    ValueType* currentItem() const { return &mpFirst[mCurrent]; }
    //@}
    
    /** \name Concept-Implementation.
     * Implementation of #CSubdivisibleIterator.
     */
    //@{
    typedef ArrayIterator<T> SubIteratorType;
    size_t size() const { return mSize; }
    SubIteratorType getSubIterator(const size_t begin, const size_t N) const {
        return SubIteratorType(&mpFirst[begin], N);
    }
    //@}
};

/**
 * Model of the #CSubdivisibleIterator concept for C arrays containing pointers.
 * This will loop over the array's content and return the contained pointer.
 * So, the array must contain values of type T*.
 * 
 * Note that the array is not copied, so it's lifetime may not exceed the one
 * of the iterator.
 * 
 * @tparam T    Type of pointed content of array (i.e. array has type T** or T* t[N]).
 */
template< typename T >
class ArrayIteratorP: public ArrayIterator<T*>  {
public:
    /** Construct by linking it to a given array with size N. */
    ArrayIteratorP(T** const array, const int N): ArrayIterator<T*>(array, N) { }
    
    /** \name Concept-Implementation.
     * Implementation of #CIterator.
     */
    //@{
    // inherit all from ContainerIterator besides:
    typedef T ValueType;
    ValueType* currentItem() const { return (this->mpFirst[this->mCurrent]); }
    //@}
    
    /** \name Concept-Implementation.
     * Implementation of #CSubdivisibleIterator.
     */
    //@{
    typedef ArrayIteratorP<T> SubIteratorType;
    size_t size() const { return this->mSize; }
    SubIteratorType getSubIterator(const size_t begin, const size_t N) const {
        return SubIteratorType(&(this->mpFirst[begin]), N);
    }
    //@}
};

/**
 * Model of the #CSubdivisibleIterator concept for STL Random Access Containers.
 * This will loop over the container's content and return a pointer to the
 * contained value.
 *
 * Note that the container is not copied, so it's lifetime may not exceed
 * the one of the iterator.
 *
 * @tparam Container    A type that is a model of Random Access Container concept (e.g. std::vector).
 */
template< typename Container >
class ContainerIterator {
protected:
    /** Reference to container. */
    Container& mrContainer;
    /** STL Iterator used. */
    typename Container::iterator mIter;
public:
    /** Construct by linking it to a given container. */
    ContainerIterator(Container& container):
        mrContainer(container), 
        mIter(container.begin()) { }
    
    // default destructor and copy constructor used
    // assignment operator will fail due to read-only reference
    
    /** \name Concept-Implementation.
     * Implementation of #CIterator.
     */
    //@{
    typedef typename Container::value_type ValueType;
    void first() { mIter = mrContainer.begin(); }
    void next() { ++mIter; }
    bool isDone() const { return mIter == mrContainer.end(); }
    ValueType* currentItem() const { return &(*mIter); }
    //@}
    
    /** \name Concept-Implementation.
     * Implementation of #CSubdivisibleIterator.
     */
    //@{
    typedef ArrayIterator<ValueType> SubIteratorType;
    size_t size() const { return mrContainer.size(); }
    SubIteratorType getSubIterator(const size_t begin, const size_t N) const {
        return SubIteratorType(&mrContainer[begin], N);
    }
    //@}
};

/**
 * Model of the #CSubdivisibleIterator concept for STL Random Access Containers containing pointers.
 * This will loop over the container's content and return the contained pointer.
 *
 * Note that the container is not copied, so it's lifetime may not exceed
 * the one of the iterator.
 * 
 * @tparam Container    A type that is a model of Random Access Container concept (e.g. std::vector).
 * @tparam T            Container contains items of type T*.
 */
template< typename Container, typename T >
class ContainerIteratorP: public ContainerIterator<Container>  {
public:
    /** Construct by linking it to a given container. */
    ContainerIteratorP(Container& container):
        ContainerIterator<Container>(container) { }
    
    /** \name Concept-Implementation.
     * Implementation of #CIterator.
     */
    //@{
    // inherit all from ContainerIterator besides:
    typedef T ValueType;
    ValueType* currentItem() const { return *(this->mIter); }
    //@}
    
    /** \name Concept-Implementation.
     * Implementation of #CSubdivisibleIterator.
     */
    //@{
    typedef ArrayIteratorP<ValueType> SubIteratorType;
    size_t size() const { return this->mrContainer.size(); }
    SubIteratorType getSubIterator(const size_t begin, const size_t N) const {
        return SubIteratorType(&(this->mrContainer[begin]), N);
    }
    //@}
};


///////@@@@@@@

/**
 * Model of the #CSubdivisibleIterator concept for C arrays.
 * This will loop over the Particles of an array of Cells and return a pointer
 * to the contained value.
 * 
 * The subdivision of the iterator works on the cells and so this will not
 * parallelize well if we have few cells.
 * 
 * Note that the array is not copied, so it's lifetime may not exceed the one
 * of the iterator.
 * 
 * @tparam T    Type of content of array (i.e. array has type T* or T t[N]).
 */
template< typename C>
class ArrayIteratorIterable {
	
	typedef ArrayIterator<typename C::IterableType> IteratorType;
	
protected:
    /** First element in Cell array. */
    C* const mpFirst;
    /** Iterator over Array of Particles. */
	IteratorType mIter;
    /** Number of elements in array. */
    const int mSize;
    /** Current element in array. */
    int mCurrent;
public:
    /** Construct by linking it to a given array of Cells with size N. */
    ArrayIteratorIterable(C* const array, const int N):
	mpFirst(array),mSize(N), mCurrent(0) ,mIter(mpFirst->getIterator()){}
    
    // default destructor and copy constructor used
    // assignment operator will fail due to read-only consts
    
    /** \name Concept-Implementation.
     * Implementation of #CIterator.
     */
    //@{
    typedef typename C::IterableType ValueType;

    void first() { 
		mCurrent = 0; 
		//mIter=IteratorType(mpFirst[mCurrent].getCollection(),mpFirst[mCurrent].getCollectionSize());
		//@@@ assignment iterator does not work;
		 mIter=mpFirst[mCurrent].getIterator();
	}
    void next() { 
		mIter.next();
		if (mIter.isDone()){
			mCurrent++;
			//@@@ assignment iterator does not work;
			if(mCurrent<mSize) mIter=mpFirst[mCurrent].getIterator();
		}
	}
    bool isDone() const { return mCurrent >= mSize; }
    ValueType* currentItem() const { return mIter.currentItem(); }
    //@}
    
    /** \name Concept-Implementation.
     * Implementation of #CSubdivisibleIterator.
     */
    //@{
    typedef ArrayIteratorIterable<C> SubIteratorType;
    size_t size() const { return mSize; }
    SubIteratorType getSubIterator(const size_t begin, const size_t N) const {
        return SubIteratorType(&mpFirst[begin], N);
    }
    //@}
};

///////@@@@@@@

/**
 * Model of the #CSubdivisibleIterator concept for C arrays.
 * This will loop over an array of Cells containing an array of pointers to particles
 * and return a pointer to the contained value.
 * 
 * Assumption 
 *
 * Note that the array is not copied, so it's lifetime may not exceed the one
 * of the iterator.
 * 
 * @tparam T    Type of content of array (i.e. array has type T* or T t[N]).
 */
template< typename C, int I>
class ArrayIteratorIterableP {
	
	typedef ArrayIteratorP<typename C::IterableType> IteratorType;
	
protected:
    /** First element in Cell array. */
    C* const mpFirst;
    /** Iterator over Array of Particles. */
	IteratorType mIter;
    /** Number of elements in array. */
    const int mSize;
    /** Current element in array. */
    int mCurrent;
public:
    /** Construct by linking it to a given array of Cells with size N. */
    ArrayIteratorIterableP(C* const array, const int N):
	mpFirst(array),mSize(N), mCurrent(0) ,mIter(mpFirst->getIteratorP(I)){}
    
    // default destructor and copy constructor used
    // assignment operator will fail due to read-only consts
    
    /** \name Concept-Implementation.
     * Implementation of #CIterator.
     */
    //@{
    typedef typename C::IterableType ValueType;
	
    void first() { 
		/* @@@ first original code.. does not consider that these iterators could be empty
		mCurrent = 0;
		//mIter=IteratorType(mpFirst[mCurrent].getCollection(),mpFirst[mCurrent].getCollectionSize());
		//@@@ assignment iterator does not work;
		mIter=mpFirst[mCurrent].getIteratorP(I);
		 */
		
		mCurrent = 0;
		mIter=mpFirst[mCurrent].getIteratorP(I);
		while(mIter.isDone() && mCurrent<mSize){
			mCurrent++;
			mIter=mpFirst[mCurrent].getIteratorP(I);
		}
	}
    void next() { 
		/* first original code.. does not consider that these iterators could be empty
		mIter.next();
		if (mIter.isDone()){
			mCurrent++;
			//@@@ assignment iterator does not work;
			if(mCurrent<mSize) mIter=mpFirst[mCurrent].getIteratorP(I);
		}
		*/
		mIter.next();
		if (mIter.isDone()){
			while(mIter.isDone()&&mCurrent<mSize){
				mCurrent++;
				mIter=mpFirst[mCurrent].getIteratorP(I);
			}
		}		
	}
    bool isDone() const { return mCurrent >= mSize; }
    ValueType* currentItem()const { return mIter.currentItem(); } //@@@ should be currentItem()const.. but does not work
    //@}
    
    /** \name Concept-Implementation.
     * Implementation of #CSubdivisibleIterator.
     */
    //@{
    typedef ArrayIteratorIterableP<C,I> SubIteratorType;
    size_t size() const { return mSize; }
    SubIteratorType getSubIterator(const size_t begin, const size_t N) const {
        return SubIteratorType(&mpFirst[begin], N);
    }
    //@}
};


