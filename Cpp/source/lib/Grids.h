/**\file Grids.h
 * Collection of grids (models of #CGrid).
 *
 * #include "Grids.h"
 *
 * @author Gerardo Tauriello
 * @date 7/20/10
 *
 * @see CGrid, GridGeneric
 */

#pragma once

// project includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"
#include "Matrix.h"
#include "Interpolators.h"

// system includes

// forward references

#pragma mark CONCEPTS
/**\addtogroup concepts
 * @{
 */
/**
 * Concept for representations of grids.
 * 
 * A grid is an extension of a matrix in that it can work on positions given
 * in real (or physical) space, has a notion of grid spacing and can compute
 * values and gradients at arbitrary positions. The real space is defined by
 * a domain with origin and size, while the index space has 0-origin and is
 * used to directly access grid points. Note that values obtained in real space
 * are not guaranteed to be indentical to the one in index space (it depends
 * on whether an interpolating kernel is used).
 *
 * It may also be responsible for putting a ghost layer around your data and
 * updating it.
 *
 * Requirements:
 * - Must be a model of #CAssignable
 * - Associated types:
 *   - <code>ElementType</code> - Type of element at a grid point
 *   - <code>GradientType</code> - Type of gradient at a grid point
 * - Methods:
 *   - <code>VecTypeI getGridSize() const</code> - Get number of non-ghost grid points
 *   - <code>VecType getDomainOffset() const</code> - Get domain origin in real space
 *   - <code>void setDomainOffset(VecType)</code> - Set domain origin in real space
 *   - <code>VecType getDomainSize() const</code> - Get domain size in real space
 *   - <code>void setDomainSize(VecType)</code> - Set domain size in real space
 *   - <code>VecType getH() const</code> - Get grid spacing in real space.
 *   - <code>ElementType operator()(VecTypeI) const</code>\n
 *     Read only access to grid point (valid indices are in [0,getGridSize()-1])
 *   - <code>ElementType& operator()(VecTypeI)</code>\n
 *     Access grid point (valid indices are in [0,getGridSize()-1])
 *   - <code>ElementType operator()(VecType) const</code>\n
 *     Interpolate value in real space (valid coordinates are in
 *     [getDomainOffset(), getDomainOffset() + getDomainSize()].
 *   - <code>GradientType gradient(VecType) const</code>\n
 *     Interpolate gradient of values in real space (valid coordinates are in
 *     [getDomainOffset(), getDomainOffset() + getDomainSize()].
 * .
 * Example using variable g of type X:
\code
X::ElementType val1 = g(VecTypeI(0));
X::ElementType val2 = g(g.getDomainOffset());
// usually val1 == val2, but that depends on the interpolation

// loop usually first in z, then in y, then in x
VecTypeI posI, nx = g.getGridSize();
for (posI[2] = 0; posI[2] < nx[2]; ++posI[2]) {
    for (posI[1] = 0; posI[1] < nx[1]; ++posI[1]) {
        for (posI[0] = 0; posI[0] < nx[0]; ++posI[0]) {
            g(posI) = 0;
        }
    }
}
\endcode
 *
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CGrid<X>) \endcode
 * 
 * @see GridGeneric, GridGhostedD
 */
template <class X>
class CGrid: CAssignable<X> {
	
    CONCEPT_USAGE(CGrid) {
		// get types
		typedef typename X::ElementType ET;
		typedef typename X::GradientType GT;
		
		// get sizes
		VecTypeI nx = xc.getGridSize();
		VecType minD = xc.getDomainOffset();
		VecType sizeD = xc.getDomainSize();
		VecType h = xc.getH();
		
		// set sizes
		x.setDomainOffset(minD);
		x.setDomainSize(sizeD);
		
		// access elements
		VecTypeI posI;
		VecType posR;
#if DIM == 2
		// y-loop
		for (posI[1] = 0; posI[1] < nx[1]; ++posI[1]) {
			posR[1] = (posI[1]+Real(0.5))*h[1] + minD[1];
			if (posI[1] == nx[1]-1) posR[1] -= h[1];
			// x-loop
			for (posI[0] = 0; posI[0] < nx[0]; ++posI[0]) {
				posR[0] = (posI[0]+Real(0.5))*h[0] + minD[0];
				if (posI[0] == nx[0]-1) posR[0] -= h[0];
				
				// storage for scalars and vectors
				ET elem;
				GT v;
				// const access
				elem = xc(posI);
				elem = xc(posR);
				v = xc.gradient(posR);
				
				// non-const assign
				x(posI) = elem;
			}
		}
#elif DIM == 3
		// z-loop
		for (posI[2] = 0; posI[2] < nx[2]; ++posI[2]) {
			posR[2] = (posI[2]+Real(0.5))*h[2] + minD[2];
			if (posI[2] == nx[2]-1) posR[2] -= h[2];
			// y-loop
			for (posI[1] = 0; posI[1] < nx[1]; ++posI[1]) {
				posR[1] = (posI[1]+Real(0.5))*h[1] + minD[1];
				if (posI[1] == nx[1]-1) posR[1] -= h[1];
				// x-loop
				for (posI[0] = 0; posI[0] < nx[0]; ++posI[0]) {
					posR[0] = (posI[0]+Real(0.5))*h[0] + minD[0];
					if (posI[0] == nx[0]-1) posR[0] -= h[0];
					
					// storage for scalars and vectors
					ET elem;
					GT v;
					// const access
					elem = xc(posI);
					elem = xc(posR);
					v = xc.gradient(posR);
					
					// non-const assign
					x(posI) = elem;
				}
			}
		}
#endif
    }
    
private:
    // dummy data (NEVER created)
    const X xc;
	X x;
};
/** @} */ // end of group concepts

/**
 * Generic grid class to be linked to a given Matrix and Interpolator (model of #CGrid).
 *
 * Matrix needs to include all necessary ghost points and you cannot access
 * those points through the Grid-class (unless using getM()).
 * Matrix can be accessed directly for low-level access.
 *
 * Grid allows you to access scalars and gradients at locations defined in
 * real space (given by origin and size of domain).
 *
 * \par Example usage:
\code
typedef Matrix::D2D<double> M;
typedef InterpolatorGhosts<KernelHermite> I;
typedef GridGeneric<M,I> G;
G g(M(20,20), VecType(-5), VecType(10));	// Note: 17x17 includes any req. ghosts
...
g.getM().dump(...);
\endcode
 * See #CGrid for how to access grid.
 * 
 * @tparam Matrix Model of #CMatrix2D or #CMatrix3D (must match DIM).
 * @tparam Interpolator	Model of #CInterpolator
 */
template <class Matrix, class Interpolator>
class GridGeneric {
#pragma mark LIFECYCLE
public:
	/**
	 * Default constructor.
	 * Grid must be linked to matrix at construction time.
	 * @param m		Matrix used to generate grid.
	 * @param minD	Origin of domain in real space.
	 * @param sizeD	Size of domain in real space.
	 */
	GridGeneric(const Matrix& m, const VecType minD, const VecType sizeD): mM(m), mMinD(minD), mSizeD(sizeD) {
		resetSize();
	}
    
	// default destructor, copy constructor and assignment operator used
	// (note that copy constructor, operator=, destructor of Matrix are implicitly used)
	
#pragma mark CGrid
	
	/** \name Concept-Implementation.
     * Implementation of #CGrid.
     */
    //@{
	// extract typedefs
	typedef typename Matrix::ElementType ElementType;
	typedef VectorElement<ElementType, DIM> GradientType;
	
	// access info
	VecTypeI getGridSize() const { return mGridSize; }
	VecType getDomainOffset() const { return mMinD; }
	void setDomainOffset(const VecType minD) { mMinD = minD; }
	VecType getDomainSize() const { return mSizeD; }
	void setDomainSize(const VecType sizeD) {
		mSizeD = sizeD;
		resetH();
	}	
	VecType getH() const { return mH; }
	
	// access elements
	ElementType operator()(const VecTypeI posI) const {
#if DIM == 2
		assert(0 <= posI[0] && posI[0] < mGridSize[0]);
		assert(0 <= posI[1] && posI[1] < mGridSize[1]);
		return mM(posI[0] + Interpolator::sGhostStart,
				  posI[1] + Interpolator::sGhostStart);
#elif DIM == 3
		assert(0 <= posI[0] && posI[0] < mGridSize[0]);
		assert(0 <= posI[1] && posI[1] < mGridSize[1]);
		assert(0 <= posI[2] && posI[2] < mGridSize[2]);
		return mM(posI[0] + Interpolator::sGhostStart,
				  posI[1] + Interpolator::sGhostStart,
				  posI[2] + Interpolator::sGhostStart);
#endif
	}
	
	ElementType& operator()(const VecTypeI posI) {
#if DIM == 2
		assert(0 <= posI[0] && posI[0] < mGridSize[0]);
		assert(0 <= posI[1] && posI[1] < mGridSize[1]);
		return mM(posI[0] + Interpolator::sGhostStart,
				  posI[1] + Interpolator::sGhostStart);
#elif DIM == 3
		assert(0 <= posI[0] && posI[0] < mGridSize[0]);
		assert(0 <= posI[1] && posI[1] < mGridSize[1]);
		assert(0 <= posI[2] && posI[2] < mGridSize[2]);
		return mM(posI[0] + Interpolator::sGhostStart,
				  posI[1] + Interpolator::sGhostStart,
				  posI[2] + Interpolator::sGhostStart);
#endif
	}
	
	ElementType operator()(const VecType posR) const {
		return Interpolator::scalar(mM, getIndex(posR));
	}
	
	GradientType gradient(const VecType posR) const {
		return Interpolator::gradient(mM, getIndex(posR), mInvH);
	}
    //@}
	
#pragma mark MORE ACCESS
	// note: add to concept?
	
	/** Type of returned matrix. */
	typedef Matrix MatrixType;
	/**
	 * Unprotected access to matrix (please use with care).
	 * Please use setM to completely replace matrix if that changes its size.
	 */
	Matrix& getM() { return mM; }
	/** Replace matrix. */
	void setM(const Matrix& m) { mM = m; resetSize(); }
	
#pragma mark HELPERS
protected:
	/** Reset grid size and grid spacing after changing matrix size. */
	void resetSize() {
		// Matrix assumed to include ghosts
		const int ghostsize = Interpolator::sGhostStart + Interpolator::sGhostEnd;
		mGridSize[0] = mM.getSizeX() - ghostsize;
		mGridSize[1] = mM.getSizeY() - ghostsize;
#if DIM == 3
		mGridSize[2] = mM.getSizeZ() - ghostsize;
#endif
		resetH();
	}
	/** Reset grid spacing after changing mGridSize or mSizeD. */
	void resetH() {
		for (int i = 0; i < DIM; ++i) {
			mH[i] = mSizeD[i] / Real(mGridSize[i]-1);
			mInvH[i] = 1/mH[i];
		}
	}
	
	/** Convert from index space to real space. */
	VecType getPos(const VecType posI) const {
		return mMinD + (posI - Real(Interpolator::sGhostStart))*mH;
	}
	/** Convert from real space to index space. */
	VecType getIndex(const VecType posR) const {
		return (posR - mMinD) * mInvH + Real(Interpolator::sGhostStart);
	}
	
	/**
	 Periodic BC to get indices in 0, mGridSize-1:
	 - clean: 
	 if (posI[i] < 0) {
		pos[i] = (posI[i] % mGridSize[i]) + mGridSize[i];
	 } else {
		pos[i] = posI[i] % mGridSize[i];
	 }
	 - for posI > -mGridSize
	 pos[i] = (posI[i] + mGridSize[i]) % mGridSize[i];
	 
	 Periodic BC in real space:
	 pos[i] = posR[i] - mSizeD[i] * floor((posR[i] - mMinD[i]) / mSizeD);
	 */
    
protected:
#pragma mark FIELDS
	/** Domain origin. */
	VecType mMinD;
	/** Domain size. */
	VecType mSizeD;
	/** Grid spacing. */
	VecType mH;
	/** Inverse of grid spacing (1/mH). */
	VecType mInvH;
	/** Size of grid. */
	VecTypeI mGridSize;
	/** Matrix containing all the data. */
	Matrix mM;
	
	// check template parameter
private:
#if DIM == 2
	CONCEPT_ASSERT(CMatrix2D< Matrix >);
#elif DIM == 3
	CONCEPT_ASSERT(CMatrix3D< Matrix >);
#endif
	CONCEPT_ASSERT(CInterpolator< Interpolator, Matrix>);
};

/**
 * Grid class which automatically handles ghosts (model of #CGrid).
 *
 * Matrix will be internally extended to accomodate ghosts.
 * Use is very similar to #GridGeneric, but getM() will return a copy of the
 * matrix to get the data without the ghosts (e.g. for dumping) and setM()
 * will only replace the non-ghost values in the grid.
 *
 * \par Example usage:
\code
typedef Matrix::D2D<double> M;
typedef InterpolatorGhosts<KernelHermite> I;
typedef GridGhostedD<M,I> G;
G g(M(17,17), VecType(-5), VecType(10));
...
g.getM().dump(...);
\endcode
 * See #CGrid for how to access grid.
 * 
 * @tparam Matrix Model of #CMatrix2D or #CMatrix3D (must match DIM) with (nx,ny) or (nx,ny,nz) constructor.
 * @tparam Interpolator	Model of #CInterpolator
 */
template <class Matrix, class Interpolator>
class GridGhostedD: public GridGeneric<Matrix, Interpolator> {
	// just to safe space
	typedef GridGeneric<Matrix, Interpolator> Parent;
	static const size_t sGst = Interpolator::sGhostStart;
	static const size_t sGen = Interpolator::sGhostEnd;
	static const size_t sGS = sGst + sGen;
	
#pragma mark LIFECYCLE
public:
	/**
	 * Default constructor.
	 * Grid must be linked to matrix at construction time.
	 * @param m		Matrix used to generate grid.
	 * @param minD	Origin of domain in real space.
	 * @param sizeD	Size of domain in real space.
	 */
	GridGhostedD(const Matrix& m, const VecType minD, const VecType sizeD):
#if DIM == 2
	Parent(Matrix(m.getSizeX() + sGS, m.getSizeY() + sGS), minD, sizeD)
#elif DIM == 3
	Parent(Matrix(m.getSizeX() + sGS, m.getSizeY() + sGS, m.getSizeZ() + sGS), minD, sizeD)
#endif
	{ 
		fillM(m);
	}
    
	// rest inherited...
	
#pragma mark MORE ACCESS
	
	/** Get copy of non-ghost elements of grid. */
	Matrix getM() {
		// copy content
#if DIM == 2
		size_t nx = this->mM.getSizeX() - sGS;
		size_t ny = this->mM.getSizeY() - sGS;
		Matrix result(nx,ny);
		for (size_t j = 0; j < ny; ++j) {
			for (size_t i = 0; i < nx; ++i) {
				result(i,j) = this->mM(i + sGst, j + sGst);
			}
		}
#elif DIM == 3
		size_t nx = this->mM.getSizeX() - sGS;
		size_t ny = this->mM.getSizeY() - sGS;
		size_t nz = this->mM.getSizeZ() - sGS;
		Matrix result(nx,ny,nz);
		for (size_t k = 0; k < nz; ++k) {
			for (size_t j = 0; j < ny; ++j) {
				for (size_t i = 0; i < nx; ++i) {
					result(i,j,k) = this->mM(i + sGst, j + sGst, k + sGst);
				}
			}
		}
#endif
		return result;
	}
	/** Replace content of matrix (m is again given without ghosts). */
	void setM(const Matrix& m) {
		// check size
#if DIM == 2
		if (m.getSizeX() + sGS != this->mM.getSizeX() || 
			m.getSizeY() + sGS != this->mM.getSizeY()) {
			this->mM = Matrix(m.getSizeX() + sGS, m.getSizeY() + sGS);
		}
#elif DIM == 3
		if (m.getSizeX() + sGS != this->mM.getSizeX() || 
			m.getSizeY() + sGS != this->mM.getSizeY() || 
			m.getSizeZ() + sGS != this->mM.getSizeZ()) {
			this->mM = Matrix(m.getSizeX() + sGS, m.getSizeY() + sGS, m.getSizeZ() + sGS);
		}
#endif	
		fillM(m);
		this->resetSize();
	}
	
private:
	/** Fill internal matrix. */
	void fillM(const Matrix& m) {
		// copy content
#if DIM == 2
		size_t nx = m.getSizeX();
		size_t ny = m.getSizeY();
		assert(nx + sGS == this->mM.getSizeX());
		assert(ny + sGS == this->mM.getSizeY());
		for (size_t j = 0; j < ny; ++j) {
			for (size_t i = 0; i < nx; ++i) {
				this->mM(i + sGst, j + sGst) = m(i,j);
			}
		}
#elif DIM == 3
		size_t nx = m.getSizeX();
		size_t ny = m.getSizeY();
		size_t nz = m.getSizeZ();
		assert(nx + sGS == this->mM.getSizeX());
		assert(ny + sGS == this->mM.getSizeY());
		assert(nz + sGS == this->mM.getSizeZ());
		for (size_t k = 0; k < nz; ++k) {
			for (size_t j = 0; j < ny; ++j) {
				for (size_t i = 0; i < nx; ++i) {
					this->mM(i + sGst, j + sGst, k + sGst) = m(i,j,k);
				}
			}
		}
#endif
	}
		
#pragma mark GHOSTS
public:
	// Derivations: see Taylor.mw (Ghost values for given BC).
	
	/** Extrapolate values in ghost layer with 2nd order. */
	void ghostsExtrapolate2() {
		if (sGS == 0) return;
		// get weights
		int w[sGS][3];
		for (int i = 0; i < sGS; ++i) {
			w[i][0] =  0;
			w[i][1] =  2+i;
			w[i][2] = -1-i;
		}
		// do it
		ghostIt(w);
	}
	
	/** Extrapolate values in ghost layer with 3rd order. */
	void ghostsExtrapolate3() {
		if (sGS == 0) return;
		// get weights
		int w[sGS][4];
		for (int i = 0; i < sGS; ++i) {
			w[i][0] =  0;
			w[i][1] =  (2-i)*(3-i)/2; 	// trust me on int/2
			w[i][2] = -(1-i)*(3-i);
			w[i][3] =  (1-i)*(2-i)/2; 	// trust me on int/2
		}
		// do it
		ghostIt(w);
	}
	
	/**
	 * Neumann BC at i = 0 and i = N-1 with 2nd order.
	 * Assumes constant grid spacing in all dim and same BC in all dim.
	 */
	void ghostsNeumann2(const Real bc = 0) {
		if (sGS == 0) return;
		// get weights
		Real w[sGS][2];
		const Real h = this->mH[0];
		for (int i = 0; i < sGS; ++i) {
			w[i][0] = (h+i*h)*bc;
			w[i][1] = 1;
		}
		// do it
		ghostIt(w);
	}
	
	/**
	 * Neumann BC at i = 0 and i = N-1 with 3rd order.
	 * Assumes constant grid spacing in all dim and same BC in all dim.
	 */
	void ghostsNeumann3(const Real bc = 0) {
		if (sGS == 0) return;
		// get weights
		Real w[sGS][3];
		const Real h = this->mH[0];
		for (int i = 0; i < sGS; ++i) {
			w[i][0] = h*(1+i)*(2+i)*bc;
			w[i][1] = (2+i)*i;
			w[i][2] = (1+i)*(1+i);
		}
		// do it
		ghostIt(w);
	}
	
	/** Periodic BC (u(i=0) = u(i=N)). */
	void ghostsPeriodic() {
		if (sGS == 0) return;
		// upper bound for grid (position of point N)
		VecTypeI ub = this->mGridSize + sGst;
		Matrix& m = this->mM;
		// the low ones
		for (size_t iw1 = 0; iw1 < sGst; ++iw1) {
			const size_t i = sGst - 1 - iw1;
#if DIM == 2
			// -x (loop in y)
			for (size_t j = i+1; j < ub[1]; ++j) {
				m(i,j) = m(ub[0]-1-iw1, j);
			}
			// -y (loop in x)
			for (size_t j = i; j < ub[0]; ++j) {
				m(j,i) = m(j, ub[1]-1-iw1);		// m(i,i) filled here
			}
#elif DIM == 3
			// -x (loop in z,y)
			for (size_t k = i+1; k < ub[2]; ++k) {
				for (size_t j = i+1; j < ub[1]; ++j) {
					m(i,j,k) = m(ub[0]-1-iw1, j, k);
				}
			}
			// -y (loop in z,x)
			for (size_t k = i+1; k < ub[2]; ++k) {
				for (size_t j = i; j < ub[0]; ++j) {
					m(j,i,k) = m(j, ub[1]-1-iw1, k);	// m(i,i,k) filled here
				}
			}
			// -z (loop in z,y)
			for (size_t k = i; k < ub[2]; ++k) {
				for (size_t j = i; j < ub[0]; ++j) {
					m(j,k,i) = m(j, k, ub[1]-1-iw1);	// m(i,i,k) filled here
				}
			}
#endif
		}
		// the high ones
		for (size_t iw1 = 0; iw1 < sGen; ++iw1) {
			// get is
			size_t ix = ub[0] + iw1;
			size_t iy = ub[1] + iw1;
#if DIM == 2
			// -x (loop in y)
			for (size_t j = 0; j < iy; ++j) {
				m(ix,j) = m(sGst+iw1, j);
			}
			// -y (loop in x)
			for (size_t j = 0; j <= ix; ++j) {
				m(j,iy) = m(j, sGst+iw1);	// m(ix,iy) filled here
			}
#elif DIM == 3
			size_t iz = ub[2] + iw1;
			// -x (loop in z,y)
			for (size_t k = 0; k < iz; ++k) {
				for (size_t j = 0; j < iy; ++j) {
					m(ix,j,k) = m(sGst+iw1, j, k);
				}
			}
			// -y (loop in z,x)
			for (size_t k = 0; k < iz; ++k) {
				for (size_t j = 0; j <= ix; ++j) {
					m(j,iy,k) = m(j, sGst+iw1, k);	// m(ix,iy,k) filled here
				}
			}
			// -z (loop in z,y)
			for (size_t k = 0; k <= iy; ++k) {
				for (size_t j = 0; j <= ix; ++j) {
					m(j,k,iz) = m(j, k, sGst+iw1);	// m(ix,iy,iz) filled here
				}
			}
#endif
		}
	}
	
private:
	/**
	 * Generic ghost filler.
	 * - u_{-i-1} = -w[i][0] + sum(w[i][j]*u_j      , j = 1..M-1), i = 0..N-1
	 * - u_{N_+i} =  w[i][0] + sum(w[i][j]*u_{N-1-j}, j = 1..M-1), i = 0..N-1
	 * 
	 * @param weights	N x M array of weights, where N is >= max(sGst,sGse)
	 */
	template <size_t M, typename T>
	void ghostIt(T weights[][M]) {
		// upper bound for grid (position of point N)
		VecTypeI ub = this->mGridSize + sGst;
		Matrix& m = this->mM;
		// the low ones
		for (size_t iw1 = 0; iw1 < sGst; ++iw1) {
			T* w = &weights[iw1][0];
			const size_t i = sGst - 1 - iw1;
#if DIM == 2
			// -x (loop in y)
			for (size_t j = i+1; j < ub[1]; ++j) {
				Real val = w[0];
				for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(sGst-1+iw,j);
				m(i,j) = val;
			}
			// -y (loop in x)
			for (size_t j = i; j < ub[0]; ++j) {
				Real val = w[0];
				for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(j,sGst-1+iw);
				m(j,i) = val;	// m(i,i) filled here
			}
#elif DIM == 3
			// -x (loop in z,y)
			for (size_t k = i+1; k < ub[2]; ++k) {
				for (size_t j = i+1; j < ub[1]; ++j) {
					Real val = w[0];
					for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(sGst-1+iw,j,k);
					m(i,j,k) = val;
				}
			}
			// -y (loop in z,x)
			for (size_t k = i+1; k < ub[2]; ++k) {
				for (size_t j = i; j < ub[0]; ++j) {
					Real val = w[0];
					for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(j,sGst-1+iw,k);
					m(j,i,k) = val;	// m(i,i,k) filled here
				}
			}
			// -z (loop in z,y)
			for (size_t k = i; k < ub[2]; ++k) {
				for (size_t j = i; j < ub[0]; ++j) {
					Real val = w[0];
					for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(j,k,sGst-1+iw);
					m(j,k,i) = val;	// m(i,i,i) filled here
				}
			}
#endif
		}
		// the high ones
		for (size_t iw1 = 0; iw1 < sGen; ++iw1) {
			T* w = &weights[iw1][0];
			// get is
			size_t ix = ub[0] + iw1;
			size_t iy = ub[1] + iw1;
#if DIM == 2
			// -x (loop in y)
			for (size_t j = 0; j < iy; ++j) {
				Real val = w[0];
				for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(ub[0]-iw,j);
				m(ix,j) = val;
			}
			// -y (loop in x)
			for (size_t j = 0; j <= ix; ++j) {
				Real val = w[0];
				for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(j,ub[0]-iw);
				m(j,iy) = val;	// m(ix,iy) filled here
			}
#elif DIM == 3
			size_t iz = ub[2] + iw1;
			// -x (loop in z,y)
			for (size_t k = 0; k < iz; ++k) {
				for (size_t j = 0; j < iy; ++j) {
					Real val = w[0];
					for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(ub[0]-iw,j,k);
					m(ix,j,k) = val;
				}
			}
			// -y (loop in z,x)
			for (size_t k = 0; k < iz; ++k) {
				for (size_t j = 0; j <= ix; ++j) {
					Real val = w[0];
					for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(j,ub[1]-iw,k);
					m(j,iy,k) = val;	// m(ix,iy,k) filled here
				}
			}
			// -z (loop in z,y)
			for (size_t k = 0; k <= iy; ++k) {
				for (size_t j = 0; j <= ix; ++j) {
					Real val = w[0];
					for (size_t iw = 1; iw < M; ++iw) val += w[iw]*m(j,k,ub[2]-iw);
					m(j,k,iz) = val;	// m(ix,iy,iz) filled here
				}
			}
#endif
		}
	}
};
