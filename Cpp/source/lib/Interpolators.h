/**\file Interpolators.h
 * Collection of interpolators (models of #CInterpolator).
 *
 * #include "Interpolators.h"
 *
 * @author Gerardo Tauriello
 * @date 7/20/10
 * 
 * @see CInterpolator, GridGeneric, Kernels.h
 */

#pragma once

// project includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"
#include "Matrix.h"
#include "Kernels.h"

// system includes

// forward references

#pragma mark CONCEPTS
/**\addtogroup concepts
 * @{
 */
/**
 * Concept for an interpolator.
 *
 * This purely static class works on a matrix (usually a model of #CMatrix2D or
 * #CMatrix3D) and allows computation of values between grid points. Coordinates
 * of requested values are given in index space of a matrix that was extended by
 * the required ghost layer (i in [sGhostStart, matrixSize-sGhostEnd-1]).
 *
 * Interpolator takes care of choosing boundary condition handling and computing
 * u(x) = sum(W(x - i) * ui) and grad(u(x)) = sum(grad(W(x - i)) * ui).
 * Details on tensorial Kernels are in #CKernel.
 *
 * Boundary grid points can be taken care of explicitly or via ghost values.
 * If ghost values are required sGhostStart and sGhostEnd must be set > 0.
 *
 * Requirements:
 * - Static constants:
 *   - <code>static const size_t sGhostStart</code>\n
 *     Size of required ghost layer at low indices or 0 if none needed.
 *   - <code>static const size_t sGhostEnd</code>\n
 *     Size of required ghost layer at high indices or 0 if none needed.
 * - Static methods:
 *   - <code>static Matrix::ElementType scalar(Matrix, VecType pos)</code>\n
 *     Get value at given location in index space (pos in [sGhostStart, matrixSize-sGhostEnd-1])
 *   - <code>static VectorElement<Matrix::ElementType, DIM> gradient(Matrix, VecType pos, VecType invh)</code>\n
 *     Get gradient at given location in index space (pos in [sGhostStart, matrixSize-sGhostEnd-1]).
 *     Inverse of the grid spacing invh is required as we take the gradient in real space.
 * .
 * Example using type X to interpolate on matrix m of type M:
\code
VecType pos(sGhostStart + 0.5);
M::ElementType val = X::scalar(m, pos);
VectorElement<M::ElementType, DIM> X::gradient(m, pos, VecType(1/0.1));
\endcode
 *
 * To enforce concept for class X to interpolate on matrix M use
 * \code CONCEPT_ASSERT(CInterpolator<X,M>) \endcode
 * 
 * @see InterpolatorNoGhosts, CKernel
 */
template <class X, class Matrix>
class CInterpolator {
    
    CONCEPT_USAGE(CInterpolator) {
		// get required ghost sizes (both positive! 0 means no ghosts needed)
		// Matrix must allow access from -gs to nx-1+ge
		const size_t gs = X::sGhostStart;
		const size_t ge = X::sGhostEnd;
		
		// link to types
		typedef typename Matrix::ElementType ET;
		typedef VectorElement<ET, DIM> GT;
		VecType pos, invh;
		// pos in index space for given matrix!
		ET val = X::scalar(m, pos);
		GT n = X::gradient(m, pos, invh);
		ignore_unused_variable_warning(gs);
		ignore_unused_variable_warning(ge);
		ignore_unused_variable_warning(val);
    }
    
private:
    // dummy data (NEVER created)
	Matrix m;
};
/** @} */ // end of group concepts

#pragma mark INTERPOLATORS

/**
 * Generic interpolator with no ghosts (model of #CInterpolator).
 * This requires that we have at least Kernel::sSupport grid points!
 * Trick is that we move and adapt stencil s.t. we do not access points with 
 * i < 0 or i >= N.
 * @tparam Kernel	Model of #CKernelDirichlet.
 */
template <typename Kernel>
class InterpolatorNoGhosts {
	// check Kernel
	CONCEPT_ASSERT(CKernelDirichlet<Kernel>);
public:
	/** \name Concept-Implementation.
     * Implementation of #CInterpolator.
     */
    //@{
	static const size_t sGhostStart = 0;	// no ghosts needed
	static const size_t sGhostEnd = 0;		// no ghosts needed
	
	template <typename Matrix>
	static typename Matrix::ElementType scalar(const Matrix& m, const VecType pos) {
		// check Matrix
#if DIM == 2
		CONCEPT_ASSERT(CMatrix2D<Matrix>);
#elif DIM == 3
		CONCEPT_ASSERT(CMatrix3D<Matrix>);
#endif
		typedef typename Matrix::ElementType ET;
		ET result = 0;
		// stencil start and width
		const int KSS = Kernel::sStencilStart;
		const int KS = Kernel::sSupport;
		// floor(pos) (assume pos >= 0), t and weights w
		// x
		int i = pos[0];
		Real wx[KS];
		Kernel::weightsDirichlet(wx, pos[0] - i, i, m.getSizeX());
		// y
		int j = pos[1];
		Real wy[KS];
		Kernel::weightsDirichlet(wy, pos[1] - j, j, m.getSizeY());
#if DIM == 3
		int k = pos[2];
		Real wz[KS];
		Kernel::weightsDirichlet(wz, pos[2] - k, k, m.getSizeZ());
#endif
		
#if DIM == 2
		for (int jj = 0; jj < KS; ++jj) {
			for (int ii = 0; ii < KS; ++ii) {
				result += wx[ii]*wy[jj]*m(i+KSS+ii, j+KSS+jj);
			}
		}
#elif DIM == 3
		for (int kk = 0; kk < KS; ++kk) {
			for (int jj = 0; jj < KS; ++jj) {
				const Real w = wy[jj]*wz[kk];
				for (int ii = 0; ii < KS; ++ii) {
					result += wx[ii]*w*m(i+KSS+ii, j+KSS+jj, k+KSS+kk);
				}
			}
		}
#endif
		return result;
	}
	
	template <typename Matrix>
	static VectorElement<typename Matrix::ElementType, DIM> gradient(const Matrix& m, const VecType pos, const VecType invh) {
		// check Matrix
#if DIM == 2
		CONCEPT_ASSERT(CMatrix2D<Matrix>);
#elif DIM == 3
		CONCEPT_ASSERT(CMatrix3D<Matrix>);
#endif
		typedef typename Matrix::ElementType ET;
		typedef VectorElement<ET, DIM> GT;
		GT result = 0;
		// stencil start and width
		const int KSS = Kernel::sStencilStart;
		const int KS = Kernel::sSupport;
		// floor(pos) (assume pos >= 0), t and weights w
		// x
		int i = pos[0];
		Real wx[KS], dwx[KS];
		Kernel::weightsDirichletGrad(dwx, wx, pos[0] - i, i, m.getSizeX());
		// y
		int j = pos[1];
		Real wy[KS], dwy[KS];
		Kernel::weightsDirichletGrad(dwy, wy, pos[1] - j, j, m.getSizeY());
#if DIM == 3
		int k = pos[2];
		Real wz[KS], dwz[KS];
		Kernel::weightsDirichletGrad(dwz, wz, pos[2] - k, k, m.getSizeZ());
#endif
		
#if DIM == 2
		for (int jj = 0; jj < KS; ++jj) {
			for (int ii = 0; ii < KS; ++ii) {
				const ET tmp = m(i+KSS+ii, j+KSS+jj);
				result[0] += dwx[ii]*wy[jj]*tmp;
				result[1] += dwy[jj]*wx[ii]*tmp;
			}
		}
#elif DIM == 3
		for (int kk = 0; kk < KS; ++kk) {
			for (int jj = 0; jj < KS; ++jj) {
				const Real w0 = wy[jj]*wz[kk];
				const Real w1 = dwy[jj]*wz[kk];
				const Real w2 = wy[jj]*dwz[kk];
				for (int ii = 0; ii < KS; ++ii) {
					const ET tmp = m(i+KSS+ii, j+KSS+jj, k+KSS+kk);
					result[0] += dwx[ii]*w0*tmp;
					result[1] += w1*wx[ii]*tmp;
					result[2] += w2*wx[ii]*tmp;
				}
			}
		}
#endif
		// post
		for (int d = 0; d < DIM; ++d) {
			result[d] *= invh[d];
		}
		return result;
	}
	//@}
	
private:
	/** Purely static class, constructor hidden. */
	InterpolatorNoGhosts() {}
};

/**
 * Generic interpolator with ghosts (model of #CInterpolator).
 * Note that ghosts are not created or updated here!
 * Caller must make sure that we can access Matrix at floor(pos) - sGhostStart
 * and floor(pos) + sGhostEnd.
 * This should be faster than InterpolatorNoGhosts, but it requires more 
 * memory and additional code to update ghosts.
 * @tparam Kernel	Model of #CKernel.
 */
template <typename Kernel>
class InterpolatorGhosts {
	// check Kernel
	CONCEPT_ASSERT(CKernel<Kernel>);
public:
	/** \name Concept-Implementation.
     * Implementation of #CInterpolator.
     */
    //@{
	static const size_t sGhostStart = -Kernel::sStencilStart;
	static const size_t sGhostEnd = Kernel::sStencilEnd;
	
	template <typename Matrix>
	static typename Matrix::ElementType scalar(const Matrix& m, const VecType pos) {
		// check Matrix
#if DIM == 2
		CONCEPT_ASSERT(CMatrix2D<Matrix>);
#elif DIM == 3
		CONCEPT_ASSERT(CMatrix3D<Matrix>);
#endif
		typedef typename Matrix::ElementType ET;
		ET result = 0;
		// stencil start and width
		const int KSS = Kernel::sStencilStart;
		const int KS = Kernel::sSupport;
		// floor(pos) (assume pos >= 0), t and weights w
		// x
		const int i = pos[0];
		Real wx[KS];
		Kernel::weights(wx, pos[0] - i);
		// y
		const int j = pos[1];
		Real wy[KS];
		Kernel::weights(wy, pos[1] - j);
#if DIM == 3
		const int k = pos[2];
		Real wz[KS];
		Kernel::weights(wz, pos[2] - k);
#endif
		
#if DIM == 2
		for (int jj = 0; jj < KS; ++jj) {
			for (int ii = 0; ii < KS; ++ii) {
				result += wx[ii]*wy[jj]*m(i+KSS+ii, j+KSS+jj);
			}
		}
#elif DIM == 3
		for (int kk = 0; kk < KS; ++kk) {
			for (int jj = 0; jj < KS; ++jj) {
				const Real w = wy[jj]*wz[kk];
				for (int ii = 0; ii < KS; ++ii) {
					result += wx[ii]*w*m(i+KSS+ii, j+KSS+jj, k+KSS+kk);
				}
			}
		}
#endif
		return result;
	}
	
	template <typename Matrix>
	static VectorElement<typename Matrix::ElementType, DIM> gradient(const Matrix& m, const VecType pos, const VecType invh) {
		// check Matrix
#if DIM == 2
		CONCEPT_ASSERT(CMatrix2D<Matrix>);
#elif DIM == 3
		CONCEPT_ASSERT(CMatrix3D<Matrix>);
#endif
		typedef typename Matrix::ElementType ET;
		typedef VectorElement<ET, DIM> GT;
		GT result = 0;
		// stencil start and width
		const int KSS = Kernel::sStencilStart;
		const int KS = Kernel::sSupport;
		// floor(pos) (assume pos >= 0), t and weights w
		// x
		const int i = pos[0];
		Real wx[KS], dwx[KS];
		Kernel::weightsGrad(dwx, wx, pos[0] - i);
		// y
		const int j = pos[1];
		Real wy[KS], dwy[KS];
		Kernel::weightsGrad(dwy, wy, pos[1] - j);
#if DIM == 3
		const int k = pos[2];
		Real wz[KS], dwz[KS];
		Kernel::weightsGrad(dwz, wz, pos[2] - k);
#endif
		
#if DIM == 2
		for (int jj = 0; jj < KS; ++jj) {
			for (int ii = 0; ii < KS; ++ii) {
				const ET tmp = m(i+KSS+ii, j+KSS+jj);
				result[0] += dwx[ii]*wy[jj]*tmp;
				result[1] += dwy[jj]*wx[ii]*tmp;
			}
		}
#elif DIM == 3
		for (int kk = 0; kk < KS; ++kk) {
			for (int jj = 0; jj < KS; ++jj) {
				const Real w0 = wy[jj]*wz[kk];
				const Real w1 = dwy[jj]*wz[kk];
				const Real w2 = wy[jj]*dwz[kk];
				for (int ii = 0; ii < KS; ++ii) {
					const ET tmp = m(i+KSS+ii, j+KSS+jj, k+KSS+kk);
					result[0] += dwx[ii]*w0*tmp;
					result[1] += w1*wx[ii]*tmp;
					result[2] += w2*wx[ii]*tmp;
				}
			}
		}
#endif
		// post
		for (int d = 0; d < DIM; ++d) {
			result[d] *= invh[d];
		}
		return result;
	}
	//@}
	
private:
	/** Purely static class, constructor hidden. */
	InterpolatorGhosts() {}
};

/**
 * Generic interpolator with periodic boundaries (no ghosts needed) (model of #CInterpolator).
 * Assumes that pos + i > -N (with i in [Kernel::sStencilStart, Kernel::sStencilEnd].
 * Hard to judge speed of this one...
 * @tparam Kernel	Model of #CKernel.
 */
template <typename Kernel>
class InterpolatorPeriodic {
	// check Kernel
	CONCEPT_ASSERT(CKernel<Kernel>);
public:
	/** \name Concept-Implementation.
     * Implementation of #CInterpolator.
     */
    //@{
	static const size_t sGhostStart = 0;	// no ghosts needed
	static const size_t sGhostEnd = 0;		// no ghosts needed
	
	template <typename Matrix>
	static typename Matrix::ElementType scalar(const Matrix& m, const VecType pos) {
		// check Matrix
#if DIM == 2
		CONCEPT_ASSERT(CMatrix2D<Matrix>);
#elif DIM == 3
		CONCEPT_ASSERT(CMatrix3D<Matrix>);
#endif
		typedef typename Matrix::ElementType ET;
		ET result = 0;
		// stencil start and width
		const int KSS = Kernel::sStencilStart;
		const int KS = Kernel::sSupport;
		// floor(pos) (assume pos >= 0), t, nx and weights w
		// x
		const int i = pos[0];
		Real wx[KS];
		Kernel::weights(wx, pos[0] - i);
		const int nx = m.getSizeX();
		// y
		const int j = pos[1];
		Real wy[KS];
		Kernel::weights(wy, pos[1] - j);
		const int ny = m.getSizeY();
#if DIM == 3
		const int k = pos[2];
		Real wz[KS];
		Kernel::weights(wz, pos[2] - k);
		const int nz = m.getSizeZ();
#endif
		
#if DIM == 2
		for (int jj = 0; jj < KS; ++jj) {
			const int jm = (j+KSS+jj + ny) % ny;
			for (int ii = 0; ii < KS; ++ii) {
				const int im = (i+KSS+ii + nx) % nx;
				result += wx[ii]*wy[jj]*m(im, jm);
			}
		}
#elif DIM == 3
		for (int kk = 0; kk < KS; ++kk) {
			const int km = (k+KSS+kk + nz) % nz;
			for (int jj = 0; jj < KS; ++jj) {
				const int jm = (j+KSS+jj + ny) % ny;
				const Real w = wy[jj]*wz[kk];
				for (int ii = 0; ii < KS; ++ii) {
					const int im = (i+KSS+ii + nx) % nx;
					result += wx[ii]*w*m(im, jm, km);
				}
			}
		}
#endif
		return result;
	}
	
	template <typename Matrix>
	static VectorElement<typename Matrix::ElementType, DIM> gradient(const Matrix& m, const VecType pos, const VecType invh) {
		// check Matrix
#if DIM == 2
		CONCEPT_ASSERT(CMatrix2D<Matrix>);
#elif DIM == 3
		CONCEPT_ASSERT(CMatrix3D<Matrix>);
#endif
		typedef typename Matrix::ElementType ET;
		typedef VectorElement<ET, DIM> GT;
		GT result = 0;
		// stencil start and width
		const int KSS = Kernel::sStencilStart;
		const int KS = Kernel::sSupport;
		// floor(pos) (assume pos >= 0), t and weights w
		// x
		const int i = pos[0];
		Real wx[KS], dwx[KS];
		Kernel::weightsGrad(dwx, wx, pos[0] - i);
		const int nx = m.getSizeX();
		// y
		const int j = pos[1];
		Real wy[KS], dwy[KS];
		Kernel::weightsGrad(dwy, wy, pos[1] - j);
		const int ny = m.getSizeY();
#if DIM == 3
		const int k = pos[2];
		Real wz[KS], dwz[KS];
		Kernel::weightsGrad(dwz, wz, pos[2] - k);
		const int nz = m.getSizeZ();
#endif
		
#if DIM == 2
		for (int jj = 0; jj < KS; ++jj) {
			const int jm = (j+KSS+jj + ny) % ny;
			for (int ii = 0; ii < KS; ++ii) {
				const int im = (i+KSS+ii + nx) % nx;
				const ET tmp = m(im, jm);
				result[0] += dwx[ii]*wy[jj]*tmp;
				result[1] += dwy[jj]*wx[ii]*tmp;
			}
		}
#elif DIM == 3
		for (int kk = 0; kk < KS; ++kk) {
			const int km = (k+KSS+kk + nz) % nz;
			for (int jj = 0; jj < KS; ++jj) {
				const int jm = (j+KSS+jj + ny) % ny;
				const Real w0 = wy[jj]*wz[kk];
				const Real w1 = dwy[jj]*wz[kk];
				const Real w2 = wy[jj]*dwz[kk];
				for (int ii = 0; ii < KS; ++ii) {
					const int im = (i+KSS+ii + nx) % nx;
					const ET tmp = m(im, jm, km);
					result[0] += dwx[ii]*w0*tmp;
					result[1] += w1*wx[ii]*tmp;
					result[2] += w2*wx[ii]*tmp;
				}
			}
		}
#endif
		// post
		for (int d = 0; d < DIM; ++d) {
			result[d] *= invh[d];
		}
		return result;
	}
	//@}
	
private:
	/** Purely static class, constructor hidden. */
	InterpolatorPeriodic() {}
};

/**
 * Interpolator using nearest neighbor (model of #CInterpolator).
 * No ghosts are needed for this one!
 * Normal is discontinuous and 0 almost everywhere.
 */
class InterpolatorNearest {
public:
	/** \name Concept-Implementation.
     * Implementation of #CInterpolator.
     */
    //@{
	static const size_t sGhostStart = 0;	// no ghosts needed
	static const size_t sGhostEnd = 0;		// no ghosts needed
	
	template <typename Matrix>
	static typename Matrix::ElementType scalar(const Matrix& m, const VecType pos) {
		// check Matrix
#if DIM == 2
		CONCEPT_ASSERT(CMatrix2D<Matrix>);
#elif DIM == 3
		CONCEPT_ASSERT(CMatrix3D<Matrix>);
#endif
        // assume pos > 0 => we can round by truncation
        // => if pos in (0,matrixsize) so will i,j,k
		const size_t i = pos[0] + Real(0.5);
		const size_t j = pos[1] + Real(0.5);
#if DIM == 2
        return m(i,j);
#elif DIM == 3
		const size_t k = pos[2] + Real(0.5);
        return m(i,j,k);
#endif
	}
	
	template <typename Matrix>
	static VectorElement<typename Matrix::ElementType, DIM> gradient(const Matrix& m, const VecType pos, const VecType invh) {
		// check Matrix
#if DIM == 2
		CONCEPT_ASSERT(CMatrix2D<Matrix>);
#elif DIM == 3
		CONCEPT_ASSERT(CMatrix3D<Matrix>);
#endif
		typedef typename Matrix::ElementType ET;
		typedef VectorElement<ET, DIM> GT;
        // step-function => normal is 0 almost everywhere
        return GT(0);
	}
	//@}
	
private:
	/** Purely static class, constructor hidden. */
	InterpolatorNearest() {}
};
