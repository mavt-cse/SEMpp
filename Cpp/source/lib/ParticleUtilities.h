/**
 * Some specific Utilities for particle library.
 *
 * #include "ParticleUtilities.h"
 *
 * Any useful helper function that doesn't belong anywhere else.
 *
 * @author Gerardo Tauriello
 * @date 2/11/10
 */

#pragma once

// system includes
#include <cmath>
#include <vector>

// library includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"
#include "BoundingBox.h"

class ParticleUtilities {
public:
#pragma mark LIFECYCLE
private:
    
	/**
	 * Prevent use of constructor.
	 */
	ParticleUtilities();

#pragma mark OPERATIONS
    
    
};

#pragma mark GLOBAL FUNCTIONS
/**
 * Returns true if A => B (A implies B).
 * Useful for assertions.
 * Alternative to writing <code>A?B:true</code>.
 */
#define _implies(A, B) !(A) || (B)

