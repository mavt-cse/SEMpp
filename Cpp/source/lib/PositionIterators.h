/**\file PositionIterators.h
 * Collection of iterators going through a set of positions.
 *
 * #include "PositionIterators.h"
 *
 * Idea is to be able to "return" a list of positions on which to create
 * particles. These iterators are tailored for convenience and NOT efficiency.
 * So use them only for initializing particles.
 * All these classes are models of #CValueIterator with ValueType = VecType.
 *
 * @author Gerardo Tauriello
 * @date 2/8/10
 */

#pragma once

// system includes
#include <cmath>

// library includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"
#include "BoundingBox.h"

/**
 * Put N random positions inside a given surface.
 * @tparam Surface  Model of #CSurface.
 */
template <typename Surface>
class RandomPositions {
    CONCEPT_ASSERT(CSurface<Surface>);
    
#pragma mark LIFECYCLE
public:
    /** Default constructor. */
    RandomPositions(const Surface surface, const int N
                    ): mSurface(surface), mSize(N) { 
        // store relevant stuff from bbox
        mOffset = surface.getBoundingBox().offset;
        mLength = surface.getBoundingBox().size.max();
    }
    
    // default destructor, copy constructor and assignment operator used
    
#pragma mark CIterator
    
    /** \name Concept-Implementation.
     * Implementation of #CIterator.
     */
    //@{
    typedef VecType ValueType;
    void first() { mCurrent = 0; sample(); }
    void next() { ++mCurrent; sample(); }
    bool isDone() const { return mCurrent >= mSize; }
    ValueType currentItem() const { return mCurrentItem; }
    //@}
    
#pragma mark HELPERS
protected:
    /** Sample new position. */
    void sample() {
        // don't waste time if done
        if (isDone()) return;
        // sample until one found within bbox
        do {
            mCurrentItem = mOffset + VE_Utilities::Random<VecType>() * mLength;
            // sample uniform in [0,mLength] in all dimensions
        } while (!mSurface.isInside(mCurrentItem));
    }
    
protected:
    /** Surface to be filled. */
    Surface mSurface;
    /** Offset of bounding box. */
    VecType mOffset;
    /** Maximal length of bounding box. */
    Real mLength;
    /** Number of elements to create. */
    int mSize;
    /** Counter for current position. */
    int mCurrent;
    /** Current position. */
    VecType mCurrentItem;
};

/**
 * Put positions on a regular grid inside a given surface.
 *
 * Grid splits up bounding box of given surface into cells of given size and
 * returns cell-centers if they are within the surface.
 * Works best if bbox.size = c*mH where c is an integer.
 * For a CubeSurface this should do the job just nicely and in a predictable way
 * but for other geometries the number of positions may be hard to predict.
 * Order of traversal: x,y,z
 *
 * @tparam Surface  Model of #CSurface.
 */
template <typename Surface>
class GridPositions {
    CONCEPT_ASSERT(CSurface<Surface>);
    
#pragma mark LIFECYCLE
public:
    /** Default constructor. */
    GridPositions(const Surface surface, const VecType h
                  ): mSurface(surface), mH(h) { 
        // store relevant stuff from bbox
        mOffset = surface.getBoundingBox().offset + mH*Real(0.5);
        mMax = surface.getBoundingBox().offset + surface.getBoundingBox().size;
    }
    
    // default destructor, copy constructor and assignment operator used
    
#pragma mark CIterator
    
    /** \name Concept-Implementation.
     * Implementation of #CIterator.
     */
    //@{
    typedef VecType ValueType;
    void first() { mCurrentItem = mOffset; findItem(); }
    void next() { mCurrentItem[0] += mH[0]; findItem(); }
    bool isDone() const { return mCurrentItem[DIM-1] >= mMax[DIM-1]; }
    ValueType currentItem() const { return mCurrentItem; }
    //@}
    
#pragma mark HELPERS
protected:
    /** 
     * Find legal item on current location or later. 
     * @post (mCurrentItem[:] < mMax[:] and mSurface.isInside(mCurrentItem)) or isDone().
     */
    void findItem() {
        // checkLine takes care of all dimensions!!
        checkLine(0);
        // now we can guarantee a legal position
        while (!isDone() && !mSurface.isInside(mCurrentItem)) {
            // advance on lowest dimension
            mCurrentItem[0] += mH[0];
            checkLine(0);
        }
    }
    
    /**
     * Check for new line along given dimension (and any larger ones).
     * @pre Either i=0 or called from checkLine(i-1)
     * @post mCurrentItem[i..] < mMax[i..] or isDone().
     * @return true if mCurrentItem was changed.
     */
    bool checkLine(int i) {
        // ignore last dimension (used for isDone)
        if (i < DIM-1 && mCurrentItem[i] >= mMax[i]) {
            mCurrentItem[i] = mOffset[i];   // note: smaller i already done
            mCurrentItem[i+1] += mH[i+1];
            checkLine(i+1);
            return true;
        } else {
            return false;
        }
    }
    
protected:
    /** Surface to be filled. */
    Surface mSurface;
    /** Offset of bounding box. */
    VecType mOffset;
    /** Maximal coordinates of bounding box. */
    VecType mMax;
    /** Spacing between particles. */
    VecType mH;
    /**
     * Current position.
     * Done: mCurrentItem[DIM-1] > mMax[DIM-1]
     * if not isDone() and after call of first():
     * - mCurrentItem[:] < mMax[:]
     * - mSurface.isInside(mCurrentItem)
     */
    VecType mCurrentItem;
};

/**
 * Put positions on a hexagonal grid inside a given surface.
 * Only 2D for now!
 * Works best if bbox.size = c*(mH,mH*sqrt(3)/2) where c is an integer.
 * Order of traversal: x,y
 *
 * Logic:
 * - Positions: x = h*(i + (j%2)/2), y = h*i*sqrt(3)/2
 * - Neighbors of i,j: (i-1,j), (i,j-1), (i,j+1), (i+1,j-1), (i+1,j), (i+1,j+1)
 *
 * @tparam Surface  Model of #CSurface.
 */
template <typename Surface>
class HexPositions {
    CONCEPT_ASSERT(CSurface<Surface>);
    
#pragma mark LIFECYCLE
public:
    /** Default constructor. */
    HexPositions(const Surface surface, const Real h
                 ): mSurface(surface), mH(h) { 
        // init
        mHY = mH * std::sqrt(3)/2;
        flagY = false;
        // store relevant stuff from bbox
        mOffset = surface.getBoundingBox().offset + VecType(mH/2, mHY/2);
        mMax = surface.getBoundingBox().offset + surface.getBoundingBox().size;
    }
    
    // default destructor, copy constructor and assignment operator used
    
#pragma mark CIterator
    
    /** \name Concept-Implementation.
     * Implementation of #CIterator.
     */
    //@{
    typedef VecType ValueType;
    void first() { mCurrentItem = mOffset; findItem(); }
    void next() { mCurrentItem[0] += mH; findItem(); }
    bool isDone() const { return mCurrentItem[DIM-1] >= mMax[DIM-1]; }
    ValueType currentItem() const { return mCurrentItem; }
    //@}
    
#pragma mark HELPERS
protected:
    /** 
     * Find legal item on current location or later. 
     * @post (mCurrentItem[:] < mMax[:] and mSurface.isInside(mCurrentItem)) or isDone().
     */
    void findItem() {
        // checkLine takes care of all dimensions!!
        checkLine(0);
        // now we can guarantee a legal position
        while (!isDone() && !mSurface.isInside(mCurrentItem)) {
            // advance on lowest dimension
            mCurrentItem[0] += mH;
            checkLine(0);
        }
    }
    
    /**
     * Check for new line along given dimension (and any larger ones).
     * @pre Either i=0 or called from checkLine(i-1)
     * @post mCurrentItem[i..] < mMax[i..] or isDone().
     * @return true if mCurrentItem was changed.
     */
    bool checkLine(int i) {
        // note: ONLY for 2D for now
        assert(i==0);
        // ignore last dimension (used for isDone)
        if (i < DIM-1 && mCurrentItem[i] >= mMax[i]) {
            mCurrentItem[i] = mOffset[i];
            flagY = !flagY;
            // every other row different offset
            if (flagY) mCurrentItem[i] += mH/2;
            // update row too
            mCurrentItem[i+1] += mHY;
            //checkLine(i+1);
            return true;
        } else {
            return false;
        }
    }
    
protected:
    /** Surface to be filled. */
    Surface mSurface;
    /** Offset of bounding box. */
    VecType mOffset;
    /** Maximal coordinates of bounding box. */
    VecType mMax;
    /** Spacing between particles. */
    Real mH;
    /** Spacing between rows. */
    Real mHY;
    /** Flag for row changes (init: false). */
    bool flagY;
    /**
     * Current position.
     * Done: mCurrentItem[DIM-1] >= mMax[DIM-1]
     * if not isDone() and after call of first():
     * - mCurrentItem[:] < mMax[:]
     * - mSurface.isInside(mCurrentItem)
     */
    VecType mCurrentItem;
};
