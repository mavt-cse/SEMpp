/**\file Operators.h
 * Collection of simple operators (models of #COperator).
 * Note that some of these operators are mainly for convenience and may have
 * sub-optimal performance.
 * Oh and please just use genericOperator() and do not look at the hidden
 * complexity of the implementation.
 * 
 * #include "Operators.h"
 *
 * @author Gerardo Tauriello
 * @see #COperator
 */

#pragma once

// library includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"

// forward declarations
template <typename NH, typename Particle, typename NHIteratorType >
class GenericOperatorNH;
template <typename Particle>
class GenericOperator;

/**
 * Create a model of #COperator executing a given function (with neighborhood).
 * Say you have:
 * - a variable nhMap of type NH which is a model of #CNeighborhoodMap
 * - a particle type ParticleType which has a templated method with signature
 *   <code>template < typename Iterator > void computeRHS(Iterator)</code>
 *   where \a NH::IteratorType is a valid choice for the type \a Iterator.
 * .
 * Then you can get an operator executing computeRHS on each particle with:
 \code genericOperator(&Particle::computeRHS<NH::IteratorType>, nhMap) \endcode
 * 
 * In the simpler case that the signature was <code>void computeRHS(NH::IteratorType)</code>
 * you can write:
 \code genericOperator(&Particle::computeRHS, nhMap) \endcode
 */
template <typename NH, typename Particle, typename NHIteratorType>
inline GenericOperatorNH<NH,Particle,NHIteratorType>
genericOperator(void (Particle::* func)(NHIteratorType), const NH& nhMap) {
    return GenericOperatorNH<NH,Particle,NHIteratorType>(func, nhMap);
}

/**
 * Create a model of #COperator executing a given function (without neighborhood).
 * Say you have:
 * - a particle type ParticleType which has a method with signature
 *   <code>void update()</code>
 * .
 * Then you can get an operator executing undate on each particle with:
 \code genericOperator(&Particle::update) \endcode
 */
template <typename Particle>
inline GenericOperator<Particle>
genericOperator(void (Particle::* func)()) {
    return GenericOperator<Particle>(func);
}

/** Operator created by genericOperator() for use with neighborhood map. */
template <typename NH, typename Particle, typename NHIteratorType >
class GenericOperatorNH {
    // FRIENDS
    friend GenericOperatorNH<NH,Particle,NHIteratorType>
    genericOperator <> (void (Particle::* func)(NHIteratorType), const NH& nhMap);
    
    // concepts
    CONCEPT_ASSERT(CNeighborhoodMap<NH>);
    
    /**
     * Define type Func for member-function-pointer working on Particle-type
     * and taking Iterator-type as input.
     */
    typedef void (Particle::* Func)(NHIteratorType);
    /** Storage for function-pointer. */
    Func mFunc;
    /** Storage for ref to neighborhood map. */
    const NH& mrNhMap;
    
    /** Default constructor filling mFunc and mrNhMap. */
    GenericOperatorNH(Func func, const NH& nhMap): mFunc(func), mrNhMap(nhMap) { }
    
public:
    
    // no public constructors
    // default copy constructor and destructor used
    // assignment operator will fail due to read-only reference
    
    /** THE Operator. */
    template <typename Iterator>
    void operator()(Iterator iter) const {
        // assert concepts
        CONCEPT_ASSERT(CIterator<Iterator>);
        CONCEPT_ASSERT(CSameType<Particle, typename Iterator::ValueType>);
        // go through them
        for (iter.first(); !iter.isDone(); iter.next()) {
            Particle& part = *(iter.currentItem());
            (part.*mFunc)(mrNhMap.getHood(part));
            // NOTES:
            // - even virtual calls would work here!
            // - impossible to inline
        }
    }
};

/** Operator created by genericOperator() for use without neighborhood map. */
template <typename Particle>
class GenericOperator {
    // FRIENDS
    friend GenericOperator<Particle>
    genericOperator <> (void (Particle::* func)());
    
    /**
     * Define type Func for member-function-pointer working on Particle-type.
     */
    typedef void (Particle::* Func)();
    /** Storage for function-pointer. */
    Func mFunc;
    
    /** Default constructor filling mFunc. */
    GenericOperator(Func func): mFunc(func) { }
    
public:
    
    // no public constructors
    // default copy constructor and destructor used
    
    /** THE Operator. */
    template <typename Iterator>
    void operator()(Iterator iter) const {
        // assert concepts
        CONCEPT_ASSERT(CIterator<Iterator>);
        CONCEPT_ASSERT(CSameType<Particle, typename Iterator::ValueType>);
        // go through them
        for (iter.first(); !iter.isDone(); iter.next()) {
            Particle& part = *(iter.currentItem());
            (part.*mFunc)();
            // NOTES:
            // - even virtual calls would work here!
            // - impossible to inline
        }
    }
};

/**
 * Handles periodic BC by forcing particles back in domain.
 * Note that particle type must be model of #CBasicParticle and additionally
 * have setPos(VecType) method to update position (same req as for CellList).
 */
class OperatorPeriodic {
protected:
    /** Domain origin. */
    VecType mMinD;
    /** Domain size. */
    VecType mSizeD;
    /** Precompute mMinD + mSizeD. */
    VecType mMaxD;
public:
    /** Default constructor. */
    OperatorPeriodic(const VecType& minD, const VecType& sizeD):
    	mMinD(minD), mSizeD(sizeD), mMaxD(minD + sizeD) {
    }
    
    // no constructor with no arguments
    // default copy constructor and destructor used
    // assignment operator will fail due to read-only reference
    
    /** THE Operator. */
    template <typename Iterator>
    void operator()(Iterator iter) const {
        // assert concepts
        CONCEPT_ASSERT(CIterator<Iterator>);
        CONCEPT_ASSERT(CBasicParticle<typename Iterator::ValueType>);
        // go through them
        for (iter.first(); !iter.isDone(); iter.next()) {
            typename Iterator::ValueType& part = *(iter.currentItem());
            
            // handle periodicity
            VecType pos = part.getPos();
            for (int i = 0; i < DIM; ++i) {
	            if (pos[i] < mMinD[i]) {
                	pos[i] += mSizeD[i];
                } else if (pos[i] >= mMaxD[i]) {
                	pos[i] -= mSizeD[i];
                }
            }
            
            // apply it
            part.setPos(pos);
        }
    }
};
