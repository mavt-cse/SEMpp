/**
 * Setup for tbb.
 * Make sure to compile TbbSetup.cpp when using tbb!
 * Static singleton TbbSetup::sStartup starts up tbb automagically before main 
 * function.
 * Number of threads can be controlled by defining NTHREADS in compiler 
 * settings. By default it is set to the number of cores.
 * If NTHREADS is not given you can still set TBB_THREADS at runtime.
 *
 * #include "TbbSetup.h"
 *
 * @author Gerardo Tauriello
 */

#pragma once

// library includes
#include "Environment.h"

// whole header ignored when no tbb
#if USE_TBB == 1

// tbb includes
#include "tbb/task_scheduler_init.h"
#include "tbb/tbb_thread.h" 

// system includes
#include <assert.h>
#include <cstdlib>

class TbbSetup {
    tbb::task_scheduler_init scheduler;
    
	TbbSetup(): scheduler(tbb::task_scheduler_init::deferred) {
#ifndef NTHREADS
        // try to get it from the environment on the fly
        char * threads = getenv("TBB_THREADS");
        int num_threads = 0;
        if (threads != NULL) {
            // try to extract num_threads (gives 0 if fail)
            num_threads = atoi(threads);
        }
        if (num_threads > 0) {
            sNThreads = num_threads;
        } else {
            sNThreads = tbb::task_scheduler_init::default_num_threads();
        }
#else
        sNThreads = NTHREADS;
#endif
        scheduler.initialize(sNThreads);
		printf("TBB started with %d threads.\n", sNThreads);
        // make sure this is only used exactly once
		assert(sTicket==0);
		sTicket++;
	}
    
    /** Singleton object (created before main). */
	static TbbSetup sStartup;
	static int sTicket;
    static int sNThreads;
    
public:
    /** Get number of threads. */
    static int getNumThreads() { return sNThreads; }
    
    /** Get id of this thread. */
    static tbb::tbb_thread::id getID() { return tbb::this_tbb_thread::get_id(); }
};

#endif  // USE_TBB