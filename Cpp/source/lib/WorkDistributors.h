/**\file WorkDistributors.h
 * Collection of work distributors (models of #CWorkDistributor).
 *
 * #include "WorkDistributors.h"
 *
 * \todo Simple example using this with the predefined & custom operators
 *
 * @author Gerardo Tauriello
 * @see CWorkDistributor, \ref sph_st_client_sec
 */

#pragma once

// library includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"

#if USE_TBB == 1
// tbb includes
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"
#endif  // USE_TBB

/**
 * Dummy work distributor (the simplest model of #CWorkDistributor).
 * Just processes the whole bunch of particles.
 * 
 * @tparam Iterator     A model of #CIterator
 */
template <typename Iterator>
class DummyDistributor {
    CONCEPT_ASSERT(CIterator<Iterator>);
    
protected:
    /** Storage for copy of iterator. */
    Iterator mIterator;
    
public:
    /** Construct with a given iterator over all particles (copied). */
    DummyDistributor(const Iterator& iter): mIterator(iter) { }
    
    // no constructor with no arguments
    // default copy constructor, assignment operator and destructor used
    
    /**
     * Process by just passing the full iterator to the operator.
     * @param body  Model of #COperator doing the work.
     */
    template <typename Operator>
    void process(const Operator& body) {
        CONCEPT_ASSERT(COperator<Operator, Iterator>);
        body(mIterator);
    }
};

#if USE_TBB == 1
/**
 * Parallel work distributor using tbb threads (model of #CWorkDistributor).
 * Splits up the work using parallel_for.
 * 
 * @tparam Iterator     A model of #CIterator
 */
template <typename Iterator>
class TbbDistributor {
    CONCEPT_ASSERT(CIterator<Iterator>);
    
protected:
    /** Storage for copy of iterator. */
    Iterator mIterator;
    /** Granularity used for splitting up work (<0 = automatic). */
    int mGranularity;
    
    /** Functor wrapping block operator into tbb-style operator. */
    template <typename Operator>
    class OperatorWrapper {
    protected:
        const Operator& mrBody;
        const Iterator& mrIterator;
    public:
        /** Default constructor. */
        OperatorWrapper(const Operator& body, const Iterator& iterator): 
            mrBody(body), mrIterator(iterator) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** tbb-style operator. */
        template <typename Range>
        void operator()(const Range& r) const {
            // get sub iterator
            typedef typename Iterator::SubIteratorType SIT;
            SIT iter = mrIterator.getSubIterator(r.begin(), r.size());
            // call block operator
            mrBody(iter);
        }
    };
    
public:
    /**
     * Default constructor.
     * @param iter          Iterator over all particles (copied).
	 * @param granularity   Granularity for the parallel_for (see tbb-doc).
	 *                      Optional: if not set, auto_partitioner (see tbb-doc) will be used.
     */
    TbbDistributor(const Iterator& iter, const int granularity = -1): 
        mIterator(iter), mGranularity(granularity) { }
    
    // no constructor with no arguments
    // default copy constructor, assignment operator and destructor used
    
    /**
     * Use parallel_for (see tbb-doc) to split up work.
     * @param body  Model of #COperator doing the work.
     */
    template <typename Operator>
    void process(const Operator& body) {
        CONCEPT_ASSERT(COperator<Operator, Iterator>);
        
        // split up work
        const bool bAutomatic = mGranularity<0;
		if (bAutomatic) {
            tbb::parallel_for(tbb::blocked_range<size_t>(0, mIterator.size()),
                              OperatorWrapper<Operator>(body, mIterator),
                              tbb::auto_partitioner());
        } else {
            tbb::parallel_for(tbb::blocked_range<size_t>(0, mIterator.size(), mGranularity),
                              OperatorWrapper<Operator>(body, mIterator) );
        }
    }
};
#endif  // USE_TBB
