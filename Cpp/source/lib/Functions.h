/**
 * Collection of unsorted functions.
 * This include kernel functions, smoother etc.
 * 
 * #include "Functions.h"
 *
 * There's no use to create instances of this class, since all features are 
 * static.
 *
 * @author Gerardo Tauriello
 */

#pragma once

// library includes
#include <cmath>
#include "Environment.h"                // Particle lib environment

class Functions {
public:
    /**
     * M4 spline (cubic spline from Monaghan-2005, not to be confused with M'4).
     * Result is > 0 for d/h < 2.
     * @param d     Distance vector x_a - x_b
     * @param h     Smoothing length
     */
    static Real M4spline(const Real d, const Real h) {
        // scaling for different dims
#if DIM == 1
        const Real alpha_d = 1/(6*h);
#elif DIM == 2
        const Real alpha_d = 5/(14*M_PI*h*h);	// note: Monaghan had typo there!
#elif DIM == 3
        const Real alpha_d = 1/(4*M_PI*h*h*h);
#endif
        const Real R = d/h;
        // compute it
        if (R < 1) {
            return alpha_d * (4 - R*R*(6 - 3*R));
        } else if (R < 2) {
            const Real tmp = 2-R;
            return alpha_d * tmp*tmp*tmp;
        } else {
            return 0;
        }
    }
    
    /**
     * Wrapper for gradM4spline(VecType, Real, Real).
     */
    static VecType gradM4spline(const VecType dx, const Real h) {
        return gradM4spline(dx, h, dx.abs());
    }
    
    /**
     * Gradient (grad_a) of M4 spline (x_a-x_b).
     * Result is a*dx with a < 0 for d/h < 2 and a = 0 otherwise.
     * Note: r = 0 results in 0-vector.
     * @param dx    Distance vector x_a - x_b
     * @param h     Smoothing length
     * @param r     Precomputed length of dx
     */
    static VecType gradM4spline(const VecType dx, const Real h, const Real r) {
        // scaling for different dims
#if DIM == 1
        const Real alpha_d = 1/(6*h);
#elif DIM == 2
        const Real alpha_d = 5/(14*M_PI*h*h);	// note: Monaghan had typo there!
#elif DIM == 3
        const Real alpha_d = 1/(4*M_PI*h*h*h);
#endif
        const Real R = r / h;
        // compute it
        if (R < 1) {
            return (-alpha_d * (12 - 9*R) / (h*h)) * dx;
        } else if (R < 2) {
            const Real tmp = 2-R;
            return (-alpha_d * 3 * tmp*tmp / (h*r)) * dx;
        } else {
            return VecType(0);
        }
    }
    
    /**
     * Wrapper for springForce(VecType, Real, Real, Real).
     */
    static VecType springForce(const VecType dx, const Real k, const Real req) {
        return springForce(dx, k, req, dx.abs());
    }
    
    /**
     * Force of a linear spring.
     * Note that this is the force vector and not the energy-gradient. That is
     * it corresponds to -grad(U) where U is the spring potential.
     *
     * @param dx    Distance vector x_a - x_b
     * @param k     Spring constant
     * @param req   Equilibrium distance of spring
     * @param r     Precomputed length of vec_ab.
     */
    static VecType springForce(const VecType dx, const Real k, const Real req, const Real r) {
        if (r == 0) {
            // try to handle this (amplitude is well defined so we just choose random orientation)
            const Real amp = k*req;
#if DIM==2
            return VE_Utilities::OrientRand2D<Real>() * amp;
#elif DIM==3
            return VE_Utilities::OrientRand3D<Real>() * amp;
#endif
        } else {
            return (-k*(1-req/r)) * dx;
        }
    }
	
	/**
	 * Generate a normal distributed random variable in N(0,1).
     * For mean m and std s you do m + s*randNorm().
     * Result is guaranteed to be < Inf (actually < sqrt(2*log(RAND_MAX+1)) < 7 (with 32bit int)).
	 */
	static Real randNorm(){
		Real ur1 = ((Real)rand() + 1) / ((Real)RAND_MAX + 1);
		Real ur2 = (Real)rand()/(Real)RAND_MAX;
		// Box Muller
		return std::sqrt(-2*std::log(ur1))*std::cos(2*M_PI*ur2);
	}		
    
private:
    
	/**
	 * Prevent use of constructor.
	 */
	Functions();
};