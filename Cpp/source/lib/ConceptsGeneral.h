/**\defgroup concepts Concepts
 * Concepts are used to describe interfaces and to enforce them.
 * A class or method method with a templated argument X can check whether
 * X is a model of some concept using #CONCEPT_ASSERT.
 * @{
 */

#pragma once

/**\file ConceptsGeneral.h
 * Collection of general concepts.
 * 
 * #include "ConceptsGeneral.h"
 * 
 * @author Gerardo Tauriello
 */

// library includes
#include "ConceptAssert.h"

/** 
 * Concept for 2 types which need to match exactly.
 */
template <class T1, class T2>
class CSameType {
    
    CONCEPT_USAGE(CSameType) {
        sameType(t1, t2);
    }
    
private:
    // dummy data (NEVER created)
    T1 t1;
    T2 t2;
    
    // Helper enforcing that they are equal
    template <typename T>
    static void sameType(T const&, T const&);
};

/**
 * Concept for casts from Source to Target.
 * Requirements:
 * - One can explicitly cast from Source to Target (i.e. (Target)source)
 */
template< typename Source, typename Target >
class CCastableTo {
    
    CONCEPT_USAGE(CCastableTo) {
        // We must be able to cast from Source to Target
        (Target)s;
    }
    
private:
    // dummy data (NEVER created)
    Source s;
};

/**
 * Concept for assignments from Source to Target (without cast).
 * Requirements:
 * - Target can be constructed using a Source (i.e. Target target = source).
 * - One can assign Source to Target (i.e. target = source)
 */
template< typename Source, typename Target >
class CAssignableTo {
    
    CONCEPT_USAGE(CAssignableTo) {
        // We can create a Target out of a Source
        Target t = s;
        // We can assign a Source to a Target
        t = s;
    }
    
private:
    // dummy data (NEVER created)
    Source s;
};

/**
 * Concept for STL concept Assignable.
 * Requirements:
 * - X must have copy constructor and assignment operator (taking X as rhs) 
 *   (compiler-generated ones may suffice)
 */
template< typename X >
class CAssignable {
    
    CONCEPT_USAGE(CAssignable) {
        // Check copy constructor and assignment operator
        X x2(x);
        x2 = x;
    }
    
private:
    // dummy data (NEVER created)
    X x;
};

/**
 * Concept for generic iterator.
 *
 * The idea is to be able to iterate through an unknown data representation and
 * return items which can be dereferenced to a certain type (ValueType).
 *
 * Required properties for class X to be a model of CIterator concept:
 * - Copy constructor so we may pass iterator by value to methods
 * - Associated types:
 *   - <code>ValueType</code> - The type of the value obtained by dereferencing
 *                              result of currentItem().
 * - Methods:
 *   - <code>void first()</code> - Starts iterator at first item
 *   - <code>void next()</code> - Advances iterator to next item
 *   - <code>bool isDone()</code> - Return true if iterator reached end
 *   - <code>ValueType* currentItem()</code> - Return current item
 * .
 * Note that assignability is NOT required so we may use consts and refs in type.
 * 
 * Example using variable iter of type X:
\code
for (iter.first(); !iter.isDone(); iter.next()) {
    X::ValueType* item = iter.currentItem();
    // or X::ValueType& item = *(iter.currentItem());
    // work on element with *item or item->...
}
\endcode
 * 
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CIterator<X>) \endcode
 * 
 * Based on: http://www.lrde.epita.fr/dload/papers/coots01.html
 */
template <class X>
class CIterator {
    
    CONCEPT_USAGE(CIterator) {
        // copy construct
        X x(iter);
        // loop
        for (iter.first(); !iter.isDone(); iter.next()) {
            typename X::ValueType* item = iter.currentItem();
            // ensure it is a pointer type
            is_pointer(item);
        }
    }
    
private:
    // dummy data (NEVER created)
    X iter;
    // check if it is pointer
    template <typename Tval>
    void is_pointer(Tval* t) { }
};

/**
 * Alternative form of #CIterator returning current item as a copy.
 *
 * Can be used as iterator to return a set of read-only values.
 * Requirements are the same as for CIterator, but return type of currentItem
 * is ValueType.
 *
 * Required properties for class X to be a model of CIterator concept:
 * - Copy constructor so we may pass iterator by value to methods
 * - Associated types:
 *   - <code>ValueType</code> - The type of the value obtained from currentItem().
 * - Methods:
 *   - <code>void first()</code> - Starts iterator at first item
 *   - <code>void next()</code> - Advances iterator to next item
 *   - <code>bool isDone()</code> - Return true if iterator reached end
 *   - <code>ValueType currentItem()</code> - Return current item
 * .
 * Note that assignability is NOT required so we may use consts and refs in type.
 * 
 * Example using variable iter of type X:
\code
for (iter.first(); !iter.isDone(); iter.next()) {
    X::ValueType item = iter.currentItem();
}
\endcode
 * 
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CValueIterator<X>) \endcode
 */
template <class X>
class CValueIterator {
    
    CONCEPT_USAGE(CValueIterator) {
        // copy construct
        X x(iter);
        // loop
        for (iter.first(); !iter.isDone(); iter.next()) {
            typename X::ValueType item = iter.currentItem();
        }
    }
    
private:
    // dummy data (NEVER created)
    X iter;
};

/**
 * Concept for CItarable Object.
 *
 * The idea is to be able to iterate through an unknown data representation1 
 * containing an unknown but iterable data representation2 and
 * return items which can be dereferenced to a certain type (ValueType).
 *
 * Required properties for class X to be a model of CIterable concept:
 * - implements getIterator() method
 * - returned object of getIterator() implements CIterator
 * - Associated types:
 *   - <code>IteratorType</code> - The type of the value returned by
 *                                 get Iterator()
 * - Methods:
 *   - <code>IteratorType getIterator()</code> - Returns iterator
 * 
 * - Example for using iterable iter of type X
 * 
\code
X::IteratorType item = iter.getIterator());
\endcode
 * 
 * To enforce concept for class X use
 * \code CONCEPT_ASSERT(CIterable<X>) \endcode
 * 
 */
template <class X>
class CIterable {
    
    CONCEPT_USAGE(CIterable) {
        // copy construct
        X x(iter);
        // loop
		typename X::IteratorType subI = iter.getIterator();
    }
    
private:
    // dummy data (NEVER created)
    X iter;
};

/** @} */ // end of group concepts
