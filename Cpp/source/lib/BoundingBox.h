/**
 * Defines a simple bounding box.
 *
 * #include "BoundingBox.h"
 *
 * The box encloses the area from offset to offset+size.
 *
 * @author Gerardo Tauriello
 * @date 2/8/10
 */

#pragma once

// library includes
#include "Environment.h"                // Particle lib environment

struct BoundingBox {
#pragma mark LIFECYCLE
	/** Default constructor. */
    BoundingBox(): offset(0), size(0) { }
    /** Create with given offset and size. */
    BoundingBox(VecType _offset, VecType _size): offset(_offset), size(_size) {}
    
    // default destructor, copy constructor and assignment operator used
    
#pragma mark ACCESS
    /** Corner of bounding box with lowest coordinates. */
    VecType offset;
    /** Size of bounding box. */
    VecType size;
};
