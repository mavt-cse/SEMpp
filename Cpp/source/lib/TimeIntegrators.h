/**\file TimeIntegrators.h
 * Collection of time integrators (models of #CTimeIntegrator).
 *
 * Unless otherwise noted, all classes:
 * - are constructed by passing the full time step dt
 * - Contain subclasses (Step..) which are models of #COperator and are
 *   constructed using the same dt.
 *
 * #include "TimeIntegrators.h"
 *
 * @author Gerardo Tauriello
 */

#pragma once

// library includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"

/**
 * Model of #CTimeIntegrator - Forward Euler integrator (TVD Runge-Kutta of order 1).
 * More info: \ref euler_sec
 * Particle type on which it is used must be a model of #CParticle.
 */
class RK1Integrator {
public:
    /** Constructor initializes all operators with given timestep. */
    RK1Integrator(Real dt): step(dt) { }
    
    /** Operator class for update step. */
    class Step {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setState(item.getState() + mH*item.getRHS());
            }
        }
    };
    
    /** Operator for update step. */
    Step step;
    
    /** \name Concept-Implementation.
     * Implementation of #CTimeIntegrator.
     */
    //@{
    size_t getStages() const { return 1; }
    Real getTimeOffset(size_t stage) const { return 0; }
    template <typename WorkDistributor>
    void update(size_t stage, WorkDistributor& distributor) {
        // some assertions
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step>);
        assert(stage < getStages());
        // perform step
        distributor.process(step);
    }
    //@}
};

/**
 * Model of #CTimeIntegrator - Heun integrator (TVD Runge-Kutta of order 2).
 * More info: \ref heun_sec
 * Particle type on which it is used must be a model of #CParticle.
 */
class RK2Integrator {
    /** Storage for time step. */
    Real mDt;
public:
    /** Constructor initializes all operators with given timestep. */
    RK2Integrator(Real dt): mDt(dt), step1(dt), step2(dt) { }
    
    /** Operator class for first update step. */
    class Step1 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step1(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setStateT1(item.getRHS());
                item.setState(item.getState() + mH*item.getRHS());
            }
        }
    };
    
    /** Operator class for second update step. */
    class Step2 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step2(Real dt): mH(dt/2) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setState(item.getState() + mH*(item.getRHS() - item.getStateT1()));
            }
        }
    };
    
    /** Operator for first update step. */
    Step1 step1;
    /** Operator for second update step. */
    Step2 step2;
    
    /** \name Concept-Implementation.
     * Implementation of #CTimeIntegrator.
     */
    //@{
    size_t getStages() const { return 2; }
    Real getTimeOffset(size_t stage) const { return stage*mDt; }
    template <typename WorkDistributor>
    void update(size_t stage, WorkDistributor& distributor) {
        // some assertions
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step1>);
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step2>);
        assert(stage < getStages());
        // perform step
        if (stage == 0) {
            distributor.process(step1);
        } else {
            distributor.process(step2);
        }
    }
    //@}
};

/**
 * Model of #CTimeIntegrator - Midpoint integrator (Runge-Kutta of order 2).
 * More info: \ref midpoint_sec
 * Particle type on which it is used must be a model of #CParticle.
 */
class MidpointIntegrator {
    /** Storage for time step. */
    Real mDt;
public:
    /** Constructor initializes all operators with given timestep. */
    MidpointIntegrator(Real dt): mDt(dt), step1(dt), step2(dt) { }
    
    /** Operator class for first update step. */
    class Step1 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step1(Real dt): mH(dt/2) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setStateT1(item.getState());
                item.setState(item.getState() + mH*item.getRHS());
            }
        }
    };
    
    /** Operator class for second update step. */
    class Step2 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step2(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setState(item.getStateT1() + mH*item.getRHS());
            }
        }
    };
    
    /** Operator for first update step. */
    Step1 step1;
    /** Operator for second update step. */
    Step2 step2;
    
    /** \name Concept-Implementation.
     * Implementation of #CTimeIntegrator.
     */
    //@{
    size_t getStages() const { return 2; }
    Real getTimeOffset(size_t stage) const { return stage*mDt/2; }
    template <typename WorkDistributor>
    void update(size_t stage, WorkDistributor& distributor) {
        // some assertions
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step1>);
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step2>);
        assert(stage < getStages());
        // perform step
        if (stage == 0) {
            distributor.process(step1);
        } else {
            distributor.process(step2);
        }
    }
    //@}
};

/**
 * Model of #CTimeIntegrator - TVD Runge-Kutta integrator of order 3.
 * More info: \ref tvdrk3_sec
 * Particle type on which it is used must be a model of #CParticle.
 */
class RK3Integrator {
    /** Storage for time step. */
    Real mDt;
public:
    /** Constructor initializes all operators with given timestep. */
    RK3Integrator(Real dt): mDt(dt), step1(dt), step2(dt), step3(dt) { }
    
    /** Operator class for first update step. */
    class Step1 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step1(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setStateT1(item.getState());
                item.setState(item.getState() + mH*item.getRHS());
            }
        }
    };
    
    /** Operator class for second update step. */
    class Step2 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step2(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
            
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                const Real w1 = 0.75;
                const Real w2 = 0.25;
                item.setState(  w1 * item.getStateT1()
                              + w2 * (item.getState() + mH*item.getRHS()));
            }
        }
    };
    
    /** Operator class for third update step. */
    class Step3 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step3(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                const Real w1 = Real(1)/Real(3);
                const Real w2 = Real(2)/Real(3);
                item.setState(  w1 * item.getStateT1()
                              + w2 * (item.getState() + mH*item.getRHS()));
            }
        }
    };
    
    /** Operator for first update step. */
    Step1 step1;
    /** Operator for second update step. */
    Step2 step2;
    /** Operator for third update step. */
    Step3 step3;
    
    /** \name Concept-Implementation.
     * Implementation of #CTimeIntegrator.
     */
    //@{
    size_t getStages() const { return 3; }
    
    Real getTimeOffset(size_t stage) const { 
        assert(stage < getStages());
        if (stage == 0) {
            return 0;
        } else if (stage == 1) {
            return mDt;
        } else {
            return mDt/2;
        }
    }
    
    template <typename WorkDistributor>
    void update(size_t stage, WorkDistributor& distributor) {
        // some assertions
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step1>);
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step2>);
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step3>);
        assert(stage < getStages());
        // perform step
        if (stage == 0) {
            distributor.process(step1);
        } else if (stage == 1) {
            distributor.process(step2);
        } else {
            distributor.process(step3);
        }
    }
    //@}
};

/**
 * Model of #CTimeIntegrator -  Runge-Kutta integrator of order 4.
 * More info: \ref rk4_sec
 * Particle type on which it is used must be a model of #CParticle and requires
 * an additional tmp storage:
 * - <code>void setStateT2(StateType)</code> - sets temporal state variable
 * - <code>StateType getStateT2() const</code> - gets value of temporal state variable
 * - \todo Concept for that?
 */
class RK4Integrator {
    /** Storage for time step. */
    Real mDt;
public:
    /** Constructor initializes all operators with given timestep. */
    RK4Integrator(Real dt): mDt(dt), step1(dt), step2(dt), step3(dt), step4(dt) { }
    
    /** Operator class for first update step. */
    class Step1 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step1(Real dt): mH(dt/2) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setStateT1(item.getState());
                item.setStateT2(item.getRHS());
                item.setState(item.getState() + mH*item.getRHS());
            }
        }
    };
    
    /** Operator class for second update step. */
    class Step2 {
    protected:
        /** Storage for time step. */
        Real mH;
    public:
        /** Default constructor. */
        Step2(Real dt): mH(dt/2) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setStateT2(item.getStateT2() + 2*item.getRHS());
                item.setState(item.getStateT1() + mH*item.getRHS());
            }
        }
    };
    
    /** Operator class for third update step. */
    class Step3 {
    protected:
        /** Storage for time step. */
        Real mH;
    public:
        /** Default constructor. */
        Step3(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setStateT2(item.getStateT2() + 2*item.getRHS());
                item.setState(item.getStateT1() + mH*item.getRHS());
            }
        }
    };
    
    /** Operator class for fourth update step. */
    class Step4 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step4(Real dt): mH(dt/6) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setState(item.getStateT1() + mH*(item.getStateT2() + item.getRHS()));
            }
        }
    };
    
    /** Operator for first update step. */
    Step1 step1;
    /** Operator for second update step. */
    Step2 step2;
    /** Operator for third update step. */
    Step3 step3;
    /** Operator for fourth update step. */
    Step4 step4;
    
    /** \name Concept-Implementation.
     * Implementation of #CTimeIntegrator.
     */
    //@{
    size_t getStages() const { return 4; }
    
    Real getTimeOffset(size_t stage) const { 
        assert(stage < getStages());
        if (stage == 0) {
            return 0;
        } else if (stage == 1) {
            return mDt/2;
        } else if (stage == 2) {
            return mDt/2;
        } else {
            return mDt;
        }
    }
    
    template <typename WorkDistributor>
    void update(size_t stage, WorkDistributor& distributor) {
        // some assertions
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step1>);
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step2>);
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step3>);
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step4>);
        assert(stage < getStages());
        // perform step
        if (stage == 0) {
            distributor.process(step1);
        } else if (stage == 1) {
            distributor.process(step2);
        } else if (stage == 2) {
            distributor.process(step3);
        } else {
            distributor.process(step4);
        }
    }
    //@}
};

/**
 * Model of #CTimeIntegrator - Leapfrog integrator (2nd order) assuming that rhs depends only on position.
 * More info: \ref leapfrogV1_subsec
 * 
 * Special first iteration handled automagically.
 * 
 * Particle type on which it is used must be a model of #CParticle and requires:
 * - <code>void setPos(VecType)</code> - sets position
 * - <code>VecType getAcc() const</code> - gets acceleration (i.e. d/dt vel)
 * - \todo Concept for that?
 */
class LeapfrogV1Integrator {
public:
    /** Constructor initializes all operators with given timestep. */
    LeapfrogV1Integrator(Real dt): step0(dt), step(dt) { }
    
    /** Operator class for update step on first iteration. */
    class Step0 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step0(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setPos(item.getPos() + mH*item.getVel() + mH*mH/2*item.getAcc());
                // keep rhs of last iteration
                item.setStateT1(item.getRHS());
            }
        }
    };
    
    /** Operator class for update step. */
    class Step {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                // backup of pos
                VecType pos = item.getPos();
                item.setState(item.getState() + mH/2*(item.getRHS() + item.getStateT1()));
                item.setPos(pos + mH*item.getVel() + mH*mH/2*item.getAcc());
                // keep rhs of last iteration
                item.setStateT1(item.getRHS());
            }
        }
    };
    
    /** Operator for update step on first iteration. */
    Step0 step0;
    /** Operator for update step. */
    Step step;
    
    /** \name Concept-Implementation.
     * Implementation of #CTimeIntegrator.
     */
    //@{
    size_t getStages() const { return 1; }
    Real getTimeOffset(size_t stage) const { return 0; }
    template <typename WorkDistributor>
    void update(size_t stage, WorkDistributor& distributor) {
        // some assertions
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step>);
        assert(stage < getStages());
        // remember if it was first time
        static bool firstTime = true;
        // perform step
        if (firstTime) {
            distributor.process(step0);
            firstTime = false;
        } else {
            distributor.process(step);
        }
    }
    //@}
};

/**
 * Model of #CTimeIntegrator - Alternative leapfrog integrator assuming that rhs depends only on position.
 * More info: \ref leapfrogV1b_subsec
 * 
 * Special first iteration handled automagically.
 * 
 * Same particle type requirements as #LeapfrogV1Integrator.
 */
class LeapfrogV1bIntegrator {
public:
    /** Constructor initializes all operators with given timestep. */
    LeapfrogV1bIntegrator(Real dt): step0(dt), step(dt) { }
    
    /** Operator class for update step on first iteration. */
    class Step0 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step0(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                // backup of pos
                VecType pos = item.getPos();
                item.setState(item.getState() + mH/2*item.getRHS());
                item.setPos(pos + mH*item.getVel());
            }
        }
    };
    
    /** Operator class for update step. */
    class Step {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                // backup of pos
                VecType pos = item.getPos();
                item.setState(item.getState() + mH*item.getRHS());
                item.setPos(pos + mH*item.getVel());
            }
        }
    };
    
    /** Operator for update step on first iteration. */
    Step0 step0;
    /** Operator for update step. */
    Step step;
    
    /** \name Concept-Implementation.
     * Implementation of #CTimeIntegrator.
     */
    //@{
    size_t getStages() const { return 1; }
    Real getTimeOffset(size_t stage) const { return 0; }
    template <typename WorkDistributor>
    void update(size_t stage, WorkDistributor& distributor) {
        // some assertions
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step>);
        assert(stage < getStages());
        // remember if it was first time
        static bool firstTime = true;
        // perform step
        if (firstTime) {
            distributor.process(step0);
            firstTime = false;
        } else {
            distributor.process(step);
        }
    }
    //@}
};

/**
 * Model of #CTimeIntegrator - Leapfrog integrator (2nd order) with general rhs.
 * More info: \ref leapfrogV2_subsec
 * 
 * Same particle type requirements as #LeapfrogV1Integrator.
 */
class LeapfrogV2Integrator {
public:
    /** Storage for time step. */
    Real mDt;
public:
    /** Constructor initializes all operators with given timestep. */
    LeapfrogV2Integrator(Real dt): mDt(dt), step1(dt), step2(dt) { }
    
    /** Operator class for first update step. */
    class Step1 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step1(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setStateT1(item.getRHS());
                VecType pos = item.getPos() + mH*item.getVel() + mH*mH/2*item.getAcc();
                item.setState(item.getState() + mH*item.getRHS());
                item.setPos(pos);
            }
        }
    };
    
    /** Operator class for second update step. */
    class Step2 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step2(Real dt): mH(dt/2) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                // backup pos
                VecType pos = item.getPos();
                item.setState(item.getState() + mH*(item.getRHS() - item.getStateT1()));
                item.setPos(pos);
            }
        }
    };
    
    /** Operator for first update step. */
    Step1 step1;
    /** Operator for second update step. */
    Step2 step2;
    
    /** \name Concept-Implementation.
     * Implementation of #CTimeIntegrator.
     */
    //@{
    size_t getStages() const { return 2; }
    Real getTimeOffset(size_t stage) const { return stage*mDt; }
    template <typename WorkDistributor>
    void update(size_t stage, WorkDistributor& distributor) {
        // some assertions
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step1>);
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step2>);
        assert(stage < getStages());
        // perform step
        if (stage == 0) {
            distributor.process(step1);
        } else {
            distributor.process(step2);
        }
    }
    //@}
};

/**
 * Model of #CTimeIntegrator - Alternative leapfrog integrator with general rhs.
 * More info: \ref leapfrogV2b_subsec
 * 
 * Special first iteration handled automagically.
 * 
 * Same particle type requirements as #LeapfrogV1Integrator and additionally:
 * - <code>VecType getVelTmp() const</code>\n
 *   Get velocity based on temporary copy of state stored with setStateT1().
 */
class LeapfrogV2bIntegrator {
public:
    /** Constructor initializes all operators with given timestep. */
    LeapfrogV2bIntegrator(Real dt): step0(dt), step(dt) { }
    
    /** Operator class for update step on first iteration. */
    class Step0 {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step0(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setStateT1(item.getState() + mH/2*item.getRHS());
                VecType pos = item.getPos() + mH*item.getVelTmp();
                item.setState(item.getStateT1() + mH/2*item.getRHS());
                item.setPos(pos);
            }
        }
    };
    
    /** Operator class for update step. */
    class Step {
    protected:
        /** Storage for time step for this stage. */
        Real mH;
    public:
        /** Default constructor. */
        Step(Real dt): mH(dt) { }
        
        // no constructor with no arguments
        // default copy constructor, assignment operator and destructor used
        
        /** THE Operator. */
        template <typename Iterator>
        void operator()(Iterator iter) const {
            // assert concepts
            CONCEPT_ASSERT(CIterator<Iterator>);
            CONCEPT_ASSERT(CParticle<typename Iterator::ValueType>);
            // go through them
            for (iter.first(); !iter.isDone(); iter.next()) {
                typename Iterator::ValueType& item = *(iter.currentItem());
                item.setStateT1(item.getStateT1() + mH*item.getRHS());
                VecType pos = item.getPos() + mH*item.getVelTmp();
                item.setState(item.getStateT1() + mH/2*item.getRHS());
                item.setPos(pos);
            }
        }
    };
    
    /** Operator for update step on first iteration. */
    Step0 step0;
    /** Operator for update step. */
    Step step;
    
    /** \name Concept-Implementation.
     * Implementation of #CTimeIntegrator.
     */
    //@{
    size_t getStages() const { return 1; }
    Real getTimeOffset(size_t stage) const { return 0; }
    template <typename WorkDistributor>
    void update(size_t stage, WorkDistributor& distributor) {
        // some assertions
        CONCEPT_ASSERT(CWorkDistributor<WorkDistributor, Step>);
        assert(stage < getStages());
        // remember if it was first time
        static bool firstTime = true;
        // perform step
        if (firstTime) {
            distributor.process(step0);
            firstTime = false;
        } else {
            distributor.process(step);
        }
    }
    //@}
};

