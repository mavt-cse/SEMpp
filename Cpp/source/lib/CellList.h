/**\file CellList.h
 * Collection of neighborhood maps using cell lists.
 *
 * #include "CellList.h"
 *
 * @author Gerardo Tauriello
 * @see CNeighborhoodMap
 */

#pragma once

// library includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"

// system includes
#include <vector>
#include <cstring>

/**
 * Neighborhood map for particles within a cell (a model of #CNeighborhoodMap).
 * This uses low-level C++ memory management. Make sure to call cleanUp, 
 * when you are done using it!
 * Init routines can be called as often as you want (no need to clean up in between).
 *
 * \par Example usage:
\code
// given some iterator "candidates" on potential neighbors (e.g. CandidatesIterator of CellList)
CLLocalNH<Particle,true> localNH;
localNH.init(candidates);
// ... loop over particles ... -> part
CLLocalNH<Particle,true>::IteratorType iter = localNH.getHood(part);
// ... update particles using that neighborhood ...
// clean up (!!!!!!!!!)
localNH.cleanUp();
\endcode
 *
 * @tparam Particle  Type of elements in neighborhood. No reqs on it.
 * @tparam CopyData  True if we want to use this with a copy of the data.
 */
template <typename Particle, bool CopyData>
class CLLocalNH {
    
#pragma mark ITERATOR
public:
    
    /**
     * Iterator for all potential neighbors (model of #CIterator).
     * This will automatically exclude the particle itself.
     * @tparam PType	Particle or const Particle (sets ValueType for iterator)
     * @tparam GetCopy  Get neighbor from copy or from original
     */
    template <typename PType, bool GetCopy>
    class LocalIterator {
    protected:
        /** Reference to daddy. */
        const CLLocalNH& mDaddy;
        /** Index into daddy's arrays. */
        int mCurrent;
        /** Particle to exclude. */
        const Particle* mpParticle;
        
    public:
        /** Default constructor. */
        LocalIterator(const CLLocalNH& daddy, const Particle* part): mDaddy(daddy), mpParticle(part) { }
        
        // default destructor, copy constructor and assignment operator used
        
        // implementation of CIterator
        typedef PType ValueType;
        
        void first() {
            if (mDaddy.mppParticles[0] == mpParticle) mCurrent = 1;
            else                                      mCurrent = 0;
        }
        
        void next() {
            ++mCurrent;
            // check
            if (mDaddy.mppParticles[mCurrent] == mpParticle) ++mCurrent;
        }
        
        bool isDone() { return mCurrent >= mDaddy.mNParticles; }
        
        ValueType* currentItem() {
            if (GetCopy) return mDaddy.mpParticles + mCurrent;
            else         return mDaddy.mppParticles[mCurrent];
        }
        
        size_t size() const { return mDaddy.mNParticles; }
    };

#pragma mark LIFECYCLE
public:
    /** Default constructor. */
    CLLocalNH() {
        reset();
    }
    
    // default destructor, copy constructor and assignment operator used
    
    /** Clean up. */
    void cleanUp() {
        if (mpParticlesOwnCopy) free(mpParticles);
        if (mppParticlesOwnCopy) delete[] mppParticles;
        reset();
    }
    
protected:
    /** Reset settings. */
    void reset() {
        mpParticles = NULL;
        mppParticles = NULL;
        mpParticlesOwnCopy = mppParticlesOwnCopy = false;
        mNParticles = 0;
    }
    
#pragma mark SETUP
public:
    /**
     * Initialize with pointers to externally stored data.
     * Assumes that we somehow have all potential neighbors in a contiguous
     * array.
     * 
     * @param nParticles	Number of particles.
     * @param ppParticles	Array of Particle* to original data.
     * @param pParticles	Array of Particle to (copy of) data. Use NULL if
     *						not needed.
     */
    void init(const size_t nParticles, Particle** ppParticles, const Particle* pParticles) {
        cleanUp();
        mNParticles = nParticles;
        mppParticles = ppParticles;
        mpParticles = pParticles;
        mpParticlesOwnCopy = mppParticlesOwnCopy = false;
    }
    
    /**
     * Initialize with iterator through particles.
     * This might not be the optimal way to do it. Each cell list can provide
     * a better implementation of this.
     * If CopyData = true, this will also generate a copy of these elements.
     * 
     * @param iter			Iterator through all potential neighbors.
     * @tparam Iterator     A model of #CIterator with size()-method and
     *						Particle and Iterator::ValueType being the same type
     */
    template <typename Iterator>
    void init(Iterator iter) {
        // assert concepts
        CONCEPT_ASSERT(CIterator<Iterator>);
        CONCEPT_ASSERT(CSameType<Particle, typename Iterator::ValueType>);
        // init
        cleanUp();
        mNParticles = iter.size();
        mppParticles = new Particle*[mNParticles];
        mppParticlesOwnCopy = true;
        // copy pointers
        Particle** p_iter = mppParticles;
        for (iter.first(); !iter.isDone(); iter.next()) {
            *p_iter = iter.currentItem();
            ++p_iter;
        }
        // copy data?
        if (CopyData) copyData();
    }
    
    /**
     * Create a copy of the particles which is contiguous in memory.
     * This is then used by getHood to (hopefully) provide faster access.
     * This should only be called if CopyData = true and init was called with
     * external list of Particle* but no data-storage.
     */
    void copyData() {
        assert(CopyData);
        // init
        if (mpParticlesOwnCopy) free(mpParticles);
        mpParticles = (Particle*)malloc(mNParticles*sizeof(Particle));
        if (mpParticles == NULL) {
            std::cout << "OUT OF MEMORY PANIC\n";
            exit(1);
        }
        mpParticlesOwnCopy = true;
        // copy data
        for (size_t i = 0; i < mNParticles; ++i) {
            memcpy(mpParticles + i, *(mppParticles + i), sizeof(Particle));
        }
    }
    
#pragma mark CNeighborhoodMap
public:
    /** \name Concept-Implementation.
     * Implementation of #CNeighborhoodMap.
     */
    //@{
    typedef LocalIterator<const Particle, CopyData> IteratorType;
    typedef LocalIterator<Particle, false> IteratorTypeVar;
    typedef Particle ParticleType;
    
    IteratorType getHood(const Particle& part) const {
        assert(!CopyData || mpParticles != NULL);
        return IteratorType(*this, &part);
    }
    
    IteratorType getHood(const VecType& pos) const {
        assert(!CopyData || mpParticles != NULL);
        return IteratorType(*this, NULL);
    }
    
    IteratorTypeVar getHoodVar(const Particle& part) const {
        return IteratorTypeVar(*this, &part);
    }
    
    IteratorTypeVar getHoodVar(const VecType& pos) const {
        return IteratorTypeVar(*this, NULL);
    }
	//@}
    
#pragma mark DATA
public:
    /** Number of potential neighbors. */
    size_t mNParticles;
    /** Pointers to all particles in potential neighborhood. */
    Particle** mppParticles;
    /** Are we using our own array mppParticles? Clean up will only touch mppParticles if this is true. */
    bool mppParticlesOwnCopy;
    /** Copies of all particles in potential neighborhood. NULL if none defined. */
    Particle* mpParticles;
    /** Are we using our own array mpParticles? Clean up will only touch mpParticles if this is true. */
    bool mpParticlesOwnCopy;
};

/**
 * Enumerator used to define boundary conditions in cell lists.
 * - CL_NONE - Nothing done at boundaries
 * - CL_PERIODIC - Particles repeated periodically at boundaries
 */
enum CLBoundaryCondition {
    CL_NONE,
    CL_PERIODIC
};

/**
 * Base version of cell list (a model of #CNeighborhoodMap).
 * A cell list splits up the domain into cells of a given size. The cell in
 * which a particle is located can be determined in O(1). One can then search
 * the neighboring cells for potential neighbors of the particle.
 *
 * This version returns a neighborhood containing all particles in cells which
 * are within the cutoff radius of the particle (excluding itself).
 * 
 * \par Example usage:
\code
VecType domain_offset(0,0);
VecType domain_size(10,10);
Real cell_size = 1;
CLBoundaryCondition domain_bc = CL_PERIODIC;
CellList nhMap;
nhMap.setup(domain_offset, domain_size, cell_size, domain_bc);
// setup may be called again when domain changes
// ...
nhMap.reset(iter);  // iter is Iterator over all particles
// ... loop over particles ... -> part
CellList::IteratorType iter = nhMap.getHood(part);
// ... update particles using that neighborhood ...
\endcode
 *
 * @tparam Particle     A model of #CBasicParticle with setPos(VecType) method
 */
template <typename Particle>
class CellList {
    CONCEPT_ASSERT(CBasicParticle<Particle>);
    
    // forward reference...just to make the compiler happy
public:
    class CandidatesIterator;
    
#pragma mark PUBLIC TYPEDEFS
public:
    /** Type used to store a single cell. */
    typedef std::vector< Particle* > CellType;
    
#pragma mark LIFECYCLE
public:
    
    // default constructor, destructor, copy constructor and assignment operator used
    
    /**
     * Splits up domain into cells of given size (same in all directions).
     * For periodic boundaries cell_size is minimal size for cells. This will be
     * rescaled such that all cells have the same size.
     * 
     * @param domain_offset Coordinates of offset of domain. That is the domain
     *                      has coordinates from domain_offset to domain_offset
     *                      + domain_size (not including the latter).
     * @param domain_size   Size of domain to split up.
     * @param cell_size     Size of a cell (same in all directions).
     * @param bc            Boundary condition to be used (def: #CL_NONE).
     */
    void setup(const VecType& domain_offset, const VecType& domain_size,
               const Real cell_size, const CLBoundaryCondition bc = CL_NONE) {
        // get cell sizes in each direction
        setup(domain_offset, domain_size, VecType(cell_size), bc);
    }
    
    /**
     * Splits up domain into cells of given size.
     * See other setup-method for details. Here one can set cell_sizes per direction.
     */
    void setup(const VecType& domain_offset, const VecType& domain_size,
               const VecType& cell_sizes, const CLBoundaryCondition bc = CL_NONE) {
        // update data
        if (bc == CL_NONE) {
        	mDomainOffset = domain_offset;
        	mCellSize = cell_sizes;
        	mNCells = (domain_size / cell_sizes).ceil();
        } else if (bc == CL_PERIODIC) {
            for (int i = 0; i < DIM; ++i) {
                mNCells[i] = std::floor(domain_size[i] / cell_sizes[i]);
                mCellSize[i] = domain_size[i] / mNCells[i];
            }
            // leave space for ghosts
        	mDomainOffset = domain_offset - mCellSize;
            mNCells += 2;
            //std::cout << "Cell sizes increased from " << cell_sizes << " to " << mCellSize << std::endl;
        } else {
        	std::cerr << "Don't know how to handle given CellList BC " << bc << "!!!\n";
            exit(1);
        }
        // common stuff
        mDomainSize = domain_size;
        mBC = bc;
        // resize cell-vector
        mCells.resize(mNCells.fac());
    }
    
    /**
     * Fill in cells with particles looped by given iterator.
     * @tparam Iterator     A model of #CIterator with Particle and 
     *                      Iterator::ValueType being the same type
     */
    template <typename Iterator>
    void reset(Iterator iter) {
        // assert concepts
        CONCEPT_ASSERT(CIterator<Iterator>);
        CONCEPT_ASSERT(CSameType<Particle, typename Iterator::ValueType>);
        
        // clean up (notice that this keeps the size of the vector!)
        for (typename std::vector< CellType >::iterator it = mCells.begin(); it != mCells.end(); ++it) {
			it->clear();
        }
        // fill in particles
        for (iter.first(); !iter.isDone(); iter.next()) {
            const int index = getIndex(iter.currentItem()->getPos());
            mCells[index].push_back(iter.currentItem());
        }
        // update helpers
        mActiveCells.clear();
        mNParticles = 0;
        for (typename std::vector< CellType >::iterator it = mCells.begin(); it != mCells.end(); ++it) {
            size_t n = it->size();
            if (n > 0) {
                mActiveCells.push_back(&*it);
                mNParticles += n;
            }
        }
        // update ghosts (important to do after active cells!)
        if (mBC == CL_PERIODIC) updateGhosts();
        //std::cout << "Added " << mNParticles << " particles in " << mActiveCells.size() << " cells\n";
    }
    
#pragma mark CNeighborhoodMap
public:
    
    /** \name Concept-Implementation.
     * Implementation of #CNeighborhoodMap.
     */
    //@{
    typedef CandidatesIterator IteratorType;
    typedef CandidatesIterator IteratorTypeVar;
    typedef Particle ParticleType;
    
    IteratorType getHood(const Particle& part) const {
        IteratorType result = getCandidates(part.getPos());
        result.mpParticle = &part;
        result.first(); // makes sure it is properly initialized
        return result;
    }
    
    IteratorType getHood(const VecType& pos) const {
        IteratorType result = getCandidates(pos);
        result.mpParticle = NULL;
        result.first(); // makes sure it is properly initialized
        return result;
    }
    
    IteratorTypeVar getHoodVar(const Particle& part) const {
    	if (mBC == CL_PERIODIC) {
        	std::cerr << "Periodic read-write access is not allowed!!!\n";
            exit(1);
        }
        return getHood(part);
    }
    IteratorTypeVar getHoodVar(const VecType& pos) const {
    	if (mBC == CL_PERIODIC) {
        	std::cerr << "Periodic read-write access is not allowed!!!\n";
            exit(1);
        }
        return getHood(pos);
    }
	//@}

#pragma mark FANCY OPTIMIZED ACCESS
public:
    // this is very experimental (GT - Jan 2011)
    // -> iterator goes over cells -> which are CellType = vector<Particle*>
    // Usage:
    // - create WorkDistributor over CellIterator (which you get with getCellIterator)
    // - get local neighborhood for particles with getLocalHood or getLocalHoodCopy
    // - use getParticleInCellIterator(CellType) to get particle iterator for that cell
    
    /** Type of Iterator going over all non-empty cells of the CellList. */
    typedef ContainerIteratorP< std::vector< CellType* >, CellType > CellIterator;
    /** Get iterator on cells of cell list (note: this does NOT iterate over particles). */
    CellIterator getCellIterator() { return CellIterator(mActiveCells); }
    
    /** Type of Iterator going over all particles within a cell. */
    typedef ContainerIteratorP<CellType, Particle> ParticleInCellIterator;
    /** Get iterator on particles for a given cell of the cell list. */
    ParticleInCellIterator getParticleInCellIterator(CellType& cell) const {
        return ParticleInCellIterator(cell);
    }
    
    /** Get local neighborhood for particles. */
    CLLocalNH<Particle, false> getLocalHood(CellType& cell) const {
        CLLocalNH<Particle,false> result;
        // NOTE: we could probably generate mppParticle of result in a better way...
        CandidatesIterator candidates = getCandidates(cell[0]->getPos());
        result.init(candidates);
        return result;
    }
    
    /** As getLocalHood but generates a copy of the neighborhood for (hopefully) faster access. */
    CLLocalNH<Particle, true> getLocalHoodCopy(CellType& cell) const {
        CLLocalNH<Particle,true> result;
        // NOTE: we could probably generate mppParticle and mpParticle of result in a better way...
        CandidatesIterator candidates = getCandidates(cell[0]->getPos());
        result.init(candidates);
        return result;
    }
    
#pragma mark MORE ACCESS
public:
	// Get Information of CellList
	/** Get Domain Offset. */
	VecType getDomainOffset(){return mDomainOffset;}
	/** Get Cell Size. */
    VecType getCellSize(){return mCellSize;}
    /** Get the Number of cells in each direction. */
    VecTypeI getNumberCells(){return mNCells;}
    
    /** Just for testing. */
    void debugDump() {
        std::cout << "OFFSET " << mDomainOffset << std::endl;
        std::cout << "SIZE " << mDomainSize << std::endl;
        std::cout << "CELLSIZE " << mCellSize << std::endl;
        std::cout << "NCELLS " << mNCells << std::endl;
        std::cout << "BC " << mBC << std::endl;
    	
        for (int src = 0; src < mCells.size(); ++src) {
            std::cout << "CONTENT OF CELL " << src << std::endl;
		    for (typename CellType::const_iterator it = mCells[src].begin(); it != mCells[src].end(); ++it) {
                std::cout << " pos " << (*it)->getPos() << std::endl;
            }
        }
    }
    
#pragma mark HELPER
private:
    /** Integer floor (also for negative numbers). */
    int iFloor(Real r) const {
        return (r < 0) ? int(r)-1 : int(r);
    }
    
protected:
    /** Return index into mCells for a given position. */
    int getIndex(const VecType& pos) const {
        // this will be in range [0, mNCells) for positions in domain
        const VecType iReal = (pos - mDomainOffset) / mCellSize;
        // compute index
        int index = 0;
        for (int i = 0; i < DIM; ++i) {
            const int j = iFloor(iReal[i]);
            assert((mBC == CL_NONE && j >= 0 && j < mNCells[i]) || (mBC == CL_PERIODIC && j >= 1 && j < mNCells[i]-1));
            index = index*mNCells[i] + j;
        }
        // resulting index is (with I[i] = iFloor(iReal[i]), i=0..DIM-1):
        // 1D: I[0]
        // 2D: I[0]*mNCells[1] + I[1]
        // 3D: I[0]*mNCells[1]*mNCells[2] + I[1]*mNCells[2] + I[2]
        return index;
    }
    
    /**
     * Update ghost cells for periodic BC.
     * This can only be called after reset has filled all usual cells.
     * - Potentially active cells are in [1,mNCells-2] (border is ghosts)
     * - Ghost particles are copied into helper array
     */
    void updateGhosts() {
    	// clean up
        mGhosts.clear();
        // count ghosts to reserve space
        int ghost_count = updateGhosts(true);
        mGhosts.reserve(ghost_count);
        // do it
        updateGhosts(false);
    }
    
    /**
     * Do the work for updateGhosts.
     * @param count_only	True if we only want to check number of ghosts
     * @return Number of ghost particles
     */
    int updateGhosts(bool count_only) {
        // use cool code-access from Diego's BlockLab (for 2D and 3D)
        // - code[i] in [-1,1] for each dimension
        // - s and e are start and end for loop (not incl e) through ghost cells
        // - off_cell is offset to displace cell index with (to get real cell)
        // - off_pos is offset to displace particles with
#if DIM == 1
		int ghost_count = mCells[1].size() + mCells[mNCells[0]-2].size();
		if (!count_only) {
        	copyCellToGhost(mNCells[0]-2, 0, VecType(-mDomainSize[0]));
			copyCellToGhost(1, mNCells[0]-1, VecType(mDomainSize[0]));
        }
        return ghost_count;
#endif

		int ghost_count = 0;
		for(int icode = 0; icode < (DIM == 2 ? 9 : 27); ++icode)
        {

#if DIM == 2
            if (icode == 1*1 + 3*1) continue;
            
            const int code[2] = { (icode/3)%3-1, icode%3-1 };
#elif DIM == 3
            if (icode == 1*1 + 3*1 + 9*1) continue;
            
            const int code[3] = { (icode/9)%3-1, (icode/3)%3-1, icode%3-1};
#endif
            int s[DIM], e[DIM], off_cell[DIM];
            VecType off_pos;
            
            for (int i = 0; i < DIM; ++i) {
                if (code[i] < 0) {
                    s[i] = 0;
                    e[i] = 1;
                    off_cell[i] = mNCells[i]-2;
                    off_pos[i] = -mDomainSize[i];
                } else if (code[i] < 1) {
                    s[i] = 1;
                    e[i] = mNCells[i]-1;
                    off_cell[i] = 0;
                    off_pos[i] = 0;
                } else {
                    s[i] = mNCells[i]-1;
                    e[i] = mNCells[i];
                    off_cell[i] = 2-mNCells[i];
                    off_pos[i] = mDomainSize[i];
                }
            }
        
#if DIM == 2
            for(int ix=s[0]; ix<e[0]; ++ix)
            	for(int iy=s[1]; iy<e[1]; ++iy)
                {
                    const int dst = ix*mNCells[1] + iy;
                    const int src = (ix+off_cell[0])*mNCells[1] + iy+off_cell[1];
                    ghost_count += mCells[src].size();
                    if (!count_only) copyCellToGhost(src, dst, off_pos);
                }
#elif DIM == 3
			for(int ix=s[0]; ix<e[0]; ++ix)
                for(int iy=s[1]; iy<e[1]; ++iy)
                    for(int iz=s[2]; iz<e[2]; ++iz)
                    {
                    	const int dst = (ix*mNCells[1] + iy)*mNCells[2] + iz;
                    	const int src = ((ix+off_cell[0])*mNCells[1] + (iy+off_cell[1]))*mNCells[2] + iz+off_cell[2];
                        ghost_count += mCells[src].size();
                    	if (!count_only) copyCellToGhost(src, dst, off_pos);
                    }
#endif
        }
        return ghost_count;
    }
    
    /** Helper for update ghosts which copies cell. */
    void copyCellToGhost(const int src, const int dst, const VecType& off) {
    	assert(src >= 0 && src < mCells.size());
    	assert(dst >= 0 && dst < mCells.size());
        for (typename CellType::iterator it = mCells[src].begin(); it != mCells[src].end(); ++it) {
        	mGhosts.push_back(**it);
            Particle* cur_item = &mGhosts.back();
            cur_item->setPos(cur_item->getPos() + off);
            mCells[dst].push_back(cur_item);
        }
    }
    
    /**
     * Handle boundary conditions.
     * Assumes that one can split up index of cells into DIM dimensions.
     * @param i     Index into cell list for one dimension.
     * @param n     Number of cells in that dimension.
     * @return Corrected index or -1 if invalid.
     */
    int handleBC(int i, const int n) const {
        if (i < 0 || i >= n) {
        	assert(mBC != CL_PERIODIC);
            return -1;
        } else {
        	return i;
        }
    }
    
    /** Get potential neighbors for particle at a given position (1D). */
    CandidatesIterator getCandidates(const VectorElement<Real, 1>& pos) const {
        CandidatesIterator result;
        
        // this will be in range [0, mNCells) for positions in domain
        const VecType iReal = (pos - mDomainOffset) / mCellSize;
        for (int offX = -1; offX <= 1; ++offX) {
            int ix = handleBC(iFloor(iReal[0]) + offX, mNCells[0]);
            // check for out of bounds
            if (ix < 0) continue;
            assert(ix < mNCells[0]);
            // add cells
            result.addCell(mCells[ix]);
        }
        
        return result;
    }
    
    /** Get potential neighbors for particle at a given position (2D). */
    CandidatesIterator getCandidates(const VectorElement<Real, 2>& pos) const {
        CandidatesIterator result;
        
        // this will be in range [0, mNCells) for positions in domain
        const VecType iReal = (pos - mDomainOffset) / mCellSize;
        // loop X
        for (int offX = -1; offX <= 1; ++offX) {
            int ix = handleBC(iFloor(iReal[0]) + offX, mNCells[0]);
            // check for out of bounds
            if (ix < 0) continue;
            assert(ix < mNCells[0]);
            // loop Y
            for (int offY = -1; offY <= 1; ++offY) {
                int iy = handleBC(iFloor(iReal[1]) + offY, mNCells[1]);
                // check for out of bounds
                if (iy < 0) continue;
                assert(iy < mNCells[1]);
                // add cells
                result.addCell(mCells[ix*mNCells[1] + iy]);
            }
        }
        
        return result;
    }
    
    /** Get potential neighbors for particle at a given position (3D). */
    CandidatesIterator getCandidates(const VectorElement<Real, 3>& pos) const {
        CandidatesIterator result;
        
        // this will be in range [0, mNCells) for positions in domain
        const VecType iReal = (pos - mDomainOffset) / mCellSize;
        // loop X
        for (int offX = -1; offX <= 1; ++offX) {
            int ix = handleBC(iFloor(iReal[0]) + offX, mNCells[0]);
            // check for out of bounds
            if (ix < 0) continue;
            assert(ix < mNCells[0]);
            // loop Y
            for (int offY = -1; offY <= 1; ++offY) {
                int iy = handleBC(iFloor(iReal[1]) + offY, mNCells[1]);
                // check for out of bounds
                if (iy < 0) continue;
                assert(iy < mNCells[1]);
                // loop Z
                for (int offZ = -1; offZ <= 1; ++offZ) {
                    int iz = handleBC(iFloor(iReal[2]) + offZ, mNCells[2]);
                    // check for out of bounds
                    if (iz < 0) continue;
                    assert(iz < mNCells[2]);
                    // add cells
                    result.addCell(mCells[(ix*mNCells[1] + iy)*mNCells[2] + iz]);
                }
            }
        }
        
        return result;
    }
    
#pragma mark DATA
protected:
    /** Storage of cells. */
    std::vector< CellType > mCells;
    /** Domain offset (incl ghosts). */
    VecType mDomainOffset;
    /** Domain size (without ghosts). */
    VecType mDomainSize;
    /** Cell sizes. */
    VecType mCellSize;
    /** Number of cells in each direction (incl ghosts). */
    VecTypeI mNCells;
    /** Boundary condition used. */
    CLBoundaryCondition mBC;
    /** Storage for ghost particles (read-only). */
    std::vector< Particle > mGhosts;
    
    // HELPERS
    /** Vector of cells with at least one particle. */
    std::vector< CellType* > mActiveCells;
    /** Number of particles in the cell list. */
    int mNParticles;
    
#pragma mark ITERATOR
public:
    
    // TODO: fix problem of non-copiable Iterator:
    // - currently we need to call first() before we are allowed to copy it
    // - Constructor should somehow make sure of that
    // - problem are unitialized std-iterator (mNHIterator, mCellIteratorCur, mCellIteratorLast)
    
    /**
     * Iterator for all potential neighbors (model of #CIterator).
     * This will automatically exclude the particle itself.
     */
    class CandidatesIterator {
    protected:
        // FRIENDS
        friend class CellList;
        
        // TYPES
        /** Storage for neighborhood of cells. */
        typedef std::vector< const CellType* > NHCellType;
        
        // DATA
        /** Pointer to particle for which these are potential neighbors. */
        const Particle* mpParticle;
        /** Pointers to cells with potential neighbors. */
        NHCellType mNHCells;
        /** Number of potential neighbors. */
        size_t mNCandidates;
        
        /** Iterator for neighbor cells. */
        typename NHCellType::iterator mNHIterator;
        /** Iterator for current cell (current pos). */
        typename CellType::const_iterator mCellIteratorCur;
        /** Iterator for current cell (last pos). */
        typename CellType::const_iterator mCellIteratorLast;
        // note on const: this only means that pointer is constant...we can still change Particle!
        
        // OPERATIONS
        /** Add a cell to the list of potential neighbors. */
        void addCell(const CellType& cell) {
            mNHCells.push_back(&cell); 
            mNCandidates += cell.size();
        }
        /** Update current cell iterator after advance to next cell. */
        void nextCell() {
            // break if at end
            if (isDone()) return;
            // loop to next non-empty cell (unless end reached)
            while ((*mNHIterator)->empty()) {
                ++mNHIterator;
                if (isDone()) return;
            }
            // if we reached this, we have a non-empty cell
            mCellIteratorCur = (*mNHIterator)->begin();
            mCellIteratorLast = (*mNHIterator)->end();
            // make sure we are not pointing to mpParticle
            if (*mCellIteratorCur == mpParticle) next();
        }
    public:
        /** Default constructor. */
        CandidatesIterator(): mpParticle(NULL), mNCandidates(0) { }
        
        // default destructor, copy constructor and assignment operator used
        
        // implementation of CIterator
        typedef Particle ValueType;
        
        void first() {
            mNHIterator = mNHCells.begin();
            nextCell();
        }
        
        void next() {
            ++mCellIteratorCur;
            // check whether we reached end of this cell
            if (mCellIteratorCur == mCellIteratorLast) {
                // advance to next cell
                ++mNHIterator;
                nextCell();
            } else if (*mCellIteratorCur == mpParticle) {
                // if not: make sure we are not pointing to mpParticle
                next();
            }
            
        }
        
        bool isDone() { return mNHIterator == mNHCells.end(); }
        
        ValueType* currentItem() { return *mCellIteratorCur; }
        
        size_t size() const { return mNCandidates; }
    };
};

/* TODO: cutoff > cell size?
 * - may have variable cutoff which may span multiple cells
 *   -> define DIM-D integer offsets to neighbors (fixed per cutoff)
 *      -> then loop and BC-check
 */

/* TODO: More cell lists?
 * User can also choose one of the derived versions:
 * - #CellListNarrow which returns only neighbors which are really within the
 *   given cutoff distance (can be set independently)
 * - #CellListNarrowCopy which is just like #CellListNarrow but it returns
 *   copies (contiguous in memory) of all neighbors
 */
