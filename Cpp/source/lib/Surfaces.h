/**\file Surfaces.h
 * Collection of models of #CSurface.
 *
 * #include "Surfaces.h"
 *
 * Notes:
 * - Implementations are tailored for fast execution of isInside and
 *   signedDistance (rest may be more costly than necessary).
 *
 * @author Gerardo Tauriello
 * @date 2/8/10
 * 
 * @see CSurface
 */

// Notes:
// - is isInside necessary??
// - is it ok to have surfaces without normal? (cube is painful)
//   -> combine with signedDistance?
// for future: just some funny ideas:
// - RotatedSurface<CSurface>? -> same for translation and stretching?

#pragma once

// system includes
#include <cmath>
#include <vector>

// library includes
#include "Environment.h"                // Particle lib environment
#include "BoundingBox.h"
#include "Grids.h"

/**
 * Purely virtual surface (model of #CSurface that allows for inheritance).
 * @see VirtualSurface
 */
class AbstractSurface {
    // Note: dummy implementation just to make concept assertions happy
public:
    /** Virtual destructor (always needed when using polymorphism). */
    virtual ~AbstractSurface() { }
    
    /** \name Concept-Implementation.
     * Implementation of #CSurface.
     */
    //@{
    virtual BoundingBox getBoundingBox() const { return BoundingBox(); }
    virtual bool isInside(const VecType pos) const { return false; }
    virtual Real signedDistance(const VecType pos) const { return 0; }
    virtual VecType normal(const VecType pos) const { return VecType(0); }
    //@}
    
protected:
    /** Prevent construction of "purely virtual" class. */
	AbstractSurface() { }
};

/**
 * DIM-dimensional hypercube of given offset and size (model of #CSurface).
 * That is: 2 points in 1D, a box in 2D and a cube in 3D.
 */
class CubeSurface {
public:
#pragma mark LIFECYCLE
    
    /** Empty constructor. */
    CubeSurface() {
        init(VecType(0), VecType(0));
    }
    
	/** Default constructor. */
	CubeSurface(const VecType offset, const VecType size) {
        init(offset, size);
    }
    
    /** Construct using a bounding box. */
    CubeSurface(const BoundingBox bbox) {
        init(bbox.offset, bbox.size);
    }
    
    /** Construct using array of Reals (size == 2*DIM). */
    CubeSurface(const Real* data) {
#if DIM == 2
        const VecType offset(data[0], data[1]);
        const VecType size(data[2], data[3]);
#elif DIM == 3
        const VecType offset(data[0], data[1], data[2]);
        const VecType size(data[3], data[4], data[5]);
#endif
        init(offset, size);
    }
        
    
    // default destructor, copy constructor and assignment operator used
    
    /** Initialize surface. */
    void init(const VecType offset, const VecType size) {
        mOffset = offset;
        mSize = size;
        mMax = offset + size;
    }
    
#pragma mark CSurface
    
    /** \name Concept-Implementation.
     * Implementation of #CSurface.
     */
    //@{
    BoundingBox getBoundingBox() const { return BoundingBox(mOffset, mSize); }
    bool isInside(const VecType pos) const { return signedDistance(pos) < 0; }
    Real signedDistance(const VecType pos) const {
        // there are a couple of different ways to write this...
        // boils down to getting max. signed distance to 2*DIM planes...
        return std::max((mOffset-pos).max(), (pos-mMax).max());
    }
	
    VecType normal(const VecType pos) const {
        // split up into regions
        const VecType botDist = (mOffset-pos);
        const VecType topDist = (pos-mMax);
        // choose dim (the bigger the better)
        int dim = 0;
        int dir;
        Real distMax;
        if (botDist[dim] > topDist[dim]) {
            dir = -1;
            distMax = botDist[dim];
        } else {
            dir = 1;
            distMax = topDist[dim];
        }
        for (int i = 1; i < DIM; ++i) {
            if (botDist[i] > topDist[i]) {
                if (botDist[i] > distMax) {
                    dir = -1;
                    distMax = botDist[i];
                    dim = i;
                }
            } else {
                if (topDist[i] > distMax) {
                    dir = 1;
                    distMax = topDist[i];
                    dim = i;
                }
            }
        }
        
        // get normal
        VecType n(0);
        n[dim] = dir;
        return n;
    }
    //@}
    
protected:
    /** Corner of hypercube with lowest coordinates. */
    VecType mOffset;
    /** Size of hypercube. */
    VecType mSize;
    /** Corner of hypercube with highest coordinates. That is mOffset+mSize. */
    VecType mMax;
};

/**
 * n-sphere (n=DIM-1) with given radius and center (model of #CSurface).
 * That is: 2 points in 1D, a circle in 2D and a sphere in 3D.
 */
class SphereSurface {
public:
#pragma mark LIFECYCLE
    
    /** Empty constructor. */
    SphereSurface() {
        init(VecType(0), 0);
    }
    
	/** Default constructor. */
	SphereSurface(const VecType center, const Real radius) {
        init(center, radius);
    }
    
    /** Construct using array of Reals (size == DIM + 1). */
    SphereSurface(const Real* data) {
#if DIM == 2
        const VecType center(data[0], data[1]);
        const Real radius = data[2];
#elif DIM == 3
        const VecType center(data[0], data[1], data[2]);
        const Real radius = data[3];
#endif
        init(center, radius);
    }
    
    // default destructor, copy constructor and assignment operator used
    
    /** Initialize surface. */
    void init(const VecType center, const Real radius) {
        mCenter = center;
        mRadius = radius;
    }
    
#pragma mark CSurface
    
    /** \name Concept-Implementation.
     * Implementation of #CSurface.
     */
    //@{
    BoundingBox getBoundingBox() const {
        return BoundingBox(mCenter-mRadius, VecType(mRadius*2));
    }
    bool isInside(const VecType pos) const { return signedDistance(pos) < 0; }
    Real signedDistance(const VecType pos) const {
        return (pos-mCenter).abs() - mRadius;
    }
    VecType normal(const VecType pos) const {
        VecType n = (pos - mCenter);
        return n/n.abs();
    }
    //@}

#pragma mark More Access
public:
    /** Get center. */
    VecType getCenter() const { return mCenter; }
    /** Set center. */
    void setCenter(const VecType center) { mCenter = center; }
    /** Get radius. */
    Real getRadius() const { return mRadius; }
    /** Set radius. */
    void setRadius(const Real radius) { mRadius = radius; }
    
protected:
    /** Center of n-sphere. */
    VecType mCenter;
    /** Radius of n-sphere. */
    Real mRadius;
};

/**
 * Surface given by two DIM-1-dimensional hyperplanes (a channel) (model of #CSurface).
 * That is: 2 points in 1D, 2 lines in 2D and 2 planes in 3D.
 * Both hyperplanes have same normal and normal is aligned with one of the axis.
 * Hyperplanes are given by specifying:
 * - dimension of axis with which normals are aligned
 * - coordinates of two planes in the given dimension
 * Example: ChannelSurface(1, 1.0, 3.0, minD, sizeD) with DIM=2 defines two
 * lines at y = 1.0 and y = 3.0 spanning whole domain (domain given by minD
 * and sizeD -> e.g. VecType(0,0) and VecType(10,10) for x,y in [0,10]).
 */
class ChannelSurface {
public:
#pragma mark LIFECYCLE
    
	/** Empty constructor. */
    ChannelSurface() {
        init(0, 0, 0, VecType(0), VecType(0));
    }
    
	/** Default constructor. */
	ChannelSurface(const unsigned int dimension, 
                   const Real bottom,
                   const Real top,
                   const VecType minD,
                   const VecType sizeD) {
        init(dimension, bottom, top, minD, sizeD);
    }
    
    // default destructor, copy constructor and assignment operator used
    
    /** Initialize surface. */
    void init(const unsigned int dimension, 
              const Real bottom,
              const Real top,
              const VecType minD,
              const VecType sizeD) {
        
        // some checks
        assert(dimension < DIM);
        assert(bottom >= minD[dimension]);
        assert(top >= bottom);
        assert(top <= minD[dimension] + sizeD[dimension]);
        
        // setup
        mDimension = dimension;
        mBottom = bottom;
        mTop = top;
        
        // build bounding box
        mBBox.offset = minD;
        mBBox.offset[dimension] = bottom;
        mBBox.size = sizeD;
        mBBox.size[dimension] = top - bottom;
    }
    
#pragma mark CSurface
    
    /** \name Concept-Implementation.
     * Implementation of #CSurface.
     */
    //@{
    BoundingBox getBoundingBox() const { return mBBox; }
    bool isInside(const VecType pos) const { return signedDistance(pos) < 0; }
    Real signedDistance(const VecType pos) const {
        return std::max(mBottom - pos[mDimension], pos[mDimension] - mTop);
    }
	
    VecType normal(const VecType pos) const {
        VecType n(0);
        if (mBottom + mTop > 2*pos[mDimension]) {
            // upper half
            n[mDimension] = 1;
        } else {
            // lower half
            n[mDimension] = 1;
        }
        return n;
    }
    //@}
    
protected:
    /** Bounding box enclosing surface. */
    BoundingBox mBBox;
    /** Dimension to use. */
    unsigned int mDimension;
    /** Coordinate of bottom hyperplane. */
    Real mBottom;
    /** Coordinate of top hyperplane. */
    Real mTop;
};

/**
 * Surface given by a cylinder (meaning for DIM != 3 is different) (model of #CSurface).
 * 
 * Cylinder is built as being stretched over the whole domain in a given
 * dimension and having a n-sphere (n=DIM-2) as cross section. In 3D that's
 * just a cylinder, in 2D a channel and in 1D it's 0 everywhere.
 */
class CylinderSurface {
public:
#pragma mark LIFECYCLE

    /** Empty constructor. */
    CylinderSurface() {
        init(0, VecType(0), 0, VecType(0), VecType(0));
    }
    
	/** Default constructor. */
	CylinderSurface(const unsigned int dimension, 
                    const VecType center, 
                    const Real radius,
                    const VecType minD,
                    const VecType sizeD
                    ): mDimension(dimension), mCenter(center), mRadius(radius) {
        
        // build bounding box
        mBBox.offset = mCenter-mRadius;
        mBBox.offset[dimension] = minD[dimension];
        mBBox.size = VecType(mRadius*2);
        mBBox.size[dimension] = sizeD[dimension];
    }
    
    // default destructor, copy constructor and assignment operator used
    
    /** Initialize surface. */
    void init(const unsigned int dimension, 
              const VecType center, 
              const Real radius,
              const VecType minD,
              const VecType sizeD) {
        
        // some checks
        assert(dimension < DIM);
        
        // setup
        mDimension = dimension;
        mCenter = center;
        mRadius = radius;
        
        // build bounding box
        mBBox.offset = mCenter-mRadius;
        mBBox.offset[dimension] = minD[dimension];
        mBBox.size = VecType(mRadius*2);
        mBBox.size[dimension] = sizeD[dimension];
    }
    
#pragma mark CSurface
    
    /** \name Concept-Implementation.
     * Implementation of #CSurface.
     */
    //@{
    BoundingBox getBoundingBox() const { return mBBox; }
    bool isInside(const VecType pos) const { return signedDistance(pos) < 0; }
    Real signedDistance(VecType pos) const {
        // we ignore the given dimension
        pos[mDimension] = mCenter[mDimension];
        return (pos-mCenter).abs() - mRadius;
    }
	
    VecType normal(VecType pos) const {
        // we ignore the given dimension
        pos[mDimension] = mCenter[mDimension];
        // and do rest like for sphere
        VecType n = (pos - mCenter);
        return n/n.abs();
    }
    //@}
    
protected:
    /** Bounding box enclosing surface. */
    BoundingBox mBBox;
    /** Dimension to use. */
    unsigned int mDimension;
    /** Center of n-sphere. */
    VecType mCenter;
    /** Radius of n-sphere. */
    Real mRadius;
};

/**
 * Surface given by a grid (model of #CSurface).
 * Assumes that grid contains a levelset with signed distance property.
 * As such the normal should be given by the gradient on the grid.
 * @tparam Grid     Model of #CGrid with elements of type Real.
 * @tparam renorm   Renormalize levelset gradient when returning normal (should
 *                  not be needed if we have signed distance property).
 */
template <typename Grid, bool renorm>
class GridSurface {
    // check template parameter
	CONCEPT_ASSERT(CGrid< Grid >);
	CONCEPT_ASSERT(CSameType< typename Grid::ElementType, Real >);
	CONCEPT_ASSERT(CSameType< typename Grid::GradientType, VecType >);

public:
#pragma mark LIFECYCLE
    
	/** Default constructor. */
	GridSurface(const Grid& g): mG(g) {}
    
    // default destructor, copy constructor and assignment operator used
    
#pragma mark CSurface
    
    /** \name Concept-Implementation.
     * Implementation of #CSurface.
     */
    //@{
    BoundingBox getBoundingBox() const {
        return BoundingBox(mG.getDomainOffset(), mG.getDomainSize());
    }
    bool isInside(const VecType pos) const { return signedDistance(pos) < 0; }
    Real signedDistance(const VecType pos) const { return mG(pos); }
    VecType normal(const VecType pos) const {
        if (renorm) {
            const VecType n = mG.gradient(pos);
            const Real nabs = n.abs();
            if (nabs > 1e-6) {
                return n/nabs;
            } else {
                return n;
            }
        } else {
            return mG.gradient(pos);
        }
    }
    //@}
    
public:
    /** Enclosed grid (free access). */
    Grid mG;
};

/////////////////////////////////////////////////////////////////////
#pragma mark Surface Adaptors
/////////////////////////////////////////////////////////////////////

/**
 * Virtual surface encapsulating any other Surface (model of #CSurface).
 * @tparam Surface  Model of #CSurface.
 */
template <class Surface>
class VirtualSurface: public AbstractSurface {
    CONCEPT_ASSERT(CSurface<Surface>);
public:
	/** Empty constructor. */
    VirtualSurface() { }
    
	/** Default constructor. */
	VirtualSurface(const Surface& surface): mS(surface) { }
    
    // default destructor, copy constructor and assignment operator used
    
#pragma mark CSurface
    
    /** \name Concept-Implementation.
     * Implementation of #CSurface.
     */
    //@{
    BoundingBox getBoundingBox() const { return mS.getBoundingBox(); }
    bool isInside(const VecType pos) const { return mS.isInside(pos); }
    Real signedDistance(const VecType pos) const { return mS.signedDistance(pos); }
    VecType normal(const VecType pos) const { return mS.normal(pos); }
    //@}
    
public:
    /** Free access to enclosed surface. */
    Surface mS;
};

// NOTE: RestrictSurface may be superseeded by more general one which also
//       allows for enlargement and properly handles domain bounds.

/**
 * Changes given surface by returning a surface that is smaller by a given value (model of #CSurface).
 * Note that no checks are being done. User has to guarantee that restriction 
 * makes sense.
 * @tparam Surface  Model of #CSurface.
 */
template <class Surface>
class RestrictSurface {
    CONCEPT_ASSERT(CSurface<Surface>);
public:
#pragma mark LIFECYCLE
    
	/** Empty constructor. */
    RestrictSurface(): mOffset(0) { }
    
	/** Default constructor (offset >= 0 expected). */
	RestrictSurface(const Surface& surface, const Real offset): mSurface(surface), mOffset(offset) { }
    
    // default destructor, copy constructor and assignment operator used
    
    /** Initialize surface. */
	void init(const Surface& surface, const Real offset) {
        mSurface = surface;
        mOffset = offset;
    }
    
#pragma mark CSurface
    
    /** \name Concept-Implementation.
     * Implementation of #CSurface.
     */
    //@{
    BoundingBox getBoundingBox() const {
        BoundingBox result = mSurface.getBoundingBox();
        result.offset += mOffset;
        result.size -= 2*mOffset;
        return result;
    }
    bool isInside(const VecType pos) const { return signedDistance(pos) < 0; }
    Real signedDistance(const VecType pos) const {
        return mSurface.signedDistance(pos) + mOffset;
    }
	VecType normal(VecType pos) const {
		return mSurface.normal(pos);
    }
    //@}
    
protected:
    /** Surface being restricted. */
    Surface mSurface;
    /** Amount of restriction. */
    Real mOffset;
};
