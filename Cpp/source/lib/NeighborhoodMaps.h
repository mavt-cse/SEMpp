/**\file NeighborhoodMaps.h
 * Collection of neighborhood maps (models of #CNeighborhoodMap).
 *
 * Includes all available models of #CNeighborhoodMap and a dummy implementation
 * which just returns all particles as neighborhood.
 *
 * #include "NeighborhoodMaps.h"
 *
 * @author Gerardo Tauriello
 * @see CNeighborhoodMap, CellList.h
 * 
 * \example ExampleNeighborhoodMap.cpp
 * Example showing how to use neighborhood maps.
 */

#pragma once

// library includes
#include "Environment.h"                // Particle lib environment
#include "CParticles.h"
// other neighborhood maps
#include "CellList.h"

/**
 * Dummy neighborhood map (the simplest model of #CNeighborhoodMap).
 * Returns neighborhood containing all particles in the system.
 *
 * \par Example usage:
\code
DummyNeighborhoodMap nhMap(iter);  // iter is Iterator over all particles
// ... loop over particles ... -> part
DummyNeighborhoodMap::IteratorType iter = nhMap.getHood(part);
// ... update particles using that neighborhood ...
\endcode
 *
 * @tparam Particle     A model of #CBasicParticle
 * @tparam Iterator     A model of #CIterator with Particle and 
 *                      Iterator::ValueType being the same type
 */
template <typename Particle, typename Iterator>
class DummyNeighborhoodMap {
    CONCEPT_ASSERT(CBasicParticle<Particle>);
    CONCEPT_ASSERT(CIterator<Iterator>);
    CONCEPT_ASSERT(CSameType<Particle, typename Iterator::ValueType>);
    
protected:
    /**
     * Storage for copy of iterator.
     * Note that all neighborhoods will use a copy of this iterator and so they
     * will not interfere with each other.
     */
    Iterator mIterator;
    
public:
    
    /**
     * Iterator for all potential neighbors (model of #CIterator).
     * This will automatically exclude the particle itself.
     */
    template <typename PType>
    class DummyIterator {
    protected:
        // DATA
        /** Storage for copy of iterator. */
        Iterator mIterator;
        /** Pointer to particle for which these are potential neighbors. */
        const Particle* mpParticle;
        
        // OPERATIONS
        /** Make sure we never point to mpParticle. */
        void checkSelf() {
            // break if at end
            if (isDone()) return;
            // make sure we are not pointing to mpParticle
            if (currentItem() == mpParticle) next();
        }
    public:
        /** Default constructor. */
        DummyIterator(const Particle* part, const Iterator& iter):
            mpParticle(part),
            mIterator(iter) { }
        
        // default destructor, copy constructor and assignment operator used
        
        // implementation of CIterator
        typedef PType ValueType;
        
        void first() {
            mIterator.first();
            checkSelf();
        }
        
        void next() {
            mIterator.next();
            // make sure we are not pointing to mpParticle
            checkSelf();
        }
        
        bool isDone() { return mIterator.isDone(); }
        
        ValueType* currentItem() { return mIterator.currentItem(); }
    };
    
    /** Construct with a given iterator (copied). */
    DummyNeighborhoodMap(const Iterator& iter): mIterator(iter) { }
    
    // no constructor with no arguments
    // default copy constructor, assignment operator and destructor used
    
    /** \name Concept-Implementation.
     * Implementation of #CNeighborhoodMap.
     */
    //@{
    typedef DummyIterator<const Particle> IteratorType;
    typedef DummyIterator<Particle> IteratorTypeVar;
    typedef Particle ParticleType;
    
    IteratorType getHood(const Particle& part) const {
        return IteratorType(&part, mIterator);
    }
	
    IteratorType getHood(const VecType& pos) const {
        return IteratorType(NULL, mIterator);
    }
    
    IteratorTypeVar getHoodVar(const Particle& part) const {
        return IteratorTypeVar(&part, mIterator);
    }
    IteratorTypeVar getHoodVar(const VecType& pos) const {
        return IteratorTypeVar(NULL, mIterator);
    };
    //@}
};