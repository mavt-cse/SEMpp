/**
 *  SCE.h
 *  Particles
 *  
 *  Subcellular Element Implementation
 *	- models the concept CParticle
 *
 *	- implements the basic SCE model as described in Sandersius:2008
 *  - allows for multiple cell types
 * 
 *  Created by Florian Milde on 1/8/10.
 *  Copyright 2010 ETHZ. All rights reserved.
 *
 */

#pragma once

#include "Utilities.h"
#include "Environment.h"
#include "CParticles.h"
#include "PotentialFunctions.h"
#include "Functions.h"
#include "SCEParameters.h"

#include "SCEEnvironment.h"

struct SCEParams{
    //cell parameters
    int nSCEs[CELLTYPES];
    Real G0_D[CELLTYPES];	// G0 phase length (quiescence interval);
    Real I_D[CELLTYPES];	// I  phase length (doubling interval);
    Real M1_D[CELLTYPES];	// M1 phase length (quiescence after growth);
    Real M2_D[CELLTYPES];	// M2 phase length (cell splitting);
    Real M_D[CELLTYPES];	// M  phase length (cell migration polarization);
    Real rCell[CELLTYPES];  // Cell Radius
    //sce parameters
    Real potIntraParams[CELLTYPES][NPOTP];           // intra potential interaction parameters: (rho,u0,1/eq^2)
    Real potInterParams[CELLTYPES][CELLTYPES][NPOTP]; // intra potential interaction parameters: (rho,u0,1/eq^2)
    Real std[CELLTYPES];                             // standard deviation for fluctuation
    Real ieta[CELLTYPES];                            // viscosity parameter
};

class SCE {
#pragma mark TYPEDEFS
    // typedefs
    typedef SCE SelfType;
    
public:
    //public typedefs
    typedef VectorElement<Real,DIM> StateType;
    
#pragma mark LIFECYCLE
    // default constructor, setting everything to zero
    SCE(){
	mPos=VecType(0.);
	mVel=VecType(0.);
	mStateT1=StateType(0.);
	mCellType = 0;
	mCellId = 0;
	mSceId = ++SelfType::sGlobalSceId;
	_init();
    };
    
    // takes particle position, cell type and cellId
    SCE(VecType _pos, int _cellType, int _cellId){
	mPos=_pos;
	mVel=VecType(0.);
	mStateT1=StateType(0.);
	mCellType = _cellType;
	mCellId = _cellId;
	mSceId = ++SelfType::sGlobalSceId;
	_init();
    };
    
    // takes particle position, sce id, cell type and cellId
    // whatchout: Global Sce id is set to max of the seen id's
    SCE(VecType _pos, int _sceId, int _cellType, int _cellId){
	mPos=_pos;
	mVel=VecType(0.);
	mStateT1=StateType(0.);
	mCellType = _cellType;
	mCellId = _cellId;
	mSceId = _sceId;
	SelfType::sGlobalSceId = std::max(SelfType::sGlobalSceId,_sceId);
	_init();
    };
    
    void _init(){
	mPotInter = 0.;
	mPotIntra = 0.;
	mDensIntra = 0.;
	mDensInter = 0.;
	mPolarDist = 0.;
	mPolyFrac=1.;
	// membrane
#ifdef __MEMBRANE2__
	mMembrane=0;
	mNormal=0;
	angles.reserve(Params::sCells[mCellType].nSce);
#endif
    }
    
    
#pragma mark CBasicParticle
    // particle access methods
    VecType getPos()const {return mPos;};
    VecType getVel()const {return mVel;};
    
#pragma mark CParticle
    void resetRHS(){mVel=0.;}
    
    void setState(const StateType& state){
	mPos=state;
    }
    
    StateType getState() const{
	return mPos;
    }
    
    StateType getRHS() const{
	return mVel;
    }		
    
    void setStateT1(const StateType& _state){mStateT1=_state;}
    StateType getStateT1() const {return mStateT1;}
    
#pragma mark Accessors
    
    // sce methods
    void setPos(const VecType& _pos){mPos = _pos;}
    int getSceId()const {return mSceId;}
    void setCellId(int _cellId){ mCellId=_cellId;}
    int getCellId()const {return mCellId;}
    void setCellType(int _cellType){ mCellType=_cellType;}
    int getCellType()const {return mCellType;}
    void resetPot(){mPotInter=0.;mPotIntra=0;}
    Real getPotInter()const{return mPotInter;}
    void setPotInter(const Real _pot){mPotInter=_pot;}
    Real getPotIntra()const{return mPotIntra;}
    void setPotIntra(const Real _pot){mPotIntra=_pot;}
    void resetDens(){mDensInter=0.;mDensIntra=0.;}
    Real getDensInter()const{return mDensInter;}
    void setDensInter(const Real _dens){mDensInter=_dens;}
    Real getDensIntra()const{return mDensIntra;}
    void setDensIntra(const Real _dens){mDensIntra=_dens;}
    Real getPolarDist()const{return mPolarDist;}
    void setPolarDist(Real _pol){mPolarDist = _pol;}
    Real getPolyFrac()const{return mPolyFrac;}
    void setPolyFrac(Real _polyFrac){mPolyFrac=_polyFrac;}
    bool isMembrane()const{
#ifdef __MEMBRANE2__
	return mMembrane;
#else
	return(mDensIntra<=CellParameters::densThr || mDensInter>=InterCellParameters::densThr);
#endif
    }
    
    
    // returns pointer to the potential parameters for interaction with specific particle
    //	Real* getPotParams(SelfType* _part){
    //		return (mCellId == _part->getCellId() ? &sPotIntraParams[mCellType*NPOTP] : &sPotInterParams[mCellType*CELLTYPES*NPOTP+_part->getCellType()*NPOTP]);
    //	};
    
#pragma mark Operations
    
    template <typename IteratorType>
    void computeRHS(IteratorType _iter){
	resetRHS();
	resetPot();
	resetDens();
	updateRHS(_iter);
    }
    
    template <typename IteratorType>
    void updateRHS(IteratorType _iter){
        ///////////
        // recepie
        // - concept checks
        // - add particle contributions stored in _iter
        // - NEW for same cell particles, membrane detection
        // - add fluctuation
        // - make viscus
        ///////////
        
        //check that itarator returns self type
        CONCEPT_ASSERT(CIterator<IteratorType>);
        CONCEPT_ASSERT(CSameType<SCE, typename IteratorType::ValueType>);
        
        // membrane detection
#ifdef __MEMBRANE2__
        angles.clear();
#endif
        
        // iterate through iterator and add contributions to RHS
        // assume that iterator hold particles of neighborhood of same type, excluding self
        for (_iter.first(); !_iter.isDone(); _iter.next()) {
            SelfType* item = _iter.currentItem();
            if(mCellId ==item->getCellId()){
//				PotentialFunctions::addPotentialFM10DensPot(mPos,item->getPos(),mVel,mDensIntra,mPotIntra,Params::sCells[mCellType].potIntraParams);
                PotentialFunctions::addPotentialFM10DensPotPoly(mPos,item->getPos(),mVel,mDensIntra,mPotIntra,Params::sCells[mCellType].potIntraParams,fmax(item->getPolyFrac()*mPolyFrac,M_EPS));
                
#ifdef __MEMBRANE2__
                // membrane detection
                const VecType dx = item->getPos()-mPos;
                if(dx.abs()<Params::sCells[mCellType].req*1.5)
                    angles.push_back(std::atan2(dx[1],dx[0]));
#endif
            } else {
//				PotentialFunctions::addPotentialFM10DensPot(mPos,item->getPos(),mVel,mDensInter,mPotInter,Params::sInterCells[mCellType][item->getCellType()].potInterParams);
                PotentialFunctions::addPotentialFM10DensPotPoly(mPos,item->getPos(),mVel,mDensInter,mPotInter,Params::sInterCells[mCellType][item->getCellType()].potInterParams,fmax(item->getPolyFrac()*mPolyFrac,M_EPS));
            }
        }
        
#ifdef __MEMBRANE2__
        // membrane detection
        // sort them
        std::sort(angles.begin(),angles.end());
        // find largest interval and medium angle -> max_interval, normal_angle
        Real max_interval = 0;
        Real normal_angle;
        if (!angles.empty()) {
            Real last_angle = angles.back() - 2*M_PI;
            for (std::vector<Real>::iterator it = angles.begin(); it != angles.end(); ++it) {
                const Real cur_angle = *it;
                const Real cur_interval = cur_angle - last_angle;
                if (cur_interval > max_interval) {
                    max_interval = cur_interval;
                    normal_angle = 0.5 * (last_angle + cur_angle);
                }
                last_angle = cur_angle;
            }
        } else {
            // WEIRD case
            max_interval = 2*M_PI;
            normal_angle = 0;
        }
        
        //Threshold at approx. 90 Deg
        if(max_interval>1.5){
            mMembrane=true;
            mNormal[0]=cos(normal_angle);
            mNormal[1]=sin(normal_angle);
        }else {
            mMembrane=false;
        }
#endif
	
	
	// add Fluctuation to RHS
	// moved to outside for multi stage integrators
	//addFluctuationRHS();
	
	// add Walls
	//addWallsRHS();
	if(Params::sSim.bc==0) addBoundaryForce(Params::sSim.boundary);
	
	
	// add viscus term
	mVel*=Params::sCells[mCellType].ieta;
	
	
    };
    
    void addFluctuationRHS(){
	Real ur1=(Real)rand()/(Real)RAND_MAX;
	Real ur2=(Real)rand()/(Real)RAND_MAX;
	Real alpha=2.*M_PI*(Real)rand()/(Real)RAND_MAX;
	//Box Muller
	Real nr1= sqrt(-2.*log(ur1))*cos(2.*M_PI*ur2)*Params::sCells[mCellType].std;
#if DIM==2
	VecType fluc = VecType(cos(alpha)*nr1,sin(alpha)*nr1);
#elif DIM==3
	// dont rmember from where..
	Real z=2.f*(Real)rand()/(Real)RAND_MAX-1.;
	Real sz=sqrt(1.f-z*z);	
	VecType fluc = VecType( cos(alpha)*nr1*sz,
			       sin(alpha)*nr1*sz,
			       z*nr1);
#endif
	//@@@ corrected random fluctuation scaling with dt??@@@
	mVel += fluc/sqrt(Params::sSim.dt);
    };
    
    
    /** Add force from boundary. */
    template <typename Surface>
    void addBoundaryForce(const Surface& boundarySurface) {
        // check for surface
        CONCEPT_ASSERT(CSurface<Surface>);
        
        // Note: this is a bit of a hack
        // -> assumes strong repulsion at distance req/2
	
        Real d = boundarySurface.signedDistance(mPos);
	VecType bn = boundarySurface.normal(mPos);
        //bn=(d>0)?bn:-bn;	// switch the direction of the vector if distance is positive
	bn=-bn;
	d=fmin(Params::sCells[mCellType].req,fmax(Params::sCells[mCellType].req/200,-d));
	
	VecType tmp=bn*((Params::sCells[mCellType].req/d)-1);
	mVel+=tmp;
	
	// density
	if(d<Params::sCells[mCellType].req/2*CellParameters::densRad)mDensInter++;
	
    };
    
#pragma mark STATIC FUNCTIONS
    // returns current highest Global Sce Id
    static int getGlobalSceId(){return SelfType::sGlobalSceId;};
    //set the inter potential parameters for one celltype
    
    
    // TOGO
    // cell parameters
    static void setNSCEs(int _cellType, int _nSCEs){
	sSCEParams.nSCEs[_cellType]=_nSCEs;
    };
    
    static int getNSCEs(int _cellType) {return SelfType::sSCEParams.nSCEs[_cellType];};
    
    static void setG0_D(int _cellType, Real _param){
	sSCEParams.G0_D[_cellType]=_param;
    }
    static Real getG0_D(int _cellType) {return SelfType::sSCEParams.G0_D[_cellType];};
    
    static void setI_D(int _cellType, Real _param){
	sSCEParams.I_D[_cellType]=_param;
    }
    static Real getI_D(int _cellType) {return SelfType::sSCEParams.I_D[_cellType];};
    
    static void setM1_D(int _cellType, Real _param){
	sSCEParams.M1_D[_cellType]=_param;
    }
    static Real getM1_D(int _cellType) {return SelfType::sSCEParams.M1_D[_cellType];};
    
    static void setM2_D(int _cellType, Real _param){
	sSCEParams.M2_D[_cellType]=_param;
    }
    static Real getM2_D(int _cellType) {return SelfType::sSCEParams.M2_D[_cellType];};
    
    static void setM_D(int _cellType, Real _param){
	sSCEParams.M_D[_cellType]=_param;
    }
    static Real getM_D(int _cellType) {return SelfType::sSCEParams.M_D[_cellType];};
    
    static void setRCell(int _cellType, Real _param){
	sSCEParams.rCell[_cellType]=_param;
    }
    static Real getRCell(int _cellType) {return SelfType::sSCEParams.rCell[_cellType];};
    
    
    // sce parameters
    static void setPotIntraParams(int _cellType,Real* _params){
	for(int i=0;i<NPOTP;i++){
	    sSCEParams.potIntraParams[_cellType][i]=_params[i];
	}
    };	
    
    //set the intra Potential Parameters between 2 cell types
    static void setPotInterParams(int _cellType1,int _cellType2,Real* _params){
	for(int i=0;i<NPOTP;i++){
	    sSCEParams.potInterParams[_cellType1][_cellType2][i]=_params[i];
	    sSCEParams.potInterParams[_cellType2][_cellType1][i]=_params[i];
	}
    };
    // set the fluctuation parameter for one cell type
    static void setFlucParam(int _cellId,Real _std){ 
	sSCEParams.std[_cellId]=_std;
    };
    // set the fluctuation parameter for all cell types
    static void setFlucParams(Real* _std){
	for(int i=0;i<CELLTYPES;i++){sSCEParams.std[i]=_std[i];}
    };
    // set the viscosity parameter for one cell type
    static void setViscParam(int _cellId,Real _eta){
	sSCEParams.ieta[_cellId]=1./_eta;
    };
    // set the viscosity parameter for all cell types
    static void setViscParams(Real* _eta){
	for(int i=0;i<CELLTYPES;i++){sSCEParams.ieta[i]=1./_eta[i];}
    };
    //ENDTOGO
    
#pragma mark STATIC FUNCTIONS
    static bool greater(SCE* elem1, SCE* elem2){
	return elem1->getPolarDist() > elem2->getPolarDist();
    }
    
    static bool denser(SCE* elem1, SCE* elem2){
	return (elem1->getDensIntra()+elem1->getDensInter()) > (elem2->getDensIntra()+elem2->getDensInter());
    }
    
    static bool denserIntra(SCE* elem1, SCE* elem2){
	return elem1->getDensIntra() > elem2->getDensIntra();
    }
    
    static bool denserInter(SCE* elem1, SCE* elem2){
	return elem1->getDensInter() > elem2->getDensInter();
    }
    
    static bool sparser(SCE* elem1, SCE* elem2){
	return (elem1->getDensIntra()+elem1->getDensInter()) < (elem2->getDensIntra()+elem2->getDensInter());
    }
    
    static bool sparserIntra(SCE* elem1, SCE* elem2){
	return elem1->getDensIntra() < elem2->getDensIntra();
    }
    
    static bool sparserInter(SCE* elem1, SCE* elem2){
	return elem1->getDensInter() < elem2->getDensInter();
    }
    
#pragma mark STATIC MEMBERS
    // static members
    static int sGlobalSceId;			// counter to keep track of id's
    static SCEParams sSCEParams;		// SCE Interaction Parameters
    
protected:
#pragma mark PROTECTED
    VecType mPos;
    VecType mVel;
    StateType mStateT1;
    Real mPotIntra;
    Real mPotInter;
    Real mDensIntra;
    Real mDensInter;
    Real mPolarDist;
    Real mPolyFrac;
    
    // membrane detection
#ifdef __MEMBRANE2__
    std::vector<Real> angles;
    bool mMembrane;
    VecType mNormal;
#endif
    
    int mSceId;	// id of SCE
    int mCellId;	// id of Cell to which SCE bellongs
    int mCellType;  // type of Cell
};