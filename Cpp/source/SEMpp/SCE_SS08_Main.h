/*
 *  SCE_SS08_Main.h
 *  Particles
 *
 *  Created by Florian Milde on 1/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

// system includes
#include <stdio.h>						// Standard I/O
#include <stdlib.h>						// Standard helper
#include <iostream>                     // Standard C++ I/O

// library includes
#include "Environment.h"

// remove if no OpenGL
#ifndef __NO_GL__
#define __WITH_GL__
#endif

// remove if no Grid
#define __SKETCH_GRID__

// DEBUG level (no = 0, max = 10)
const int DEBUG_LEVEL = 1;
