/*
 *  FileHandler.h
 *  Particles
 *
 *  Created by Florian Milde on 2/25/10.
 *  Copyright 2010 EtH Zurich. All rights reserved.
 *
 */
#pragma once

#include <stdio.h>
#include <string>
#include <iostream>

#include "SCE.h"
#include "SCECell.h"

#include "Iterators.h"

class FileHandler {

#pragma mark WRITE
public:
	template <typename IteratorType>
	static void writeCellsToFile(IteratorType* cellIter, std::string name, int it=0){
		/**
		/* RECIPIE
		/* write Cells to file: name gives file name pattern: "folder/filename_%5d.dat"
		/* iterate CellIter to file
		/* iterate through SCE's in Cell
		/* dumo to file
		/* open file
		/*
		/* File Format
		/* cellId cellYype cellPhase cellClock iDur polX polY [polZ]
		/* sceId posX posY [posZ]
		/* ...
		/* cellId cellType
		/* sceId posX posY [posZ]
		/* ...
		*/
		
		int i;
		//open file
		char fname[60];
		sprintf(fname,name.c_str(),it);
		FILE * file = fopen(fname,"w");
        if (file==NULL) {
            std::cout << "Failed to write " << fname << "!\n";
            abort();
        }
		//run through cells
		for(cellIter->first(); !cellIter->isDone(); cellIter->next()){
			typename IteratorType::ValueType& cell = *(cellIter->currentItem());
			typename IteratorType::ValueType::CollectionIterator sceIter = cell.getIterator();
			// write to file
//			int i = cell.getCellPhase();
//			int j = C_MI;
//			int k = C_G0;
//			int l = C_I;
			VecType pol = cell.getPolarity();
			VecType pos = cell.getPos();
#if DIM==2
			fprintf(file,"CELL %d %d %d %d %f %f %f %f %f %f",cell.getCellId(),cell.getCellType(),cell.getCellPhenotype(),cell.getCellPhase(),
					(float)cell.getClock(),(float)cell.getIDur(),(float)pol[0],(float)pol[1],(float)pos[0],(float)pos[1]);
#elif DIM==3
			fprintf(file,"CELL %d %d %d %d %f %f %f %f %f %f %f %f",cell.getCellId(),cell.getCellType(),cell.getCellPhenotype(),cell.getCellPhase(),
					(float)cell.getClock(),(float)cell.getIDur(),(float)pol[0],(float)pol[1],(float)pol[2],(float)pos[0],(float)pos[1],(float)pos[2]);
#endif
			typename IteratorType::ValueType::StateType cellState=cell.getState();
			for(i=0;i<NSPEC-1;i++){
				fprintf(file," %f",cellState[i]);
			}
			fprintf(file," %f\n",cellState[i]);
			
			// run through sce's of cell
			for(sceIter.first(); !sceIter.isDone(); sceIter.next()){
				typename IteratorType::ValueType::CollectionIterator::ValueType& sce = *(sceIter.currentItem());
				// write them to file
				VecType pos = sce.getPos();
				int membrane = (sce.getDensIntra()<=CellParameters::densThr || sce.getDensInter()>=InterCellParameters::densThr);
#if DIM==2
				fprintf(file,"SCE %d %f %f %f %f %d\n",sce.getSceId(),(float)pos[0],(float)pos[1],sce.getDensInter(),sce.getDensIntra(),membrane);
#elif DIM==3
				fprintf(file,"SCE %d %f %f %f %f %f %d\n",sce.getSceId(),(float)pos[0],(float)pos[1],(float)pos[2],sce.getDensInter(),sce.getDensIntra(),membrane);
#endif				
			}
		}
		fclose(file);
	}
						
#pragma mark READ
	template<typename SCECellType>
	static void readCellsFromFile(std::vector<SCECellType>& vCells,std::string name,int it){
		/**
		 /* RECIPIE
		 /* read Cells from file as written above
		 /* put ino cell collection Cells
		 */
		int i;
		// open file
		char fname[60];
		sprintf(fname,name.c_str(),it);
		FILE * pFile=fopen(fname,"r");
        if (pFile==NULL) {
            std::cout << "Failed to open " << fname << "!\n";
            abort();
        }
        
		int cellId,cellType,cellPhenotype,cellPhase;
		float cellClock,cellIDur,polX,polY,polZ;
		float densIntra, densInter;
		float fCellState;
		int membrane;
		typename SCECellType::StateType cellState;
		VecType polarity;
		int sceId;
		float posX,posY,posZ;
		
		//read cells
#if DIM==2
		while(fscanf(pFile,"CELL %d %d %d %d %f %f %f %f %f %f",&cellId,&cellType,&cellPhenotype,&cellPhase,&cellClock,&cellIDur,&polX,&polY,&posX,&posY)==10){
			polarity = VecType(polX,polY);
#elif DIM==3
		while(fscanf(pFile,"CELL %d %d %d %d %f %f %f %f %f %f %f %f",&cellId,&cellType,&cellPhenotype,&cellPhase,&cellClock,&cellIDur,&polX,&polY,&polZ,&posX,&posY,&posZ)==12){
			polarity = VecType(polX,polY,polZ);
#endif
			typename SCECellType::StateType cellState;
			for(i=0;i<NSPEC-1;i++){
				fscanf(pFile," %f",&fCellState);
				cellState[i]=fCellState;
			}
			fscanf(pFile," %f\n",&fCellState);
			cellState[i]=fCellState;
			SCECellType cell = SCECellType(cellId,cellType,cellPhenotype,cellPhase,cellClock,cellIDur,polarity);
			cell.setState(cellState);
			
			//read SCEs
#if DIM==2
			while(fscanf(pFile,"SCE %d %f %f %f %f %d\n",&sceId,&posX,&posY,&densInter,&densIntra,&membrane)==6){
				VecType pos = VecType(posX,posY);
#elif DIM==3
			while(fscanf(pFile,"SCE %d %f %f %f %f %f %d\n",&sceId,&posX,&posY,&posZ,&densInter,&densIntra,&membrane)==7){
				VecType pos = VecType(posX,posY,posZ);
#endif
				typename SCECellType::SCEType sce(pos,sceId,cellType,cellId);
				sce.setDensInter(densInter);
				sce.setDensIntra(densIntra);
				cell.addSCE(sce);
//				cell.addSCE(pos,sceId,densInter,densIntra);
			}
			vCells.push_back(cell);
		}
        fclose(pFile);
	}
};