/*
 *  SCE.cpp
 *  Particles
 *
 *  Created by Florian Milde on 1/8/10.
 *  Copyright 2010 ETHZ. All rights reserved.
 *
 */

#include "SCE.h"
#include "PotentialFunctions.h"
#include "CParticles.h"

//initialize static variables
int  SCE::sGlobalSceId=0;
SCEParams SCE::sSCEParams;
