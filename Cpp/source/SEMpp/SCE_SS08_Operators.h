/*
 *  SCE_SS08_Operators.h
 *  Particles
 *
 *  Created by Florian Milde on 4/7/11.
 *  Copyright 2011 ETH Zurich. All rights reserved.
 *
 */


#pragma once

// project includes
#include "SCE_SS08_Model.h"
#include "SCEParameters.h"

// library includes
#include "Environment.h"
#include "Operators.h"
#include "WorkDistributors.h"
#include "Functions.h"

// system includes
#include <cmath>
template <typename NH>
class UpdateParticlesOperator {
protected:
    /** Storage for ref to neighborhood map. */
    const NH& mNhMap;
public:
    /** Default constructor. */
    UpdateParticlesOperator(const NH& nhMap): mNhMap(nhMap) { }
    
    // no constructor with no arguments
    // default copy constructor and destructor used
    // assignment operator will fail due to read-only reference
    
    /** THE Operator. */
    template <typename Iterator>
    void operator()(Iterator iter) const {
        // assert concepts
        CONCEPT_ASSERT(CIterator<Iterator>);
        // go through them
        for (iter.first(); !iter.isDone(); iter.next()) {
            typename Iterator::ValueType& part = *(iter.currentItem());
            part.computeRHS(mNhMap.getHood(part));
        }
    }
};

class UpdateCellsOperator {
protected:
    /** Storage for ref to neighborhood map. */
public:
    /** Default constructor. */
    UpdateCellsOperator(){ }
    
    // no constructor with no arguments
    // default copy constructor and destructor used
    // assignment operator will fail due to read-only reference
    
    /** THE Operator. */
    template <typename Iterator>
    void operator()(Iterator iter) const {
        // assert concepts
        CONCEPT_ASSERT(CIterator<Iterator>);
        // go through them
        for (iter.first(); !iter.isDone(); iter.next()) {
            typename Iterator::ValueType& cell = *(iter.currentItem());
            cell.computeRHS();
        }
    }
};

class OperatorFluctuation {
protected:
    /** Store sqrt(dt) for fluctation. */
    const Real mSqrtDt;
public:
    /** Default constructor. */
    OperatorFluctuation(const Real dt): mSqrtDt(std::sqrt(dt)) {
    }
    
    // no constructor with no arguments
    // default copy constructor and destructor used
    // assignment operator will fail due to read-only reference
    
    /** THE Operator. */
    template <typename Iterator>
    void operator()(Iterator iter) const {
        // assert concepts
        CONCEPT_ASSERT(CIterator<Iterator>);
        CONCEPT_ASSERT(CSameType<SCE, typename Iterator::ValueType>);
        // go through them
        for (iter.first(); !iter.isDone(); iter.next()) {
            SCE& part = *(iter.currentItem());
            
            // compute fluctuation vector
            const Real nr = Functions::randNorm();
            const Real nr1 = nr * mSqrtDt * Params::getFlucParam(part.getCellType());
#if DIM==2
            const VecType fluc = VE_Utilities::OrientRand2D<Real>() * nr1;
#elif DIM==3
            const VecType fluc = VE_Utilities::OrientRand3D<Real>() * nr1;
#endif
            
            // apply it
            part.setPos(part.getPos() + fluc);
        }
    }
};

/** THE work horse. Computes the rhs for the SEM. */

//template <typename NH>
//class OperatorRhs {
//    // concepts
//    CONCEPT_ASSERT(CNeighborhoodMap<NH>);
//protected:
//    /** Storage for ref to neighborhood map. */
//    const NH& mrNhMap;
//    /** Link to Model instance. */
//    Model& mrModel;
//public:
//    /** Default constructor. */
//    OperatorRhs(const NH& nhMap, Model& model): mrNhMap(nhMap), mrModel(model) {
//    }
//    
//    // no constructor with no arguments
//    // default copy constructor and destructor used
//    // assignment operator will fail due to read-only reference
//    
//    /** THE Operator. */
//    template <typename Iterator>
//    void operator()(Iterator iter) const {
//        // assert concepts
//        CONCEPT_ASSERT(CIterator<Iterator>);
//        CONCEPT_ASSERT(CSameType<SCE, typename Iterator::ValueType>);
//        // go through them
//        for (iter.first(); !iter.isDone(); iter.next()) {
//            SCE& part = *(iter.currentItem());
//            typename NH::IteratorType hood = mrNhMap.getHood(part);
//            
//            // detect membrane
//            const Real req = SceParams::getEq(part.getCellType(), part.getSceType());
//            if (SceParams::sSim.bcCell[0] != BCC_NONE) part.detectMembrane(hood, 2*req);
//            
//            // get connections
//            if (SceParams::sSim.sceLink != SL_NONE) {
//                typename NH::IteratorTypeVar hoodVar = mrNhMap.getHoodVar(part);
//                if (SceParams::sSim.sceLink == SL_CLASSIC) {
//                    part.findConnections(hoodVar, 1.5*req);
//                } else if (SceParams::sSim.sceLink == SL_EX) {
//                    part.findConnectionsEx(hoodVar, 1.5*req);
//                } else if (SceParams::sSim.sceLink == SL_GENERIC) {
//                    part.findConnectionsGeneric(hoodVar, 1.5*req);
//                }
//            }
//            
//            // reset stuff
//            part.resetRHS();
//            part.resetStats();
//            
//            // add forces
//            part.addParticleForces(hood);
//            if (SceParams::sSim.bcCell[0] == BCC_DAMP) {
//                part.addNormalDamping(SceParams::sSim.bcCell[1]);
//            } else if (SceParams::sSim.bcCell[0] == BCC_DAMP_POLAR) {
//                part.addNormalDamping((SceParams::sSim.wallPolarity[2]-part.mPolarity)*SceParams::sSim.bcCell[1]);
//            }
//            part.addBoundaryForce(*(mrModel.mpBoundarySurface));
//            
//            // make viscous
//            part.applyViscosity();
//        }
//    }
//};