/*
 *  SCE_SS08_Model.h
 *  Particles
 *
 *  Created by Florian Milde on 1/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#pragma once


// project includes
#include "SCE_SS08_Main.h"
#include "SCE.h"
#include "SCECell.h"
#include "Iterators.h"
#include "NeighborhoodMaps.h"
//#include "HashMap.h"
//#include "SCEParameters.h"

// openGL
#ifdef __WITH_GL__
#include <GLUT/glut.h>
#include "Visualization/VisualizationUtils.h"
#endif

// system includes
#include <vector>
#include <string>

class Model
{
    
#pragma mark TYPEDEFS
protected:
    /** Parameter Types. */
    typedef VectorElement<Real,CELLTYPES> SCEParamType;
    typedef VectorElement<int,CELLTYPES> SCEParamTypeI;

    /** SCE Type. */
    typedef SCE SCEType;
    /** Collection of particles. */
    typedef std::vector<SCEType> ParticleCollection;
    /** Iterator for particles and particle pointers. */
    typedef ArrayIterator<SCEType> ParticleCollectionIterator;
    typedef ArrayIteratorP<SCEType> ParticleCollectionIteratorP;
    
    /** Cell Type. */
    typedef SCECell<SCEType,ParticleCollectionIterator> SCECellType;
    /** Cellection of cells. */
    typedef std::vector<SCECellType> CellCollection;
    /** iterator for particles and particle pointers over cells. */
    typedef ArrayIteratorIterable<SCECellType> CellParticleCollectionIterator;
    typedef ArrayIteratorIterableP<SCECellType,1> CellParticleCollectionIteratorP;
    
    /** iterator for cells and cell pointers*/
    typedef ArrayIterator<SCECellType> CellCollectionIterator;
    typedef ArrayIteratorP<SCECellType> CellCollectionIteratorP;

    /** lookup table for cells*/
    typedef std::map<int,SCECellType*> MapType;
    
    /** Neighbourhood map. */
    typedef CellList<SCEType> NHM;
	
	/** current maximum signalling levels */
	typedef VectorElement<Real,NSPEC> StateVar;
    
public:
#pragma mark LIFECYCLE
    /**
     * Default constructor.
     * @param ctrl  Optional filename for used ctrl-file.
     */
    Model(std::string ctrl = "Ctrl.xml");
    
    /**
     * Destructor. Also takes care of Cell and SCE instances.
     */
    ~Model();
    
    // default copy constructor and assignment operator are used
    
#pragma mark OPERATIONS
    /** Initialize model. */
    void init();
    /** Perform one iteration step . */
    void step();
    /** for visualization from txt files. */
    void readNext();
    
#ifdef __WITH_GL__
    /** Render current state (OpenGL). */
    void view();
#endif
    
#pragma mark DATA
    /** General Model */
    /** Name of the model. */
    std::string mName;
    /** Description of the model. */
    std::string mDescription;
    
    /** Current iteration. */
    int mIt;
    /** Current physycal time. */
    Real mTime;
    /** Dump every mDump iterations. */
    int mDump;
    /** Display every mDisp iterations. */
    int mDisp;
    /** Are we done yet? */
    bool mDone;
    
    /** SCE specific */
    
protected:
#pragma mark PROTECTED
    /** Simulation Parameters. */
    /** Filename of used control file. */
    std::string mCtrl;
    
    /** NEW PARAMETER OBJECTS **/
    SimParameters mSimParams;
    CellParameters mCellParams[CELLTYPES];
    InterCellParameters mInterCellParams[CELLTYPES][CELLTYPES];
    
    /** Current time step. */
    Real mDt;
    
    /** Number of Cells. */
    SCEParamType mNCells;
    /* collection of particles */
    ParticleCollection mParticles;
    /** Cellection of cells. */
    CellCollection mCells;
    /** lookup table for cells*/
    MapType mMap;
    /** Neighborhood map (for Potential interactions)*/
    NHM mNeighMap;
    /** Neighborhood map (for Signaling interactions)*/
    NHM mNeighMapJux;
    
    /** Get iterator for mParticles. */
    //CollectionIterator getIterator() { return CollectionIterator(mParticles); }  // ContainerIterator
    ParticleCollectionIterator getIterator() { return ParticleCollectionIterator(&mParticles[0], mParticles.size()); }  // ArrayIterator	


	

	StateVar maxSpec;
	

    /** get Iterators. */
    CellParticleCollectionIterator getCellParticleIterator() { return CellParticleCollectionIterator(&mCells[0],mCells.size());} //ArrayIteratorIterable
    CellParticleCollectionIteratorP getCellParticleIteratorP(int i) {return CellParticleCollectionIteratorP(&mCells[0],mCells.size());} //ArrayIteratorIterableP
    CellCollectionIterator getCellIterator() { return CellCollectionIterator(&mCells[0],mCells.size());}

    
#pragma mark INITIALIZATION
    /** Read Simulation parameters. */
    void readParameters();
    /** Read Simulation parameters. */
    void readParameters(std::string);
    /** Set and scale dependent parameters. */
    void setParameters();
    
    /** Initialize particles. */
    void initParticles();
    /** Initialize particles in a square. */
    void initParticlesSquare();
    /** Create cell andInitialize particles at random position in sphere with radius rad at middle of domain */	
    void initCellRandomSphere(VecType,Real,int,CCellPhenotype=P_Q,CCellPhase=C_G0,VecType _poly=VecType(1));
    /** init cells at random inside a sphere. */
    void initCellsRandomSphere(VecType,Real,int);
    /** create positions vector on regular grid. */
    std::vector<VecType> createPositionsRG(VecType);
    /** create positions vector on staggered grid. */
    std::vector<VecType> createPositionsSG(VecType,VecType shift=VecType(0));
    /** init cells at specific positions. */
    void initCellsFromPositions(std::vector<VecType>);
    /** init cells at specific positions and orient at random. */	
    void initCellsFromPositionsAndOrient(std::vector<VecType>,CCellPhenotype);
    void initCellsFromPositionsAndOrient(std::vector<VecType>,CCellPhenotype,CCellPhenotype);
    /** init cells from file dump. */
    void initCellsFromFile(int);
    
    /** init the map */
    void initMap();
    
	/** reset signalling levels (for ic 5 */
	void  updateCellStates();
	
#pragma mark UPDATE
    /** Update particles for 1 time step. */
    void updateParticles();
    /** Update Particles contained in cells for 1 time step. */
    void updateCellParticles();
    /** update internal cell pointers. */
    void updateCellPointers();
    /** Update cells for 1 time step. */
    void updateCells();
    void updateCells1(); // with Juxtacrine
 
#pragma mark IO
    /** Dump output to disc. */
    void dump();
    /** Compute and display diagnostics. */
    void diag();

	/** Write information for signalling to file. */
	void dumpSignalling();
	void writeSignallingToFile();
	/** Write Cells To txt File. */
	void writeCellsToFile();
	
    
#pragma mark HELPER
    /** HELPER FUNCTIONS.. COULD GO SOMEWHERE ELSE */
    /* sample random position inside wphere. */
    VecType randPosSphere(VecType,Real);
    
private:
#pragma mark PRIVATE
    
#ifdef __SKETCH_GRID__
    VecType mGRIDmin;
    VecType mGRIDCellsSize;
    VecTypeI mGRIDNCells;
#endif
};