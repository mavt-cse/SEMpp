 /*
 *  SCE_SS08_Main.cpp
 *  Particles
 *
 *  Created by Florian Milde on 1/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 * Main entry point of this sample application.
 *
 * Sets up problem and controls I/O.
 * Reqs on Model-class: (use typedef if other class name)
 * - Constructor taking ctrl-filename as input
 * - Public fields: mName, mDescription, mIt, mDone.
 * - Public funtions init(), step()
 * - for OpenGL: view()
 */

// project includes
#include "SCE_SS08_Main.h"
#include "SCE_SS08_Model.h"

// tbb
#if USE_TBB == 1
#include "tbb/tick_count.h"
#endif

// openGL
#ifdef __WITH_GL__
#include <GLUT/glut.h>

#define _TAKINGSCREENIES_
// Note: Creates TGA files in out subfolder
#ifdef _TAKINGSCREENIES_
#include "Visualization/screenshot.h"
#endif

/** Helper class to run model with OpenGL visuals. */
struct VisualSupport
{
    /** Singleton model-instance. */
	static Model * spModel;
    
    /** Is simulation paused? */
	static bool sPaused;
    /** Does simulation pause when done? */
	static bool sNeverPaused;
    /** Take screenshot now? */
	static bool sTakeScreenie;
    
    /** Run model (must be initialized). */
    static void run(Model * model)
	{
		spModel = model;
        sPaused = false;
        sNeverPaused = false;
        sTakeScreenie = false; 
		
        // register callbacks and start loop now
		glutDisplayFunc(display);
		glutIdleFunc(idle);
        glutKeyboardFunc(keyboard);
		
		glutMainLoop();
	}
    
    /** Display current model-content. */
	static void display()
	{

		glClear(GL_COLOR_BUFFER_BIT);
		spModel->view();
		glutSwapBuffers();
        
        // screenshot?
#ifdef _TAKINGSCREENIES_
        // set < 0 to enable only manual screenshots
        if ((Params::sSim.tgadump > 0 && spModel->mIt % Params::sSim.tgadump == 0) || sTakeScreenie) {
            char buf[300];
            const char * name_format = "out/%s_%07d.tga";
            if (Params::sSim.tgadump > 0) {
                sprintf(buf, name_format, spModel->mName.c_str(),spModel->mIt / Params::sSim.tgadump);
            } else {
                sprintf(buf, name_format, spModel->mName.c_str(), spModel->mIt);
            }
            if (!gltWriteTGA(buf, true,700,700)) {
                // error returns != 0
                std::cout << "Failed to write screenshot to " << buf << "\n";
            } else if (DEBUG_LEVEL > 1) {
                std::cout << "Screenshot written to " << buf << "\n";
            }
            sTakeScreenie = false;
        }
#endif
	}
	
    /** Proceed simulation by one step. */
	static void idle()
	{
		static int myIter = 0;
        
        if (!sPaused) {
            // step forward
#ifdef __VISUALIZE_TXT__
			spModel->readNext();
#else
            spModel->step();
#endif
            myIter++;
            // display every now and then
			if (myIter%Params::sSim.disp ==0){
                glutPostRedisplay();
            }
            
            // are we done?
            if (!sNeverPaused && spModel->mDone) {
                sPaused = true;
                std::cout << spModel->mName << ": Successfull termination of the programm - praise him who giveth \n";
				exit(0);
            }
        }
	}
    
    /** Callback whenever key is pressed. */
    static void keyboard(unsigned char key, int x, int y) {
        switch (key) {
            case '?':
                // show some shortcuts
                std::cout << "?     Show shortcuts\n";
                std::cout << "SPACE Perform a sim. step\n";
                std::cout << "p     Pause/Unpause simulation\n";
                std::cout << "t     Take screenshot\n";
                std::cout << "ESC   Stop simulation\n";
                break;
            case 'p':
                sPaused = !sPaused;
                if (sPaused) {
                    std::cout << "Switched to paused\n";
                } else {
                    std::cout << "Switched to unpaused\n";
                    if (spModel->mDone) {
                        // override completed sim. run
                        sNeverPaused = true;
                    }
                }
                break;
            case 't':
                sTakeScreenie = true;
                break;
            case ' ':
#ifdef __VISUALIZE_TXT__
				spModel->readNext();
#else
				spModel->step();
#endif
                break;
            case 27:
                // KILL
                delete spModel;
                exit(0);
            default:
                std::cout << "Key pressed: " << (int)key << std::endl;
                return;
        }
        // show update
        glutPostRedisplay();
    }
    
    /**
     * Setup a window for OpenGL output.
     * Shows flat rectangular plane with x,y in [-1,1].
     * Title for window can be passed.
     */
    static void setup(int argc, char ** argv, std::string title = "MUH")
    {
        double maxWindowSize = 700;
        double windowSizeX = 0.0;
        double windowSizeY = 0.0;
        double sizeX = 10.0;
        double sizeY = 10.0;
        if( sizeX >= sizeY ){ windowSizeX = maxWindowSize; windowSizeY = maxWindowSize * sizeY / sizeX; }
        else{ windowSizeY = maxWindowSize; windowSizeX = maxWindowSize * sizeX / sizeY; }
        
        
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_DEPTH | GLUT_STENCIL | GLUT_RGBA | GLUT_DOUBLE);
        glutInitWindowSize(windowSizeX,windowSizeY);
        glutInitWindowPosition(0, 0);
        glutCreateWindow(title.c_str());
        
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, 1, 0, 1, 0, 1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnable(GL_TEXTURE_2D);
        
        // blending... (note: GLUT_ALPHA needed for blendings using DST_ALPHA)
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    }
};
// define static variables (all set in run)
Model * VisualSupport::spModel;
bool VisualSupport::sPaused;
bool VisualSupport::sNeverPaused;
bool VisualSupport::sTakeScreenie;
#endif  // ifdef __WITH_GL__

/** MAIN entry point of application. */
int main (int argc, char ** argv) {
    // read cl arguments
	std::string ctrl;
	if(argc >=2) {
		ctrl = argv[1];
	} else {
		ctrl = "Ctrl.dat";
	}
	
    // init model
    Model * model = new Model(ctrl);
    model->init();
    std::cout << model->mName << std::endl;
    std::cout << model->mDescription << std::endl;
    
    // time execution
#if USE_TBB == 1
	tbb::tick_count t1, t2;
    t1 = tbb::tick_count::now();
#else
	clock_t t1,t2;
	t1 = clock();
#endif
	
    
    // run it
#ifdef __WITH_GL__
    VisualSupport::setup(argc, argv, model->mName);
    VisualSupport::run(model);
#else
    for(int i=0; !model->mDone; i++) {
        model->step();
    }
#endif
    
    // timing
    double runtime;
#if USE_TBB == 1
    t2 = tbb::tick_count::now();
    runtime = (t2-t1).seconds();
#else
	t2 = clock();
	runtime = (double)(t2-t1)/(double)CLOCKS_PER_SEC;
#endif
	std::cout << model->mName << ": Successfull termination of the programm - praise him who giveth \n";
	std::cout << model->mName << ": Runtime (w/o initialization):"<< runtime <<"\n";
    
    // clean up
    delete model;
    
    return 0;
}