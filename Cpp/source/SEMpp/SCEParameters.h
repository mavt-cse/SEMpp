/*
 *  SCEParameters.h
 *  Particles
 *
 *  Created by Florian Milde on 2/26/10.
 *  Copyright 2010 EtH Zurich. All rights reserved.
 *
 */

#pragma once

#include "Environment.h"
#include "SCEEnvironment.h"
#include "Utilities.h"
#include "Surfaces.h"

#include <fstream>

struct BaseParameters{
	std::string name;
};

struct SimParameters : BaseParameters {
	//	std::string name;        // simulation name
	// simulation general
	std::string desc;        // description for simulaiton
	VecType minD;		     // lower corner of domain
	VecType sizeD;		     // upper corner of domain
	Real bBoxOffset;		 // boundary offset of bounding box
	int bc;			 // boundary condition (0=none;1=periodic)
	CubeSurface boundary;    // domain boundary box
	int maxIt;			     // max number of iterations
	Real maxTime;            // max simulation time
	Real dt;                 // time step
	int tgadump;			 // dumping frequency tga
	int txtdump;			 // dumping frequency txt
	int disp;                // display frequency
	// initial condition
	int ic;                  // 0: random position inside sphere , 1: on lattice, 2: from position file, 3: restart
	VecType icCenter;		 // center for ic=0;
	Real icRadius;           // radius for ic=0;
	VecType icCells;         // number of cells
	Real icDensity;          // density for cell initialization
	VecType icShift;         // shift of initial condition 
	int icRestart;           // restart iteration ic=3
	int icSeed;              // seed for random number generator
	
	typedef VectorElement<Real,NSPEC> StateVar;
	StateVar icJuxLevel;	//initial conditions for juxtacrine signalling
	Real dtJux;				//time step for juxtacrine signalling
	int txtdiag;
	Real icNoise;			//level of initial noise for juxtacrine signalling levels
	
	
	void printParams(){
		std::cout<<"simParameters:"<<std::endl;
		std::cout<<"   NAME:"<<name<<std::endl;
		std::cout<<"   DESC:"<<desc<<std::endl;
		std::cout<<"   MIND:"<<minD<<std::endl;
		std::cout<<"   SIZED:"<<sizeD<<std::endl;
		std::cout<<"   BBOXOFFSET:"<<bBoxOffset<<std::endl;
		std::cout<<"   BC:"<<bc<<std::endl;
		std::cout<<"   MAXIT:"<<maxIt<<std::endl;
		std::cout<<"   MAXTIME:"<<maxTime<<std::endl;
		std::cout<<"   DT:"<<dt<<std::endl;
		std::cout<<"   TGADUMP:"<<tgadump<<std::endl;
		std::cout<<"   TXTDUMP:"<<txtdump<<std::endl;
		std::cout<<"   DISP:"<<disp<<std::endl;
		std::cout<<"   IC:"<<ic<<std::endl;
		std::cout<<"   ICCENTER:"<<icCenter<<std::endl;
		std::cout<<"   ICRADIUS:"<<icRadius<<std::endl;
		std::cout<<"   ICCELLS:"<<icCells<<std::endl;
		std::cout<<"   ICDENSITY:"<<icDensity<<std::endl;
		std::cout<<"   ICSHIFT:"<<icShift<<std::endl;
		std::cout<<"   ICRESTART:"<<icRestart<<std::endl;
		std::cout<<"   ICSEED:"<<icSeed<<std::endl;
		std::cout<<"   ICJUXLEVEL:"<<icJuxLevel<<std::endl;
		std::cout<<"   DTJUX:"<<dtJux<<std::endl;	
		std::cout<<"   TXTDIAG:"<<txtdiag<<std::endl;
		std::cout<<"   ICNOISE:"<<icNoise<<std::endl;
	}
	
	void readParams(std::ifstream &file){	
		std::string line;
		while(getline(file,line)){
			std::istringstream iss(line);
			std::string var;
			// return value is false for empty lines
			getline(iss, var, '=');
			var = Utilities::StringToUpper(var);
			// check for empty line or comment
			if (var.empty() || var[0] == '#') continue;
			if (var == "</SIM_PARAMETERS>") break;
			if (var == "NAME") getline(iss,name,'\n');
			if (var == "DESC") getline(iss,desc,'\n');
			if (var == "MIND") Utilities::NumFromStream(minD,iss);
			if (var == "SIZED") Utilities::NumFromStream(sizeD,iss);
			if (var == "BBOXOFFSET") Utilities::NumFromStream(bBoxOffset,iss);
			if (var == "BC") Utilities::NumFromStream(bc,iss);
			if (var == "MAXIT") Utilities::NumFromStream(maxIt,iss);
			if (var == "MAXTIME") Utilities::NumFromStream(maxTime,iss);
			if (var == "DT") Utilities::NumFromStream(dt,iss);
			if (var == "TGADUMP") Utilities::NumFromStream(tgadump,iss);
			if (var == "TXTDUMP") Utilities::NumFromStream(txtdump,iss);
			if (var == "DISP") Utilities::NumFromStream(disp,iss);
			if (var == "IC") Utilities::NumFromStream(ic,iss);
			if (var == "ICCENTER") Utilities::NumFromStream(icCenter,iss);
			if (var == "ICRADIUS") Utilities::NumFromStream(icRadius,iss);
			if (var == "ICCELLS") Utilities::NumFromStream(icCells,iss);
			if (var == "ICDENSITY") Utilities::NumFromStream(icDensity,iss);
			if (var == "ICSHIFT") Utilities::NumFromStream(icShift,iss);
			if (var == "ICRESTART") Utilities::NumFromStream(icRestart,iss);
			if (var == "ICSEED") Utilities::NumFromStream(icSeed,iss);
			if (var == "ICJUXLEVEL") Utilities::NumFromStream(icJuxLevel,iss);
			if (var == "DTJUX") Utilities::NumFromStream(dtJux,iss);
			if (var == "TXTDIAG") Utilities::NumFromStream(txtdiag,iss);
			if (var == "ICNOISE") Utilities::NumFromStream(icNoise,iss);
		}
		
		boundary=CubeSurface(minD+bBoxOffset,sizeD-2*bBoxOffset);
		
		printParams();
	};
};

struct CellParameters : BaseParameters {
	//	std::string name;   // cell name
	static Real densThr;
	static Real densRad;
	
	int type;           // cell type
	int nSce;           // number of elements per cell
	Real radius;        // cell radius
	// cell cycle phases
	Real G0_dur;		// duration of G0 phase
	Real G0_fluc;
	Real I_dur;         // duration of inter phase
	Real I_fluc;
	Real M1_dur;        // duration of mito-mitosis phase
	Real M1_fluc;
	Real M2_dur;		// duration of mito-cytokinesis phase
	Real M2_fluc;
	Real MI_dur;		// migration poly-depolymerization rate
	Real MI_fluc;
	Real EQ_dur;
	Real EQ_fluc;
	// sce intra params
	Real req;          // equilibrium distance of SCEs
	Real rho;
	Real kappa;
	Real tau;
	Real alpha;
	Real eta;
	Real u0;
	Real d;
	// resulting potential parameters
	Real potIntraParams[NPOTP]; // intra potential params (rho,u0,1/req^2,cutoff,alpha,densRad)
	Real std;           // standard deviation
	Real ieta;          // vsicosity parameter
	// migration parameters
	Real MI_pfluc;          // polarization fluctuation
	int MI_pmod;          // polarization model
	int MI_cand;			// candidates for poly (0:membrane, 1:internal, 2:all)
	// quiescent polaryzation parameters
	int EQ_mod;
	int EQ_cand;
	
	void printParams(){
		std::cout<<"cellParameters:"<<std::endl;
		std::cout<<"   NAME:"<<name<<std::endl;
		std::cout<<"   TYPE:"<<type<<std::endl;
		std::cout<<"   NSCE:"<<nSce<<std::endl;
		std::cout<<"   RADIUS:"<<radius<<std::endl;
		std::cout<<"   G0_DUR:"<<G0_dur<<std::endl;
		std::cout<<"   G0_FLUC:"<<G0_fluc<<std::endl;
		std::cout<<"   I_DUR:"<<I_dur<<std::endl;
		std::cout<<"   I_FLUC:"<<I_fluc<<std::endl;
		std::cout<<"   M1_DUR:"<<M1_dur<<std::endl;
		std::cout<<"   M1_FLUC:"<<M1_fluc<<std::endl;
		std::cout<<"   M2_DUR:"<<M2_dur<<std::endl;
		std::cout<<"   M2_FLUC:"<<M2_fluc<<std::endl;
		std::cout<<"   MI_DUR:"<<MI_dur<<std::endl;
		std::cout<<"   MI_FLUC:"<<MI_fluc<<std::endl;
		std::cout<<"   REQ:"<<req<<std::endl;
		std::cout<<"   INTRAPARAMS:"<<potIntraParams[0];
		for(int i=1;i<NPOTP;i++){
			std::cout<<" "<<potIntraParams[i];
		}
		std::cout<<std::endl;
		std::cout<<"   STD:"<<std<<std::endl;
		std::cout<<"   IETA:"<<ieta<<std::endl;
		std::cout<<"   MI_PFLUC:"<<MI_pfluc<<std::endl;
		std::cout<<"   MI_PMOD:"<<MI_pmod<<std::endl;
		std::cout<<"   MI_CAND:"<<MI_cand<<std::endl;
		std::cout<<"   EQ_MOD:"<<EQ_mod<<std::endl;
		std::cout<<"   EQ_CAND:"<<EQ_cand<<std::endl;
	}
	
	void readParams(std::ifstream &file){
		/** TODO
		 /* read the input parameters
		 /* assign and scale them to the model parameters
		 */
		
		//temporal parameters
		Real kappa0;
		Real eta0;
#if DIM ==2
		Real cpd = M_PI/(2.*sqrt(3.));
#elif DIM==3
		Real cpd = 0.7405;
#endif
		
		// read parameters
		std::string line;
		while(getline(file,line)){
			std::istringstream iss(line);
			std::string var;
			// return value is false for empty lines
			getline(iss, var, '=');
			var = Utilities::StringToUpper(var);
			// check for empty line or comment
			if (var.empty() || var[0] == '#') continue;
			if (var == "</CELL_PARAMETERS>") break;
			if (var == "NAME") getline(iss,name,'\n');
			if (var == "TYPE") Utilities::NumFromStream(type,iss);
			if (var == "NSCE") Utilities::NumFromStream(nSce,iss);
			if (var == "RADIUS") Utilities::NumFromStream(radius,iss);
			if (var == "G0_DUR") Utilities::NumFromStream(G0_dur,iss);
			if (var == "G0_FLUC") Utilities::NumFromStream(G0_fluc,iss);
			if (var == "I_DUR") Utilities::NumFromStream(I_dur,iss);
			if (var == "I_FLUC") Utilities::NumFromStream(I_fluc,iss);
			if (var == "M1_DUR") Utilities::NumFromStream(M1_dur,iss);
			if (var == "M1_FLUC") Utilities::NumFromStream(M1_fluc,iss);
			if (var == "M2_DUR") Utilities::NumFromStream(M2_dur,iss);
			if (var == "M2_FLUC") Utilities::NumFromStream(M2_fluc,iss);
			if (var == "MI_DUR") Utilities::NumFromStream(MI_dur,iss);
			if (var == "MI_FLUC") Utilities::NumFromStream(MI_fluc,iss);		
			// temporal sce parameters
			if (var == "RHO") Utilities::NumFromStream(rho,iss);
			if (var == "KAPPA") Utilities::NumFromStream(kappa0,iss);
			if (var == "ETA") Utilities::NumFromStream(eta0,iss);
			if (var == "TAU") Utilities::NumFromStream(tau,iss);
			if (var == "ALPHA") Utilities::NumFromStream(alpha,iss);
			if (var == "D") Utilities::NumFromStream(d,iss);
			if (var == "MI_PFLUC") Utilities::NumFromStream(MI_pfluc,iss);
			if (var == "MI_PMOD") Utilities::NumFromStream(MI_pmod,iss);
			if (var == "MI_CAND") Utilities::NumFromStream(MI_cand,iss);
			if (var == "EQ_MOD") Utilities::NumFromStream(EQ_mod,iss);
			if (var == "EQ_CAND") Utilities::NumFromStream(EQ_cand,iss);
		}
		
#if DIM == 2
		Real ir2_N = 1./pow(nSce, Real(0.5));
		kappa = kappa0*ir2_N*(1.-tau*ir2_N); //probably wrong
		eta = eta0/nSce; // probably right
		req = 2.*radius*pow(cpd/nSce, Real(0.5)); //correct
		//		req *= sqrt(rho/(sqrt(rho + log(2/alpha))));
		u0 = kappa*req*req/(8.f*rho*rho); // probably right
		std = sqrt(2*eta*eta*d);
		ieta = 1./eta;
#elif DIM == 3
		Real onethrd = 1./3.;
		Real ir3_N = 1./pow(nSce,onethrd);
		kappa = kappa0*ir3_N*(1.-tau*ir3_N);
		eta = eta0/nSce;
		req = 2.*radius*pow(cpd/nSce,1./3.);
		req *= sqrt(rho/(sqrt(rho + log(2/alpha))));
		u0 = kappa*req*req/(8.*rho*rho);
		std = sqrt(2.*eta*eta*d);
		ieta=1./eta;
#endif
		potIntraParams[0] = rho;
		potIntraParams[1] = u0;
		potIntraParams[2] = 1./(req*req);
		potIntraParams[3] = 3.0*req;
		potIntraParams[4] = alpha;
		potIntraParams[5] = densRad*req;
		potIntraParams[6] = sqrt(rho+log(2/alpha))/sqrt(rho);
		printParams();
	};
};

struct InterCellParameters : BaseParameters {
	//  std::string name; // name of the interaction
	static Real densThr;
	static Real densRad;
	
	int type1;					// cell type 1
	int type2;					// cell type 2 (can be same as 1)
	Real potInterParams[NPOTP]; // the interaction parameters
	
	void printParams(){
		std::cout<<"interCellParameters:"<<std::endl;
		std::cout<<"   NAME:"<<name<<std::endl;
		std::cout<<"   TYPE1:"<<type1<<std::endl;
		std::cout<<"   TYPE2:"<<type2<<std::endl;
		std::cout<<"   INTERPARAMS:"<<potInterParams[0];
		for(int i=1;i<NPOTP;i++){
			std::cout<<" "<<potInterParams[i];
		}
		std::cout<<std::endl;
	};
	
	void readParams(std::ifstream &file, CellParameters cellParams[CELLTYPES]){	
		/** ASSUMPTION
		 /* inter cell potentials are given by prefactors 
		 /* to the average values of the intra parameters
		 */
		
		// temporary variales
		VectorElement<Real,4> params;
		// read parameters
		std::string line;
		while(getline(file,line)){
			std::istringstream iss(line);
			std::string var;
			// return value is false for empty lines
			getline(iss, var, '=');
			var = Utilities::StringToUpper(var);
			// check for empty line or comment
			if (var.empty() || var[0] == '#') continue;
			if (var == "</INTER_CELL_PARAMETERS>") break;
			if (var == "NAME") getline(iss,name,'\n');
			if (var == "TYPE1") Utilities::NumFromStream(type1,iss);
			if (var == "TYPE2") Utilities::NumFromStream(type2,iss);
			if (var == "INTERPARAMS") Utilities::NumFromStream(params,iss);
		}
		
		// set parameters
        const double myreq = (cellParams[type1].req+cellParams[type2].req)/2.;
        potInterParams[0]=params[0]*(cellParams[type1].potIntraParams[0]+cellParams[type2].potIntraParams[0])/2.;
        potInterParams[1]=params[1]*(cellParams[type1].potIntraParams[1]+cellParams[type2].potIntraParams[1])/2.;
        potInterParams[2]=1 / (params[2]*params[2]*myreq*myreq);
		potInterParams[3]=std::max(cellParams[type1].potIntraParams[3],cellParams[type2].potIntraParams[3]);
        potInterParams[4]=params[3]*(cellParams[type1].potIntraParams[4]+cellParams[type2].potIntraParams[4])/2.;
		potInterParams[5]=densRad * myreq;
		potInterParams[6]=(cellParams[type1].potIntraParams[6]+cellParams[type2].potIntraParams[6])/2.;
		
		// print parameters
		printParams();
	};	
};

struct Params{
	static SimParameters sSim;
	static CellParameters sCells[CELLTYPES];
	static InterCellParameters sInterCells[CELLTYPES][CELLTYPES];
	static Real maxReq;
	static bool set;

	// signaling parameters depend on chosen model for juxtacrine signalling (defined in JUXPATH)
#if JUXPATH == 1
	static Real sigA;
	static Real sigB;
	static Real sigK;
	static Real sigH;
	static Real sigV;
	static Real sigIters;
#elif JUXPATH == 2
	static Real kASS;
	static Real kDISS;
	static Real kINT;
	static Real degA;
	static Real degF;
	static Real HillA;
	static Real HillF;
	static Real C1;
	static Real C2;
	static Real C3;
	static Real C4;
	static Real C5;	
	static Real sigIters;
#endif
	
	
	static Real getReqMax(){
		if(set) return maxReq;
		for(int i=0;i<CELLTYPES;i++) maxReq=std::max(maxReq,sCells[i].req);
		return maxReq;
	};
	
	static Real getFlucParam(const int cell_type){
		return sCells[cell_type].std;
	};
};