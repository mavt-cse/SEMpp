/*
 *  SCEEnvironment.h
 *  Particles
 *
 *  Created by Florian Milde on 1/26/10.
 *  Copyright 2010 EtH Zurich. All rights reserved.
 *
 *  used to set all the SCE specific constants
 */

#pragma once

// Cell Types
#ifndef CELLTYPES
#define CELLTYPES 1
#endif

// Model definitions
#ifndef NPOTP
#define NPOTP 7
#endif

// Number of Chemical Species In Cells
#ifndef JUXPATH
#define JUXPATH 2

#if		JUXPATH == 1
#define		NSPEC 2
#elif	JUXPATH == 2
#define		NSPEC 3
#endif
#endif

//
//if defined, uses angle dependent membrane detection
#define __MEMBRANE2__