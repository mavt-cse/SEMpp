/*
 *  SCE_SS08_Model.cpp
 *  Particles
 *
 *  Created by Florian Milde on 1/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

// Implementation of SCE_SS08_Model.h

// project includes
#include "SCE_SS08_Model.h"
#include "SCE_SS08_Operators.h"

// library includes
#include "WorkDistributors.h"
#include "TimeIntegrators.h"
#include "Operators.h"
#include "Utilities.h"
#include "FileHandler.h"

// system includes
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

//SCECell
template <typename T, typename I>int SCECell<T,I>::sGlobalCellId=0;

/*=======================================
 MODEL SPECIFIC INCLUDES
 =======================================*/

#define __JUX__NOT

/*=======================================
 END MODEL SPECIFIC INCLUDES
 ========================================*/

#pragma mark LIFECYCLE

Model::Model(std::string _ctrl) {
	mName			= "SCE";
	mDescription	= "SCE extensions";
	mCtrl			= _ctrl;
}

Model::~Model() {
	// clean up
}

#pragma mark OPERATIONS

void Model::init(){
	
	// init parameters
	readParameters(mCtrl);
	setParameters();
	
	// init cells/particles/neighbourhood map
	initParticles();
	initMap();
	updateCellPointers();
	
	// dump first one
	dump();
	// mention how often we will dump
	std::cout << "Dump tga every " << Params::sSim.tgadump << " iterations (i.e. every dt = "<< mDt*Params::sSim.tgadump << ")\n";
	std::cout << "Dump txt every " << Params::sSim.txtdump << " iterations (i.e. every dt = "<< mDt*Params::sSim.txtdump << ")\n";
	std::cout << "Dump diag every " << Params::sSim.txtdiag << " iterations (i.e. every dt = "<< mDt*Params::sSim.txtdiag << ")\n";
	
	
}

void Model::step() {
	if (DEBUG_LEVEL > 2) {
		std::cout << mName << ": Iteration - "<< mIt << " (t=" << mTime << ") \n";
	}
	
	// update particle elements
	if (Params::sSim.ic==5) {
		if(mIt==Params::sSim.icRestart) {
			updateCellParticles();
			mTime=0;
			mDt=Params::sSim.dtJux;
		}
	} else if(Params::sSim.ic==7) {
		if ((mIt*mDt)<Params::sSim.icRestart) {
			updateCellParticles();
			mTime=0;
			updateCellStates();
		} else if ((mIt*mDt)==Params::sSim.icRestart) {
			mTime=0;
			mDt=Params::sSim.dtJux;
			updateCellStates();
			updateCellParticles(); //to be removed if faster timestep should be used after restetting
		} else {//also this shoud be removed then...
			updateCellParticles();
		}	
	} else {
        updateCellParticles();
    }
	
	// update the cells();
	updateCells1();
	
	// proceed and check if done
	mIt++;
	mTime += mDt;
	mDone = (mIt >= Params::sSim.maxIt || mTime >= Params::sSim.maxTime);
	
	// do the IO
	if (Params::sSim.txtdump > 0 && mIt%Params::sSim.txtdump==0) {
		std::cout << mName << ": Dump at Iteration - "<< mIt << " (t=" << mTime << ") \n";
		dump();
	}
    
	// diags for juxtacrine signaling
	if (Params::sSim.txtdiag > 0 && mIt%Params::sSim.txtdiag==0) {
		if (Params::sSim.ic != 7 || (mIt*mDt)>=Params::sSim.icRestart) {
			std::cout << mName << ": Dump diag at Iteration - "<< mIt << " (t=" << mTime << ") \n";
			diag();
			dumpSignalling();
		}
	}
}

void Model::readNext() {
	mCells.clear();
	mIt+=Params::sSim.txtdump;
	std::cout << "Reading: "<< mIt << std::endl;
	FileHandler::readCellsFromFile(mCells,"txt/"+mName+"_%07d.txt",mIt);
}

#pragma mark INIT 

void Model::readParameters(std::string _ctrl) {
	std::ifstream file;
	std::string line;
	
	// open file	
	file.open(_ctrl.c_str());
    if (!file.is_open()) {
        std::cout << "Failed to open " << _ctrl << "!\n";
		abort();
    }
	
	// walk through file and read parameters
	while(getline(file, line)){
		std::istringstream iss(line);
		std::string var;
		// return value is false for empty lines
		getline(iss, var, '=');
		var = Utilities::StringToUpper(var);
		// check for empty line or comment
		if (var.empty() || var[0] == '#') continue;
		if (var == "<SIM_PARAMETERS>"){
			Params::sSim.readParams(file);
		}
		if (var == "<CELL_PARAMETERS>"){
			CellParameters cellParams;
			cellParams.readParams(file);
			Params::sCells[cellParams.type]=cellParams;
		}
		if (var == "<INTER_CELL_PARAMETERS>"){
			InterCellParameters interCellParams;
			interCellParams.readParams(file,Params::sCells);
			Params::sInterCells[interCellParams.type1][interCellParams.type2]=interCellParams;
			if(interCellParams.type1!=interCellParams.type2){
				Params::sInterCells[interCellParams.type2][interCellParams.type1]=interCellParams;
			}
		}
	}
	
	//close file
	file.close();
}

void Model::setParameters() {
	//Set some additional parameters
	mName			= Params::sSim.name;
	mDescription	= Params::sSim.desc;
	
	// simulation start
	mDt=Params::sSim.dt;
	mIt=0;
	mTime=0;
	mDone = false;
	
	for (int i=0; i<NSPEC; i++) {
		maxSpec[i] = Params::sSim.icJuxLevel[i];
	}
	
	// set random number seed
	srand(Params::sSim.icSeed);
}

void Model::initParticles() {
	std::vector<VecType> positions;
	
	//initialize Neighbourhood Maps
	Real cellSize=3.1*Params::getReqMax();
	mNeighMap.setup(Params::sSim.minD, Params::sSim.sizeD, cellSize, Params::sSim.bc ? CL_PERIODIC : CL_NONE);
#ifdef __JUX__
	cellSize=1.1*Params::getReqMax();
	mNeighMapJux.setup(Params::sSim.minD, Params::sSim.sizeD, cellSize, Params::sSim.bc ? CL_PERIODIC : CL_NONE);
#endif
	
	switch (Params::sSim.ic) {
		case 0: // at random position
			mNCells[0]=Params::sSim.icCells.fac();
			initCellsRandomSphere(Params::sSim.icCenter,Params::sSim.icRadius,0);
			break;
		case 1: // init on lattice locations
			positions = createPositionsRG(Params::sSim.icCells);
			initCellsFromPositions(positions);
			break;
		case 2: // init on staggered grid for migration
			positions = createPositionsSG(Params::sSim.icCells,Params::sSim.icShift);
			initCellsFromPositionsAndOrient(positions,P_EQ,P_MI);
			break;
		case 3: //restart
			initCellsFromFile(Params::sSim.icRestart);
			mIt = Params::sSim.icRestart;
			mTime = mIt*mDt;
			break;
		case 4: // init on staggered grid for migration
			positions = createPositionsSG(Params::sSim.icCells,Params::sSim.icShift);
			initCellsFromPositionsAndOrient(positions,P_MI);
			break;
		case 5: // restart for jux: Start from equilibrated state and reset signalling levels
			initCellsFromFile(Params::sSim.icRestart);
			mIt = Params::sSim.icRestart;
			mTime = mIt*mDt;
			updateCellStates();
			break;
		case 6: // init for jux: Generate equilibrated state on staggered grid
			positions = createPositionsSG(Params::sSim.icCells,Params::sSim.icShift);
			initCellsFromPositionsAndOrient(positions,P_Q);
			updateCellStates();
			break;	
		case 7: // reset states after time specified in icrestart (physical time) and stop moving SCE
			positions = createPositionsSG(Params::sSim.icCells,Params::sSim.icShift);
			initCellsFromPositionsAndOrient(positions,P_Q);
			//updateCellStates();
			break;	
		default:
			printf("INVALID INITIALIZATION METHOD\n");
			exit(0);
			break;
	}
}

void Model::initMap(){
	mMap.clear();
	CellCollectionIterator iter=getCellIterator();
	for(iter.first(); !iter.isDone();iter.next()){
		SCECellType* cell = iter.currentItem();
		mMap.insert(std::pair<int,SCECellType*>(cell->getCellId(),cell));
	}
	
	// CHECK - check if everybody is in the map
	/*	for(iter.first(); !iter.isDone();iter.next()){
	 SCECellType* cell = iter.currentItem();
	 SCECellType* cell2 = mMap.find(cell->getCellId())->second;
	 cout<<"found element:"<<cell2->getCellId()<<endl;
	 }
	 exit(0);*/
}

void  Model::updateCellStates(){
	// for each cell, updates signalling levels (mState)
	srand(Params::sSim.icSeed);
	CellCollectionIterator iter = getCellIterator();
	for (iter.first(); !iter.isDone(); iter.next()){
		SCECellType& cell= (*iter.currentItem());
		for (int k=0; k<NSPEC; k++) {
			cell.setmState(k, Params::sSim.icJuxLevel[k]);
		}
	}
}

void Model::initCellsRandomSphere(VecType _origin, Real _rad, int _cellType) {
	//init mNCellof type _cellType random location inside sphere of radius _rad centered at _origin
	// grid centered at middle of domain	// therefore: mNCells =2D: x^2; 3D: x^3
	for (int c=0;c<mNCells[_cellType];c++){
		VecType pos = randPosSphere(VecType(0.),_rad);									// get random location inside circle/sphere
		initCellRandomSphere(pos,0.1,_cellType);	// add to particle container
	}
}


void Model::initCellRandomSphere(VecType _origin, Real _density, int _cellType, CCellPhenotype _cellPhenotype, CCellPhase _cellPhase, VecType _poly){
	// sample particle positions randomly inside a sphere of radius rad
	// sample random angle in 2d/3d
	// sample random length [0,1] and take square root
	SCECellType cell = SCECellType(_cellType);
	for(int i=0;i<Params::sCells[_cellType].nSce;i++){
		// for random position in sphere;
		VecType pos = randPosSphere(_origin,_density*Params::sCells[_cellType].radius);
		// create-add particle
		cell.addSCE(pos);
	}	
	
	cell.setCellPhenotype(_cellPhenotype,_cellPhase);
	cell.setPolarity(_poly);
	
	mCells.push_back(cell);
	
	// CHECK - check sth
	//	SCECellType cell2 = mCells.back();
	//	SCEType * sceIn = cell2.getPolyIn();
	//	SCEType * sceOut = cell2.getPolyOut();
	//	std::cout<< "just inserted in back"<<std::endl;
	//	std::cout<< "cellId"<<cell2.getCellId()<<std::endl;
	//	std::cout<< "sceInId"<<cell2.getPolyInId()<<std::endl;			
	//	std::cout<< "sceOutId"<<cell2.getPolyOutId()<<std::endl;	
	//	std::cout<< "PsceIn"<<sceIn->getSceId()<<std::endl;
	//	std::cout<< "PsceOut"<<sceOut->getSceId()<<std::endl<<std::endl;
	//
	
}


std::vector<VecType> Model::createPositionsRG(VecType lattice){
	std::vector<VecType> positions;
	Real cd=Params::sSim.icDensity; // density
	Real cdx = cd*2.*Params::sCells[0].radius;
	
	VecType limX=(lattice-1)/2.;
	VecType midD=Params::sSim.minD+(Params::sSim.sizeD/2.);
	for(Real i=-limX[0];i<=limX[0];i+=1.){
		for(Real j=-limX[1];j<=limX[1];j+=1.){
#if DIM==2
			VecType pos = midD+VecType(i*cdx,j*cdx);
			positions.push_back(pos);
#elif DIM==3
			for(Real k=-limX[2];k<=limX[2];k+=1.){
				VecType pos = midD+VecType(i*cdx,j*cdx,k*cdx);
				positions.push_back(pos);
			}
#endif
		}
	}
	return positions;
}

std::vector<VecType> Model::createPositionsSG(VecType lattice,VecType shift){
	// 2D for now
#if DIM==2
	std::vector<VecType> positions;
	Real cd=Params::sSim.icDensity; // density
	Real cdx = cd*2.*Params::sCells[0].radius;
	Real cdy = sqrt(3)/2*cdx;
	
	VecType limX=(lattice-1)/2.;
	VecType midD=Params::sSim.minD+(Params::sSim.sizeD/2.);
	
	// if PBC chosen...
	if(Params::sSim.bc){
		for(Real j=-limX[1];j<=limX[1];j+=1.){
			Real off = (Real)abs((int)floor(j)%2)/2.;
			for(Real i=-(limX[0]-off);i<=(limX[0]+(off-1/limX[0]));i+=1.){
				VecType pos = midD+VecType(i*cdx,j*cdy)+shift;
				positions.push_back(pos);
			}
		}	
	}
	else{
		//else
		for(Real j=-limX[1];j<=limX[1];j+=1.){
			Real off = (Real)abs((int)floor(j)%2)/2.;
			for(Real i=-(limX[0]-off);i<=(limX[0]-off);i+=1.){
				VecType pos = midD+VecType(i*cdx,j*cdy)+shift;
				positions.push_back(pos);
			}
		}
	}
	return positions;
#elif DIM==3
	std::cout<<"Staggered initialization not implemented for 3D"<<std::endl;
	exit(0);
#endif
}

void Model::initCellsFromPositions(std::vector<VecType> _positions){
	std::vector<VecType>::iterator it;
	for (it=_positions.begin();it<_positions.end();it++){
		initCellRandomSphere(*it,0.1,0,P_MI);
	}
}

void Model::initCellsFromPositionsAndOrient(std::vector<VecType> _positions, CCellPhenotype cp) {
    initCellsFromPositionsAndOrient(_positions, cp, cp);
}

void Model::initCellsFromPositionsAndOrient(std::vector<VecType> _positions, CCellPhenotype cp, CCellPhenotype cp2) {
    bool usecp = true;
	std::vector<VecType>::iterator it;
	for (it=_positions.begin();it<_positions.end();it++){
#if DIM==2
		VecType poly=VecType(rand(),rand());
#else
		VecType poly=VecType(rand(),rand(),rand());
#endif
        if (usecp) {
            initCellRandomSphere(*it,0.1,0,cp,C_G0,poly);
            usecp = false;
        } else {
            initCellRandomSphere(*it,0.1,0,cp2,C_G0,poly);
            usecp = true;
        }
	}
}

VecType Model::randPosSphere(VecType _origin, Real _rad){
	
	Real alpha=2.*M_PI*(Real)rand()/(Real)RAND_MAX;
#if DIM==2
	Real nr1= pow(((Real)rand()/(Real)RAND_MAX),0.5)*_rad;
	VecType pos = VecType(cos(alpha)*nr1,sin(alpha)*nr1);
#elif DIM==3
	Real nr1= pow(((Real)rand()/(Real)RAND_MAX),1./3.)*_rad;
	// dont rmember from where..
	Real z=2.f*(Real)rand()/(Real)RAND_MAX-1.;
	Real sz=sqrt(1.f-z*z);	
	VecType pos = VecType( cos(alpha)*nr1*sz,
			      sin(alpha)*nr1*sz,
			      z*nr1);
#endif
	// shift to middle of domain
	pos+=_origin;
	return pos;
}

void Model::initParticlesSquare(){
	// init a square symetric around 0
	std::cout << "Init Particles Square \n";
	
	int tmp = (int)ceil(pow(Params::sCells[0].nSce,1./(double)DIM));
	Real lim = ((Real)tmp-1.)/2.;
	
	int tot_sce=0;
	Real dx_sce = Params::sCells[0].req*0.8;
#if DIM==3
	for(Real k_sce=-lim;k_sce<=lim;k_sce+=1.){
#endif
		for(Real j_sce=-lim;j_sce<=lim;j_sce+=1.){
			for(Real i_sce=-lim;i_sce<=lim;i_sce+=1.){
				
#if DIM==2
				const VecType pos=VecType(i_sce*dx_sce,j_sce*dx_sce);
				//const VecType pos=VecType(0,0);
#elif DIM==3
				const VecType pos=VecType(i_sce*dx_sce,j_sce*dx_sce,k_sce*dx_sce);
#endif
				SCEType sce = SCEType(pos,0,0);
				mParticles.push_back(SCEType(pos,0,0));
				tot_sce ++;
			}
		}
#if DIM==3
	}
#endif
}

void Model::initCellsFromFile(int _it){
	FileHandler::readCellsFromFile(mCells,"rst/"+mName+"_%07d.txt",_it);
}

void Model::updateParticles() {
	// typedefs
	
	typedef CellList<SCEType> NHM;
	
	// create neighborhood map
	ParticleCollectionIterator iter = getIterator();
	
	// create neighborhood map
	CLBoundaryCondition domain_bc = CL_NONE;
	NHM neighMap;
	Real cellSize = 3.1*Params::maxReq;//mReq*12.1;
	neighMap.setup(Params::sSim.minD,Params::sSim.sizeD,cellSize,domain_bc);
	neighMap.reset(iter);
	
#ifdef __SKETCH_GRID__
	// store cell list information for visualization
	mGRIDmin = neighMap.getDomainOffset();
	mGRIDCellsSize = neighMap.getCellSize();
	mGRIDNCells = neighMap.getNumberCells();
#endif
	
	//workload distributors
#if USE_TBB==0
	DummyDistributor<ParticleCollectionIterator> distributor(iter);
#else
	TbbDistributor<ParticleCollectionIterator> distributor(iter);
#endif
	
	// define operators
	UpdateParticlesOperator<NHM> update(neighMap);
	RK3Integrator integrator(mDt);
	
	for(int stage=0;stage<integrator.getStages();stage++){
		distributor.process(update);
		//	distributor.process(genericOperator(&SCEType::computeRHS<NHM::IteratorType>, neighMap));
		integrator.update(stage,distributor);
	}
}

void Model::updateCellParticles(){
	// typedefs
	
	typedef CellList<SCEType> NHM;
	
	// create neighborhood map
	CellParticleCollectionIterator iter = getCellParticleIterator();
	typedef NHM::CellIterator CLIterator;
	NHM::CellIterator cell_iter = mNeighMap.getCellIterator();
	
	//    CLBoundaryCondition domain_bc = CL_NONE;
	//    NHM neighMap;
	
	
	//    Real cellSize = 3.1*Params::getReqMax();//mReq*12.1;
	//    neighMap.setup(Params::sSim.minD,Params::sSim.sizeD,cellSize,domain_bc);
	//    neighMap.reset(iter);
	
#ifdef __SKETCH_GRID__
	// store cell list information for visualization
	mGRIDmin = mNeighMap.getDomainOffset();
	mGRIDCellsSize = mNeighMap.getCellSize();
	mGRIDNCells = mNeighMap.getNumberCells();
#endif
	
	// workload distributors
	DummyDistributor<CellParticleCollectionIterator> serial_distributor(iter);
#if USE_TBB==0
	DummyDistributor<CellParticleCollectionIterator> distributor(iter);
	//    DummyDistributor<CLIterator> cell_distributor(cell_iter);
#else
	TbbDistributor<CellParticleCollectionIterator> distributor(iter);
	//    TbbDistributor<CLIterator> cell_distributor(cell_iter);    
#endif
	
	// define operators
	OperatorFluctuation randomKicks(mDt);
	OperatorPeriodic periodic(Params::sSim.minD, Params::sSim.sizeD);
	UpdateParticlesOperator<NHM> update(mNeighMap);
	
	// define integrator
	typedef RK2Integrator Integrator;
	Integrator integrator(mDt);
	
	for(int stage=0;stage<integrator.getStages();stage++){
		mNeighMap.reset(iter);
		distributor.process(update);
		integrator.update(stage,distributor);
		if (Params::sSim.bc && stage < integrator.getStages()-1) distributor.process(periodic);
	}
	serial_distributor.process(randomKicks);
	if (Params::sSim.bc) distributor.process(periodic);
}


void Model::updateCells(){
	// make sure there is enough space int the cell vector
	if(mCells.size()*2>mCells.capacity()){
		mCells.reserve(mCells.size()*2.5);
	}
	
	// iterate through cells and update them:
	// proliferation/migration/identify membrane
	// @@ should be parallized
	CellCollectionIterator iter = getCellIterator();
	for (iter.first(); !iter.isDone(); iter.next()) {
		SCECellType& cell = *(iter.currentItem());
		cell.invalidateMembrane();
		cell.update(mDt,&mCells);
	}
}

void Model::updateCells1(){
	// check when to update the cell id map:
	bool updateMap=false;
	
	// make sure there is enough space in the cell vector
	if(mCells.size()*2>mCells.capacity()){
		mCells.reserve(mCells.size()*2.5);
		updateMap=true;
	}
	
	// iterate through cells and update them:
	// proliferation/migration/lable membrane
	// @@ should be parallized
	CellCollectionIterator iter = getCellIterator();
	for (iter.first(); !iter.isDone(); iter.next()) {
		SCECellType& cell = *(iter.currentItem());
		//		cell.invalidateMembrane();
		updateMap |= cell.update(mDt,&mCells);
		//		if(!cell.getMembraneUpdated()){
		cell.updateMembrane();
		//		}
	}
	
	
#ifdef __JUX__
	//------------
	// JUXTACRINE SIGNALING
	//------------
	if(updateMap){
		initMap();
	}
	
	// create topology for juxatcrine signaling
	//    typedef CellList<SCEType> NHM;
	// create neighborhood map
	CellParticleCollectionIteratorP iter1 = getCellParticleIteratorP(0);
	//    CLBoundaryCondition domain_bc = CL_NONE;
	//    NHM neighMap;
	//    Real cellSize = 1.1*Params::getReqMax();//mReq*12.1;
	//    neighMap.setup(Params::sSim.minD,Params::sSim.sizeD,cellSize,domain_bc);
	mNeighMapJux.reset(iter1);	
	
	for (iter.first(); !iter.isDone(); iter.next()){
		SCECellType& cell = *(iter.currentItem());
		cell.updateNeighborhood(mNeighMapJux,mMap);
	}
	
	//workload distributors
	//    DummyDistributor<CellCollectionIterator> distributor(iter);
	//workload distributors
#if USE_TBB==0
	DummyDistributor<CellCollectionIterator> distributor(iter);
#else
	TbbDistributor<CellCollectionIterator> distributor(iter);
#endif
	
	// define operators
	UpdateCellsOperator update;
	
	// define integrator
	typedef RK1Integrator Integrator;
	Integrator integrator(mDt/Params::sigIters);
	
	for(int i=0;i<Params::sigIters;i++){
		for(int stage=0;stage<integrator.getStages();stage++){
			distributor.process(update);
			//	    distributor.process(genericOperator(&SCECellType::computeRHS));
			integrator.update(stage,distributor);
		}
	}
#endif
}


void  Model::updateCellPointers(){
	// for each cell, updates pointers to polymerizing/depolymerizing elements
	CellCollectionIterator iter = getCellIterator();
	for (iter.first(); !iter.isDone(); iter.next()){
		SCECellType& cell= (*iter.currentItem());
		cell.updatePolyP();
	}
}


//void Model::updateParticlesCheckMap
void Model::dump() {
	writeCellsToFile();
}
void Model::dumpSignalling() {
	writeSignallingToFile();
}

void Model::diag() {
    // for juxtacrine signaling
	// loops over all cells and computes 3 energy functions E1, E2, E3 and writes values to file
	Real E1=0;
	Real E2=0;
	Real E3=0;
	
	CellCollectionIterator iter = getCellIterator();
	for (iter.first(); !iter.isDone(); iter.next()){
		SCECellType& cell= (*iter.currentItem());
		//compute energy for each cell and add to current energy, we take the 'last' chemical species
		E1+=cell.compEnergy(1,NSPEC-1); 
		E2+=cell.compEnergy(2, NSPEC-1);
		E3+=cell.compEnergy(3, NSPEC-1);
	}
	E1=sqrt(E1);
	E2=sqrt(E2);
	E3=sqrt(E3);
	
	//print to file
	const std::string name = "txt/"+mName+"_Diag.txt";
	FILE * file = fopen(name.c_str(),"a");
	fprintf(file,"%f	%f	%f	%f \n",mTime,E1,E2,E3);
	fclose(file);
}

void Model::writeSignallingToFile(){
	CellCollectionIterator iter = getCellIterator();
	std::string name="txt/"+mName+"_Signalling.txt";
	//run through cells
	for (iter.first(); !iter.isDone(); iter.next()){
		SCECellType& cell= (*iter.currentItem());
		VecType pos = cell.getPos();
		//open file
		FILE * file = fopen(name.c_str(),"a");
        if (file==NULL) {
            std::cout << "Failed to open " << name << "!\n";
            abort();
        }
#if DIM==2
		fprintf(file,"%f	%d	%f	%f",mTime,cell.getCellId(),(float)pos[0],(float)pos[1]);
#elif DIM==3
		fprintf(file,"%f	%d	%f	%f	%f",mTime,cell.getCellId(),(float)pos[0],(float)pos[1],(float)pos[2]);
#endif
		for(int i=0;i<NSPEC;i++){
			fprintf(file,"	%f",cell.getSpec(i));
		}
		fclose(file);
		cell.writeNeighborIDs("txt/"+mName+"_Signalling.txt");
		
	}
}


void Model::writeCellsToFile(){
	CellCollectionIterator tmp = getCellIterator();
    FileHandler::writeCellsToFile(&tmp,"txt/"+mName+"_%07d.txt",mIt);
}

#pragma mark PRIVATE

#pragma mark VISUALIZATION

#ifdef __WITH_GL__
void Model::view() {
	// note: x,y in viewpoint go from -1 to 1...
	
	// setup
	const int corners = 15;  // for circles
	// HACK: pixel scaling from height of glOrtho (2) and of glutInitWindowSize (700)
	const float pixelSize = 1.0f/700;
	// offsets/scalings/corners
	const float offs = 0.04;
	const VecType scale = (1-2.*offs)/Params::sSim.sizeD;
	const VecType offsP=offs-Params::sSim.minD;
	const float xStart = offs;
	const float yStart = offs;
	const float xEnd = 1.-offs;
	const float yEnd = 1.-offs;
	Real radius[CELLTYPES];
	for(int i=0;i<CELLTYPES;i++){
		radius[i]= Params::sCells[i].req*scale[0]/2.;
	}
	// color settings
	const float gray = 0.5;     // for most text
	const float gray2 = 0.2;    // for extended grid on plots
	const float alpha = 1.0;    // alpha value for lines and text
	
	// draw some notes
	std::ostringstream oss;
	oss << mName << " - " << mDescription;
	// 9 = max-font-pos + min-font-pos (13 - 4 for GLUT_BITMAP_HELVETICA_18)
	VisualizationUtils::renderBitmapStringM(VectorElement<Real,2>(0.5,1.0-18*pixelSize),
						oss.str().c_str(), 
						pixelSize,
						RGBA(gray,gray,gray,alpha));
	oss.str("");    // reset stream
	oss << "i = " << mIt << ", t = " << mTime;
	VisualizationUtils::renderBitmapStringM(VectorElement<Real,2>(0.5, 0.0+5*pixelSize), oss.str().c_str(), pixelSize);
	
	// Draw Grid
#ifdef __SKETCH_GRID__
	//	glLineStipple(1, 0x0707);
	//	glEnable(GL_LINE_STIPPLE);
	
	// draw box
	glBegin(GL_LINES);
	glColor4f(gray,gray,gray,alpha);
	glVertex2f(xStart, yStart);
	glVertex2f(xStart, yEnd);
	glVertex2f(xStart, yEnd);
	glVertex2f(xEnd, yEnd);
	glVertex2f(xEnd, yEnd);
	glVertex2f(xEnd, yStart);
	glVertex2f(xEnd, yStart);
	glVertex2f(xStart, yStart);
	glEnd();
	//	glDisable(GL_LINE_STIPPLE);
	
	
	// draw gridlines
	glLineStipple(1, 0x0707);
	glEnable(GL_LINE_STIPPLE);
	glBegin(GL_LINES);
	glColor4f(gray2,gray2,gray2,alpha);
	// in X
	for(int i=1;i<mGRIDNCells[0];i++){
		const float xLoc = offs + (float)i*mGRIDCellsSize[0]*scale[0];
		glVertex2f(xLoc, yStart);
		glVertex2f(xLoc, yEnd);
	}
	// in Y
	for(int i=1;i<mGRIDNCells[1];i++){
		const float yLoc = offs + (float)i*mGRIDCellsSize[1]*scale[0];
		glVertex2f(xStart, yLoc);
		glVertex2f(xEnd, yLoc);
	}
	
	glEnd();	
	glDisable(GL_LINE_STIPPLE);	
#endif
	
	// iterate over particles and draw them
	// loop over all particles
	
	// some tmp variables
	Real intensity;
	
#ifndef __JUX__
	// visualize membrane using sceType attributes
	updateCellPointers();
	CellCollectionIterator iter1 = getCellIterator();
	for (iter1.first(); !iter1.isDone(); iter1.next()){
		SCECellType& cell=*(iter1.currentItem());
		CCellPhenotype cpt = cell.getCellPhenotype();
		int ct = cell.getCellType();
		Real ctf=1.0*Params::sCells[ct].radius;
		ParticleCollectionIterator iter = cell.getIterator();
		// Real radius
		for(iter.first(); !iter.isDone();iter.next()){
			
			const SCEType& sce = *(iter.currentItem());
			
			Real intFrac=sce.getPolyFrac();
			
			VecType pos = VecType(offs)+(sce.getPos()-Params::sSim.minD)*scale;
			//Real ctf=1.0*Params::sCells[sce.getCellType()].radius;
			if(cpt==P_Q || cpt==P_P){
				intensity = 0.8;
			}else{
				// intensity on polarization
				intensity = (sce.getPolarDist()-cell.getMinPolarDist()+ctf)/(cell.getMaxPolarDist()-cell.getMinPolarDist()+ctf);
				// intensity on density
				switch(cpt){
					case P_MI: // polarization
						intensity = (sce.getPolarDist()-cell.getMinPolarDist()+ctf)/(cell.getMaxPolarDist()-cell.getMinPolarDist()+ctf);
						break;
					case P_EQ: // denser/sparser all
						switch(Params::sCells[ct].EQ_mod){
							case 0:
							case 1:
								intensity = (sce.getDensInter()+sce.getDensIntra())/6;
								break;
							case 2: // denser/sparser inter
							case 3:
								intensity = 0.5+sce.getDensInter()/3;
								break;
							case 4: // denser/sparser intra
							case 5:
								intensity = sce.getDensIntra()/6;
								break;
							default:
								exit(1);
								
						}
				}
			}
			RGBA color2 = RGBA::colorMapRandom(sce.getCellId(),intensity);
			RGBA color;
#ifdef __MEMBRANE2__
			if(!sce.isMembrane()){
#else
			if(sce.getDensIntra()>CellParameters::densThr && sce.getDensInter()<InterCellParameters::densThr){
#endif
				color=RGBA::colorMapRandom(sce.getCellId(),0);
				//				color=RGBA(1,0,0,0);
			}else{
				if(cpt!=P_MI){
					color=RGBA::colorMapRandom(sce.getCellId(),intensity);
				}else{
					color=RGBA::colorMapRandom(sce.getCellId(),1);
				}
				//				color=RGBA(1,0,0,1);
			}
				
				VisualizationUtils::drawCircle(pos, intFrac*radius[ct]/2., color2, corners, true);
				VisualizationUtils::drawCircle(pos, intFrac*radius[ct]/1.2, color, corners, false);
				
				// print number of membrane particles connected to
				if(true){
					int dens = sce.getDensInter();
					if(dens>0){
						std::ostringstream oss;
						oss << dens;
						VisualizationUtils::renderBitmapStringM(pos,oss.str().c_str(),pixelSize);
					}
				}
				
				// visualize potential/u0
				intensity = (sce.getPotIntra()+sce.getPotInter());
				if(intensity>0){
					color=RGBA(1,0,0,intensity*0.05/Params::sCells[ct].u0);
				}else {
					color=RGBA(0,1,0,-intensity*0.03/Params::sCells[ct].u0);
				}
				VisualizationUtils::drawCircle(pos, intFrac*radius[ct]/5., color, corners, true);
			}
			
			
			// Mark in/out elements.. if migrating
			if(cpt==P_MI || cpt==P_EQ){
				//cell.updatePolyP();
				SCEType* sceIn = cell.getPolyIn();
				SCEType* sceOut = cell.getPolyOut();
				
                if (sceIn != NULL) {
                    VecType posIn = VecType(offs)+(sceIn->getPos()-Params::sSim.minD)*scale;
                    Real intIn=sceIn->getPolyFrac();
                    VisualizationUtils::drawCircle(posIn, intIn*radius[sceIn->getCellType()]/2., RGBA(1,1,1,intIn), corners, true);
                }
                
                if (sceOut != NULL) {
    	            VecType posOut = VecType(offs)+(sceOut->getPos()-Params::sSim.minD)*scale;
                    Real intOut=sceOut->getPolyFrac();
                    VisualizationUtils::drawCircle(posOut, intOut*radius[sceOut->getCellType()]/2., RGBA(1,1,1,intOut), corners, true);
                }
			}		
		}
		
		
		
		// draw stats over everything else
		// visualize cells stats
		
		if(true){
			iter1 = getCellIterator();
			for (iter1.first(); !iter1.isDone(); iter1.next()){
				SCECellType& cell=*(iter1.currentItem());
				
				bool mShowCellAxis=true;
				bool mShowCellBBox=false;
				bool mShowPolarization=true;
				// draw cell stats
				if (mShowCellAxis || mShowCellBBox) {
					CellStats stats = cell.getStats();
					
					// draw box
					if (mShowCellBBox) {
						const VecType boxStart = VecType(offs)+(stats.bbox.offset-Params::sSim.minD)*scale;
						const VecType boxEnd = VecType(offs)+(stats.bbox.offset+stats.bbox.size-Params::sSim.minD)*scale;
						
						glBegin(GL_LINE_LOOP);
						glColor4f(gray,gray,gray,alpha);
						glVertex2f(boxStart[0], boxStart[1]);
						glVertex2f(boxStart[0], boxEnd[1]);
						glVertex2f(boxEnd[0], boxEnd[1]);
						glVertex2f(boxEnd[0], boxStart[1]);
						glEnd();
					}
					
					// draw axis
					if (mShowCellAxis) {
						
						const VecType centroid = VecType(offs)+(stats.centroid-Params::sSim.minD)*scale;			
						const VecType majorStart = centroid - stats.majorAxis*scale/2;
						const VecType majorEnd = centroid + stats.majorAxis*scale/2;
						const VecType minorStart = centroid - stats.minorAxis*scale/2;
						const VecType minorEnd = centroid + stats.minorAxis*scale/2;
						
						glLineWidth(3);
						glEnable(GL_LINE_SMOOTH);
						glBegin(GL_LINES);
						
						
						glColor4f(0.7,0.1,0.8,0.5);
						glVertex2f(majorStart[0], majorStart[1]);
						glVertex2f(majorEnd[0], majorEnd[1]);
						glColor4f(0.2,0.1,0.8,0.5);
						glVertex2f(minorStart[0], minorStart[1]);
						glVertex2f(minorEnd[0], minorEnd[1]);
						glEnd();
						glLineWidth(1);
					}
					
					// draw polarization
					if (mShowPolarization) {
						
						const VecType polyStart = VecType(offs)+(stats.centroid-Params::sSim.minD)*scale;			
						const VecType polyEnd = polyStart + cell.getPolarity()*Params::sCells[cell.getCellType()].radius*scale;
						
						glLineWidth(3);
						glEnable(GL_LINE_SMOOTH);
						glBegin(GL_LINES);
						
						
						glColor4f(0,1,0,0.5);
						glVertex2f(polyStart[0], polyStart[1]);
						glVertex2f(polyEnd[0], polyEnd[1]);
						glEnd();
						glLineWidth(1);
					}
				}
			}
		}
#else
		// visualize membrane using getIteratorP function
		CellCollectionIterator iter1 = getCellIterator();
		
		bool updateMax = true;
		for (iter1.first(); !iter1.isDone(); iter1.next()) {
			//check for current maximum signalling level for all species
			SCECellType& cell=*(iter1.currentItem());
			if(updateMax){
				for (int i=0; i<NSPEC; i++){
					maxSpec[i] = cell.getSpec(i);
				}
				updateMax = false;
			}		
			for (int i=0; i<NSPEC; i++) {
				if(maxSpec[i]<cell.getSpec(i)) {
					maxSpec[i] = cell.getSpec(i);
				}
			}
		}	
		
		for (iter1.first(); !iter1.isDone(); iter1.next()){
			SCECellType& cell=*(iter1.currentItem());
			CCellPhenotype cpt = cell.getCellPhenotype();
			ParticleCollectionIterator iter = cell.getIterator();
			
			
			// visualize Elements
			// if in migration mode, visualize gradient
			for(iter.first(); !iter.isDone();iter.next()){
				const SCEType& sce = *(iter.currentItem());
				VecType pos = VecType(offs)+(sce.getPos()-Params::sSim.minD)*scale;
				Real ctf=1.0*Params::sCells[sce.getCellType()].radius;
				if(cpt!=P_MI){
					intensity = 0.8;
				}else{
					intensity = (sce.getPolarDist()-cell.getMinPolarDist()+ctf)/(cell.getMaxPolarDist()-cell.getMinPolarDist()+ctf);
				}
				RGBA color2 = RGBA::colorMapRandom(sce.getCellId(),intensity);
				VisualizationUtils::drawCircle(pos, radius[sce.getCellType()]/2., color2, corners, true);
			}
			if(!cell.getMembraneUpdated()){
				cell.updateMembrane();
			}
			// visualize Membrane
			// if in migration mode, visualize gradient
			ParticleCollectionIteratorP iter2 = cell.getIteratorP(0);
			for(iter2.first(); !iter2.isDone();iter2.next()){
				const SCEType& sce = *(iter2.currentItem());
				VecType pos = VecType(offs)+(sce.getPos()-Params::sSim.minD)*scale;
				Real ctf=1.0*Params::sCells[sce.getCellType()].radius;
				if(cpt!=P_MI){
					intensity = 0.8;
				}else{
					intensity = (sce.getPolarDist()-cell.getMinPolarDist()+ctf)/(cell.getMaxPolarDist()-cell.getMinPolarDist()+ctf);
				}
				RGBA color;
				if(cpt!=P_MI){
					color=RGBA::colorMapRandom(sce.getCellId(),intensity);
				}else{
					color=RGBA::colorMapRandom(sce.getCellId(),1);
				}
				VisualizationUtils::drawCircle(pos, radius[sce.getCellType()]/1.2, color, corners, false);
			}
			
			
			
			// visualize Signalling
			VecType pos1=VecType(offs)+(cell.getPos()-Params::sSim.minD)*scale;
			RGBA color= RGBA::red(cell.getSpec(1)/std::max((Real)maxSpec[1],1.0));  //Delta / f --> red
			RGBA color2= RGBA::blue(cell.getSpec(0)/std::max((Real)maxSpec[0],1.0));//Notch / a --> blue
			
			RGBA color3=RGBA::white(); //zero level
			
			// Delta, f
			VisualizationUtils::drawCircle(pos1,radius[cell.getCellType()]/0.55,color3,corners,true);
			VisualizationUtils::drawCircle(pos1,radius[cell.getCellType()]/0.55,color,corners,true);
			
#if JUXPATH==2
			RGBA color4= RGBA::black(cell.getSpec(2)/std::max((Real)maxSpec[2],1.0));// b --> black
			// b
			VisualizationUtils::drawCircle(pos1,radius[cell.getCellType()]/0.75,color3,corners,true);
			VisualizationUtils::drawCircle(pos1,radius[cell.getCellType()]/0.75,color4,corners,true);
			
			//printf("Cell %d got C1: %f, C2: %f, C3: %f, Max: %f %f %f\n", cell.getCellId(), cell.getSpec(0), cell.getSpec(1), cell.getSpec(2), maxSpec[0], maxSpec[1], maxSpec[2]);
#elif JUXPATH==1
			//printf("Cell %d got C1: %f, C2: %f , Max: %f %f %f\n", cell.getCellId(), cell.getSpec(0), cell.getSpec(1), maxSpec[0], maxSpec[1], maxSpec[2]);
#endif
			
			// Notch, a
			VisualizationUtils::drawCircle(pos1,radius[cell.getCellType()]/1.0,color3,corners,true);
			VisualizationUtils::drawCircle(pos1,radius[cell.getCellType()]/1.0,color2,corners,true);		
			
			
			
			//visualize Neighbourhood connections
			ArrayIterator<SCECellType::NeighborType> iter3 = cell.getNeighborIterator();
			for(iter3.first();!iter3.isDone();iter3.next()){
				
				SCECellType& neigh = *(iter3.currentItem()->first);
				assert(neigh.getCellId()>0&&neigh.getCellId()<=100);
				VecType pos1 = VecType(offs)+(cell.getPos()-Params::sSim.minD)*scale;
				VecType pos2 = VecType(offs)+(neigh.getPos()-Params::sSim.minD)*scale;
				if(Params::sSim.bc==1){
					pos1=cell.getPos();
					pos2=neigh.getPos();
					VecType dist=pos2-pos1;
					VecType adist;
					adist[0]=abs(dist[0]);
					adist[1]=abs(dist[1]);
					VecType dl=Params::sSim.sizeD;
					VecType dl2=dl/(Real)2.;
					if(adist[0]>dl2[0]){
						dist[0]=(adist[0]-dl[0])*dist[0]/adist[0];
					}
					if(adist[1]>dl2[1]){
						dist[1]=(adist[1]-dl[1])*dist[1]/adist[1];
					}
					pos2=pos1+dist;
					//rescale positions
					pos1 = VecType(offs)+(pos1-Params::sSim.minD)*scale;
					pos2 = VecType(offs)+(pos2-Params::sSim.minD)*scale;
				}
				
				assert(neigh.getPos()[0]!=0.0 && neigh.getPos()[1]!=0.0);
				glBegin(GL_LINES);
				glColor4f(1,0,1,0.5);
				glVertex2f(pos1[0],pos1[1]);
				glVertex2f(pos2[0],pos2[1]);
				glEnd();
				
			}
		}
#endif	
	}
#endif
