/*
 *  SCEParameters.cpp
 *  Particles
 *
 *  Created by Florian Milde on 3/2/10.
 *  Copyright 2010 EtH Zurich. All rights reserved.
 *
 */

#include "SCEParameters.h"

//initialize static variables
SimParameters Params::sSim;
CellParameters Params::sCells[CELLTYPES];
InterCellParameters Params::sInterCells[CELLTYPES][CELLTYPES];
Real Params::maxReq=-1;
bool Params::set=false;
// the empirical ones
//Real Params::densThrIntra=5;
//Real Params::densRadIntra=1.5;  // in equilibrium distances
//Real Params::densThrInter=1;
//Real Params::densRadInter=1;    // in equilibrium distances

// the empirical ones at another level
Real CellParameters::densThr=5;
Real CellParameters::densRad=1.4;
Real InterCellParameters::densThr=1;
Real InterCellParameters::densRad=1.1;



// some temporal ones.. not sure where they will go
// signaling parameters
#if	JUXPATH == 1
//   ...for JUXPATH 1 (2 species: Delta, Notch(
Real Params::sigA=0.01;
Real Params::sigB=100;
Real Params::sigK=2;
Real Params::sigH=2;
Real Params::sigV=1;
Real Params::sigIters=1;
#elif JUXPATH == 2
//   ...for JUXPATH 2 (3 species: a, f, b)
Real Params::kASS=0.0003; //association of complex, 1/(molecules*min)
Real Params::kDISS=0.12;  //dissociation of complex, 1/min
Real Params::kINT=0.019;  //internalisation of complex, 1/min
Real Params::degA=0.006;  //degradation of a, 1/min
Real Params::degF=0.03;   //degradation of f, 1/min
// parameters of feedback functions
Real Params::HillA=1;     //Hill coefficient in feedback function for a
Real Params::HillF=3;	  //Hill coefficient in feedback function for f
Real Params::C1=61;//110;// 
Real Params::C2=1000;//2500;//
Real Params::C3=30;//90;//
Real Params::C4=7.7;//7.4;//
Real Params::C5=3600;//5450;//
Real Params::sigIters=1;
#endif
