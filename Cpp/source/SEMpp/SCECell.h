/*
 *  Cell.h
 *  Particles
 *
 *  Created by Florian Milde on 1/24/10.
 *  Copyright 2010 EtH Zurich. All rights reserved.
 *
 * Implements the conept CIterable
 * IDEA:- the cell is a functional unit which consists of an agglomeration of SCE's
 *		- Members:
 *          - container of SCE's
 *          - type
 *          - id 
 *		- Methods:
 *			- Grow
 *			- Migrate
 *			- Divide
 *      - Static Members:
 *			- Type
 *          - Id
 *      - Static Methods:
 *          - getGlobalCellId
 */

#pragma once

#include "Utilities.h"
#include "Environment.h"
#include "SCE.h"
#include "Functions.h"
#include "Iterators.h"
#include <map>


/** Struct for stats of cell (comparable to Matlab regionprops). 
 * @@@ copy paste from Gerardos Sepal code
 */
class CellStats {
public:
	VecType centroid;
	Real area;
	BoundingBox bbox;
	Real majorAxisLength;
	Real minorAxisLength;
	Real eccentricity;  // fit ellipse (major a, minor b): b = a*sqrt(1-e^2)
	Real orientation;   // in rad (Matlab gives in deg)
	VecType majorAxis;  // draw: centroid - major/2 until centroid + major/2
	VecType minorAxis;
	
	/** Support for C++-streams. */
	friend std::ostream& operator<< (std::ostream& out, const CellStats& stats) {
		out << "centroid: " << stats.centroid << std::endl;
		out << "area: " << stats.area << std::endl;
		out << "bbox.offset: " << stats.bbox.offset << std::endl;
		out << "bbox.size: " << stats.bbox.size << std::endl;
		out << "majorAxisLength: " << stats.majorAxisLength << std::endl;
		out << "minorAxisLength: " << stats.minorAxisLength << std::endl;
		out << "eccentricity: " << stats.eccentricity << std::endl;
		out << "orientation: " << stats.orientation << std::endl;
		out << "majorAxis: " << stats.majorAxis << std::endl;
		out << "minorAxis: " << stats.minorAxis << std::endl;
		return out;
	}    
};

/** Cell Phases */
enum CCellPhase {
	C_G0, // gap0 phase
	C_I,  // inter phase (growth)
	C_M1, // division phase
	C_M2, // separation phase
	C_MI  // temporal. got to go
};

/** Cell Phenotypes */
enum CCellPhenotype {
	P_Q,
	P_EQ,
	P_P,
	P_MI,
};

template <typename T, typename I>
class SCECell{
#pragma mark TYPEDEFS
	typedef SCECell<T,I> SelfType;
	typedef std::vector<SelfType> CellCollection;
public:
	//public typedefs
	/** Collection of particles. */
	typedef std::vector<T> Collection;
	typedef std::vector<T*> CollectionP;
	/** Iterator for particles. */
	typedef I CollectionIterator;
	typedef ArrayIteratorP<T> CollectionIteratorP;
	/** particles Type. */
	typedef T SCEType;
	typedef SCEType IterableType;
	/** neighborCouple id,contribution */
	typedef std::pair<SelfType*,Real> NeighborType;
	/** iteratro for neighbors*/
	typedef ArrayIterator<NeighborType> CellNeighborCollectionIterator;
	/** Molecular Species for integration */
	typedef VectorElement<Real,NSPEC> StateType;
	
#pragma mark LIFECYCLE
	
	//default constructor
	SCECell(){
		//give id
		mCellId = ++SelfType::sGlobalCellId;
		//assign type
		mCellType = 0;
		//allocate space for particle array (2*N to allow to grow to double the size)
		mParticles.reserve(2*Params::sCells[mCellType].nSce);
		//allocate space for membraneParticles  maybe to much..
		mMembraneParticles.reserve(Params::sCells[mCellType].nSce);
		//allocate space for neighbourCells .. how much?
		mNeighborMap.reserve(10);
		mCPT= P_Q;
		mCP = C_G0;
		mClock =0.;
		mPolarity =VecType(1.)/VecType(1.).abs();
		_init();
	};
	
	SCECell(int _cellType){
		//give id
		mCellId = ++SelfType::sGlobalCellId;
		//assign type
		mCellType = _cellType;
		//allocate space for particle array (2*N to allow to grow to double the size)
		mParticles.reserve(2*Params::sCells[mCellType].nSce);
		//allocate space for membraneParticles  maybe to much..
		mMembraneParticles.reserve(Params::sCells[mCellType].nSce);
		//allocate space for neighbourCells .. how much?
		mNeighborMap.reserve(10);
		mCPT= P_Q;
		mCP = C_G0;
		mClock =0.;
		mPolarity =VecType(1.)/VecType(1.).abs();
		_init();
	};
	
	SCECell(int _cellId, int _cellType, int _cellPhenotype, int _cellPhase, Real _cellClock, Real _iDur,VecType _polarity){
		//give id
		mCellId = _cellId;
		SelfType::sGlobalCellId=std::max(SelfType::sGlobalCellId,_cellId);
		//assign type
		mCellType = _cellType;
		//allocate space for particle array (2*N to allow to grow to double the size)
		mParticles.reserve(2*Params::sCells[mCellType].nSce);
		//allocate space for membraneParticles  maybe to much..
		mMembraneParticles.reserve(Params::sCells[mCellType].nSce);
		//allocate space for neighbourCells .. how much?
		mNeighborMap.reserve(10);
		mCPT = CCellPhenotype(_cellPhenotype);
		mCP = CCellPhase(_cellPhase);
		mClock = _cellClock;
		mIDur = _iDur;
		mPolarity = _polarity;
		_init();
	};
	
	void _init(){
		mMinPolarDist=0.;
		mMaxPolarDist=0.;
		mMembraneUpdated=false;
		for(int i=0; i<NSPEC; i++){
			mState[i]=Params::sSim.icJuxLevel[i];
		}
        mPolyIn = NULL;
        mPolyOut = NULL;
	}
	
#pragma mark CIterable
	/** Get iterator for mParticles. */
	CollectionIterator getIterator() { return CollectionIterator(&mParticles[0], mParticles.size()); }  // ArrayIterator	
	/** Get iterator for a defined subset of mParticles. */
	CollectionIteratorP getIteratorP(int i){
		if(mMembraneParticles.size()==0)	return CollectionIteratorP(NULL,0);
		return CollectionIteratorP(&mMembraneParticles[0], mMembraneParticles.size());
	} //ArrayIteratorP
	CellNeighborCollectionIterator getNeighborIterator(){
		if(mNeighborMap.size()==0) return CellNeighborCollectionIterator(NULL,0);
		return CellNeighborCollectionIterator(&mNeighborMap[0], mNeighborMap.size());
	}
	
	// concept not integrated yet
#pragma mark CIntegrable
	void resetRHS(){mRHS=0.;}
	
	void setState(const StateType& state){
		mState=state;
	}
	
	StateType getState() const{
		return mState;
	}
	
	StateType getRHS() const{
		return mRHS;
	}		
	
	void setStateT1(const StateType& _state){mStateT1=_state;}
	StateType getStateT1() const {return mStateT1;}
	
	
#pragma mark OPERATIONS
	/** set/access properties */
	void setCellPhase(CCellPhase _phase, Real _clock=0){
		mCP=_phase;
		mClock=_clock;
		mIDur = Params::sCells[mCellType].I_dur/Params::sCells[mCellType].nSce;
	}
	
	CCellPhase getCellPhase() const {return mCP;}
	
	void setCellPhenotype(CCellPhenotype _phenotype,CCellPhase _phase, Real _clock=0){
		mCPT = _phenotype;
		mCP=_phase;
		mClock = _clock;
		Real alpha;
		switch(mCPT){
			case P_Q: // QUIESCENT
				break;
			case P_EQ:
			case P_MI: // MIGRATING
				/**
				 * set polarization time
				 * init polarization direction
				 * tag in element
				 * create out element
				 * tag out element
				 * set fraction to 1
				 */
				// set time interval
				mIDur = Params::sCells[mCellType].MI_dur/(Real)Params::sCells[mCellType].nSce;
				// initialize the polarization
#if DIM==2
				alpha=2.0*M_PI*(Real)rand()/(Real)RAND_MAX;
				mPolarity=VecType(cos(alpha),sin(alpha));
#else
				mPolarity=VecType(cos(alpha),sin(alpha),sin(alpha));
				printf("@@@NO RANDOM INITIALIZATION");
#endif
				
				// create out element.. in center
				addSCE(getPos());
				// tag in/out elements.. in:anyone(first) / out:last
				mPolyIn = &mParticles.front();
				mPolyOut = &mParticles.back();
				mPolyInId= mPolyIn->getSceId();
				mPolyOutId= mPolyOut->getSceId();
				// set fraction
				mPolyIn->setPolyFrac(1);
				mPolyOut->setPolyFrac(0);
				
				break;
			case P_P: // PROLIFERATING
				mIDur = Params::sCells[mCellType].I_dur/Params::sCells[mCellType].nSce;
				break;
			default:
				std::cout<< "Something Wrong set Phenotype" << std::endl;
				break;
		}
	}
	
	CCellPhenotype getCellPhenotype() const {return mCPT;}
	int getCellId() const {return mCellId;}
	int getCellType() const {return mCellType;}	 
	Real getClock()const{return mClock;}
	void setClock(Real _clock){mClock=_clock;}
	Real getIDur()const{return mIDur;}
	void setIDur(Real _idur){mIDur=_idur;}
	VecType getPolarity()const{return mPolarity;}
	void setPolarity(VecType _polarity){mPolarity = _polarity/_polarity.abs();}
	Real getMinPolarDist()const{return mMinPolarDist;}
	Real getMaxPolarDist()const{return mMaxPolarDist;}
	bool getMembraneUpdated()const{return mMembraneUpdated;}
	void invalidateMembrane(){mMembraneUpdated=false;}
	
	/** add particle to container */
	void addSCE(SCEType _sce){ 
		// adjust id and cell type
		_sce.setCellType(mCellType);
		_sce.setCellId(mCellId);
		mParticles.push_back(_sce);
	}
	void addSCE(VecType _pos){	            mParticles.push_back(SCEType(_pos,mCellType,mCellId));};
	void addSCE(VecType _pos,int _sceId){	mParticles.push_back(SCEType(_pos,_sceId,mCellType,mCellId));};
	
	/** get center of cell */
	VecType getPos() {
		assert(mParticles.size()!=0);
		if(Params::sSim.bc==0){
			CollectionIterator iter = getIterator();
			VecType cellCenter=0.;
			for (iter.first(); !iter.isDone(); iter.next()) {
				SCEType* item = iter.currentItem();
				cellCenter+=item->getPos();
			}
			return cellCenter/mParticles.size();
		}else{
			// periodic BC:
			// accumulate positions
			// check min/max in x and y. if bigger than domain/2
			// redo that dimension
			CollectionIterator iter = getIterator();
			VecType cellCenter=0.;
			VecType minD=Params::sSim.minD;
			VecType sizeD=Params::sSim.sizeD;
			VecType sizeDH=sizeD/(Real)2.;
			VecType min = Params::sSim.minD+Params::sSim.sizeD;
			VecType max= Params::sSim.minD;
			
			for (iter.first(); !iter.isDone(); iter.next()) {
				SCEType* item = iter.currentItem();
				VecType pos=item->getPos();
				cellCenter+=pos;
				min=VE_Utilities::min(min,pos);
				max=VE_Utilities::max(max,pos);
			}
			VecType dist=max-min;
			int redo[2];
			redo[0]=(dist[0]>Params::sSim.sizeD[0]*0.9)?1:0;
			redo[1]=(dist[1]>Params::sSim.sizeD[1]*0.9)?1:0;
			if(redo[0]||redo[1]){
				cellCenter=0.;
				if(redo[0]&&redo[1]){
					for(iter.first(); !iter.isDone();iter.next()){
						SCEType* item = iter.currentItem();
						VecType pos=item->getPos()-minD+sizeD;;
						pos=VE_Utilities::mod(pos-sizeDH,sizeD)+sizeDH;
						cellCenter+=pos;
					}
				}else if(redo[0]){
					for(iter.first(); !iter.isDone();iter.next()){
						SCEType* item = iter.currentItem();
						VecType pos=item->getPos()-minD+sizeD;
						pos[0]=fmod((pos[0]-sizeDH[0]),sizeD[0])+sizeDH[0];
						cellCenter+=pos;
					}
					
				}else if(redo[1]){
					for(iter.first(); !iter.isDone();iter.next()){
						SCEType* item = iter.currentItem();
						VecType pos=item->getPos()-minD+sizeD;
						pos[1]=fmod((pos[1]-sizeDH[1]),sizeD[1])+sizeDH[1];
						cellCenter+=pos;
					}
				}
				return VE_Utilities::mod(cellCenter/mParticles.size(),sizeD)+minD;
			}else{
				return cellCenter/mParticles.size();
			}
			
			
		}
	}
	
	const VecType readPos() {
		CollectionIterator iter = getIterator();
		VecType cellCenter=0.;
		for (iter.first(); !iter.isDone(); iter.next()) {
			SCEType* item = iter.currentItem();
			cellCenter+=item->getPos();
		}
		return cellCenter/(Real)mParticles.size();
	}
	
	Real getSurface() const { return mMembraneParticles.size();}
	
	Real getSpec(int i) const { return mState[i];}
	//Real getNotch() const { return mState[0];}
	//Real getDelta() const { return mState[1];}
	
	void writeNeighborIDs(std::string name){
		//open file
		FILE * file = fopen(name.c_str(),"a");
        if (file==NULL) {
            std::cout << "Failed to open " << name << "!\n";
            abort();
        }
        
		fprintf(file,"	%d",(int) mNeighborMap.size());
		typename std::vector<NeighborType>::iterator it;
		for (it=mNeighborMap.begin();it!=mNeighborMap.end();it++){
			SelfType* neigh = it->first;
			fprintf(file,"	%d",neigh->getCellId());
		}
		fprintf(file,"\n");
		fclose(file);
	}
	
	int getNumNeighbors(){
		return mNeighborMap.size();
	}
	
	
	
	int getPolyInId() {return mPolyInId;}
	int getPolyOutId() {return mPolyOutId;}
	void updatePolyP() {
		CollectionIterator iter = getIterator();
		bool bIn, bOut;
		bIn=bOut=false;
		for (iter.first(); !iter.isDone(); iter.next()){
			SCEType* item = iter.currentItem();
			if(item->getSceId()==mPolyInId){
				mPolyIn=item;
				bIn=true;
			}else{ 
				if(item->getSceId()==mPolyOutId){
					mPolyOut=item;
					bOut=true;
				}
			}
			if(bOut && bIn)
				break;
		}
	}
	
	SCEType* getPolyIn() {return mPolyIn;}
	SCEType* getPolyOut() {return mPolyOut;}
	
	
	
#pragma mark POLYMERIZATION
	
	/** equilibration polymerization/depolimerization */
	void polyEquilibration(){
		int candidates=Params::sCells[mCellType].EQ_cand; // 0: membrane, 1: internal, 2: all
		switch(Params::sCells[mCellType].EQ_mod){
			case 0:
				// denser all
				polyDensitySmooth(0,0,candidates);
				break;
			case 1:
				// sparser all
				polyDensitySmooth(0,1,candidates);
				break;
			case 2:
				// denser inter
				polyDensitySmooth(1,0,candidates);
				break;
			case 3:
				// sparser inter
				polyDensitySmooth(1,1,candidates);
				break;
			case 4:
				// denser intra
				polyDensitySmooth(2,0,candidates);
				break;
			case 5:
				// sparser intra
				polyDensitySmooth(2,1,candidates);
				break;				
			default:
				break;
		}		
		
	}
	
	/** migration polymerization/depolimerization */
	void polyMigration(){
		int candidates=Params::sCells[mCellType].MI_cand; // 0: membrane, 1: internal, 2: all
		
		switch(Params::sCells[mCellType].MI_pmod){
			case 0:
				//				updatePolarityRand(dt);
				break;
			case 1:
				updatePolarityOrientation();
				break;
			default:
				// do nothing
				break;
		}
		// follow polarization vector
		//polyPolarity2();
		polyPolaritySmooth2(candidates);
		
	}
	
	/** MIGRATION-poly1: polymerize - depolymerize at random locations*/
	void polyRandom(){
		int dp = getRandSCEInd();
		int p = getRandSCEInd();
		mParticles[dp].setPos(mParticles[p].getPos());
	}
	
	/** MIGRATION-poly2: polymerize - depolymerize at random location on cell surface */
	void polyRandomSurf(){
		// get the actin elements
		std::vector<SCEType*> candidates = getActinElements();
		// randomly select two
		int dp=std::min((int)floor((Real)rand()/(Real)RAND_MAX*(Real)candidates.size()),(int)candidates.size()-1);
		int p=std::min((int)floor((Real)rand()/(Real)RAND_MAX*(Real)candidates.size()),(int)candidates.size()-1);
		candidates[dp]->setPos(candidates[p]->getPos());
	}
	
	/** MIGRATION-poly3: poly - depoly polarization
	 * select pol-location and depol-particle of actine elements according to polarization vector
	 * depolarize and polarize..
	 */
	void polyPolarity(){
		std::vector<SCEType*> actinElements = getActinElements();
		int dp=-1;
		int p=-1;
		// select polarization and depolarization elements
		Real minDist=9999999.;
		Real maxDist=-1;
		if(actinElements.size()>1){
			for(int i=0;i<actinElements.size();i++){
				if(actinElements[i]->getPolarDist()>maxDist){
					maxDist=actinElements[i]->getPolarDist();
					p=i;
				}	
				if(actinElements[i]->getPolarDist()<minDist){
					minDist=actinElements[i]->getPolarDist();
					dp=i;
				}
				//				if(dp>=0 && p>=0) break;
			}
			actinElements[dp]->setPos(actinElements[p]->getPos());
		}
	}
	
	/** MIGRATION-poly4: poly - depoly polarization
	 * select pol-location and depol-particle according to polarization vector
	 * select randomly from percentage of high and low polar dist edge
	 * depolarize and polarize..
	 * polarize at 2/3 of req from selected element towards the cell center
	 */
	void polyPolarity2(){
		std::vector<SCEType*> actinElements = getActinElements();
		Real perc = 0.2;
		// sort the vector
		std::sort(actinElements.begin(),actinElements.end(),SCE::greater);
		// select polarization and depolarization elements
		// select from percentage in front
		// remove furthest in the back
		int elems=actinElements.size();
		if(actinElements.size()>1){
			int add=floor(((Real)rand()/(Real)RAND_MAX)*perc*(Real)elems);
			VecType shift=readPos()-actinElements[add]->getPos();
			actinElements.back()->setPos(actinElements[add]->getPos()+shift/shift.abs()*Params::sCells[mCellType].req*2/3);
			mMembraneUpdated=false;
		}
	}
	
	/** MIGRATION-poly5: poly - depoly polarization
	 * select pol-location and depol-particle according to polarization vector
	 * select randomly from percentage of high and low polar dist edge
	 * depolarize and polarize..
	 * polarize at 2/3 of req from selected element towards the cell center
	 */
	void polyPolaritySmooth(){
		std::vector<SCEType*> polyCandidates = getPolyCandidates(0);
		Real perc = 0.2;
		// sort the vector
		std::sort(polyCandidates.begin(),polyCandidates.end(),SCE::greater);
		// select polarization and depolarization elements
		// select from percentage in front
		// remove furthest in the back
		int elems=polyCandidates.size();
		if(elems>1){
			int i_ind=floor(((Real)rand()/(Real)RAND_MAX)*perc*(Real)elems);
			const VecType i_pos = polyCandidates[i_ind]->getPos();
			VecType i_shift=readPos()-i_pos;
			mPolyOut->setPos(i_pos+i_shift/i_shift.abs()*Params::sCells[mCellType].req*2/3);
			mPolyOut->setPolyFrac(0);
			mPolyIn=mPolyOut;
			mPolyInId=mPolyOutId;
			
			mPolyOut=polyCandidates.back();
			mPolyOut->setPolyFrac(1);
			mPolyOutId=mPolyOut->getSceId();
			mMembraneUpdated=false;
		}
	}
	
	/** MIGRATION-poly6: poly - depoly polarization
	 * select pol-location according to polarization vector
	 * select depol-location according to internal density
	 * select randomly from percentage of high polar dist edge
	 * polarize at 2/3 of req from selected element towards the cell center
	 */
	void polyPolaritySmooth2(int _candidates=0){
		Real perc = 0.2;
		// polarization
		std::vector<SCEType*> polyCandidates = getPolyCandidates(_candidates);
		std::sort(polyCandidates.begin(),polyCandidates.end(),SCE::greater);
		// depolarization
		std::vector<SCEType*> depolyCandidates = getPolyCandidates(3);
		std::sort(depolyCandidates.begin(),depolyCandidates.end(),SCE::denserIntra);
		
		// select polarization and depolarization elements
		// select from percentage in front
		// remove furthest in the back
		int elems=polyCandidates.size();
		if(elems>1){
			int i_ind=floor(((Real)rand()/(Real)RAND_MAX)*perc*(Real)elems);
			const VecType i_pos = polyCandidates[i_ind]->getPos();
			VecType i_shift=readPos()-i_pos;
			mPolyOut->setPos(i_pos+i_shift/i_shift.abs()*Params::sCells[mCellType].req*2/3);
			mPolyOut->setPolyFrac(0);
			mPolyIn=mPolyOut;
			mPolyInId=mPolyOutId;
			
			mPolyOut=depolyCandidates.back();
			mPolyOut->setPolyFrac(1);
			mPolyOutId=mPolyOut->getSceId();
			mMembraneUpdated=false;
		}
	}
	
	/** MIGRATION-dens1: poly - depoly density
	 * select pol-location and depol-particle according to density
	 */
	void polyDensity(){
		std::vector<SCEType*> actinElements = getActinElements();
		Real perc = 0.2;
		// sort the vector
		std::sort(actinElements.begin(),actinElements.end(),SCE::denser);
		// select polarization and depolarization elements
		// select from percentage in front
		// remove furthest in the back
		int elems=actinElements.size();
		if(actinElements.size()>1){
			int add=floor(((Real)rand()/(Real)RAND_MAX)*perc*(Real)elems);
			VecType shift=readPos()-actinElements[add]->getPos();
			actinElements.back()->setPos(actinElements[add]->getPos()+shift/shift.abs()*Params::sCells[mCellType].req*2/3);
			mMembraneUpdated=false;
		}
	}	
	
	/** MIGRATION-dens2: poly - depoly density
	 * update polarity vector depending on cell density pol-location and depol-particle according to density
	 * only considering internal density
	 */
	void polyDensity2(){
		std::vector<SCEType*> actinElements = getActinElements();
		Real perc = 0.2;
		// sort the vector
		std::sort(actinElements.begin(),actinElements.end(),SCE::denserIntra);
		// select polarization and depolarization elements
		// select from percentage in front
		// remove furthest in the back
		int elems=actinElements.size();
		if(actinElements.size()>1){
			int add=floor(((Real)rand()/(Real)RAND_MAX)*perc*(Real)elems);
			VecType shift=readPos()-actinElements[add]->getPos();
			actinElements.back()->setPos(actinElements[add]->getPos()+shift/shift.abs()*Params::sCells[mCellType].req*2/3);
			mMembraneUpdated=false;
		}
	}
	
	/** MIGRATION-dens4: poly - depoly density smoothly
	 * update polarity vector depending on cell density pol-location and depol-particle according to density
	 * only considering internal density
	 * take out at low density, enter at high density (or inverse)
	 */
	void polyDensitySmooth(int _mode=0,bool _inverse=false, int _candidates=0){
		
		std::vector<SCEType*> polyCandidates = getPolyCandidates(_candidates);
		std::random_shuffle ( polyCandidates.begin(), polyCandidates.end() );
		
		int shiftFac=3/2;
		if(_candidates>0)
			shiftFac=0;
		
		
		Real perc = 0.2;
		// sort the vector
		if(_inverse){
			switch(_mode){
				case 0: // full density
					std::sort(polyCandidates.begin(),polyCandidates.end(),SCE::sparser);
					break;
				case 1: // internal density
					std::sort(polyCandidates.begin(),polyCandidates.end(),SCE::sparserInter);
					break;
				case 2: // external density
					std::sort(polyCandidates.begin(),polyCandidates.end(),SCE::sparserIntra);
					break;
				default: //
					exit(0);					
			}
		}else{
			switch(_mode){
				case 0: // full density
					std::sort(polyCandidates.begin(),polyCandidates.end(),SCE::denser);
					break;
				case 1: // internal density
					std::sort(polyCandidates.begin(),polyCandidates.end(),SCE::denserInter);
					break;
				case 2: // external density
					std::sort(polyCandidates.begin(),polyCandidates.end(),SCE::denserIntra);
					break;
				default: //
					exit(0);
			}					
		}
		// select polarization and depolarization elements
		// select from percentage in front
		// - find density of perc.
		// - find object furthest in back with same density
		// find density in back
		// - select from all elements with same density
		int elems=polyCandidates.size();
		if(elems>1){
			int i_ind=floor(((Real)rand()/(Real)RAND_MAX)*perc*(Real)elems);
			
			const VecType i_pos = polyCandidates[i_ind]->getPos();
			VecType i_shift=readPos()-i_pos;
			mPolyOut->setPos(i_pos+i_shift/i_shift.abs()*Params::sCells[mCellType].req*shiftFac);
			mPolyOut->setPolyFrac(0);
			mPolyIn=mPolyOut;
			mPolyInId=mPolyOutId;
			
			mPolyOut=polyCandidates.back();
			
			mPolyOut->setPolyFrac(1);
			mPolyOutId=mPolyOut->getSceId();
			mMembraneUpdated=false;
		}
	}
	
	/** Update the vector containing the pointers to the Membrane particles */
	void updateMembrane(){
		mMembraneParticles.clear();
		CollectionIterator iter = getIterator();
		for(iter.first();!iter.isDone();iter.next()){
			SCEType* item = iter.currentItem();
#ifdef __MEMBRANE2__
			if (item->isMembrane())
#else
				if (item->getDensIntra()<=CellParameters::densThr || item->getDensInter()>=InterCellParameters::densThr)
#endif
					mMembraneParticles.push_back(item);
		}
		mMembraneUpdated=true;
	}
	
	/** Retrun a vector containing the actin elements in this cell */
	// @@@maybe soon obsolete..
	std::vector<SCEType*> getActinElements(){
		std::vector<SCEType*> candidates;
		CollectionIterator iter = getIterator();
		for(iter.first();!iter.isDone();iter.next()){
			SCEType* item = iter.currentItem();
#ifdef __MEMBRANE2__
			if (item->isMembrane())
#else
				if (item->getDensIntra()<=CellParameters::densThr || item->getDensInter()>=InterCellParameters::densThr)
#endif
					candidates.push_back(item);
		}
		return candidates;
	}
	
	std::vector<SCEType*> getPolyCandidates(int _candidates=0){
		
		switch(_candidates){
			case 0: // membrane elements
				return getPolyCandidatesMembrane();
				break;
			case 1: // internal elements
				return getPolyCandidatesInternal();
				break;
			case 2: // all
				return getPolyCandidatesAll();
				break;
			case 3: // just return elements of the tail of the oriented cell
				return getPolyCandidatesTail();
				break;
			default:
				exit(0);
		}
	}
	
	CollectionP getPolyCandidatesMembrane(){
		CollectionP candidates;
		candidates.reserve(mParticles.size());
		CollectionIterator iter = getIterator();
		for(iter.first();!iter.isDone();iter.next()){
			SCEType* item = iter.currentItem();
			if(item != mPolyOut){
#ifdef __MEMBRANE2__
				if(item->isMembrane())
#else
					if (item->getDensIntra()<=CellParameters::densThr || item->getDensInter()>=InterCellParameters::densThr)
#endif
						candidates.push_back(item);
			}
		}
		return candidates;
	}
	
	std::vector<SCEType*> getPolyCandidatesInternal(){
		CollectionP candidates;
		candidates.reserve(mParticles.size());
		CollectionIterator iter = getIterator();
		for(iter.first();!iter.isDone();iter.next()){
			SCEType* item = iter.currentItem();
			if(item != mPolyOut){
#ifdef __MEMBRANE2__
				if (!item->isMembrane())
#else
					if (item->getDensIntra()>CellParameters::densThr && item->getDensInter()<InterCellParameters::densThr)
#endif
						candidates.push_back(item);
			}
		}
		return candidates;
	}
	
	std::vector<SCEType*> getPolyCandidatesAll(){
		CollectionP candidates;
		candidates.reserve(mParticles.size());
		CollectionIterator iter = getIterator();
		for(iter.first();!iter.isDone();iter.next()){
			SCEType* item = iter.currentItem();
			if(item != mPolyOut)
				candidates.push_back(item);
		}
		return candidates;
	}
	
	std::vector<SCEType*> getPolyCandidatesTail(){
		CollectionP candidates;
		candidates.reserve(mParticles.size());
		CollectionIterator iter = getIterator();
		for(iter.first();!iter.isDone();iter.next()){
			SCEType* item = iter.currentItem();
			if(item != mPolyOut && (item->getPolarDist()<0))
				candidates.push_back(item);
		}
		return candidates;
	}
	
#pragma mark POLARIZATION
	
	/** update the polarity vector */
	void updatePolarity(Real dt){
		switch(Params::sCells[mCellType].MI_pmod){
			case 0:
				// random
				updatePolarityRand(dt);
				break;
			case 1:
				// orientation
				// @@ for reasons of visualization do it here
				updatePolarityOrientation();
				// do nothing, calculate when needed
				break;
			case 2:
				// orientation
				// update continuously towards orientation
			default:
				// random
				updatePolarityRand(dt);
		}
	}
	
	/** rotate polarity vector at random*/
	void updatePolarityRand(Real dt){
		// sample random rotation angle
		Real dev=Params::sCells[mCellType].MI_pfluc;
		Real alphaFluct=Functions::randNorm()*dev;
#if DIM==2
		Real sign=(mPolarity[1] > 0. ? 1. : -1.);
		Real alpha = sign*acos(mPolarity[0]);
		//alpha+=fmod(alphaFluct+2.*M_PI,2.*M_PI);
		alpha+=alphaFluct*dt;
		mPolarity[0]=cos(alpha);
		mPolarity[1]=sin(alpha);
#elif DIM==3
		// @@@ TODOOOO
		// converte to spherical coordinates
		Real theta;
		Real phi;
		printf("Not implemented for 3D");
		exit(0);
		// rotate
		
		
		// converte back to cartesian
		//	mPolarity[0]=sin(theta)*cos(phi);
		//	mPolarity[1]=sin(theta)*sin(phi);
		//	mPolarity[2]=cos(theta);
		
		
		/*		Real sign=(mPolarity[1]> 0. ? 1.: -1);
		 Real absXY2=mPolarity[0]*mPolarity[0]+mPolarity[1]*mPolarity[1];
		 Real alpha = sign*acos(mPolarity[0]/sqrt(absXY2));
		 alpha+=(alphaFluct+2.*M_PI)%(2.*M_PI);
		 scale = sqrt(absXY2+mPolarity[2]*mPolarity[2]);
		 
		 Real alpha2Fluct=Functions::randNorm()*dev;
		 Real alpha2=asin(mPolarity[2]/scale);
		 alpha2+=alpha2Fluct;
		 
		 mPolarity[2]=sin(alpha2);
		 mPolarity[1]=sin(alpha2)*abs(cos(alpha2));
		 mPolarity[1]=cos(alpha2)*abs(cos(alpha2));
		 */
#endif
	}
	
	/** set polarization vector to orientation of cell
	 * project current polarization vector onto cell orientation
	 */
	void updatePolarityOrientation(){
		// find current long axis of cell
		CellStats stats=getStats();
		
		// get major axis
		VecType ma=stats.majorAxis;
		// normalize
		ma=ma/ma.abs();
		// find axis angle between -pi and pi
		Real sign=(ma[1] > 0. ? 1. : -1.);
		Real alphaA = sign*acos(ma[0]);
		
		// find polarization angle -pi and pi
		sign=(mPolarity[1] > 0. ? 1. : -1.);
		Real alphaP = sign*acos(mPolarity[0]);
		
		//
		div_t divres=div(abs(alphaA-alphaP),2*M_PI);
		
		if(divres.rem>M_PI/2 && divres.rem<3/2*M_PI)
			mPolarity=-ma;
		else 
			mPolarity=ma;
		
		
		
		
		// project polarity vector onto long axis
		
		
#if DIM==2
		
#elif DIM==3
		
#endif
	}
	
	/** calculate distance of actin elements to polarity plane */
	void polarizeActinPlane(){
		// get the actin elements
		std::vector<SCEType*> candidates = getActinElements();		
		
		VecType center = getPos();
		// calculate distance of elements to plane
		mMinPolarDist=1;
		mMaxPolarDist=-1;
		for(int i=0;i<candidates.size();i++){
			Real dist = ((center-candidates[i]->getPos())*mPolarity).sum();
			candidates[i]->setPolarDist(dist);
			mMinPolarDist=std::min(dist,mMinPolarDist);
			mMaxPolarDist=std::max(dist,mMaxPolarDist);
		}
	}
	
	/** calculate distance of all elements to polarity plane */
	void polarizePlane(){
		// get the actin elements
		CollectionIterator iter=getIterator();
		
		VecType center = getPos();
		// calculate distance of elements to plane
		mMinPolarDist=1;
		mMaxPolarDist=-1;
		for(iter.first();!iter.isDone();iter.next()){
			SCEType* part = iter.currentItem();
			Real dist = mPolarity.dot(part->getPos()-center);
			part->setPolarDist(dist);
			mMinPolarDist=std::min(dist,mMinPolarDist);
			mMaxPolarDist=std::max(dist,mMaxPolarDist);
		}
	}
	
#pragma mark CELL GROWTH
	
	/** grow an SCE (Growth) */
	void createSCE(){
		//createSplitSCERand();
		createSCEMiddle();
	}
	
	/** split a random SCE */
	void createSplitSCERand(){
		//pick random element
		int ind=getRandSCEInd();
		mParticles[ind].getPos();
		//SCEType part = SCEType(mParticles[ind].getPos(),mCellType,mCellId);
		addSCE(mParticles[ind].getPos());
	}
	
	void createSCEMiddle(){
		addSCE(getPos());
	}
	
	void splitCellRand(CellCollection* _pCells){
		//split cell in a random direction
		VecType mid=getPos();
		//pick random division plane
		Real dist = Params::sCells[mCellType].radius/2.;
		Real alpha = 2.*M_PI*(float)rand()/(float)RAND_MAX;
#if DIM==2
		VecType shift=VecType(cos(alpha),sin(alpha))*dist;
#elif DIM==3
		Real z=2.f*(Real)rand()/(Real)RAND_MAX-1.;
		Real sz=sqrt(1.f-z*z);	
		VecType shift = VecType(cos(alpha)*sz,
					sin(alpha)*sz,
					z)*dist;
#endif
		mC1 = mid+shift;
		mC2 = mid-shift;
		
		
		//create new cell
		SelfType cell = SelfType(mCellType);
		
		//iterate backwards through mParticles
		//all particles closer the mC1 are kept
		//other elements are assigned to new cell
		// -> note: backwards iteration should not invalidate iterator as it proceeds
		Real d1,d2;
		for(int i=mParticles.size()-1;i>=0;i--){
			SCEType part = mParticles[i];
			VecType pos=mParticles[i].getPos();
			d1=(pos-mC1).abs();
			d2=(pos-mC2).abs();
			if(d2<d1){
				// insert sce in new cell
				cell.addSCE(part);
				// remove position from array
				mParticles.erase(mParticles.begin()+i);
			}
		}
		// set state:
		cell.setState(mState);
		cell.setCellPhenotype(mCPT,mCP,C_G0);
		_pCells->push_back(cell);
	}
	
#pragma mark CELL CYCLE / PHENOTYPE
	
	
	// update Cell
	/** The cell update depends on the cell phenotype
	 *   Phenotypes so far:
	 *   - Migrating
	 *   - Proliferating
	 *     - associated with a cell cycle phase
	 *   - Quiescent
	 *
	 *   Transition inbetween the phenotypes not implemented yet
	 */
	
	bool update(Real dt,CellCollection* _pCells){
		mClock+=dt;
		// update polarity
		updatePolarity(dt);
		polarizePlane();
		bool cellCreated=false;
		Real pfrac;
		
		// cell cycle dependent stuff
		switch (mCPT) {
			case P_Q: //QUIESCENT.. do nothing
				break;
			case P_EQ: //QUIESCENT but EQUILIBRATIONG
				//wait interval time and depolymerize/polymerize
				pfrac=std::max(mClock/(mIDur*0.9),0.1);
				mPolyIn->setPolyFrac(pfrac);
				mPolyOut->setPolyFrac(1-pfrac);
				if(mClock>mIDur){
					mPolyIn->setPolyFrac(1);
					mPolyOut->setPolyFrac(0);
					polyEquilibration();
					mClock-=mIDur+mIDur*Functions::randNorm()*Params::sCells[mCellType].EQ_fluc;
				}
				break;				
			case P_MI: // MIGRATING
				//wait interval time and depolymerize/polymerize
				pfrac=std::max(mClock/(mIDur*0.9),0.1);
				mPolyIn->setPolyFrac(pfrac);
				mPolyOut->setPolyFrac(1-pfrac);
				if(mClock>mIDur){
					mPolyIn->setPolyFrac(1);
					mPolyOut->setPolyFrac(0);
					polyMigration();
					mClock-=mIDur+mIDur*Functions::randNorm()*Params::sCells[mCellType].MI_fluc;
				}
				break;
			case P_P: //Proliferation
				switch(mCP){
					case C_G0: // RESTING PHASE
						// advance cell clock
						// quiscent cells
						if(Params::sCells[mCellType].G0_dur<0) break;
						// if phase time over, move on to next phase
						if(mClock>=Params::sCells[mCellType].G0_dur){
							mClock-=Params::sCells[mCellType].G0_dur;
							mIDur=Params::sCells[mCellType].I_dur/Params::sCells[mCellType].nSce;
							//for time step constraints
							assert(dt<mIDur);
							mCP=C_I;
						}else{
							break;
						}
					case C_I:  //GROWTH PHASE
						// grow after growth interval has passed
						if (mClock>mIDur){
							// create an SCE
							createSCE();
							// reset Clock
							mClock-=mIDur;
						}
						// check if end of growth phase
						if(mParticles.size()<Params::sCells[mCellType].nSce*2){
							break;
						}else{
							mCP=C_M1;
						}
					case C_M1: //SEPARATE NUCELI
						// wait a bit before dividing
						if(mClock>=Params::sCells[mCellType].M1_dur){
							mClock-=Params::sCells[mCellType].M1_dur;
							mCP=C_M2;
						}else{
							break;
						}				
					case C_M2: //SPLIT CELL
						// split cell in 2
						splitCellRand(_pCells);
						//reset clock
						mClock=0.;
						mCP=C_G0;
						mMembraneUpdated=false;
						cellCreated=true;
						break;
					default:	
						std::cout<<"Something Wrong In Cell Cycle"<<std::endl;
						break;
				}
				break;
			default:
				std::cout<<"SOMETHING WRONG IN CELL PHENOTYPE!!"<<std::endl;
				break;
		}
		return cellCreated;
	}
	
	/*
	 void update(Real dt,CellCollection* _pCells){
	 mClock+=dt;
	 // update polarity
	 updatePolarity(dt);
	 polarizePlane();
	 
	 // cell cycle dependent stuff
	 switch (mCP) {
	 case C_G0: //QUIESCENT
	 // advance cell clock
	 // quiscent cells
	 if(Params::sCells[mCellType].G0_dur<0) break;
	 // if phase time over, move on to next phase
	 if(mClock>=Params::sCells[mCellType].G0_dur){
	 mClock-=Params::sCells[mCellType].G0_dur;
	 mIDur=Params::sCells[mCellType].I_dur/Params::sCells[mCellType].nSce;
	 //for time step constraints
	 assert(dt<mIDur);
	 mCP=C_I;
	 }else{
	 break;
	 }
	 case C_I:  //GROWTH PHASE
	 // grow after growth interval has passed
	 if (mClock>mIDur){
	 // create an SCE
	 createSCE();
	 // reset Clock
	 mClock-=mIDur;
	 }
	 // check if end of growth phase
	 if(mParticles.size()<Params::sCells[mCellType].nSce*2){
	 break;
	 }else{
	 mCP=C_M1;
	 }
	 case C_M1: //SEPARATE NUCELI
	 // wait a bit before dividing
	 if(mClock>=Params::sCells[mCellType].M1_dur){
	 mClock-=Params::sCells[mCellType].M1_dur;
	 mCP=C_M2;
	 }else{
	 break;
	 }				
	 case C_M2: //SPLIT CELL
	 // split cell in 2
	 splitCellRand(_pCells);
	 //reset clock
	 mClock=0.;
	 mCP=C_G0;
	 break;
	 case C_MI: //Migration
	 //wait interval time and depolymerize/polymerize
	 if(mClock>mIDur){
	 polyMigration();
	 mClock-=mIDur;
	 }
	 default:
	 break;
	 }
	 }
	 */
	
#pragma mark JUXATRINE SIGNALING
	
	void setmState(int i, Real j){
		Real noise;
		noise=Params::sSim.icNoise*((Real)rand()/((Real)RAND_MAX)-0.5); //x% uniform noise
		mState[i] = j*(1+noise);
	}
	
	
	/** Update the Neighbourhood of the cell 
	 *  - iterate through membrane particles
	 *    - iterate through neighborhood
	 *		- store id of interacting cells
	 *      - accumulate contribution of interacting cells
	 *    - find pointer to neighboring cells for cell id
	 *    - calculate absolute contribution of neighboring cells
	 */
	
	template <typename NHM, typename M>
	void updateNeighborhood(const NHM& _nhm,M& _m){
		//some concept checks?
		
		// clear the mNeighbourMap
		mNeighborMap.clear();
		// iterate through membrane particles
		CollectionIteratorP iter1 = getIteratorP(0);
		for(iter1.first(); !iter1.isDone();iter1.next()){
			//iterate through neighborhood
			SCEType& part = *(iter1.currentItem());
			int cid=part.getCellId();
			typename NHM::IteratorType iter2 = _nhm.getHood(part);
			for(iter2.first(); !iter2.isDone();iter2.next()){
				const SCEType& part2 = *(iter2.currentItem());
				if(part2.getCellId()!=cid){
					// find distance
					VecType dist = part.getPos() - part2.getPos();
					if(dist.abs()*sqrt(Params::sInterCells[mCellType][part2.getCellType()].potInterParams[2]) <= InterCellParameters::densRad){
						// @@@ something tricky..
						// particles have advanced already, densInter based on old distances.
						addNeighborContribution(NeighborType(_m.find(part2.getCellId())->second,1./std::max((Real)part2.getDensInter(),1.0)));
					}
				} 
			}
		}
	}
	
	void addNeighborContribution(NeighborType nt){
		// look in mNeighborMap if already there.
		// if so, add contribution, if not, add neighbor with contribution
		typename std::vector<NeighborType>::iterator it;
		for (it=mNeighborMap.begin();it!=mNeighborMap.end();it++){
			if(it->first==nt.first){
				it->second+=nt.second;
				return;
			}
		}
		mNeighborMap.push_back(nt);	
	}
	
	
	
	void computeRHS(){		
		//update the rhs of the juxtacrine signalling 
		
		resetRHS();
		
		// Compute contributions from neighboring cells, values for all species stored in NContrib
		for (int i=0;i<NSPEC;i++) {
			NContrib[i] = 0;
			//iterate through neighboring cells to retrieve the delta contribution
			typename std::vector<NeighborType>::iterator it;
			for (it=mNeighborMap.begin();it!=mNeighborMap.end();it++){
				SelfType* neigh = it->first;
				assert(neigh->getSurface()!=0);
				assert(neigh->getSpec(i)!=0);
				NContrib[i]+=(neigh->getSpec(i))*it->second/neigh->getSurface();
			}
		}
		
		
#if JUXPATH == 1
		//update the rhs of the juxtacrine signalling pathway JUXPATH 1 (Collier, 1996)
		//  2 species: [0] - active Notch, [1] - active Delta 
		
		mRHS[0]=pow(NContrib[1],Params::sigK)/(Params::sigA+pow(NContrib[1],Params::sigK))-mState[0];//notch
		mRHS[1]=Params::sigV*(1./(1.+Params::sigB*pow(mState[0],Params::sigH))-mState[1]);//delta
		
		
		
#elif JUXPATH == 2
		//update the rhs of the juxtacrine signalling pathway JUXPATH 2 (Owen 1998, 2000)
		//  3 species: [0] - free ligand a, [1] - free receptor f, [2] - bound receptor b (= af-complex)
		
		//NContribution for a NContrib[0] and f NContrib[1] and b NContrib[2] needed
		
		mRHS[0]=-Params::kASS*mState[0]*NContrib[1] + Params::kDISS*NContrib[2] - Params::degA*mState[0] + (pow(Params::C1,Params::HillA)*pow(mState[2],Params::HillA))/(pow(Params::C2,Params::HillA)+pow(mState[2],Params::HillA));//free ligand a
		mRHS[1]=-Params::kASS*mState[1]*NContrib[0] + Params::kDISS*mState[2] - Params::degF*mState[1] + (Params::C3+(pow(Params::C4,Params::HillF)*pow(mState[2],Params::HillF))/(pow(Params::C5,Params::HillF)+pow(mState[2],Params::HillF)));//free receptor f
		mRHS[2]=Params::kASS*mState[1]*NContrib[0] - Params::kDISS*mState[2] - Params::kINT*mState[2]; //bound receptor b
		
#endif
		
	}
	
	VecType getVel(){return getPos();}
	
	Real compEnergy(int i, int ESpec) {
		/*returns term for this cell for energy function (X_c-X_{Nc})^2
		 with X_{Nc} = weighted average of neighbour contribution relative to shared membrane fraction
		 1) of neighbour
		 2) of current cell
		 */
		Real X_Nc=0;
		Real area_tot=0;
		Real neighbor_surf=0;
		
		typename std::vector<NeighborType>::iterator it;
		for (it=mNeighborMap.begin();it!=mNeighborMap.end();it++){
			SelfType* neigh = it->first;
			
			if (i==1) {
				//compute E1
				assert(neigh->getSurface()!=0);
				assert(neigh->getSpec(ESpec)!=0);
				//X_Nc+=(neigh->getSpec(ESpec))*it->second;
				X_Nc+=(neigh->getSpec(ESpec))*it->second/neigh->getSurface();
			}
			else if (i==2){
				//compute E2
				assert(mMembraneParticles.size()!=0);
				assert(neigh->getSpec(ESpec)!=0);
				X_Nc+=(neigh->getSpec(ESpec))*it->second/mMembraneParticles.size();
				//X_Nc+=(neigh->getSpec(ESpec))*it->second;
				
			}
			else{
				//Compute E3
				assert(neigh->getSpec(ESpec)!=0);
				X_Nc+=(neigh->getSpec(ESpec));
			}
			area_tot+=it->second;
			neighbor_surf+=neigh->getSurface();
		}	
		if(i==1){
			X_Nc/=(area_tot/(neighbor_surf/mNeighborMap.size()));
		}
		else if(i==2){
			X_Nc/=(area_tot/mMembraneParticles.size());
		}
		else {
			X_Nc/=mNeighborMap.size();
		}
		
		Real Energy=pow(mState[ESpec]-X_Nc,2);
		return Energy;
	}
	
#pragma mark CELL STATISTICS
	/**
	 * Get additional stats (TODO: Currently 2D only).
	 * Can be generalized for SCEs of varying shape (see Gerardo's testGC.m).
	 * @@@ copypase from Gerardos Sepal code
	 */
	CellStats getStats() {
		CellStats result;
		
#if DIM==2
		
		// assume all same size
		const Real rSCE = Params::sCells[mCellType].req;
		const Real rSCE2 = rSCE*rSCE;
		// area of SCE (assume circle)
		Real elementArea = M_PI*rSCE2;
		// second moment of area of SCE (assume circle)
		Real elementIxx = M_PI/4 * rSCE2*rSCE2;
		Real elementIyy = elementIxx;
		Real elementIxy = 0;
		
		// precompute some more stuff
		size_t N = mParticles.size();
		
		// need center first
		result.centroid = getPos();
		
		// get rest (sumXX = sum of pos(X)*pos(X))
		result.area = N * elementArea;
		VecType minPos = result.centroid;
		VecType maxPos = result.centroid;
		Real sum00 = 0;
		Real sum11 = 0;
		Real sum01 = 0;
		CollectionIterator iter = getIterator();
		for (iter.first(); !iter.isDone(); iter.next()) {
			const SCEType& item = *(iter.currentItem());
			const VecType pos = item.getPos();
			for (int i = 0; i < DIM; ++i) {
				minPos[i] = std::min(minPos[i], pos[i]);
				maxPos[i] = std::max(maxPos[i], pos[i]);
			}
			VecType cpos = pos - result.centroid;
			sum00 += cpos[0]*cpos[0];
			sum11 += cpos[1]*cpos[1];
			sum01 += cpos[0]*cpos[1];
		}
		// normalized second moments of area
		const Real uxx = sum00/N + elementIxx/elementArea;
		const Real uyy = sum11/N + elementIyy/elementArea;
		const Real uxy = sum01/N + elementIxy/elementArea;
		// get ellipse with same normalized moments of area
		const Real common = std::sqrt((uxx - uyy)*(uxx - uyy) + 4*uxy*uxy);
		const Real major = 2*std::sqrt(2*(uxx + uyy + common));
		const Real minor = 2*std::sqrt(2*(uxx + uyy - common));
		const Real ecc = std::sqrt(major*major/4 - minor*minor/4) * 2/major;
		// calculate orientation (measured in counter-clockwise direction)
		// orientation between -90 and 90 degrees
		Real num, den, phi;
		if (uyy > uxx) {
			num = uyy - uxx + common;
			den = 2*uxy;
		} else {
			num = 2*uxy;
			den = uxx - uyy + common;
		}
		if (den == 0) {
			phi = std::atan2(num,den);
		} else {
			phi = std::atan(num/den);
		}
		
		// fill result
		result.bbox.offset = minPos;
		result.bbox.size = maxPos-minPos;
		result.majorAxisLength = major;
		result.minorAxisLength = minor;
		result.eccentricity = ecc;
		result.orientation = phi;
		const Real dx = cos(phi);
		const Real dy = sin(phi);
		result.majorAxis = major * VecType(dx,dy);
		result.minorAxis = minor * VecType(-dy,dx);
		
		return result;
#else
		printf("no statistics");
#endif
	}
	
#pragma mark STATIC FUNCTIONS
	static int getGlobalCellId(){return SCECell::sGlobalCellId;};
	
protected:
#pragma mark PROTECTED MEMBERS
	int mCellId;
	int mCellType;
	Real mClock;
	Real mIDur; //interval duration
	VecType mC1; //cellcenter1
	VecType mC2; //cellcenter2
	VecType mPolarity; //polarization direction (unit vector)
	Real mMinPolarDist;
	Real mMaxPolarDist;
	SCEType * mPolyIn;  // for smooth polymerization
	SCEType * mPolyOut; // ""
	int mPolyInId;  // ""
	int mPolyOutId; // ""
	Real mPolyFrac;    // fraction of how much polymerization has happened
	bool mMembraneUpdated;
	std::vector<NeighborType> mNeighborMap;
//???	std::map<int,SelfType*> mMap;
	
	//signaling (dimension according to number of chemical species, NSPEC)
	StateType mRHS,mState,mStateT1,NContrib;
	
	CCellPhase mCP;
	CCellPhenotype mCPT;
	Collection mParticles;
	CollectionP mMembraneParticles;
	
	
#pragma mark STATIC MEMBERS
	//static members
	static int sGlobalCellId;
	
#pragma mark HelperFunctions
	int getRandSCEInd()const{return std::min((int)floor((Real)rand()/(Real)RAND_MAX*(Real)mParticles.size()),(int)mParticles.size()-1);}
	
};