HOW TO CITE:
Milde F., Tauriello G., Haberkern H., Koumoutsakos P., SEM++: A particle model of cellular growth, signaling and migration, Comp. Part. Mech., 1(2):211�227, 2014
DOI: http://dx.doi.org/10.1137/130943078

HOW TO COMPILE:
- Go to folder makefiles
- Adapt "SETUP" part of Makefile: turn on/off OpenGL visuals, parallelization with TBB and juxtacrine signaling.
- Adapt Make.user for your purposes. Choose compiler to use and adapt paths and libraries for OpenGL and TBB support
- Execute "make" and make sure no errors are reported

HOW TO RUN:
- Go to folder prg
- Run as "sem CTRLFILE", where CTRLFILE is the desired control file to read in (if not given, the default "Ctrl.dat" is used)
-> see Ctrl.dat for explanations on the parameters provided in the control file
- We require subfolders "out", "rst" and "txt" for the execution (existing in prg)
-> "out" contains snapshots from OpenGL visualization
-> "rst" used for restarting simulations
-> "txt" contains simulation results
- You can run long simulations on a super computer and visualize later
-> compile with VISUALS=0 on compute cluster and run there
-> copy simulation results (txt) back, compile with VISUALS=1 and VISUALTXT=1 and rerun

KEYBOARD CONTROLS in VISUAL mode:
- '?' - Show shortcuts
- 'p' - Pause/Unpause simulation
- ' ' - Perform a sim. step if in paused mode
- 't' - Take screenshot
- ESC - Stop simulation

NOTES:
- OpenGL support was only tested on Mac
- Code without OpenGL was tested on ETH's brutus compute cluster using Intel compilers (up to icc 12.1.2)

FOLDERS:
- makefiles: use "make" in there to compile SEM++
- prg: "sem" application created there and Ctrl-files available there
- source/SEM: SEM++ code
- source: generic particle library